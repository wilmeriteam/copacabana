/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package local;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Wilmer
 */
public class NewMain extends DAO {

    String algo;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        NewMain hola = new NewMain();
//        hola.inicio();
        hola.insertaSolicitud();
        hola.insertaDetalleAsignacion();
        hola.updateDetalleASignacion();
        hola.estadoUpdate();
        hola.updateVidautil();
        hola.updateSolicitud();
//        hola.deleteUserlow();
    }

    public void insertaSolicitud() throws Exception {
        ResultSet rs;
        String sql;
        PreparedStatement pst;
        try {
            Conectar();
            sql = "insert into tab_solicitud \n"
                    + "select max('01/01/2014'),max(1),max(uh.ush_id),max('01/01/2014'),null,max('Dotación por sistema (Poblado)'),1,7,ROW_NUMBER() OVER (ORDER BY u.usu_id) AS RowNumber,NULL\n"
                    + "from tab_ropatrabajo rt join tab_areatrabajocargo atc on atc.tpe_id = rt.tpe_id\n"
                    + "join tab_usuariohistorial uh on uh.car_id = atc.car_id join tab_usuario u on u.usu_id = uh.usu_id\n"
                    + "join tab_sucursales s on s.suc_id = uh.suc_id join tab_ubicacion ub on ub.ubi_id = s.ubi_id\n"
                    + "where uh.ush_estado = 1\n"
                    + "group by u.usu_id";
            System.out.println(sql);
            pst = getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean insertaDetalleAsignacion() throws Exception {
        ResultSet rs;
        String sql = "";
        String sql2 = "";
//        int c = 0;
        int igr = 0;
        PreparedStatement pst;
        try {
            Conectar();
            this.getCn().setAutoCommit(false);
            sql = "select usu_id,max(ush_id) ush_id,max(u_item) u_item,igr_id,ss.contador,"
                    + "ss.CODITEM,ss.ID_EMPLEADO,max(ubi_id) ubi_id,MAX(u_cantidad) u_cantidad from (\n"
                    + "	select h.ush_id,h.usu_id,rt.u_item,p.igr_id,su.ubi_id,rt.u_cantidad\n"
                    + "	from tab_usuariohistorial h \n"
                    + "	join tab_areatrabajocargo atc on atc.car_id = h.car_id\n"
                    + "	join tab_usuario u on u.usu_id = h.usu_id\n"
                    + "	join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n"
                    + "	join tab_sucursales su on su.suc_id = h.suc_id\n"
                    + "	join tab_producto p on p.pro_itemcode = rt.u_item\n"
                    + "	where h.ush_estado = 1 AND (p.pro_genero = u.usu_sexo OR p.pro_genero = '')\n"
                    + ") dd left join (\n"
                    + "	select ID_EMPLEADO,CI,CODITEM,ROW_NUMBER() OVER (ORDER BY ID_EMPLEADO) contador from asignacion3 where TIPO = 'DOTACION'\n"
                    + "	group by ID_EMPLEADO,CI,CODITEM\n"
                    + ") ss on ss.CODITEM = dd.u_item AND ss.ID_EMPLEADO = dd.usu_id\n"
                    + "group by dd.usu_id,igr_id,ss.contador,ss.CODITEM,ss.ID_EMPLEADO\n"
                    + "order by usu_id,igr_id,ss.contador DESC";
            pst = getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                for (int i = 0; i < rs.getInt("u_cantidad"); i++) {
                    try {
                        sql2 = "insert into tab_detalleasignacion\n"
                                + "select (select sol_id from tab_solicitud where usu_idempleado = " + rs.getInt("ush_id") + "),"
                                + "a.al_id,1,null,null,1,null,'2014-01-01',null\n"
                                + "from tab_almacen a join tab_producto p on p.pro_id = a.pro_id \n"
                                + "where p.pro_itemcode = '" + rs.getString("u_item") + "' and a.ubi_id = " + rs.getInt("ubi_id");
                        System.out.println(sql);
                        if (igr != rs.getInt("igr_id")) {
                            System.out.println(sql2);
                            pst = getCn().prepareStatement(sql2);
                            pst.executeUpdate();
                        } else if (rs.getInt("contador") > 1) {
                            System.out.println(sql2);
                            pst = getCn().prepareStatement(sql2);
                            pst.executeUpdate();
                        }
                    } catch (Exception e) {
                        System.out.println(sql2);
                        System.out.println(e);
                    }
                }
                igr = rs.getInt("igr_id");
            }
            this.getCn().commit();
        } catch (Exception e) {
            try {
                this.getCn().rollback();
            } catch (SQLException ex1) {
                System.err.println("No se pudo deshacer" + ex1.getMessage());
            }
            throw e;
        }
        return true;
    }

    public boolean updateDetalleASignacion() throws Exception {
        ResultSet rs;
        ResultSet rs2;
        String sql = "";
        String sql2 = "";
        int c = 0;
        PreparedStatement pst;
        try {
            Conectar();
            this.getCn().setAutoCommit(false);
            sql = "select ID_EMPLEADO,CODITEM,COUNT(cantidad) cantidad,FECHAINICIAL from asignacion3 where TIPO = 'DOTACION' group by ID_EMPLEADO,CODITEM,cantidad,FECHAINICIAL";
            pst = getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                sql = "select da.deta_id,uh.ush_id,a.al_id,p.pro_vidautil from tab_detalleasignacion da \n"
                        + "join tab_solicitud s on s.sol_id = da.sol_id\n"
                        + "join tab_usuariohistorial uh on uh.ush_id = s.usu_idempleado\n"
                        + "join tab_usuario u on u.usu_id = uh.usu_id\n"
                        + "join tab_almacen a on a.al_id = da.al_id\n"
                        + "join tab_producto p on p.pro_id = a.pro_id\n"
                        + "where u.usu_id = " + rs.getInt("ID_EMPLEADO") + " and p.pro_itemcode = '" + rs.getString("CODITEM") + "'";
                System.out.println(sql);
                pst = getCn().prepareStatement(sql);
                rs2 = pst.executeQuery();

                for (int i = 0; i < rs.getInt("cantidad"); i++) {
                    while (rs2.next()) {
                        sql = "WITH UpdateList_view AS (\n"
                                + "  SELECT TOP 1 da.deta_estadoprenda,s.usu_idempleado,da.al_id,"
                                + "da.deta_fechaasignacion,da.deta_fechatentativa from tab_detalleasignacion da\n"
                                + "  join tab_solicitud s on s.sol_id = da.sol_id\n"
                                + "  WHERE s.usu_idempleado = " + rs2.getInt("ush_id") + " AND da.al_id = " + rs2.getInt("al_id")
                                + " AND deta_estadoprenda = 1\n"
                                + ")\n"
                                + "update UpdateList_view \n"
                                + "set deta_estadoprenda = 2,"
                                + "deta_fechaasignacion = convert(datetime,'" + rs.getString("FECHAINICIAL") + "',102),"
                                + "deta_fechatentativa = DATEADD(MONTH," + rs2.getInt("pro_vidautil") + ",convert(datetime,'" + rs.getString("FECHAINICIAL") + "',102))";
                        System.out.println(sql);
                        System.out.println(sql);
                        System.out.println(++c);
                        pst = getCn().prepareStatement(sql);
                        pst.executeUpdate();

                    }
                }
            }
            this.getCn().commit();
        } catch (Exception e) {
            try {
                this.getCn().rollback();
            } catch (SQLException ex1) {
                System.err.println("No se pudo deshacer" + ex1.getMessage());
            }
            throw e;
        }
        return true;
    }

    public void estadoUpdate() throws Exception {
        String sql;
        PreparedStatement pst;
        try {
            Conectar();
            this.getCn().setAutoCommit(false);
            sql = "UPDATE tab_detalleasignacion \n"
                    + "SET deta_estadoprenda = 2, \n"
                    + "    deta_fechaasignacion = '2014-01-01',\n"
                    + "    deta_fechatentativa = dateadd(month,i.pro_vidautil,'2014-01-01')\n"
                    + "FROM (\n"
                    + "    SELECT p.pro_vidautil \n"
                    + "    FROM tab_almacen a join tab_producto p on p.pro_id = a.pro_id) i";
            System.out.println(sql);
            pst = getCn().prepareStatement(sql);
            pst.executeUpdate();
            this.getCn().commit();
        } catch (Exception e) {
            try {
                this.getCn().rollback();
            } catch (SQLException ex1) {
                System.err.println("No se pudo deshacer" + ex1.getMessage());
            }
            throw e;
        }
    }

    public void updateVidautil() throws Exception {
        String sql;
        PreparedStatement pst;
        try {
            Conectar();
            sql = "WITH UpdateList_view AS (\n"
                    + "	SELECT da.deta_estadoprenda,s.usu_idempleado,da.al_id,p.pro_vidautil,da.deta_fechaasignacion,da.deta_fechatentativa from tab_detalleasignacion da\n"
                    + "	join tab_solicitud s on s.sol_id = da.sol_id\n"
                    + "	join tab_almacen a on a.al_id = da.al_id\n"
                    + "	join tab_producto p on p.pro_id = a.pro_id\n"
                    + ")\n"
                    + "update UpdateList_view \n"
                    + "set deta_fechatentativa = DATEADD(MONTH,pro_vidautil,convert(datetime,deta_fechaasignacion,102))";
            System.out.println(sql);
            pst = getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
    public void updateSolicitud() throws Exception {
        String sql;
        PreparedStatement pst;
        try {
            Conectar();
            sql = "update tab_solicitud set sol_estado = 2";
            System.out.println(sql);
            pst = getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void deleteUserlow() throws Exception{
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        ResultSet rs2;
        try {
            Conectar();
            sql = "select h.ush_id,u.usu_id from tab_usuariohistorial h join tab_usuario u on u.usu_id = h.usu_id where ush_ffinal is not null";
            pst = getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                sql = "select sol_id from tab_solicitud where usu_idempleado = "+rs.getInt("ush_id");
                pst = getCn().prepareStatement(sql);
                rs2 = pst.executeQuery();
                while(rs2.next()){
                    sql = "delete from tab_detalleasignacion where sol_id = "+rs2.getInt("sol_id");
                    pst = getCn().prepareStatement(sql);
                    pst.executeUpdate();
                }
                sql = "delete from tab_solicitud where usu_idempleado = "+rs.getInt("ush_id");
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();
                
                sql = "delete from tab_usuariohistorial where ush_id = "+rs.getInt("ush_id");
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();

                sql = "delete from tab_usuario where usu_id = "+rs.getInt("usu_id");
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        }
    }
}