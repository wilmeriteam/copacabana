package model;

public class Usuariohistorial {

    private int ush_id;
    private int usu_id;
    private String ush_fingreso;
    private String ush_ffinal;
    private String ush_correo;
    private String ush_password;
    private int ush_estado;
    private int car_id;
    private int suc_id;
    private int rol_id;
    private String usu_sexo;
    private String usu_nombre;
    private String usu_apellido;
    private String nomubicacion;
    private String nomrol;
    private String nomcargo;
    private String claveanterior;
    private String clavenuevo;
    private String claveconfirmar;
    private boolean value1;
    private int division;
    private int area;
    private String nomarea;
    private int cantpendiente;
    private String usu_ci;
    private int alfanumeric;

    public int getCantpendiente() {
        return cantpendiente;
    }

    public void setCantpendiente(int cantpendiente) {
        this.cantpendiente = cantpendiente;
    }

    
    public String getNomarea() {
        return nomarea;
    }

    public void setNomarea(String nomarea) {
        this.nomarea = nomarea;
    }
    

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public boolean isValue1() {
        return value1;
    }

    public void setValue1(boolean value1) {
        this.value1 = value1;
    }
    

    public String getClaveconfirmar() {
        return claveconfirmar;
    }

    public void setClaveconfirmar(String claveconfirmar) {
        this.claveconfirmar = claveconfirmar;
    }

    public String getClavenuevo() {
        return clavenuevo;
    }

    public void setClavenuevo(String clavenuevo) {
        this.clavenuevo = clavenuevo;
    }

    public String getClaveanterior() {
        return claveanterior;
    }

    public void setClaveanterior(String claveanterior) {
        this.claveanterior = claveanterior;
    }

    public String getNomcargo() {
        return nomcargo;
    }

    public void setNomcargo(String nomcargo) {
        this.nomcargo = nomcargo;
    }

    public String getNomrol() {
        return nomrol;
    }

    public void setNomrol(String nomrol) {
        this.nomrol = nomrol;
    }

    public String getUsu_sexo() {
        return usu_sexo;
    }

    public void setUsu_sexo(String usu_sexo) {
        this.usu_sexo = usu_sexo;
    }

    public String getNomubicacion() {
        return nomubicacion;
    }

    public void setNomubicacion(String nomubicacion) {
        this.nomubicacion = nomubicacion;
    }

    public int getUsh_id() {
        return ush_id;
    }

    public void setUsh_id(int ush_id) {
        this.ush_id = ush_id;
    }

    public int getUsu_id() {
        return usu_id;
    }

    public void setUsu_id(int usu_id) {
        this.usu_id = usu_id;
    }

    public String getUsh_fingreso() {
        return ush_fingreso;
    }

    public void setUsh_fingreso(String ush_fingreso) {
        this.ush_fingreso = ush_fingreso;
    }

    public String getUsh_ffinal() {
        return ush_ffinal;
    }

    public void setUsh_ffinal(String ush_ffinal) {
        this.ush_ffinal = ush_ffinal;
    }

    public String getUsh_correo() {
        return ush_correo;
    }

    public void setUsh_correo(String ush_correo) {
        this.ush_correo = ush_correo;
    }

    public String getUsh_password() {
        return ush_password;
    }

    public void setUsh_password(String ush_password) {
        this.ush_password = ush_password;
    }

    public int getUsh_estado() {
        return ush_estado;
    }

    public void setUsh_estado(int ush_estado) {
        this.ush_estado = ush_estado;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public int getSuc_id() {
        return suc_id;
    }

    public void setSuc_id(int suc_id) {
        this.suc_id = suc_id;
    }

    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public String getUsu_nombre() {
        return usu_nombre;
    }

    public void setUsu_nombre(String usu_nombre) {
        this.usu_nombre = usu_nombre;
    }

    public String getUsu_apellido() {
        return usu_apellido;
    }

    public void setUsu_apellido(String usu_apellido) {
        this.usu_apellido = usu_apellido;
    }

    public String getUsu_ci() {
        return usu_ci;
    }

    public void setUsu_ci(String usu_ci) {
        this.usu_ci = usu_ci;
    }

    public int getAlfanumeric() {
        return alfanumeric;
    }

    public void setAlfanumeric(int alfanumeric) {
        this.alfanumeric = alfanumeric;
    }

}
