/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author wilmer
 */
public class Stock {
    private int idSol;
    private String ustGrupo;
    private String ustSubcargo;
    private String docDate;
    private String docDueDate;
    private String comment;
    private String journal;
    private int paymentGroupCode;
    private int series;
    private String itemCode;
    private String acountcode;
    private String sigla;
    private String costingCode;
    private int quatity;

    public String getUstGrupo() {
        return ustGrupo;
    }

    public void setUstGrupo(String ustGrupo) {
        this.ustGrupo = ustGrupo;
    }

    public String getUstSubcargo() {
        return ustSubcargo;
    }

    public void setUstSubcargo(String ustSubcargo) {
        this.ustSubcargo = ustSubcargo;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }

    public String getDocDueDate() {
        return docDueDate;
    }

    public void setDocDueDate(String docDueDate) {
        this.docDueDate = docDueDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getJournal() {
        return journal;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public int getPaymentGroupCode() {
        return paymentGroupCode;
    }

    public void setPaymentGroupCode(int paymentGroupCode) {
        this.paymentGroupCode = paymentGroupCode;
    }

    public int getSeries() {
        return series;
    }

    public void setSeries(int series) {
        this.series = series;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getAcountcode() {
        return acountcode;
    }

    public void setAcountcode(String acountcode) {
        this.acountcode = acountcode;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getCostingCode() {
        return costingCode;
    }

    public void setCostingCode(String costingCode) {
        this.costingCode = costingCode;
    }

    public int getQuatity() {
        return quatity;
    }

    public void setQuatity(int quatity) {
        this.quatity = quatity;
    }

    public int getIdSol() {
        return idSol;
    }

    public void setIdSol(int idSol) {
        this.idSol = idSol;
    }
    
}
