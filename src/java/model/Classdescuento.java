
package model;

public class Classdescuento {
private int clas_id;
private String clas_nombre;
private String clas_tipo;
private int clas_estado;

    public int getClas_id() {
        return clas_id;
    }

    public void setClas_id(int clas_id) {
        this.clas_id = clas_id;
    }

    public String getClas_nombre() {
        return clas_nombre;
    }

    public void setClas_nombre(String clas_nombre) {
        this.clas_nombre = clas_nombre;
    }

    public String getClas_tipo() {
        return clas_tipo;
    }

    public void setClas_tipo(String clas_tipo) {
        this.clas_tipo = clas_tipo;
    }

    public int getClas_estado() {
        return clas_estado;
    }

    public void setClas_estado(int clas_estado) {
        this.clas_estado = clas_estado;
    }
}
