package model;

public class Asignacion {
    private int sol_id;
    private String nomsolicitante;
    private String nomempleado;
    private String car_nombre;
    private String tipoprenda;
    private String fechasolicitud;
    private String talla_id;
    private String color_id;
    private String logo_id;
    private String sucursal;
    private int sol_estado;
    private String fechaasignado;
    private String fechadevolucion;
    private String motivo_solicitud;
    private int alfanumeric;

    public int getAlfanumeric() {
        return alfanumeric;
    }

    public void setAlfanumeric(int alfanumeric) {
        this.alfanumeric = alfanumeric;
    }

    public String getMotivo_solicitud() {
        return motivo_solicitud;
    }

    public void setMotivo_solicitud(String motivo_solicitud) {
        this.motivo_solicitud = motivo_solicitud;
    }

    public String getFechadevolucion() {
        return fechadevolucion;
    }

    public void setFechadevolucion(String fechadevolucion) {
        this.fechadevolucion = fechadevolucion;
    }

    public String getFechaasignado() {
        return fechaasignado;
    }

    public void setFechaasignado(String fechaasignado) {
        this.fechaasignado = fechaasignado;
    }

    public int getSol_estado() {
        return sol_estado;
    }

    public void setSol_estado(int sol_estado) {
        this.sol_estado = sol_estado;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public String getNomsolicitante() {
        return nomsolicitante;
    }

    public void setNomsolicitante(String nomsolicitante) {
        this.nomsolicitante = nomsolicitante;
    }


    public String getNomempleado() {
        return nomempleado;
    }

    public void setNomempleado(String nomempleado) {
        this.nomempleado = nomempleado;
    }

 
    public String getCar_nombre() {
        return car_nombre;
    }

    public void setCar_nombre(String car_nombre) {
        this.car_nombre = car_nombre;
    }

    public String getTipoprenda() {
        return tipoprenda;
    }

    public void setTipoprenda(String tipoprenda) {
        this.tipoprenda = tipoprenda;
    }

    public String getFechasolicitud() {
        return fechasolicitud;
    }

    public void setFechasolicitud(String fechasolicitud) {
        this.fechasolicitud = fechasolicitud;
    }

    public String getTalla_id() {
        return talla_id;
    }

    public void setTalla_id(String talla_id) {
        this.talla_id = talla_id;
    }

    public String getColor_id() {
        return color_id;
    }

    public void setColor_id(String color_id) {
        this.color_id = color_id;
    }

    public String getLogo_id() {
        return logo_id;
    }

    public void setLogo_id(String logo_id) {
        this.logo_id = logo_id;
    }
    
    
}
