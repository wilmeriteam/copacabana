
package model;

public class Ubicacion {
    private int ubi_id;
    private String ubi_nombre;
    private String sigla;
    private int ubi_estado;

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    
    public int getUbi_id() {
        return ubi_id;
    }

    public void setUbi_id(int ubi_id) {
        this.ubi_id = ubi_id;
    }

    public String getUbi_nombre() {
        return ubi_nombre;
    }

    public void setUbi_nombre(String ubi_nombre) {
        this.ubi_nombre = ubi_nombre;
    }

    public int getUbi_estado() {
        return ubi_estado;
    }

    public void setUbi_estado(int ubi_estado) {
        this.ubi_estado = ubi_estado;
    }
}
