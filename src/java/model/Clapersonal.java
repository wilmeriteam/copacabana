
package model;
public class Clapersonal {
    private int clap_id;
    private String clap_nombre;
    private int clap_estado;

    public int getClap_id() {
        return clap_id;
    }

    public void setClap_id(int clap_id) {
        this.clap_id = clap_id;
    }

    public String getClap_nombre() {
        return clap_nombre;
    }

    public void setClap_nombre(String clap_nombre) {
        this.clap_nombre = clap_nombre;
    }

    public int getClap_estado() {
        return clap_estado;
    }

    public void setClap_estado(int clap_estado) {
        this.clap_estado = clap_estado;
    }
    
}
