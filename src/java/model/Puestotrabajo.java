
package model;

public class Puestotrabajo {
private int ptb_id;
private String ptb_nombre;
private int ptb_estado;

    public int getPtb_id() {
        return ptb_id;
    }

    public void setPtb_id(int ptb_id) {
        this.ptb_id = ptb_id;
    }

    public String getPtb_nombre() {
        return ptb_nombre;
    }

    public void setPtb_nombre(String ptb_nombre) {
        this.ptb_nombre = ptb_nombre;
    }

    public int getPtb_estado() {
        return ptb_estado;
    }

    public void setPtb_estado(int ptb_estado) {
        this.ptb_estado = ptb_estado;
    }
}
