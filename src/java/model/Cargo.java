
package model;

public class Cargo {
    private int car_id;
    private String car_nombre;
    private int car_estado;

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public String getCar_nombre() {
        return car_nombre;
    }

    public void setCar_nombre(String car_nombre) {
        this.car_nombre = car_nombre;
    }

    public int getCar_estado() {
        return car_estado;
    }

    public void setCar_estado(int car_estado) {
        this.car_estado = car_estado;
    }
}
