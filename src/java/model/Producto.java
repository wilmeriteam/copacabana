package model;

public class Producto {
    private int pro_id;
    private String pro_itemcode;
    private String pro_itemname;
    private String pro_unidad;
    private int pre_id;
    private String pro_color;
    private String pro_descripcion;
    private String pro_genero;
    private String pro_talla;
    private String pro_logo;
    private String pro_material;
    private int trp_id;
    private int pro_vidautil;
    private int igr_id;
    private String pro_nomenclatura;
    private int pro_estado;

    public int getPro_id() {
        return pro_id;
    }

    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_itemcode() {
        return pro_itemcode;
    }

    public void setPro_itemcode(String pro_itemcode) {
        this.pro_itemcode = pro_itemcode;
    }

    public String getPro_itemname() {
        return pro_itemname;
    }

    public void setPro_itemname(String pro_itemname) {
        this.pro_itemname = pro_itemname;
    }

    public String getPro_unidad() {
        return pro_unidad;
    }

    public void setPro_unidad(String pro_unidad) {
        this.pro_unidad = pro_unidad;
    }

    public int getPre_id() {
        return pre_id;
    }

    public void setPre_id(int pre_id) {
        this.pre_id = pre_id;
    }

    public String getPro_color() {
        return pro_color;
    }

    public void setPro_color(String pro_color) {
        this.pro_color = pro_color;
    }

    public String getPro_descripcion() {
        return pro_descripcion;
    }

    public void setPro_descripcion(String pro_descripcion) {
        this.pro_descripcion = pro_descripcion;
    }

    public String getPro_genero() {
        return pro_genero;
    }

    public void setPro_genero(String pro_genero) {
        this.pro_genero = pro_genero;
    }

    public String getPro_talla() {
        return pro_talla;
    }

    public void setPro_talla(String pro_talla) {
        this.pro_talla = pro_talla;
    }

    public String getPro_logo() {
        return pro_logo;
    }

    public void setPro_logo(String pro_logo) {
        this.pro_logo = pro_logo;
    }

    public String getPro_material() {
        return pro_material;
    }

    public void setPro_material(String pro_material) {
        this.pro_material = pro_material;
    }

    public int getTrp_id() {
        return trp_id;
    }

    public void setTrp_id(int trp_id) {
        this.trp_id = trp_id;
    }

    public int getPro_vidautil() {
        return pro_vidautil;
    }

    public void setPro_vidautil(int pro_vidautil) {
        this.pro_vidautil = pro_vidautil;
    }

    public int getIgr_id() {
        return igr_id;
    }

    public void setIgr_id(int igr_id) {
        this.igr_id = igr_id;
    }

    public String getPro_nomenclatura() {
        return pro_nomenclatura;
    }

    public void setPro_nomenclatura(String pro_nomenclatura) {
        this.pro_nomenclatura = pro_nomenclatura;
    }

    public int getPro_estado() {
        return pro_estado;
    }

    public void setPro_estado(int pro_estado) {
        this.pro_estado = pro_estado;
    }
    
}
