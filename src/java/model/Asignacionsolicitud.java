package model;

public class Asignacionsolicitud {
    private int sol_id;
    private String nomsolicitante;
    private String nomempleado;
    private String car_nombre;
    private String tipoprenda;
    private String fechasolicitud;
    private String talla_id;
    private String color_id;
    private String logo_id;
    private String sucursal;
    private String sol_fechaasignacion;
    private String sol_fechadevolucion;
    private String fechai;
    private int sol_estado;
    private String nombres;
    private String apellidos;
    private int usu_idempleado;
    private int car_id;
    private int ubi_id;
    private int nroform;
    private String motivosolicitud;
    private String area;
    private String departamento;    
    private String ci;
    private String genero;
    private String dpto;

    public int getUbi_id() {
        return ubi_id;
    }

    public void setUbi_id(int ubi_id) {
        this.ubi_id = ubi_id;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public int getUsu_idempleado() {
        return usu_idempleado;
    }

    public void setUsu_idempleado(int usu_idempleado) {
        this.usu_idempleado = usu_idempleado;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    

    public int getSol_estado() {
        return sol_estado;
    }

    public void setSol_estado(int sol_estado) {
        this.sol_estado = sol_estado;
    }

    public String getFechai() {
        return fechai;
    }

    public void setFechai(String fechai) {
        this.fechai = fechai;
    }

    public String getFechaf() {
        return fechaf;
    }

    public void setFechaf(String fechaf) {
        this.fechaf = fechaf;
    }
    private String fechaf;

    public String getSol_fechaasignacion() {
        return sol_fechaasignacion;
    }

    public void setSol_fechaasignacion(String sol_fechaasignacion) {
        this.sol_fechaasignacion = sol_fechaasignacion;
    }

    public String getSol_fechadevolucion() {
        return sol_fechadevolucion;
    }

    public void setSol_fechadevolucion(String sol_fechadevolucion) {
        this.sol_fechadevolucion = sol_fechadevolucion;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public String getNomsolicitante() {
        return nomsolicitante;
    }

    public void setNomsolicitante(String nomsolicitante) {
        this.nomsolicitante = nomsolicitante;
    }


    public String getNomempleado() {
        return nomempleado;
    }

    public void setNomempleado(String nomempleado) {
        this.nomempleado = nomempleado;
    }

 
    public String getCar_nombre() {
        return car_nombre;
    }

    public void setCar_nombre(String car_nombre) {
        this.car_nombre = car_nombre;
    }

    public String getTipoprenda() {
        return tipoprenda;
    }

    public void setTipoprenda(String tipoprenda) {
        this.tipoprenda = tipoprenda;
    }

    public String getFechasolicitud() {
        return fechasolicitud;
    }

    public void setFechasolicitud(String fechasolicitud) {
        this.fechasolicitud = fechasolicitud;
    }

    public String getTalla_id() {
        return talla_id;
    }

    public void setTalla_id(String talla_id) {
        this.talla_id = talla_id;
    }

    public String getColor_id() {
        return color_id;
    }

    public void setColor_id(String color_id) {
        this.color_id = color_id;
    }

    public String getLogo_id() {
        return logo_id;
    }

    public void setLogo_id(String logo_id) {
        this.logo_id = logo_id;
    }

    public int getNroform() {
        return nroform;
    }

    public void setNroform(int nroform) {
        this.nroform = nroform;
    }

    public String getMotivosolicitud() {
        return motivosolicitud;
    }

    public void setMotivosolicitud(String motivosolicitud) {
        this.motivosolicitud = motivosolicitud;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDpto() {
        return dpto;
    }

    public void setDpto(String dpto) {
        this.dpto = dpto;
    }
    
}
