package model;

public class Solicitud {

    private int sol_id;
    private String itemname;
    private String sol_fecha;
    private int usu_idsolicitante;
    private int usu_idempleado;
    private String sol_fechaasignacion;
    private String sol_fechadevolucion;
    private String sol_descripcion;
    private int sol_estado;
    private String nomusuario_empleado;
    private String sucursal;
    private String nomcargo;
    private int tip_id;
    private int clast_id;
    private int mot_id;
    private int alfanumeric;
    private int car_id;
    private int ubi_id;
    private int tpe_id;
    private String usu_sexo;
    private String motivo;
    private String excepcional;
    private String ci;
    private int actividad;

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    
    public String getItemname() {
        return itemname;
    }

    public void setItemname(String itemname) {
        this.itemname = itemname;
    }

    
    public int getActividad() {
        return actividad;
    }

    public void setActividad(int actividad) {
        this.actividad = actividad;
    }

    public String getExcepcional() {
        return excepcional;
    }

    public void setExcepcional(String excepcional) {
        this.excepcional = excepcional;
    }
    
    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public int getMot_id() {
        return mot_id;
    }

    public void setMot_id(int mot_id) {
        this.mot_id = mot_id;
    }

    public String getUsu_sexo() {
        return usu_sexo;
    }

    public void setUsu_sexo(String usu_sexo) {
        this.usu_sexo = usu_sexo;
    }

    public int getTpe_id() {
        return tpe_id;
    }

    public void setTpe_id(int tpe_id) {
        this.tpe_id = tpe_id;
    }

    public int getUbi_id() {
        return ubi_id;
    }

    public void setUbi_id(int ubi_id) {
        this.ubi_id = ubi_id;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public int getAlfanumeric() {
        return alfanumeric;
    }

    public void setAlfanumeric(int alfanumeric) {
        this.alfanumeric = alfanumeric;
    }

    public int getClast_id() {
        return clast_id;
    }

    public void setClast_id(int clast_id) {
        this.clast_id = clast_id;
    }

    public int getTip_id() {
        return tip_id;
    }

    public void setTip_id(int tip_id) {
        this.tip_id = tip_id;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public String getSol_fecha() {
        return sol_fecha;
    }

    public void setSol_fecha(String sol_fecha) {
        this.sol_fecha = sol_fecha;
    }

    public int getUsu_idsolicitante() {
        return usu_idsolicitante;
    }

    public void setUsu_idsolicitante(int usu_idsolicitante) {
        this.usu_idsolicitante = usu_idsolicitante;
    }

    public int getUsu_idempleado() {
        return usu_idempleado;
    }

    public void setUsu_idempleado(int usu_idempleado) {
        this.usu_idempleado = usu_idempleado;
    }

    public String getSol_fechaasignacion() {
        return sol_fechaasignacion;
    }

    public void setSol_fechaasignacion(String sol_fechaasignacion) {
        this.sol_fechaasignacion = sol_fechaasignacion;
    }

    public String getSol_fechadevolucion() {
        return sol_fechadevolucion;
    }

    public void setSol_fechadevolucion(String sol_fechadevolucion) {
        this.sol_fechadevolucion = sol_fechadevolucion;
    }

    public String getSol_descripcion() {
        return sol_descripcion;
    }

    public void setSol_descripcion(String sol_descripcion) {
        this.sol_descripcion = sol_descripcion;
    }

    public int getSol_estado() {
        return sol_estado;
    }

    public void setSol_estado(int sol_estado) {
        this.sol_estado = sol_estado;
    }

    public String getNomusuario_empleado() {
        return nomusuario_empleado;
    }

    public void setNomusuario_empleado(String nomusuario_empleado) {
        this.nomusuario_empleado = nomusuario_empleado;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getNomcargo() {
        return nomcargo;
    }

    public void setNomcargo(String nomcargo) {
        this.nomcargo = nomcargo;
    }

}
