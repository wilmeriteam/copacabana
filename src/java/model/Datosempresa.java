
package model;
public class Datosempresa {
    private int datemp_id;
    private String datemp_nombreempresa;
    private String datemp_direccion;
    private String datemp_telefono;
    private String datemp_nit;
    private String datemp_logo;

    public int getDatemp_id() {
        return datemp_id;
    }

    public void setDatemp_id(int datemp_id) {
        this.datemp_id = datemp_id;
    }

    public String getDatemp_nombreempresa() {
        return datemp_nombreempresa;
    }

    public void setDatemp_nombreempresa(String datemp_nombreempresa) {
        this.datemp_nombreempresa = datemp_nombreempresa;
    }

    public String getDatemp_direccion() {
        return datemp_direccion;
    }

    public void setDatemp_direccion(String datemp_direccion) {
        this.datemp_direccion = datemp_direccion;
    }

    public String getDatemp_telefono() {
        return datemp_telefono;
    }

    public void setDatemp_telefono(String datemp_telefono) {
        this.datemp_telefono = datemp_telefono;
    }

    public String getDatemp_nit() {
        return datemp_nit;
    }

    public void setDatemp_nit(String datemp_nit) {
        this.datemp_nit = datemp_nit;
    }

    public String getDatemp_logo() {
        return datemp_logo;
    }

    public void setDatemp_logo(String datemp_logo) {
        this.datemp_logo = datemp_logo;
    }
}
