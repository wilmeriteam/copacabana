
package model;

public class Permisos {
    private int usm_id;
    private int rol_id;
    private int men_id;
    private int usm_estado;

    public int getUsm_id() {
        return usm_id;
    }

    public void setUsm_id(int usm_id) {
        this.usm_id = usm_id;
    }

    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public int getMen_id() {
        return men_id;
    }

    public void setMen_id(int men_id) {
        this.men_id = men_id;
    }

    public int getUsm_estado() {
        return usm_estado;
    }

    public void setUsm_estado(int usm_estado) {
        this.usm_estado = usm_estado;
    }
}
