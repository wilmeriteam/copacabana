package model;

public class Almacen {

    private int al_id;
    private int al_cantidad;
    private int al_estado;
    private float al_costo;
    private int gclas_id;
    private int ubi_id;
    private int pro_id;
    private int alfanumeric;
    private int estadoasignar;
    private String pro_itemcode;
    private String pro_itemname;
    private String pro_unidad;
    private int pre_id;
    private String pro_color;
    private String pro_descripcion;
    private String pro_genero;
    private String pro_talla;
    private String pro_logo;
    private String pro_material;
    private int trp_id;
    private int pro_vidautil;
    private int igr_id;
    private String pro_nomenclatura;
    private int pro_estado;
    private int deta_id;
    private int clast_id;
    private String ropa;
    private String trp_nombre;
    private int deta_estadoprenda;
    private int estadodevolucion;
    private int cantasignado;
    private int estadoitem;
    private int maxcantidad;
    private int restriccion;
    private int setMot_id;
    private int cantidadcalculado;
    private String fechatentativa;
    private String usuarioalmacen;
    private int sol_id;
    private int asignado;
    private int cantidad = 1;
    private int deta_cantidadsolicitada;

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getAsignado() {
        return asignado;
    }

    public void setAsignado(int asignado) {
        this.asignado = asignado;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public int getCantidadcalculado() {
        return cantidadcalculado;
    }

    public void setCantidadcalculado(int cantidadcalculado) {
        this.cantidadcalculado = cantidadcalculado;
    }
    

    public int getSetMot_id() {
        return setMot_id;
    }

    public void setSetMot_id(int setMot_id) {
        this.setMot_id = setMot_id;
    }
    
    

    public int getRestriccion() {
        return restriccion;
    }

    public void setRestriccion(int restriccion) {
        this.restriccion = restriccion;
    }
    
    public int getMaxcantidad() {
        return maxcantidad;
    }

    public void setMaxcantidad(int maxcantidad) {
        this.maxcantidad = maxcantidad;
    }
    

    public int getEstadoitem() {
        return estadoitem;
    }

    public void setEstadoitem(int estadoitem) {
        this.estadoitem = estadoitem;
    }

    public int getCantasignado() {
        return cantasignado;
    }

    public void setCantasignado(int cantasignado) {
        this.cantasignado = cantasignado;
    }

    public int getEstadodevolucion() {
        return estadodevolucion;
    }

    public void setEstadodevolucion(int estadodevolucion) {
        this.estadodevolucion = estadodevolucion;
    }

    public int getDeta_estadoprenda() {
        return deta_estadoprenda;
    }

    public void setDeta_estadoprenda(int deta_estadoprenda) {
        this.deta_estadoprenda = deta_estadoprenda;
    }

    public String getTrp_nombre() {
        return trp_nombre;
    }

    public void setTrp_nombre(String trp_nombre) {
        this.trp_nombre = trp_nombre;
    }

    public String getRopa() {
        return ropa;
    }

    public void setRopa(String ropa) {
        this.ropa = ropa;
    }

    public int getClast_id() {
        return clast_id;
    }

    public void setClast_id(int clast_id) {
        this.clast_id = clast_id;
    }

    public int getDeta_id() {
        return deta_id;
    }

    public void setDeta_id(int deta_id) {
        this.deta_id = deta_id;
    }

    public String getPro_itemcode() {
        return pro_itemcode;
    }

    public void setPro_itemcode(String pro_itemcode) {
        this.pro_itemcode = pro_itemcode;
    }

    public String getPro_itemname() {
        return pro_itemname;
    }

    public void setPro_itemname(String pro_itemname) {
        this.pro_itemname = pro_itemname;
    }

    public String getPro_unidad() {
        return pro_unidad;
    }

    public void setPro_unidad(String pro_unidad) {
        this.pro_unidad = pro_unidad;
    }

    public int getPre_id() {
        return pre_id;
    }

    public void setPre_id(int pre_id) {
        this.pre_id = pre_id;
    }

    public String getPro_color() {
        return pro_color;
    }

    public void setPro_color(String pro_color) {
        this.pro_color = pro_color;
    }

    public String getPro_descripcion() {
        return pro_descripcion;
    }

    public void setPro_descripcion(String pro_descripcion) {
        this.pro_descripcion = pro_descripcion;
    }

    public String getPro_genero() {
        return pro_genero;
    }

    public void setPro_genero(String pro_genero) {
        this.pro_genero = pro_genero;
    }

    public String getPro_talla() {
        return pro_talla;
    }

    public void setPro_talla(String pro_talla) {
        this.pro_talla = pro_talla;
    }

    public String getPro_logo() {
        return pro_logo;
    }

    public void setPro_logo(String pro_logo) {
        this.pro_logo = pro_logo;
    }

    public String getPro_material() {
        return pro_material;
    }

    public void setPro_material(String pro_material) {
        this.pro_material = pro_material;
    }

    public int getTrp_id() {
        return trp_id;
    }

    public void setTrp_id(int trp_id) {
        this.trp_id = trp_id;
    }

    public int getPro_vidautil() {
        return pro_vidautil;
    }

    public void setPro_vidautil(int pro_vidautil) {
        this.pro_vidautil = pro_vidautil;
    }

    public int getIgr_id() {
        return igr_id;
    }

    public void setIgr_id(int igr_id) {
        this.igr_id = igr_id;
    }

    public String getPro_nomenclatura() {
        return pro_nomenclatura;
    }

    public void setPro_nomenclatura(String pro_nomenclatura) {
        this.pro_nomenclatura = pro_nomenclatura;
    }

    public int getPro_estado() {
        return pro_estado;
    }

    public void setPro_estado(int pro_estado) {
        this.pro_estado = pro_estado;
    }

    public int getEstadoasignar() {
        return estadoasignar;
    }

    public void setEstadoasignar(int estadoasignar) {
        this.estadoasignar = estadoasignar;
    }

    public int getAlfanumeric() {
        return alfanumeric;
    }

    public void setAlfanumeric(int alfanumeric) {
        this.alfanumeric = alfanumeric;
    }

    public int getAl_id() {
        return al_id;
    }

    public void setAl_id(int al_id) {
        this.al_id = al_id;
    }

    public int getAl_cantidad() {
        return al_cantidad;
    }

    public void setAl_cantidad(int al_cantidad) {
        this.al_cantidad = al_cantidad;
    }

    public int getAl_estado() {
        return al_estado;
    }

    public void setAl_estado(int al_estado) {
        this.al_estado = al_estado;
    }

    public float getAl_costo() {
        return al_costo;
    }

    public void setAl_costo(float al_costo) {
        this.al_costo = al_costo;
    }

    public int getGclas_id() {
        return gclas_id;
    }

    public void setGclas_id(int gclas_id) {
        this.gclas_id = gclas_id;
    }

    public int getUbi_id() {
        return ubi_id;
    }

    public void setUbi_id(int ubi_id) {
        this.ubi_id = ubi_id;
    }

    public int getPro_id() {
        return pro_id;
    }

    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }

    public String getFechatentativa() {
        return fechatentativa;
    }

    public void setFechatentativa(String fechatentativa) {
        this.fechatentativa = fechatentativa;
    }

    public String getUsuarioalmacen() {
        return usuarioalmacen;
    }

    public void setUsuarioalmacen(String usuarioalmacen) {
        this.usuarioalmacen = usuarioalmacen;
    }

    public int getDeta_cantidadsolicitada() {
        return deta_cantidadsolicitada;
    }

    public void setDeta_cantidadsolicitada(int deta_cantidadsolicitada) {
        this.deta_cantidadsolicitada = deta_cantidadsolicitada;
    }

}
