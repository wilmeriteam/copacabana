
package model;

public class Detalleasignacion {
    private int deta_id;
    private int sol_id;
    private int al_id;
    private int deta_estado;

    public int getDeta_id() {
        return deta_id;
    }

    public void setDeta_id(int deta_id) {
        this.deta_id = deta_id;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public int getAl_id() {
        return al_id;
    }

    public void setAl_id(int al_id) {
        this.al_id = al_id;
    }

    public int getDeta_estado() {
        return deta_estado;
    }

    public void setDeta_estado(int deta_estado) {
        this.deta_estado = deta_estado;
    }
}
