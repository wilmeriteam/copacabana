
package model;

public class Motivodescuento {
    private int mot_id;
    private String mot_nombremotivo;
    private int mot_estado;

    public int getMot_id() {
        return mot_id;
    }

    public void setMot_id(int mot_id) {
        this.mot_id = mot_id;
    }

    public String getMot_nombremotivo() {
        return mot_nombremotivo;
    }

    public void setMot_nombremotivo(String mot_nombremotivo) {
        this.mot_nombremotivo = mot_nombremotivo;
    }

    public int getMot_estado() {
        return mot_estado;
    }

    public void setMot_estado(int mot_estado) {
        this.mot_estado = mot_estado;
    }
}
