package model;

public class Sucursal {
    private int suc_id;
    private String suc_direccion;
    private int sucestado;
    private int ubi_id;

    public int getSucestado() {
        return sucestado;
    }

    public void setSucestado(int sucestado) {
        this.sucestado = sucestado;
    }

    public int getSuc_id() {
        return suc_id;
    }

    public void setSuc_id(int suc_id) {
        this.suc_id = suc_id;
    }

    public String getSuc_direccion() {
        return suc_direccion;
    }

    public void setSuc_direccion(String suc_direccion) {
        this.suc_direccion = suc_direccion;
    }

    public int getUbi_id() {
        return ubi_id;
    }

    public void setUbi_id(int ubi_id) {
        this.ubi_id = ubi_id;
    }
}
