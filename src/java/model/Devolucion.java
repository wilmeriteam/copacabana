package model;
public class Devolucion {
    private int usu_idempleado;
    private String sol_fechaasignacion;
    private String sol_fecha;
    private String deta_fechatentativa;
    private String nomempleado;
    private String car_nombre;
    private String usu_nombre;
    private String usu_apellido;
    private String pro_genero;
    private String pro_itemname;
    private String pro_descripcion;
    private String pro_material;
    private String pro_itemcode;
    private String pro_nomenclatura;
    private String motivo;
    private String sucursal;
    private int al_cantidad;
    private String pro_vidautil;
    private float vidautil;
    private float al_costo;
    private String pro_color;
    private String pro_logo;
    private int al_id;
    private int car_id;
    private int sol_id;
    private int deta_id;
    private int deta_estadoprenda;
    private int ush_id;
    private String pro_talla;
    private String nomitem;
    private int epr_id;
    private int alfanumeric;
    private int estadofecha;
    private int mot_id; 
    private int candidadsolicitada;
    private int statusBoton;
    private String nomsolicitante;
    private String devuelto;
    private String usu_ci;
    private String ubi_nombre;
    private String division;
    private int nro_form;
    private float descuento;

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public int getNro_form() {
        return nro_form;
    }

    public void setNro_form(int nro_form) {
        this.nro_form = nro_form;
    }

    public float getVidautil() {
        return vidautil;
    }

    public void setVidautil(float vidautil) {
        this.vidautil = vidautil;
    }
    
    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }
    

    public String getUbi_nombre() {
        return ubi_nombre;
    }

    public void setUbi_nombre(String ubi_nombre) {
        this.ubi_nombre = ubi_nombre;
    }

    public String getUsu_ci() {
        return usu_ci;
    }

    public void setUsu_ci(String usu_ci) {
        this.usu_ci = usu_ci;
    }

    public String getDevuelto() {
        return devuelto;
    }

    public void setDevuelto(String devuelto) {
        this.devuelto = devuelto;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getSol_fecha() {
        return sol_fecha;
    }

    public void setSol_fecha(String sol_fecha) {
        this.sol_fecha = sol_fecha;
    }

    public String getNomsolicitante() {
        return nomsolicitante;
    }

    public void setNomsolicitante(String nomsolicitante) {
        this.nomsolicitante = nomsolicitante;
    }

    public int getStatusBoton() {
        return statusBoton;
    }

    public void setStatusBoton(int statusBoton) {
        this.statusBoton = statusBoton;
    }
    
    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    
    public int getCandidadsolicitada() {
        return candidadsolicitada;
    }

    public void setCandidadsolicitada(int candidadsolicitada) {
        this.candidadsolicitada = candidadsolicitada;
    }

    public int getMot_id() {
        return mot_id;
    }

    public void setMot_id(int mot_id) {
        this.mot_id = mot_id;
    }

    public int getUsu_idempleado() {
        return usu_idempleado;
    }

    public void setUsu_idempleado(int usu_idempleado) {
        this.usu_idempleado = usu_idempleado;
    }

    public String getSol_fechaasignacion() {
        return sol_fechaasignacion;
    }

    public void setSol_fechaasignacion(String sol_fechaasignacion) {
        this.sol_fechaasignacion = sol_fechaasignacion;
    }

    public String getDeta_fechatentativa() {
        return deta_fechatentativa;
    }

    public void setDeta_fechatentativa(String deta_fechatentativa) {
        this.deta_fechatentativa = deta_fechatentativa;
    }

    public String getNomempleado() {
        return nomempleado;
    }

    public void setNomempleado(String nomempleado) {
        this.nomempleado = nomempleado;
    }

    public String getCar_nombre() {
        return car_nombre;
    }

    public void setCar_nombre(String car_nombre) {
        this.car_nombre = car_nombre;
    }

    public String getUsu_nombre() {
        return usu_nombre;
    }

    public void setUsu_nombre(String usu_nombre) {
        this.usu_nombre = usu_nombre;
    }

    public String getUsu_apellido() {
        return usu_apellido;
    }

    public void setUsu_apellido(String usu_apellido) {
        this.usu_apellido = usu_apellido;
    }

    public String getPro_genero() {
        return pro_genero;
    }

    public void setPro_genero(String pro_genero) {
        this.pro_genero = pro_genero;
    }

    public String getPro_itemname() {
        return pro_itemname;
    }

    public void setPro_itemname(String pro_itemname) {
        this.pro_itemname = pro_itemname;
    }

    public String getPro_descripcion() {
        return pro_descripcion;
    }

    public void setPro_descripcion(String pro_descripcion) {
        this.pro_descripcion = pro_descripcion;
    }

    public String getPro_material() {
        return pro_material;
    }

    public void setPro_material(String pro_material) {
        this.pro_material = pro_material;
    }

    public String getPro_itemcode() {
        return pro_itemcode;
    }

    public void setPro_itemcode(String pro_itemcode) {
        this.pro_itemcode = pro_itemcode;
    }

    public String getPro_nomenclatura() {
        return pro_nomenclatura;
    }

    public void setPro_nomenclatura(String pro_nomenclatura) {
        this.pro_nomenclatura = pro_nomenclatura;
    }

    public int getAl_cantidad() {
        return al_cantidad;
    }

    public void setAl_cantidad(int al_cantidad) {
        this.al_cantidad = al_cantidad;
    }

    public String getPro_vidautil() {
        return pro_vidautil;
    }

    public void setPro_vidautil(String pro_vidautil) {
        this.pro_vidautil = pro_vidautil;
    }

    public float getAl_costo() {
        return al_costo;
    }

    public void setAl_costo(float al_costo) {
        this.al_costo = al_costo;
    }

    public String getPro_color() {
        return pro_color;
    }

    public void setPro_color(String pro_color) {
        this.pro_color = pro_color;
    }

    public String getPro_logo() {
        return pro_logo;
    }

    public void setPro_logo(String pro_logo) {
        this.pro_logo = pro_logo;
    }

    public int getAl_id() {
        return al_id;
    }

    public void setAl_id(int al_id) {
        this.al_id = al_id;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public int getDeta_id() {
        return deta_id;
    }

    public void setDeta_id(int deta_id) {
        this.deta_id = deta_id;
    }

    public int getDeta_estadoprenda() {
        return deta_estadoprenda;
    }

    public void setDeta_estadoprenda(int deta_estadoprenda) {
        this.deta_estadoprenda = deta_estadoprenda;
    }

    public int getUsh_id() {
        return ush_id;
    }

    public void setUsh_id(int ush_id) {
        this.ush_id = ush_id;
    }

    public String getPro_talla() {
        return pro_talla;
    }

    public void setPro_talla(String pro_talla) {
        this.pro_talla = pro_talla;
    }

    public String getNomitem() {
        return nomitem;
    }

    public void setNomitem(String nomitem) {
        this.nomitem = nomitem;
    }

    public int getEpr_id() {
        return epr_id;
    }

    public void setEpr_id(int epr_id) {
        this.epr_id = epr_id;
    }

    public int getAlfanumeric() {
        return alfanumeric;
    }

    public void setAlfanumeric(int alfanumeric) {
        this.alfanumeric = alfanumeric;
    }

    public int getEstadofecha() {
        return estadofecha;
    }

    public void setEstadofecha(int estadofecha) {
        this.estadofecha = estadofecha;
    }

    
}
