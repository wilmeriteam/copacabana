package model;

public class Selectmenu {

    private int men_id;
    private String men_nombre;
    private int rol_id;

    public Selectmenu(int men_id, String men_nombre) {
        this.men_id = men_id;
        this.men_nombre = men_nombre;
    }

    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public int getMen_id() {
        return men_id;
    }

    public void setMen_id(int men_id) {
        this.men_id = men_id;
    }

    public String getMen_nombre() {
        return men_nombre;
    }

    public void setMen_nombre(String men_nombre) {
        this.men_nombre = men_nombre;
    }
}
