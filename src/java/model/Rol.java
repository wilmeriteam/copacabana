package model;

public class Rol {
    private int rol_id;
    private String rol_nombre;
    private int rol_estado;


    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public String getRol_nombre() {
        return rol_nombre;
    }

    public void setRol_nombre(String rol_nombre) {
        this.rol_nombre = rol_nombre;
    }

    public int getRol_estado() {
        return rol_estado;
    }

    public void setRol_estado(int rol_estado) {
        this.rol_estado = rol_estado;
    }
}
