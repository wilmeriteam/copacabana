
package model;

public class Motivosolicitud {
    private int mtv_id;
    private String mtv_nombre;
    private int mtv_estado;

    public int getMtv_id() {
        return mtv_id;
    }

    public void setMtv_id(int mtv_id) {
        this.mtv_id = mtv_id;
    }

    public String getMtv_nombre() {
        return mtv_nombre;
    }

    public void setMtv_nombre(String mtv_nombre) {
        this.mtv_nombre = mtv_nombre;
    }

    public int getMtv_estado() {
        return mtv_estado;
    }

    public void setMtv_estado(int mtv_estado) {
        this.mtv_estado = mtv_estado;
    }
}
