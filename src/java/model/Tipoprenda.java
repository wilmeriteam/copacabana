
package model;

public class Tipoprenda {
    private int tip_id;
    private String tip_nombre;
    private int tip_estado;

    public int getTip_id() {
        return tip_id;
    }

    public void setTip_id(int tip_id) {
       
        this.tip_id = tip_id;
    }

    public String getTip_nombre() {
        return tip_nombre;
    }

    public void setTip_nombre(String tip_nombre) {
        this.tip_nombre = tip_nombre;
    }

    public int getTip_estado() {
        return tip_estado;
    }

    public void setTip_estado(int tip_estado) {
        this.tip_estado = tip_estado;
    }
}
