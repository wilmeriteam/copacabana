
package model;

public class Menu {
    private int men_id;
    private String men_nombre;
    private String men_alias;
    private int men_estado;
    private String[] selectMenu;

//    public Menu(int men_id, String men_nombre) {
//        this.men_id = men_id;
//        this.men_nombre = men_nombre;
//    }

    public String[] getSelectMenu() {
        return selectMenu;
    }

    public void setSelectMenu(String[] selectMenu) {
        this.selectMenu = selectMenu;
    }

    public int getMen_id() {
        return men_id;
    }

    public void setMen_id(int men_id) {
        this.men_id = men_id;
    }

    public String getMen_nombre() {
        return men_nombre;
    }

    public void setMen_nombre(String men_nombre) {
        this.men_nombre = men_nombre;
    }

    public String getMen_alias() {
        return men_alias;
    }

    public void setMen_alias(String men_alias) {
        this.men_alias = men_alias;
    }

    public int getMen_estado() {
        return men_estado;
    }

    public void setMen_estado(int men_estado) {
        this.men_estado = men_estado;
    }

}
