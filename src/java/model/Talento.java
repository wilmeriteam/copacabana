package model;

public class Talento {

    private int sol_id;
    private String sol_fecha;
    private int usu_idsolicitante;
    private int usu_idempleado;
    private String sol_fechaasignacion;
    private String sol_fechadevolucion;
    private String sol_descripcion;
    private int sol_estado;
    private String nomusuario_empleado;
    private String sucursal;
    private String nomcargo;
    private int tip_id;
    private int clast_id;
    private int mot_id;
    private int motivo;

    private int alfanumeric;
    private int car_id;
    private int ci;
    private int ubi_id;
    private String ush_sexo;
    private String nombremotivo;
    private String clasempleado;
    private int caranterior;
    private String turno;

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public int getMotivo() {
        return motivo;
    }

    public void setMotivo(int motivo) {
        this.motivo = motivo;
    }

    public int getMot_id() {
        return mot_id;
    }

    public void setMot_id(int mot_id) {
        this.mot_id = mot_id;
    }
    
    public String getUsh_sexo() {
        return ush_sexo;
    }

    public void setUsh_sexo(String ush_sexo) {
        this.ush_sexo = ush_sexo;
    }

    public int getUbi_id() {
        return ubi_id;
    }

    public void setUbi_id(int ubi_id) {
        this.ubi_id = ubi_id;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    public int getAlfanumeric() {
        return alfanumeric;
    }

    public void setAlfanumeric(int alfanumeric) {
        this.alfanumeric = alfanumeric;
    }

  

    public int getClast_id() {
        return clast_id;
    }

    public void setClast_id(int clast_id) {
        this.clast_id = clast_id;
    }

    public int getTip_id() {
        return tip_id;
    }

    public void setTip_id(int tip_id) {
        this.tip_id = tip_id;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public String getSol_fecha() {
        return sol_fecha;
    }

    public void setSol_fecha(String sol_fecha) {
        this.sol_fecha = sol_fecha;
    }

    public int getUsu_idsolicitante() {
        return usu_idsolicitante;
    }

    public void setUsu_idsolicitante(int usu_idsolicitante) {
        this.usu_idsolicitante = usu_idsolicitante;
    }

    public int getUsu_idempleado() {
        return usu_idempleado;
    }

    public void setUsu_idempleado(int usu_idempleado) {
        this.usu_idempleado = usu_idempleado;
    }

    public String getSol_fechaasignacion() {
        return sol_fechaasignacion;
    }

    public void setSol_fechaasignacion(String sol_fechaasignacion) {
        this.sol_fechaasignacion = sol_fechaasignacion;
    }

    public String getSol_fechadevolucion() {
        return sol_fechadevolucion;
    }

    public void setSol_fechadevolucion(String sol_fechadevolucion) {
        this.sol_fechadevolucion = sol_fechadevolucion;
    }

    public String getSol_descripcion() {
        return sol_descripcion;
    }

    public void setSol_descripcion(String sol_descripcion) {
        this.sol_descripcion = sol_descripcion;
    }

    public int getSol_estado() {
        return sol_estado;
    }

    public void setSol_estado(int sol_estado) {
        this.sol_estado = sol_estado;
    }

    public String getNomusuario_empleado() {
        return nomusuario_empleado;
    }

    public void setNomusuario_empleado(String nomusuario_empleado) {
        this.nomusuario_empleado = nomusuario_empleado;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getNomcargo() {
        return nomcargo;
    }

    public void setNomcargo(String nomcargo) {
        this.nomcargo = nomcargo;
    }

    public String getNombremotivo() {
        return nombremotivo;
    }

    public void setNombremotivo(String nombremotivo) {
        this.nombremotivo = nombremotivo;
    }

    public String getClasempleado() {
        return clasempleado;
    }

    public void setClasempleado(String clasempleado) {
        this.clasempleado = clasempleado;
    }

    public int getCaranterior() {
        return caranterior;
    }

    public void setCaranterior(int caranterior) {
        this.caranterior = caranterior;
    }


}
