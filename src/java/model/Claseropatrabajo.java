
package model;

public class Claseropatrabajo {
private int clast_id;
private String clast_nombre;
private int clast_estado;

    public int getClast_id() {
        return clast_id;
    }

    public void setClast_id(int clast_id) {
        this.clast_id = clast_id;
    }

    public String getClast_nombre() {
        return clast_nombre;
    }

    public void setClast_nombre(String clast_nombre) {
        this.clast_nombre = clast_nombre;
    }

    public int getClast_estado() {
        return clast_estado;
    }

    public void setClast_estado(int clast_estado) {
        this.clast_estado = clast_estado;
    }
}
