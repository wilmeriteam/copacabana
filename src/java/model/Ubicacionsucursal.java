
package model;

public class Ubicacionsucursal {
    private int suc_id;
    private int ubi_id;
    private String suc_direccion;
    private String suc_ubicacion;

    public int getSuc_id() {
        return suc_id;
    }

    public void setSuc_id(int suc_id) {
        this.suc_id = suc_id;
    }

    public int getUbi_id() {
        return ubi_id;
    }

    public void setUbi_id(int ubi_id) {
        this.ubi_id = ubi_id;
    }

    public String getSuc_direccion() {
        return suc_direccion;
    }

    public void setSuc_direccion(String suc_direccion) {
        this.suc_direccion = suc_direccion;
    }

    public String getSuc_ubicacion() {
        return suc_ubicacion;
    }

    public void setSuc_ubicacion(String suc_ubicacion) {
        this.suc_ubicacion = suc_ubicacion;
    }
}
