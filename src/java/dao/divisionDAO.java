/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Division;

/**
 *
 * @author Wilmer
 */
public class divisionDAO extends DAO {
    public List<Division> listarDivision() throws Exception {

        List<Division> arr = new ArrayList<>();
        ResultSet rs;

        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("select div_id,division,activo from tab_division");
            rs = st.executeQuery();
            while (rs.next()) {
                Division div = new Division();
                div.setId(rs.getInt("div_id"));
                div.setDivision(rs.getString("division"));
                div.setActivo(rs.getInt("activo"));
                arr.add(div);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }    
}
