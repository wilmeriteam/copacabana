package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Rol;
import model.Excepcional;

public class rolDAO extends DAO {

    public List<Rol> listarRoles() throws Exception {

        List<Rol> arr = new ArrayList<>();
        ResultSet rs;

        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("select* from dbo.tab_rol where rol_estado=1");
            rs = st.executeQuery();
            while (rs.next()) {
                Rol rol = new Rol();
                rol.setRol_id(rs.getInt("rol_id"));
                rol.setRol_nombre(rs.getString("rol_nombre"));
                rol.setRol_estado(rs.getInt("rol_estado"));
                arr.add(rol);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public void registrarDao(Rol rol) throws Exception {

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("insert into dbo.tab_rol values(?,1)");
            pst.setString(1, rol.getRol_nombre());
            pst.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

  

    public void modificarDao(Rol rol) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_rol set rol_nombre=? where rol_id=? and rol_estado=1 ");
            pst.setString(1, rol.getRol_nombre());
            pst.setInt(2, rol.getRol_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void eliminarRol(Rol rol) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_rol set rol_estado=2 where rol_id=? ");
            pst.setInt(1, rol.getRol_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public Rol leerRol(Rol rol_id) throws Exception {
        ResultSet st;
        Rol rolmodel = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from dbo.tab_rol where rol_id=? and rol_estado=1");
            pst.setInt(1, rol_id.getRol_id());
            st = pst.executeQuery();
            while (st.next()) {
                rolmodel = new Rol();
                rolmodel.setRol_id(st.getInt("rol_id"));
                rolmodel.setRol_nombre(st.getString("rol_nombre"));
                rolmodel.setRol_estado(st.getInt("rol_estado"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return rolmodel;
    }
    
    public boolean getExcepcional(int id) throws Exception{
        ResultSet st;
        PreparedStatement pst;
        String sql;
        Excepcional excep;
        int cont = 0;
        try {
            this.Conectar();
            sql = "select excep_id,usuario_id,excepcional from tab_excepcional where usuario_id = "+id;
            pst = this.getCn().prepareStatement(sql);
            st = pst.executeQuery();
            
            while(st.next()){
                excep = new Excepcional();
                excep.setExcepid(st.getInt("excep_id"));
                excep.setUsuarioid(st.getInt("usuario_id"));
                excep.setExcepcional(st.getInt("excepcional"));
                ++cont;
            }
        } catch (Exception e) {
            throw e;
        }finally {
            this.Cerrar();
        }
        if(cont > 0)
            return true;
        else
            return false;
    }

}
