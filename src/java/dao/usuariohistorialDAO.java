package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import model.Usuariohistorial;
import org.apache.commons.codec.digest.DigestUtils;
//import org.apache.log4j.Logger;

public class usuariohistorialDAO extends DAO {
    
//    private static final Logger logger = Logger.getLogger(usuariohistorialDAO.class);
    private final HttpServletRequest httpServletR;
    private final FacesContext faceCont;
    private int ush_id = 0;
    private int rol_id = 0;

//    int u = 0;

    public usuariohistorialDAO() {
        faceCont = FacesContext.getCurrentInstance();
        httpServletR = (HttpServletRequest) faceCont.getExternalContext().getRequest();
    }

    public void insertarUsuario(Usuariohistorial us) throws Exception{
        String sql;
        PreparedStatement pst;
        ResultSet st;
        int idusu = 0;
        try {
            this.Conectar();
            sql = "insert into tab_usuario (usu_id,usu_nombre,usu_apellido,usu_sexo) values ((select MAX(usu_id) + 1 from tab_usuario),'" + us.getUsu_nombre() + "','" + us.getUsu_apellido() + "','"+us.getUsu_sexo()+"')";
//            logger.info("inserta usuario: " + sql);
            pst = this.getCn().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
            pst.executeUpdate();
            st = pst.getGeneratedKeys();
            while (st.next()) {
                idusu = st.getInt(1);
            }
            
            sql = "insert into tab_usuariohistorial (usu_id,ush_estado,car_id,suc_id,Area,division,clasempleado,ush_usuario,rol_id) values ("
                    + "(select MAX(usu_id) from tab_usuario),1,'"+us.getCar_id()+"','"+us.getSuc_id()+"','"+us.getArea()+"','"+us.getDivision()+"','N',1,29)";
//            logger.info("insertando historialusuario: "+sql);
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        }finally {
            this.Cerrar();
        }
    }
    
    public boolean VerificarUsuario(String usu, String pass) throws Exception {
        String sql;
        String clave = DigestUtils.sha1Hex(pass);
        PreparedStatement st;
        ResultSet rs;
        int c = 0;
        int exc = 0;
        try {
            this.Conectar();
            sql = "select ush_id,rol_id from tab_usuariohistorial where ush_nick='" + usu + "' and ush_password='" + clave + "' and ush_estado=1";
//            logger.info("Login: "+sql);
            st = this.getCn().prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                ush_id = rs.getInt("ush_id");
                rol_id = rs.getInt("rol_id");
                c++;
            }
            if(c > 0){
                
                sql = "select excep_id,usuario_id,excepcional from tab_excepcional where usuario_id = "+ush_id;
                st = this.getCn().prepareStatement(sql);
                rs = st.executeQuery();
                while(rs.next()){
                    exc = rs.getInt("excep_id");
               }
                httpServletR.getSession().setAttribute("excep_id",exc);
            }
            
            
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        if (c > 0) {
            httpServletR.getSession().setAttribute("ush_id", ush_id);
            httpServletR.getSession().setAttribute("rol_id", rol_id);
            return true;
        } else {
            return false;
        }
    }

    public String nomUsuario(String usu) throws Exception{
        String usu_nombre = "";
        String sql;
        PreparedStatement pst;
        
        ResultSet rs;
        try {
            this.Conectar();
            sql = "select u.usu_nombre + ' ' + u.usu_apellido nombre from tab_usuario u \n" +
                    "join tab_usuariohistorial h on u.usu_id = h.usu_id where h.ush_id = "+Integer.parseInt(usu);
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                usu_nombre = rs.getString("nombre");
            }
        } catch (Exception e) {
            throw e;
        } finally{
            this.Cerrar();
        }
        return usu_nombre;
    }
    public String nomUsuariorol(String usu) throws Exception {
        String usu_nombre = null;
        String usu_apellido = null;
        String rol_nombre = null;
        int us = Integer.parseInt(usu);
        ResultSet rs;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_rol.rol_nombre,\n"
                    + "dbo.tab_usuario.usu_nombre,\n"
                    + "dbo.tab_usuario.usu_apellido,\n"
                    + "dbo.tab_usuariohistorial.ush_id,\n"
                    + "dbo.tab_usuariohistorial.rol_id\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_usuario\n"
                    + "INNER JOIN dbo.tab_usuariohistorial ON dbo.tab_usuario.usu_id = dbo.tab_usuariohistorial.usu_id\n"
                    + "INNER JOIN dbo.tab_rol ON dbo.tab_rol.rol_id = dbo.tab_usuariohistorial.rol_id\n"
                    + "WHERE\n"
                    + "dbo.tab_usuariohistorial.ush_estado = 1 AND\n"
                    + "dbo.tab_usuariohistorial.ush_id =" + us);
            rs = pst.executeQuery();
            while (rs.next()) {

                usu_nombre = rs.getString("usu_nombre");
                usu_apellido = rs.getString("usu_apellido");
                rol_nombre = rs.getString("rol_nombre");

            }
            if (usu_apellido == null) {
                usu_apellido = "";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return rol_nombre + " - " + usu_nombre + " " + usu_apellido;
    }

    public List<Usuariohistorial> listaCodigo(int codigo) throws Exception{
        List<Usuariohistorial> arrcodigo = new ArrayList<>();
        ResultSet rs;
        String sql;
        try {
            this.Conectar();
            sql = "select h.ush_id id,u.usu_nombre+' '+ u.usu_apellido nombres,c.car_nombre cargo,ar.area,x.total\n" +
                "from tab_usuariohistorial h \n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join tab_cargo c on c.car_id = h.car_id\n" +
                "join tab_area ar on ar.area_id = h.Area\n" +
                "join (\n" +
                "	select h.ush_id\n" +
                "	from tab_detalleasignacion d \n" +
                "	join tab_solicitud s on s.sol_id = d.sol_id\n" +
                "	join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
                "	where d.al_id = "+codigo+" and d.deta_estadoprenda = 1 group by h.ush_id ) b on b.ush_id = h.ush_id\n" +
                "join (\n" +
                "	select COUNT(*) total,usu_idempleado\n" +
                "	from tab_detalleasignacion d \n" +
                "	join tab_solicitud s on s.sol_id = d.sol_id\n" +
                "	where d.al_id = "+codigo+" and d.deta_estadoprenda = 1 group by usu_idempleado ) x on x.usu_idempleado = h.ush_id\n";
            System.out.println(sql);
            PreparedStatement st = this.getCn().prepareStatement(sql);            
            rs = st.executeQuery();
            Usuariohistorial pend = new Usuariohistorial();
            while (rs.next()) {
                
                pend.setUsh_id(rs.getInt("id"));
                pend.setUsu_nombre(rs.getString("nombres"));
                pend.setNomcargo(rs.getString("cargo"));
                pend.setNomarea(rs.getString("area"));
                pend.setCantpendiente(rs.getInt("total"));
                arrcodigo.add(pend);
            }   
        } catch (Exception e) {
//            System.out.println("wilmer error");
            throw e;
        } finally {
            this.Cerrar();
        }
        return arrcodigo;
    }
    
    public List<Usuariohistorial> listarUsuarios(String palabra) throws Exception {

        List<Usuariohistorial> arrusu = new ArrayList<>();
        ResultSet rs;
        String sql;
        try {
            this.Conectar();
            sql = "select r.rol_nombre,c.car_nombre nomcargo,h.ush_id,u.usu_id,ush_fingreso,case when h.ush_nick is null then 'X' else h.ush_nick end ush_nick,h.ush_password,h.ush_estado,u.usu_nombre,u.usu_apellido,u.usu_ci,\n" +
                "h.car_id,h.rol_id,h.suc_id,h.Area,h.division,u.usu_sexo from tab_usuariohistorial h\n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join tab_rol r on r.rol_id = h.rol_id\n" +
                "join tab_cargo c on c.car_id = h.car_id\n" +
                "join tab_sucursales s on s.suc_id = h.suc_id\n" +
                "join tab_area a on a.area_id = h.Area\n" +
                "join tab_division d on d.div_id = h.division\n" +
                "where h.ush_estado = 1 and (u.usu_nombre like '%"+palabra+"%' or u.usu_apellido like '%"+palabra+"%')";
//            logger.info("lista usuarios: "+sql);
            PreparedStatement st = this.getCn().prepareCall(sql);
            rs = st.executeQuery();
            String sexo = null;
            while (rs.next()) {
                Usuariohistorial rol = new Usuariohistorial();
                rol.setUsh_id(rs.getInt("ush_id"));
                rol.setUsu_nombre(rs.getString("usu_nombre") + " " + rs.getString("usu_apellido"));
                if (rs.getString("usu_sexo").equals("M")) {
                    sexo = "MASCULINO";
                } else {
                    sexo = "FEMENINO";
                }
                rol.setUsu_sexo(sexo);
                rol.setUsh_correo(rs.getString("ush_nick"));
                rol.setNomrol(rs.getString("rol_nombre"));
                rol.setNomcargo(rs.getString("nomcargo"));
//                rol.setNomubicacion(rs.getString("suc_direccion") + " " + rs.getString("ubi_nombre"));
                arrusu.add(rol);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arrusu;
    }

    public ArrayList<Usuariohistorial> listarNombresSolicicitud(int uu) throws Exception {
        ResultSet st;
        ArrayList<Usuariohistorial> arr = new ArrayList<>();
        ResultSet starea;
        int area = 0;
        try {
            this.Conectar();
            //obtener el usuario area
            PreparedStatement psarea = this.getCn().prepareCall("select * from tab_usuariohistorial where ush_id=" + uu);
            starea = psarea.executeQuery();
            while (starea.next()) {
                area = starea.getInt("Area");
            }

            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "                    dbo.tab_usuario.usu_apellido,\n"
                    + "                    dbo.tab_usuario.usu_nombre,\n"
                    + "                    dbo.tab_usuario.usu_sexo,\n"
                    + "                    dbo.tab_usuariohistorial.ush_id\n"
                    + "\n"
                    + "FROM dbo.tab_usuariohistorial\n"
                    + "                    INNER JOIN dbo.tab_usuario ON dbo.tab_usuario.usu_id = dbo.tab_usuariohistorial.usu_id\n"
                    + "WHERE\n"
                    + "\n"
                    + "dbo.tab_usuariohistorial.ush_estado = 1 AND\n"
//                    + "dbo.tab_usuariohistorial.ush_usuario = 2 AND\n"
                    + "dbo.tab_usuariohistorial.Area = " + area + "\n"
                    + "ORDER BY usu_nombre");
            st = pst.executeQuery();
            while (st.next()) {
                Usuariohistorial usu = new Usuariohistorial();
                usu.setUsh_id(st.getInt("ush_id"));
                usu.setUsu_nombre(st.getString("usu_nombre"));
                usu.setUsu_apellido(st.getString("usu_apellido"));
                usu.setUsu_sexo(st.getString("usu_sexo"));
                arr.add(usu);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

        public ArrayList<Usuariohistorial> listarNombresSolicicitudExcepcional(int uu) throws Exception {
        ResultSet st;
        ArrayList<Usuariohistorial> arr = new ArrayList<>();
        ResultSet starea;
        PreparedStatement psarea;
        int area = 0;
        int division = 0;
        String sql;
        try {
            this.Conectar();
            //obtener el usuario area
            psarea = this.getCn().prepareCall("select * from tab_usuariohistorial where ush_id=" + uu);
            starea = psarea.executeQuery();
            while (starea.next()) {
                area = starea.getInt("Area");
                division = starea.getInt("division");
            }
            sql = "SELECT h.ush_id,u.usu_nombre,u.usu_apellido,u.usu_sexo \n" +
                "from tab_usuariohistorial h \n" +
                "join tab_usuario u on u.usu_id = h.usu_id \n" +
                "where h.ush_estado = 1 and h.division ="+division+" order by usu_nombre asc";
//            logger.info("lista_excep: "+sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);
            st = pst.executeQuery();
            while (st.next()) {
                Usuariohistorial usu = new Usuariohistorial();
                usu.setUsh_id(st.getInt("ush_id"));
                usu.setUsu_nombre(st.getString("usu_nombre"));
                usu.setUsu_apellido(st.getString("usu_apellido"));
                usu.setUsu_sexo(st.getString("usu_sexo"));
                arr.add(usu);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    public ArrayList<Usuariohistorial> listarNombres() throws Exception {
        ResultSet st;
        ArrayList<Usuariohistorial> arr = new ArrayList<>();
        try {
            this.Conectar();
            String sql = "SELECT\n" +
                            "u.usu_apellido,\n" +
                            "u.usu_nombre,\n" +
                            "uh.ush_id,u.usu_sexo \n" +
                            "FROM tab_usuariohistorial uh\n" +
                            "JOIN tab_usuario u ON u.usu_id = uh.usu_id\n" +
                            "WHERE\n" +
                            " uh.ush_estado = 1 and uh.clasempleado in ('R','N','F') and ush_nick is not null";
//            logger.info("Listar Nombres: "+sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);
            st = pst.executeQuery();
            while (st.next()) {
                Usuariohistorial usu = new Usuariohistorial();
                usu.setUsh_id(st.getInt("ush_id"));
                usu.setUsu_nombre(st.getString("usu_nombre"));
                usu.setUsu_apellido(st.getString("usu_apellido"));
                usu.setUsu_sexo(st.getString("usu_sexo"));
                arr.add(usu);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public Usuariohistorial leerUsuario(Usuariohistorial usuh) throws Exception {
        ResultSet st;
        Usuariohistorial usuariohistorial = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("select * from tab_usuariohistorial h join tab_usuario u on u.usu_id = h.usu_id where ush_id = ?");
            pst.setInt(1, usuh.getUsh_id());
            st = pst.executeQuery();
            while (st.next()) {
                usuariohistorial = new Usuariohistorial();
                usuariohistorial.setUsh_id(st.getInt("ush_id"));
                usuariohistorial.setUsu_id(st.getInt("usu_id"));
                usuariohistorial.setUsh_correo(st.getString("ush_nick"));
                usuariohistorial.setRol_id(st.getInt("rol_id"));
                usuariohistorial.setUsu_nombre(st.getString("usu_nombre"));
                usuariohistorial.setUsu_apellido(st.getString("usu_apellido"));
                usuariohistorial.setUsu_sexo(st.getString("usu_sexo"));
                usuariohistorial.setSuc_id(st.getInt("suc_id"));
                usuariohistorial.setCar_id(st.getInt("car_id"));
                usuariohistorial.setDivision(st.getInt("division"));
                usuariohistorial.setArea(st.getInt("Area"));
                usuariohistorial.setUsh_estado(st.getInt("ush_estado"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return usuariohistorial;

    }

    public Usuariohistorial leerUsuariodao(int id) throws Exception {
        ResultSet st;
        Usuariohistorial usuariohistorial = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "rtrim(dbo.tab_usuario.usu_apellido) as usu_apellido,\n"
                    + "rtrim(dbo.tab_usuario.usu_nombre) as usu_nombre,\n"
                    + "dbo.tab_usuariohistorial.ush_id,\n"
                    + "dbo.tab_usuariohistorial.usu_id,\n"
                    + "dbo.tab_usuariohistorial.ush_fingreso,\n"
                    + "dbo.tab_usuariohistorial.ush_ffinal,\n"
                    + "rtrim(dbo.tab_usuariohistorial.ush_nick) as ush_nick,\n"
                    + "rtrim(dbo.tab_usuariohistorial.ush_password) as ush_password,\n"
                    + "dbo.tab_usuariohistorial.ush_estado,\n"
                    + "dbo.tab_usuariohistorial.car_id,\n"
                    + "dbo.tab_usuariohistorial.rol_id,\n"
                    + "dbo.tab_usuariohistorial.suc_id\n"
                    + "\n"
                    + "FROM dbo.tab_usuariohistorial\n"
                    + "                    INNER JOIN dbo.tab_usuario ON dbo.tab_usuario.usu_id = dbo.tab_usuariohistorial.usu_id\n"
                    + "WHERE\n"
                    + "dbo.tab_usuariohistorial.ush_estado = 1 AND\n"
                    + "dbo.tab_usuariohistorial.ush_id = " + id);
            st = pst.executeQuery();
            while (st.next()) {
                usuariohistorial = new Usuariohistorial();
                usuariohistorial.setUsh_id(st.getInt("ush_id"));
                usuariohistorial.setUsh_correo(st.getString("ush_nick"));
                usuariohistorial.setUsu_nombre(st.getString("usu_nombre") + " " + st.getString("usu_apellido"));
                usuariohistorial.setUsh_password(st.getString("ush_password"));
                usuariohistorial.setUsh_estado(st.getInt("ush_estado"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return usuariohistorial;

    }

    public void insertCredencial(Usuariohistorial usu,boolean value1) throws Exception {
        String clave = DigestUtils.sha1Hex("srt123");
        String sql = "update tab_usuariohistorial set ush_nick=?,ush_password='" + clave + "',rol_id=? where ush_id=? and ush_estado=1";
        PreparedStatement pst;
        ResultSet st;
        try {
            this.Conectar();
            pst = this.getCn().prepareCall(sql);
            pst.setString(1, usu.getUsh_correo());
            pst.setInt(2, usu.getRol_id());
            pst.setInt(3, usu.getUsh_id());
            pst.executeUpdate();
            int cont = 0;
            sql = "select * from tab_excepcional where usuario_id = "+usu.getUsh_id();
            pst = getCn().prepareStatement(sql);
            st = pst.executeQuery();
            while(st.next()){
                ++cont;
            }
            if(value1){
                if(cont == 0){
                    sql = "insert into tab_excepcional values ("+usu.getUsh_id()+",1)";
                    pst = getCn().prepareStatement(sql);
                    pst.executeUpdate();
                }
            } else {
                if(cont > 0){
                    sql = "delete from tab_excepcional where usuario_id = "+usu.getUsh_id();
                    pst = getCn().prepareStatement(sql);
                    pst.executeUpdate();
                }                
            }
            
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void saveclavedao(int id, String sh) throws Exception {

        String clavegenerado = DigestUtils.sha1Hex(sh);

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("update tab_usuariohistorial set ush_password='" + clavegenerado + "' where ush_id=" + id + " and ush_estado=1");
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void updateUsuario(Usuariohistorial us) throws Exception{
        String sql;
        PreparedStatement pst;
        try {
            this.Conectar();
            sql = "update tab_usuario set usu_nombre = '"+us.getUsu_nombre()+"', usu_apellido = '"+us.getUsu_apellido()+"', usu_sexo = '"+us.getUsu_sexo()+"' where usu_id = "+us.getUsu_id();
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
            
            sql = "update tab_usuariohistorial set car_id = "+us.getCar_id()+",suc_id="+us.getSuc_id()+",Area="+us.getArea()+",division="+us.getDivision()+" where ush_id = "+us.getUsh_id();
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
