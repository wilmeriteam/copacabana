package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Tipoprenda;

public class tipoprendaDAO extends DAO {

    public ArrayList<Tipoprenda> selectTipoprenda() throws Exception {
        ResultSet st;
        ArrayList<Tipoprenda> arr = new ArrayList<>();
        Tipoprenda tip = null;

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("select tip_nombre,tip_id from dbo.tab_tipoprenda where tip_estado=1");
            st = pst.executeQuery();
            while (st.next()) {
                tip = new Tipoprenda();
                tip.setTip_id(st.getInt("tip_id"));
                tip.setTip_nombre(st.getString("tip_nombre"));
               arr.add(tip);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
}
