/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Almacen;
import model.Asignacion;
import model.Asignacionsolicitud;
import model.Devolucion;
import model.Solicitud;
import model.Usuariohistorial;

/**
 *
 * @author Wilmer
 */
public class reimpresionDAO extends DAO{
    public List<Usuariohistorial> listarreimpresion(String pal) throws Exception {

        List<Usuariohistorial> arr = new ArrayList<>();
        ResultSet rs;
        int i = 0;
        try {
            this.Conectar();
            String getSql = "select uh.ush_id,u.usu_nombre,u.usu_apellido,c.car_nombre,u.usu_ci,\n" +
"uh.ush_estado \n" +
"from tab_usuario u \n" +
"join tab_usuariohistorial uh on uh.usu_id = u.usu_id \n" +
"join tab_cargo c on c.car_id =  uh.car_id \n" +
"where u.usu_nombre like '%"+pal+"%' or u.usu_apellido like '%"+pal+"%'";
            System.out.println(getSql);
//            logger.info("Lista primera: "+getSql);
            PreparedStatement st = this.getCn().prepareCall(getSql);
            rs = st.executeQuery();
            while (rs.next()) {
                //Inyectar aqui el web service automático
                Usuariohistorial usu = new Usuariohistorial();
                usu.setAlfanumeric(++i);
                usu.setUsh_id(rs.getInt("ush_id"));
                usu.setUsu_nombre(rs.getString("usu_nombre"));
                usu.setUsu_apellido(rs.getString("usu_apellido"));
                usu.setUsu_ci(rs.getString("usu_ci"));
                usu.setNomcargo(rs.getString("car_nombre"));
                usu.setUsh_estado(rs.getInt("ush_estado"));
                arr.add(usu);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }  
    
    public Usuariohistorial listarasignacionesdetalle(int id) throws Exception {
        Usuariohistorial asig = null;

        ResultSet rs;
        try {
            this.Conectar();
            String getSql = "select uh.ush_id,u.usu_nombre,u.usu_apellido,c.car_nombre,u.usu_ci,\n" +
"uh.ush_estado \n" +
"from tab_usuario u \n" +
"join tab_usuariohistorial uh on uh.usu_id = u.usu_id \n" +
"join tab_cargo c on c.car_id =  uh.car_id \n" +
"where uh.ush_id = " + id;
                System.out.println(getSql);
//                logger.info("Lista Cabecera: "+getsql);
            PreparedStatement st = this.getCn().prepareCall(getSql);
            rs = st.executeQuery();
            String[] fecha;
            while (rs.next()) {
                asig = new Usuariohistorial();
                asig.setUsh_id(rs.getInt("ush_id"));
                asig.setUsu_nombre(rs.getString("usu_nombre") + " " +rs.getString("usu_apellido"));
                asig.setUsu_ci(rs.getString("usu_ci"));
                asig.setNomcargo(rs.getString("car_nombre"));
                asig.setUsh_estado(rs.getInt("ush_estado"));
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return asig;
    }
    
    public List<Solicitud> listarAlmacenOneRow( int id) throws Exception{
//        ArrayList arr = new ArrayList();
        List<Solicitud> arr = new ArrayList<>();
        ResultSet rs;
        String sql;
        int i = 0;
        try {
            this.Conectar();
                sql = "SELECT (RTRIM(u.usu_nombre) + ' ' +RTRIM(u.usu_apellido) ) nomusuario_empleado,\n" +
"c.car_nombre nomcargo,(RTRIM(su.suc_direccion) +' '+RTRIM(b.ubi_nombre)) sucursal,\n" +
"h.ush_fingreso fechai,h.ush_ffinal fechaf,(RTRIM(u2.usu_nombre) + ' ' +RTRIM(u2.usu_apellido) ) nomusuario_solicitante,\n" +
"s.sol_id,s.sol_fecha,s.usu_idsolicitante,s.usu_idempleado,s.sol_fechaasignacion,s.sol_fechadevolucion,s.sol_descripcion,\n" +
"s.sol_estado,suc_direccion,b.ubi_id,b.ubi_nombre,c.car_id,s.mtv_id,u.usu_nombre,u.usu_apellido,s.sol_nroform,md.mot_nombremotivo,s.sol_nroform,\n" +
"u.usu_ci,CASE WHEN u.usu_sexo = 'M' THEN 'Masculino' WHEN u.usu_sexo = 'F' THEN 'Femenino' END usu_sexo,dv.division\n" +
"FROM tab_solicitud s \n" +
"JOIN tab_usuariohistorial h ON h.ush_id = s.usu_idempleado\n" +
"JOIN tab_cargo c on c.car_id = h.car_id\n" +
"JOIN tab_sucursales su on su.suc_id = h.suc_id\n" +
"JOIN tab_usuario u on u.usu_id = h.usu_id\n" +
"JOIN tab_ubicacion b on b.ubi_id = su.ubi_id\n" +
"JOIN tab_usuariohistorial h2 ON h2.ush_id = s.usu_idsolicitante\n" +
"JOIN tab_usuario u2 on u2.usu_id = h2.usu_id\n" +
"JOIN tab_motivodescuento md on md.mot_id = s.mtv_id \n" +
"JOIN tab_division dv on h.division = dv.div_id \n" +
"where s.usu_idempleado = "+id;

System.out.println(sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            String[] fecha;
            while(rs.next()){
                Solicitud sol = new Solicitud();
                sol.setAlfanumeric(++i);
                sol.setSol_id(rs.getInt("sol_id"));
                fecha =  rs.getString("sol_fecha").split("-");
                sol.setSol_fecha(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);
                if( rs.getString("sol_fechaasignacion") != null){
                    fecha = rs.getString("sol_fechaasignacion").split("-");
                    sol.setSol_fechaasignacion(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);  
                } else {
                    sol.setSol_fechaasignacion( "" );
                }
                if( rs.getString("sol_fechadevolucion") != null){
                    fecha = rs.getString("sol_fechadevolucion").split("-");
                    sol.setSol_fechadevolucion(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);  
                } else {
                    sol.setSol_fechadevolucion( "" );
                }                
                sol.setNomusuario_empleado(rs.getString("nomusuario_empleado"));
                sol.setNomcargo(rs.getString("nomcargo"));
                sol.setMotivo(rs.getString("mot_nombremotivo"));
                sol.setSol_estado(rs.getInt("sol_estado"));
                sol.setSol_descripcion(rs.getString("sol_descripcion"));
                arr.add(sol);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();            
        }
        return arr;
    } 
    
    public Asignacionsolicitud getCabeceraReporte(int id) throws Exception {
        Asignacionsolicitud asig = null;

        ResultSet rs;
        try {
            this.Conectar();
                String getsql = "SELECT (RTRIM(u.usu_nombre) + ' ' +RTRIM(u.usu_apellido) ) nomusuario_empleado,c.car_nombre nomcargo,(RTRIM(su.suc_direccion) +' '+RTRIM(b.ubi_nombre)) sucursal,\n" +
                "h.ush_fingreso fechai,h.ush_ffinal fechaf,(RTRIM(u2.usu_nombre) + ' ' +RTRIM(u2.usu_apellido) ) nomusuario_solicitante,\n" +
                "s.sol_id,s.sol_fecha,s.usu_idsolicitante,s.usu_idempleado,s.sol_fechaasignacion,s.sol_fechadevolucion,s.sol_descripcion,\n" +
                "s.sol_estado,suc_direccion,b.ubi_id,b.ubi_nombre,c.car_id,s.mtv_id,u.usu_nombre,u.usu_apellido,s.sol_nroform,md.mot_nombremotivo,s.sol_nroform,"
                + "u.usu_ci,CASE WHEN u.usu_sexo = 'M' THEN 'Masculino' WHEN u.usu_sexo = 'F' THEN 'Femenino' END usu_sexo,dv.division\n" +
                "FROM tab_solicitud s \n" +
                "JOIN tab_usuariohistorial h ON h.ush_id = s.usu_idempleado\n" +
                "JOIN tab_cargo c on c.car_id = h.car_id\n" +
                "JOIN tab_sucursales su on su.suc_id = h.suc_id\n" +
                "JOIN tab_usuario u on u.usu_id = h.usu_id\n" +
                "JOIN tab_ubicacion b on b.ubi_id = su.ubi_id\n" +
                "JOIN tab_usuariohistorial h2 ON h2.ush_id = s.usu_idsolicitante\n" +
                "JOIN tab_usuario u2 on u2.usu_id = h2.usu_id\n" +
                "JOIN tab_motivodescuento md on md.mot_id = s.mtv_id \n" +
                "JOIN tab_division dv on h.division = dv.div_id \n" +
                "where s.sol_id = "+id;
//                logger.info("Lista Cabecera: "+getsql);
            PreparedStatement st = this.getCn().prepareCall(getsql);
            rs = st.executeQuery();
            String[] fecha;
            while (rs.next()) {
                asig = new Asignacionsolicitud();
                asig.setSol_id(rs.getInt("sol_id"));
                asig.setNomsolicitante(rs.getString("nomusuario_solicitante"));
                asig.setNomempleado(rs.getString("nomusuario_empleado"));
                asig.setCar_nombre(rs.getString("nomcargo"));
                asig.setSucursal(rs.getString("sucursal"));
                asig.setCi(rs.getString("usu_ci"));
                asig.setGenero(rs.getString("usu_sexo"));
                asig.setDpto(rs.getString("division"));
                asig.setSol_fechaasignacion(rs.getString("sol_fechaasignacion"));
                asig.setSol_fechadevolucion(rs.getString("sol_fechadevolucion"));
                asig.setFechai(rs.getString("fechai"));
                asig.setFechaf(rs.getString("fechaf"));
                asig.setSol_estado(rs.getInt("sol_estado"));
                asig.setApellidos(rs.getString("usu_apellido"));
                asig.setNombres(rs.getString("usu_nombre"));
                asig.setArea(rs.getString("suc_direccion"));
                asig.setDepartamento(rs.getString("ubi_nombre"));
                asig.setUsu_idempleado(rs.getInt("usu_idempleado"));
                asig.setCar_id(rs.getInt("car_id"));
                fecha = rs.getString("sol_fecha").split("-");
                asig.setFechasolicitud(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);
                asig.setUbi_id(rs.getInt("ubi_id"));
                asig.setNroform(rs.getInt("sol_nroform"));
                asig.setMotivosolicitud(rs.getString("mot_nombremotivo"));
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return asig;
    }
    
    
    public  ArrayList<Almacen> dataReporte(int id) throws Exception{
        ArrayList arr = new ArrayList();
        ResultSet st;
        
        try {
            this.Conectar();
            String sql = "select p.pro_itemname,p.pro_unidad,1 deta_cantidadsolicitada, \n" +
                "p.pro_descripcion,p.pro_talla\n" +
                "from tab_detalleasignacion d\n" +
                "join tab_solicitud s on s.sol_id = d.sol_id\n" +
                "join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join tab_almacen a on a.al_id = d.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id\n" +
                "where s.sol_id = "+id;
//            System.out.println(sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            st = pst.executeQuery();
            while (st.next()) {
                Almacen al = new Almacen();
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_unidad(st.getString("pro_unidad"));
                al.setDeta_cantidadsolicitada(st.getInt("deta_cantidadsolicitada"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
}
