
package dao;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import model.Clapersonal;
public class clapersonalDAO extends DAO{
    
  public ArrayList<Clapersonal> SelectListClaPersonal() throws Exception{
  ResultSet st;
  ArrayList<Clapersonal> arr=new ArrayList<>();
      try {
          this.Conectar();
          PreparedStatement pst=this.getCn().prepareCall("select* from dbo.tab_clapersonal where clap_estado=1");
          st=pst.executeQuery();
          while(st.next()){
              Clapersonal clap=new Clapersonal();
              clap.setClap_id(st.getInt("clap_id"));
              clap.setClap_nombre(st.getString("clap_nombre"));
              arr.add(clap);
          }
          
      } catch (Exception e) {
          throw e;
      }finally{
      this.Cerrar();
      }
      return arr;
  }
 
      public List<Clapersonal> listarClapersonal() throws Exception {
        
        List<Clapersonal> arr = new ArrayList<>();
        ResultSet rs;
        
        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("select* from dbo.tab_clapersonal where clap_estado=1");
            rs = st.executeQuery();
            while (rs.next()) {
                Clapersonal men = new Clapersonal();
               men.setClap_id(rs.getInt("clap_id"));
               men.setClap_nombre(rs.getString("clap_nombre"));
               men.setClap_estado(rs.getInt("clap_estado"));
                arr.add(men);
            }
            
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
    public void registrarDao(Clapersonal men) throws Exception {
        
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("insert into dbo.tab_clapersonal values(?,1)");
            pst.setString(1, men.getClap_nombre());
            pst.executeUpdate();
            
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void modificarDao(Clapersonal men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_clapersonal set clap_nombre=? where clap_id=? and clap_estado=1");
            pst.setString(1, men.getClap_nombre());
            pst.setInt(2, men.getClap_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
  public void eliminarClapersonal(Clapersonal men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_clapersonal set clap_estado=2 where clap_id=? ");
            pst.setInt(1, men.getClap_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
    public Clapersonal leerClapersonal(Clapersonal men) throws Exception {
        ResultSet st;
        Clapersonal clapersonalmodel=null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from dbo.tab_clapersonal where clap_id=? and clap_estado=1");
            pst.setInt(1, men.getClap_id());
            st = pst.executeQuery();
            while (st.next()) {
                clapersonalmodel=new Clapersonal();
                clapersonalmodel.setClap_id(st.getInt("clap_id"));
                clapersonalmodel.setClap_nombre(st.getString("clap_nombre"));
            }
        } catch (Exception e) {
            throw e;
        }finally{
        this.Cerrar();
        }
        return clapersonalmodel;
    }
    
}
