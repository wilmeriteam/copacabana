package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Motivodescuento;


public class motivodescuentoDAO extends DAO {

 public List<Motivodescuento> listarSelectDescuento() throws Exception{
     ResultSet st;
     ArrayList<Motivodescuento> arr=new ArrayList<>();
     try {
         this.Conectar();
         PreparedStatement pst=this.getCn().prepareCall("select* from tab_motivodescuento where mot_estado=1");
         st=pst.executeQuery();
         while(st.next()){
             Motivodescuento mdes=new Motivodescuento();
             mdes.setMot_id(st.getInt("mot_id"));
             mdes.setMot_nombremotivo(st.getString("mot_nombremotivo"));
             mdes.setMot_estado(st.getInt("mot_estado"));
             arr.add(mdes);
         }
     } catch (Exception e) {
        throw e;
     }finally{
     this.Cerrar();
     }
     return arr;
 }

}
