package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Menu;
import model.Permisos;
import model.Rol;

public class permisosDAO extends DAO {

    public void GuardarListaPermisos(Rol rol, List<String> men) throws Exception {
        ResultSet st;
        int u = 0;
        int i = 0;
        String where = "";
        try {
            this.Conectar();
            int cantidad = men.size();

            for (String row : men) {
                i++;
                PreparedStatement psrow = this.getCn().prepareStatement("select* from tab_permisos where rol_id=? and men_id=" + row);
                psrow.setInt(1, rol.getRol_id());
                st = psrow.executeQuery();
                while (st.next()) {
                    u++;
                }
                if (u == 0) {
                    PreparedStatement pst = this.getCn().prepareStatement("insert into tab_permisos values(?," + row + ",1)");
                    pst.setInt(1, rol.getRol_id());
                    pst.executeUpdate();
                }
                where += "men_id!=" + row;
                u = 0;
                if (cantidad > i) {
                    where += " and";
                }
            }
            if (cantidad > 0) {
                PreparedStatement delete = this.getCn().prepareStatement("delete from tab_permisos where rol_id=? and " + where);
                delete.setInt(1, rol.getRol_id());
                delete.executeUpdate();
            } else {
                PreparedStatement delete = this.getCn().prepareStatement("delete from tab_permisos where rol_id=?");
                delete.setInt(1, rol.getRol_id());
                delete.executeUpdate();
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
// public static String[]  ObtenerlistPermisos(){
////      String pickedString[];
//        
//        pickedCipher[0]="1";
//        pickedCipher[1]="2";
//        pickedCipher[2]="8";
////        pickedCipher.a
////       return new String[]{"1", "2", "8"};    
//         
//    return pickedCipher;
//// return 
// }

    public String[] ObtenerlistPermisos(Rol rol) throws Exception {
        ResultSet st;
        String[] vector = new String[30];

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select* from dbo.tab_permisos where rol_id=?");
            pst.setInt(1, rol.getRol_id());
            st = pst.executeQuery();
            int i = 0;
            while (st.next()) {
                vector[i] = Integer.toString(st.getInt("men_id"));
                i++;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return vector;
    }

    public Rol Obtenerid(Rol id) throws Exception {
        ResultSet st;
        Rol rolmodel = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from dbo.tab_rol where rol_id=? and rol_estado=1");
            pst.setInt(1, id.getRol_id());
            st = pst.executeQuery();
            while (st.next()) {
                rolmodel = new Rol();
                rolmodel.setRol_id(st.getInt("rol_id"));
                rolmodel.setRol_nombre(st.getString("rol_nombre"));
                rolmodel.setRol_estado(st.getInt("rol_estado"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return rolmodel;
    }

}
