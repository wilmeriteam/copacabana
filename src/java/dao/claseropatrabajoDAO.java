package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Claseropatrabajo;

public class claseropatrabajoDAO extends DAO {

    public ArrayList<Claseropatrabajo> obtenerSelectClaseRopa(int id) throws Exception {
        ResultSet st;
        ArrayList<Claseropatrabajo> arr = new ArrayList<>();
        Claseropatrabajo tipc = null;

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_claseropatrabajo.clast_nombre,\n"
                    + "dbo.tab_tipoprenda.tip_id,\n"
                    + "dbo.tab_claseropatrabajo.clast_id\n"
                    + "\n"
                    + "FROM\n"
                    + "                    dbo.tab_claseropatrabajo\n"
                    + "                    INNER JOIN dbo.tab_grupoclasetrabajo ON dbo.tab_claseropatrabajo.clast_id = dbo.tab_grupoclasetrabajo.clast_id\n"
                    + "                    INNER JOIN dbo.tab_tipoprenda ON dbo.tab_tipoprenda.tip_id = dbo.tab_grupoclasetrabajo.tip_id\n"
                    + "WHERE\n"
                    + "                    dbo.tab_claseropatrabajo.clast_estado = 1 AND\n"
                    + "                    dbo.tab_tipoprenda.tip_estado = 1 AND\n"
                    + "                    dbo.tab_tipoprenda.tip_id =" + id);
            st = pst.executeQuery();
            while (st.next()) {
                tipc = new Claseropatrabajo();
                tipc.setClast_id(st.getInt("clast_id"));
                tipc.setClast_nombre(st.getString("clast_nombre"));
                arr.add(tipc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
       public ArrayList<Claseropatrabajo> obtenerSelectClaseRopaAll() throws Exception {
        ResultSet st;
        ArrayList<Claseropatrabajo> arr = new ArrayList<>();
        Claseropatrabajo tipc = null;

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_claseropatrabajo.clast_nombre,\n"
                    + "dbo.tab_tipoprenda.tip_id,\n"
                    + "dbo.tab_claseropatrabajo.clast_id\n"
                    + "\n"
                    + "FROM\n"
                    + "                    dbo.tab_claseropatrabajo\n"
                    + "                    INNER JOIN dbo.tab_grupoclasetrabajo ON dbo.tab_claseropatrabajo.clast_id = dbo.tab_grupoclasetrabajo.clast_id\n"
                    + "                    INNER JOIN dbo.tab_tipoprenda ON dbo.tab_tipoprenda.tip_id = dbo.tab_grupoclasetrabajo.tip_id\n"
                    + "WHERE\n"
                    + "                    dbo.tab_claseropatrabajo.clast_estado = 1 AND\n"
                    + "                    dbo.tab_tipoprenda.tip_estado = 1");
            st = pst.executeQuery();
            while (st.next()) {
                tipc = new Claseropatrabajo();
                tipc.setClast_id(st.getInt("clast_id"));
                tipc.setClast_nombre(st.getString("clast_nombre"));
                arr.add(tipc);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
}
