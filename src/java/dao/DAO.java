
package dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;

public class DAO {
    private Connection cn;
    private static String db="ejemplo";
    private static String user="root";
    private static String pass="";
    private static String url="jdbc:mysql://localhost:3306/"+db;
//    private static Connection conn;
    
    public Connection getCn() {
        return cn;
    }

    public void setCn(Connection cn) {
        this.cn = cn;
    }
    public void Conectar() throws Exception{
        String archivo = "C:\\SRT\\srtcnx";
//        String archivo = "/home/wilmer/SRT/srtcnx";
        String cadena;
        String dat[] = new String[4];
        int i = 0;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            String datos[] = cadena.split("=");
            dat[i] = datos[1];
            i++;
        }
        b.close();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            cn= DriverManager.getConnection("jdbc:sqlserver://"+dat[3]+";databaseName="+dat[0],dat[1],dat[2]);
//            cn= DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=copabol2","sa","12345");
//            cn= DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=copabol","copabol","12345");
        } catch (Exception e) {
            throw e;
        }
    
    }
    public void Cerrar() throws Exception{
        try {
        if(cn!=null){
            if(cn.isClosed()==false){
            cn.close();
            }
        }
        } catch (Exception e) {
        throw e;
        }
    }
}
