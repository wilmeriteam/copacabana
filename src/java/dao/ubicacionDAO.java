package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Ubicacion;
import model.Ubicacionsucursal;
//import model.Ubicacion;

public class ubicacionDAO extends DAO {

    public List<Ubicacionsucursal> listarUbicacion() throws Exception {

        List<Ubicacionsucursal> arr = new ArrayList<>();
        ResultSet rs;

        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_ubicacion.ubi_nombre,\n"
                    + "dbo.tab_sucursales.suc_direccion,\n"
                    + "dbo.tab_sucursales.suc_id,\n"
                    + "dbo.tab_ubicacion.ubi_id\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_sucursales\n"
                    + "INNER JOIN dbo.tab_ubicacion ON dbo.tab_ubicacion.ubi_id = dbo.tab_sucursales.ubi_id\n"
                    + "WHERE\n"
                    + "dbo.tab_sucursales.suc_estado = 1");
            rs = st.executeQuery();
            while (rs.next()) {
                Ubicacionsucursal suc = new Ubicacionsucursal();
                suc.setSuc_id(rs.getInt("suc_id"));
                suc.setSuc_direccion((rs.getString("suc_direccion")).trim());
                suc.setSuc_ubicacion((rs.getString("ubi_nombre")).trim());
                suc.setUbi_id(rs.getInt("ubi_id"));
                arr.add(suc);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Ubicacion> SelectListUbicacion() throws Exception {
        ResultSet st;
        ArrayList arr = new ArrayList();
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("select * from dbo.tab_ubicacion where ubi_estado=1");
            st = pst.executeQuery();
            while (st.next()) {
                Ubicacion ubi = new Ubicacion();
                ubi.setUbi_id(st.getInt("ubi_id"));
                ubi.setUbi_nombre(st.getString("ubi_nombre"));
                ubi.setSigla(st.getString("sigla"));
                ubi.setUbi_estado(st.getInt("ubi_estado"));
                arr.add(ubi);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public void registrarDao(Ubicacionsucursal suc) throws Exception {

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("insert into tab_sucursales values(LTRIM(RTRIM(?)),1,?)");
            pst.setString(1, suc.getSuc_direccion());
            pst.setInt(2, suc.getUbi_id());
            pst.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void modificarDao(Ubicacionsucursal suc) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update tab_sucursales set suc_direccion=LTRIM(RTRIM(?)),ubi_id=? where suc_id=? and suc_estado=1");
            pst.setString(1, suc.getSuc_direccion());
            pst.setInt(2, suc.getUbi_id());
            pst.setInt(3, suc.getSuc_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void eliminarUbicacion(Ubicacionsucursal suc) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_sucursales set suc_estado=2 where suc_id=? ");
            pst.setInt(1, suc.getSuc_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public Ubicacionsucursal leerUbicacion(Ubicacionsucursal suc) throws Exception {
        ResultSet st;
        Ubicacionsucursal ubicacionmodel = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from dbo.tab_sucursales where suc_id=? and suc_estado=1");
            pst.setInt(1, suc.getSuc_id());
            st = pst.executeQuery();
            while (st.next()) {
                ubicacionmodel = new Ubicacionsucursal();
                ubicacionmodel.setSuc_id(st.getInt("suc_id"));
                ubicacionmodel.setSuc_direccion((st.getString("suc_direccion")).trim());
                ubicacionmodel.setUbi_id(st.getInt("ubi_id"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return ubicacionmodel;
    }

}
