package dao;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import model.Almacen;
import model.Solicitud;
import model.Talento;

//import org.apache.log4j.Logger;
//import org.apache.log4j.BasicConfigurator;
public class almacenDAO extends DAO {

        public Solicitud kardexEmpleado(String ci) throws SQLException, Exception {
        String sql;
        ResultSet rs;
        ArrayList arr = new ArrayList();
        Solicitud sol = new Solicitud();
        try {
            this.Conectar();
            sql = "select h.ush_id,(u.usu_nombre+' '+u.usu_apellido) nombre,u.usu_ci,ub.ubi_id,c.car_nombre\n" +
                    "from tab_usuario u\n" +
                    "join tab_usuariohistorial h on h.usu_id = u.usu_id\n" +
                    "join tab_cargo c on c.car_id = h.car_id\n" +
                    "join tab_sucursales su on su.suc_id = h.suc_id\n" +
                    "join tab_ubicacion ub on ub.ubi_id = su.ubi_id\n" +
                    "where u.usu_ci = '"+ci+"'";
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                sol.setUsu_idempleado(rs.getInt("ush_id"));
                sol.setNomusuario_empleado(rs.getString("nombre"));
                sol.setUbi_id(rs.getInt("ubi_id"));
                sol.setNomcargo(rs.getString("car_nombre"));
                
            }
        } catch (Exception e) {
            throw e;
        } finally{
            this.Cerrar();
        }
        return sol;
    }

    public Almacen obtenerItem(String codigo, int ubi) throws SQLException, Exception {
        String sql;
        ResultSet rs;
        ArrayList arr = new ArrayList();
        Almacen al = new Almacen();
        try {
            this.Conectar();
            sql = "select a.al_id,p.pro_itemname,p.pro_itemcode,a.ubi_id \n"
                    + "from tab_almacen a join tab_producto p on p.pro_id = a.pro_id \n"
                    + "where p.pro_itemcode = '" + codigo + "' and a.ubi_id = " + ubi;
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            
            while (rs.next()) {
                al.setPro_itemname(rs.getString("pro_itemname"));
                al.setAl_id(rs.getInt("al_id"));
            }
        } catch (Exception e) {
            throw e;
        } finally{
            this.Cerrar();
        }
        return al;
    }
//    private static final Logger logger = Logger.getLogger(almacenDAO.class);

    public ArrayList<Almacen> listarAlmacen(Solicitud sol, int solid) throws Exception {
        ArrayList arr = new ArrayList();
        String where = null;
        if (solid != 0) {
            where = " and tab_detalleasignacion.sol_id=" + solid;
        }
        ResultSet st;

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "                    dbo.tab_almacen.al_id,\n"
                    + "                    dbo.tab_almacen.al_cantidad,\n"
                    + "                    dbo.tab_producto.pro_itemcode,\n"
                    + "                    dbo.tab_producto.pro_color,\n"
                    + "                    dbo.tab_producto.pro_logo,\n"
                    + "                    dbo.tab_producto.pro_itemname,\n"
                    + "                    dbo.tab_producto.pro_descripcion,\n"
                    + "                    dbo.tab_producto.pro_vidautil,\n"
                    + "                    dbo.tab_producto.pro_material,\n"
                    + "                    dbo.tab_producto.pro_genero,\n"
                    + "                    dbo.tab_producto.pro_talla,\n"
                    + "                    dbo.tab_detalleasignacion.deta_id,\n"
                    + "                    dbo.tab_detalleasignacion.deta_estadoprenda\n"
                    + "                    \n"
                    + "                    FROM\n"
                    + "                                                            dbo.tab_almacen\n"
                    + "                                                            INNER JOIN dbo.tab_producto ON dbo.tab_producto.pro_id = dbo.tab_almacen.pro_id\n"
                    + "                                                            INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "                    WHERE\n"
                    + "                                        dbo.tab_almacen.al_estado = 1 " + where);
//            pst.setInt(1, sol.getSol_id());dbo.tab_detalleasignacion.sol_id = ?
            st = pst.executeQuery();
            while (st.next()) {
                Almacen al = new Almacen();
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_material(st.getString("pro_material"));
                al.setAl_cantidad(st.getInt("al_cantidad"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_genero(st.getString("pro_genero"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setDeta_id(st.getInt("deta_id"));
                al.setDeta_estadoprenda(st.getInt("deta_estadoprenda"));

                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Almacen> getProductos(int usu_idempleado ) throws Exception {
        ArrayList arr = new ArrayList();
        String sql;
        try {
            this.Conectar();
            sql = "select a.pro_id,p.pro_itemname from (\n" +
                        "	select p.pro_id\n" +
                        "	from tab_solicitud s \n" +
                        "	join tab_detalleasignacion d on d.sol_id = s.sol_id\n" +
                        "	join tab_almacen a on a.al_id =  d.al_id\n" +
                        "	join tab_producto p on p.pro_id = a.pro_id and a.ubi_id = 1\n" +
                        "	where d.deta_fechaasignacion between '2014-01-02 00:00:00'  " +
                        " and '2017-11-02 00:00:00' \n" +
                        "	and s.usu_idempleado = "+usu_idempleado+" group by p.pro_id\n" +
                        ") a join tab_producto p on p.pro_id = a.pro_id";
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Almacen al = new Almacen();
                al.setPro_id(rs.getInt("pro_id"));
                al.setPro_itemname(rs.getString("pro_itemname"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
    public ArrayList<Almacen> listaAlmacenCantidad(int car_id) throws Exception {
        ArrayList arr = new ArrayList();
        try {
            this.Conectar();
            String sql = "select rt.u_item,p.pro_itemname,rt.u_cantidad \n" +
                "from tab_cargo c \n" +
                "join tab_areatrabajocargo atc on atc.car_id = c.car_id\n" +
                "join tab_tipopersonal tp on tp.tpe_id = atc.tpe_id\n" +
                "join tab_ropatrabajo rt on rt.tpe_id = tp.tpe_id\n" +
                "join tab_producto p on p.pro_itemcode = rt.u_item\n" +
                "where c.car_estado = 1 and c.car_id = "+car_id;
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                Almacen al = new Almacen();
                al.setPro_itemcode(rs.getString("u_item"));
                al.setPro_itemname(rs.getString("pro_itemname"));
                al.setCantidad(rs.getInt("u_cantidad"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Almacen> listarAlmacenSolicitudNuevo(Solicitud sol) throws Exception {
        ArrayList arr = new ArrayList();
        String sql;
        ResultSet st;
        int conti = 0;
        PreparedStatement pst;
        int carid = sol.getCar_id();
        int usuid = sol.getUsu_idempleado();

        int ubid = sol.getUbi_id();
        String sexo = sol.getUsu_sexo();
        try {
            this.Conectar();
            sql = "select S.igr_id,S.al_id,p.pro_itemcode,p.pro_itemname,p.pro_logo,p.pro_material,p.pro_color,p.pro_genero,a.al_cantidad,0 deta_id,CONVERT(VARCHAR(10),DATEADD(MONTH,p.pro_vidautil,convert(date, GETDATE())), 105) fechatentativa,p.pro_vidautil from (\n"
                    + "	SELECT T.igr_id,T.al_id FROM (\n"
                    + "		select p.igr_id,min(a.al_id) al_id,min(rt.u_cantidad) cant from tab_areatrabajocargo atc \n"
                    + "		join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n"
                    + "		join tab_producto p on p.pro_itemcode = rt.u_item \n"
                    + "		join tab_almacen a on a.pro_id = p.pro_id \n"
                    + "		where atc.car_id = " + carid + " and (p.pro_genero = '" + sexo + "' or p.pro_genero = '') and a.ubi_id = " + ubid + " group by p.igr_id\n"
                    + "	) T\n"
                    + "	CROSS JOIN Numeros WHERE n<=T.cant\n"
                    + ") S join tab_almacen a on a.al_id = S.al_id\n"
                    + "join tab_producto p on p.pro_id = a.pro_id\n"
                    + "where S.igr_id not in (\n"
                    + "	select p.igr_id from tab_solicitud s join tab_detalleasignacion d on d.sol_id = s.sol_id \n"
                    + "	join tab_almacen a on a.al_id = d.al_id\n"
                    + "	join tab_producto p on p.pro_id = a.pro_id\n"
                    + "	where s.usu_idempleado = " + usuid + ")";
//            logger.info("sol dotacion: " + sql);
            pst = this.getCn().prepareStatement(sql);
            st = pst.executeQuery();
            while (st.next()) {
                Almacen al = new Almacen();
                al.setAlfanumeric(++conti);
                al.setDeta_id(st.getInt("deta_id"));
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_material(st.getString("pro_material"));
                al.setAl_cantidad(st.getInt("al_cantidad"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_genero(st.getString("pro_genero"));
//                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
//                al.setEstadoitem(st.getInt("solicitud"));
                al.setFechatentativa(st.getString("fechatentativa"));
//                al.setDeta_estadoprenda(st.getInt("deta_estadoprenda"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Almacen> listarAlmacenSolicitud(Solicitud sol, int motivo) throws Exception {
        String aux = (motivo == 5) ? " where GETDATE() > fechatentativa " : "";
        ArrayList arr = new ArrayList();
        String sql;
        ResultSet st;
        int conti = 0;
        int usuid = sol.getUsu_idempleado();
        int ubid = sol.getUbi_id();
        String sexo = sol.getUsu_sexo();
        try {
            this.Conectar();
            sql = "WITH solicitudes (deta_id,al_id,igr_id,itemcode,itemname,pro_logo,pro_color,pro_genero,pro_material,pro_talla,pro_vidautil,fechatentativa,al_cantidad)\n"
                    + "AS (\n"
                    + "	select d.deta_id,d.al_id,p.igr_id,p.pro_itemcode,p.pro_itemname,p.pro_logo,p.pro_color,p.pro_genero,p.pro_material,p.pro_talla,p.pro_vidautil,\n"
                    + "	CONVERT(VARCHAR(10), temp.deta_fechatentativa, 105) fechatentativa,a.al_cantidad\n"
                    + "	from tab_solicitud s\n"
                    + "	join tab_detalleasignacion d on d.sol_id = s.sol_id\n"
                    + "	join (select * from tab_detalleasignacion d join (\n"
                    + "	select d.deta_id temp_id from tab_solicitud s join tab_detalleasignacion d on s.sol_id = d.sol_id\n"
                    + "	where s.usu_idempleado = " + usuid + " and deta_estadoprenda = 2) tablita on tablita.temp_id = d.deta_id\n"
                    + "	where d.deta_estadoprenda > 1) temp on temp.temp_id = d.deta_id\n"
                    + "	join tab_almacen a on a.al_id = d.al_id\n"
                    + "	join tab_producto p on p.pro_id = a.pro_id\n"
                    + "	where s.usu_idempleado = " + usuid + " and d.deta_id not in\n"
                    + "	(select deta_idpadre from tab_detalleasignacion where deta_estadoprenda = 1 and deta_idpadre is not null)"
                    + ") \n"
                    + "SELECT deta_id,al_id,igr_id,itemcode,itemname,pro_logo,pro_color,pro_genero,pro_material,pro_talla,pro_vidautil,fechatentativa,al_cantidad\n"
                    + "FROM solicitudes" + aux;
//            logger.info("Solicitud Detalle: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
//            pst.setInt(1, sol.getSol_id());dbo.tab_detalleasignacion.sol_id = ?
            st = pst.executeQuery();
            while (st.next()) {
                Almacen al = new Almacen();
                al.setAlfanumeric(++conti);
                al.setDeta_id(st.getInt("deta_id"));
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("itemcode"));
                al.setPro_itemname(st.getString("itemname"));
                al.setPro_material(st.getString("pro_material"));
                al.setAl_cantidad(st.getInt("al_cantidad"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_genero(st.getString("pro_genero"));
//                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
//                al.setEstadoitem(st.getInt("solicitud"));
                al.setFechatentativa(st.getString("fechatentativa"));
//                al.setDeta_estadoprenda(st.getInt("deta_estadoprenda"));

                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Almacen> listarExcepcionalaux(int sol) throws Exception {
        ArrayList arr = new ArrayList();
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        try {
            this.Conectar();
            sql = "select d.deta_id,d.sol_id,d.al_id,p.pro_itemcode,p.pro_itemname,p.pro_logo,p.pro_color,p.pro_material,p.pro_vidautil,a.al_cantidad from tab_solicitud s \n"
                    + "join tab_detalleasignacion d on d.sol_id = s.sol_id\n"
                    + "join tab_almacen a on a.al_id = d.al_id\n"
                    + "join tab_producto p on p.pro_id = a.pro_id\n"
                    + "where s.sol_id = " + sol;
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Almacen al = new Almacen();
                al.setSol_id(rs.getInt("sol_id"));
                al.setAl_id(rs.getInt("al_id"));
                al.setPro_itemcode(rs.getString("pro_itemcode"));
                al.setPro_itemname(rs.getString("pro_itemname"));
                al.setPro_logo(rs.getString("pro_logo"));
                al.setPro_color(rs.getString("pro_color"));
                al.setPro_material(rs.getString("pro_material"));
                al.setPro_vidautil(rs.getInt("pro_vidautil"));
                al.setAl_cantidad(rs.getInt("al_cantidad"));
                al.setDeta_id(rs.getInt("deta_id"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        }
        return arr;
    }

    public ArrayList<Almacen> listarExcepcional(Solicitud sol) throws Exception {
        ArrayList arr = new ArrayList();
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        try {
            this.Conectar();
            sql = "select pt.igr_id,pt.al_id,pt.cant,p.pro_itemcode,pt.pro_id,p.pro_itemname,p.pro_material,p.pro_color,p.pro_vidautil,pt.stock,p.pro_logo from (\n"
                    + "	select p.igr_id,min(a.al_id) al_id,min(p.pro_id) pro_id,min(rt.u_cantidad) cant,max(a.al_cantidad) stock from tab_areatrabajocargo atc\n"
                    + "	join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n"
                    + "	join tab_producto p on p.pro_itemcode = rt.u_item\n"
                    + "	join tab_almacen a on a.pro_id = p.pro_id\n"
                    + "	where (p.pro_genero = '" + sol.getUsu_sexo() + "' or p.pro_genero = '') and a.ubi_id = " + sol.getUbi_id() + "\n"
                    + "	group by p.igr_id ) pt join tab_producto p on p.pro_id = pt.pro_id";
//            logger.info("Excepcional: "+sql);
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Almacen al = new Almacen();
                al.setAl_id(rs.getInt("al_id"));
                al.setPro_id(rs.getInt("pro_id"));
                al.setPro_itemname(rs.getString("pro_itemname"));
                al.setPro_material(rs.getString("pro_material"));
                al.setPro_color(rs.getString("pro_color"));
                al.setPro_vidautil(rs.getInt("pro_vidautil"));
                al.setMaxcantidad(rs.getInt("stock"));
                al.setPro_logo(rs.getString("pro_logo"));
                al.setPro_itemcode(rs.getString("pro_itemcode"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        System.out.println();
        return arr;
    }

    public ArrayList<Almacen> listarAlmacentalento(Talento sol, int solid) throws Exception {
        ArrayList arr = new ArrayList();
        String where = null;
        if (solid != 0) {
            where = " and tab_detalleasignacion.sol_id=" + solid;
        }
        ResultSet st;
        try {
            this.Conectar();
            String sql = "SELECT\n"
                    + "dbo.tab_almacen.al_id,\n"
                    + "dbo.tab_almacen.al_cantidad,\n"
                    + "dbo.tab_almacen.al_numart,\n"
                    + "dbo.tab_almacen.al_color,\n"
                    + "dbo.tab_almacen.al_logo,\n"
                    + "dbo.tab_producto.pro_nombreprenda,\n"
                    + "dbo.tab_producto.pro_descripcion,\n"
                    + "dbo.tab_almacen.al_tiempovida,\n"
                    + "dbo.tab_producto.pro_material,\n"
                    + "dbo.tab_almacen.al_genero,\n"
                    + "dbo.tab_almacen.al_talla,\n"
                    + "dbo.tab_detalleasignacion.deta_id,\n"
                    + "dbo.tab_detalleasignacion.deta_estadoprenda\n"
                    + "\n"
                    + "FROM\n"
                    + "                                        dbo.tab_almacen\n"
                    + "                                        INNER JOIN dbo.tab_producto ON dbo.tab_producto.pro_id = dbo.tab_almacen.pro_id\n"
                    + "                                        INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "WHERE\n"
                    + "                    dbo.tab_almacen.al_estado = 1 " + where;
//            logger.info(sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);
//            pst.setInt(1, sol.getSol_id());dbo.tab_detalleasignacion.sol_id = ?
            st = pst.executeQuery();
            while (st.next()) {
                Almacen al = new Almacen();
                al.setAl_id(st.getInt("al_id"));
//                al.setAl_numart(st.getString("al_numart"));
//                al.setPro_nombre(st.getString("pro_nombreprenda"));
//                al.setPro_descripcion(st.getString("pro_descripcion"));
//                al.setAl_numart(st.getString("al_numart"));
//                al.setPro_material(st.getString("pro_material"));
//                al.setAl_cantidad(st.getInt("al_cantidad"));
//                al.setAl_color(st.getString("al_color"));
//                al.setAl_genero(st.getString("al_genero"));
//                al.setAl_talla(st.getString("al_talla"));
//                al.setAl_logo(st.getString("al_logo"));
//                al.setAl_tiempovida(st.getString("al_tiempovida"));
//                al.setDeta_id(st.getInt("deta_id"));
//                al.setDeta_estadoprenda(st.getInt("deta_estadoprenda"));

                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Almacen> listarAlmacenOneRow(int idsol) throws Exception {
        ArrayList arr = new ArrayList();
        String where = null;
        if (idsol != 0) {
            where = " and tab_detalleasignacion.sol_id=" + idsol;
        }
        ResultSet st;

        try {
            this.Conectar();
            String sql = "SELECT\n"
                    + "                    dbo.tab_almacen.al_id,\n"
                    + "                    dbo.tab_almacen.al_cantidad,\n"
                    + "                    dbo.tab_producto.pro_itemcode,\n"
                    + "                    dbo.tab_producto.pro_color,\n"
                    + "                    dbo.tab_producto.pro_logo,\n"
                    + "                    dbo.tab_producto.pro_itemname,\n"
                    + "                    dbo.tab_producto.pro_descripcion,\n"
                    + "                    dbo.tab_producto.pro_vidautil,\n"
                    + "                    dbo.tab_producto.pro_material,\n"
                    + "                    dbo.tab_producto.pro_genero,\n"
                    + "                    dbo.tab_producto.pro_talla,\n"
                    + "                    dbo.tab_detalleasignacion.deta_estadoprenda,\n"
                    + "                    dbo.tab_detalleasignacion.deta_id,\n"
                    + "                    dbo.tab_detalleasignacion.deta_cantidadsolicitada\n"
                    + "                    FROM\n"
                    + "                                                            dbo.tab_almacen\n"
                    + "                                                            INNER JOIN dbo.tab_producto ON dbo.tab_producto.pro_id = dbo.tab_almacen.pro_id\n"
                    + "                                                            INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "                    WHERE\n"
                    + "                                        dbo.tab_almacen.al_estado = 1 " + where;
//            logger.info("ListarAlmacenOrRow: "+sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);

//            pst.setInt(1, sol.getSol_id());dbo.tab_detalleasignacion.sol_id = ?
            st = pst.executeQuery();
            while (st.next()) {
                Almacen al = new Almacen();
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_material(st.getString("pro_material"));
                al.setAl_cantidad(st.getInt("al_cantidad"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_genero(st.getString("pro_genero"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setDeta_id(st.getInt("deta_id"));
                al.setDeta_estadoprenda(st.getInt("deta_estadoprenda"));
//                al.setCantasignado(st.getInt("deta_cantidadsolicitada"));
                al.setCantasignado(st.getInt("deta_cantidadsolicitada"));
////                al.set

                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Almacen> listarAlmacenSelectItemSolicitud2(Solicitud sol) throws Exception {
        ResultSet st;

        ArrayList<Almacen> arrayropa = new ArrayList<>();
        try {
            this.Conectar();

            PreparedStatement pst = this.getCn().prepareStatement("select ca.car_id,ca.atcar_id,ca.tpe_id,r.u_item,a.al_id,a.ubi_id,p.pro_itemname,p.pro_unidad,pro_color, \n"
                    + "p.pro_descripcion, p.pro_genero, p.pro_talla, p.pro_logo,p.pro_material,p.pro_vidautil,a.al_cantidad,a.al_costo,i.grupo\n"
                    + "from tab_areatrabajocargo ca \n"
                    + "join tab_ropatrabajo r on r.tpe_id = ca.tpe_id\n"
                    + "join (select MAX(p.pro_itemcode) itemcode\n"
                    + "from tab_producto p join tab_itemgrupo i on i.igr_id = p.igr_id\n"
                    + "group by p.igr_id,p.pro_genero) temp on temp.itemcode = r.u_item\n"
                    + "join tab_producto p on temp.itemcode = p.pro_itemcode\n"
                    + "join tab_almacen a on a.pro_id = p.pro_id\n"
                    + "join tab_itemgrupo i on i.igr_id = p.igr_id\n"
                    + "where ca.car_id = ? AND a.ubi_id = ? AND p.pro_estado = 1"
                    + "and (RTRIM(p.pro_genero)='' OR RTRIM(p.pro_genero) = ?)");
            pst.setInt(1, sol.getCar_id());
            pst.setInt(2, sol.getUbi_id());
            pst.setString(3, sol.getUsu_sexo());
            st = pst.executeQuery();
            while (st.next()) {
                Almacen rp = new Almacen();
                rp.setAl_id(st.getInt("al_id"));
                rp.setRopa(st.getString("grupo") + " " + st.getString("pro_descripcion"));
                rp.setAl_cantidad(st.getInt("al_cantidad"));
                rp.setPro_color(st.getString("pro_color"));
                arrayropa.add(rp);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arrayropa;
    }

    public ArrayList<Almacen> listarAlmacenSelectItemSolicitudAsignacion(int car_id, int ubi_id) throws Exception {
        ResultSet st;

        ArrayList<Almacen> arrayropa = new ArrayList<>();
        try {
            this.Conectar();

            PreparedStatement pst = this.getCn().prepareCall("select ca.car_id,ca.atcar_id,ca.tpe_id,r.u_item,a.al_id,a.ubi_id,p.pro_itemname,p.pro_unidad,pro_color, \n"
                    + "p.pro_descripcion, p.pro_genero, p.pro_talla, p.pro_logo,p.pro_material,p.pro_vidautil,a.al_cantidad,a.al_costo,i.grupo\n"
                    + "from tab_areatrabajocargo ca \n"
                    + "join tab_ropatrabajo r on r.tpe_id = ca.tpe_id\n"
                    + "join (select MAX(p.pro_itemcode) itemcode\n"
                    + "from tab_producto p join tab_itemgrupo i on i.igr_id = p.igr_id\n"
                    + "group by p.igr_id) temp on temp.itemcode = r.u_item\n"
                    + "join tab_producto p on temp.itemcode = p.pro_itemcode\n"
                    + "join tab_almacen a on a.pro_id = p.pro_id\n"
                    + "join tab_itemgrupo i on i.igr_id = p.igr_id\n"
                    + "where ca.car_id = " + car_id + " AND a.ubi_id = " + ubi_id + " AND p.pro_estado = 1");
            st = pst.executeQuery();
            while (st.next()) {
                Almacen rp = new Almacen();
                rp.setAl_id(st.getInt("al_id"));
                rp.setRopa(st.getString("grupo") + " " + st.getString("pro_descripcion"));
                rp.setAl_cantidad(st.getInt("al_cantidad"));
                rp.setPro_color(st.getString("pro_color"));
                arrayropa.add(rp);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arrayropa;
    }

    public ArrayList<Almacen> listarAlmacenSelectItem(int ubi, int al_id, int idempleado) throws Exception {
        ResultSet st;
        ArrayList<Almacen> arrayropa = new ArrayList<>();
        try {
            this.Conectar();
            String sql = "select a.al_id,p.pro_itemname,p.pro_unidad,pro_color, \n"
                    + "p.pro_descripcion,p.pro_talla, \n"
                    + "p.pro_logo,p.pro_material,a.al_cantidad\n"
                    + "from tab_almacen a join tab_producto p on p.pro_id = a.pro_id\n"
                    + "where p.igr_id = (select p2.igr_id from tab_producto p2 join tab_almacen al on al.pro_id = p2.pro_id where al_id = " + al_id + ")\n"
                    + "AND a.ubi_id = " + ubi + " AND (p.pro_genero = (select u3.usu_sexo from tab_usuariohistorial h3 join tab_usuario u3 on u3.usu_id = h3.usu_id where h3.ush_id = " + idempleado + ")\n"
                    + "OR p.pro_genero = '')";
//            logger.info("mod talla");
//            logger.info(sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);
            st = pst.executeQuery();
            while (st.next()) {
                Almacen rp = new Almacen();
                rp.setAl_id(st.getInt("al_id"));
                rp.setRopa(st.getString("pro_itemname") + " " + st.getString("pro_color") + " " + st.getString("pro_descripcion") + " Talla (" + st.getString("pro_talla") + " ) ");
                rp.setAl_cantidad(st.getInt("al_cantidad"));
                rp.setPro_color(st.getString("pro_color"));
                arrayropa.add(rp);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arrayropa;
    }

    public Almacen SeleccionarCantidadItemSolicitud(int id, Solicitud sol) throws Exception {
        ResultSet st;
        int cantidad = 0;
        Almacen al = new Almacen();
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("SELECT\n"
                    + "p.pro_itemname,\n"
                    + "p.pro_descripcion,\n"
                    + "p.pro_material,\n"
                    + "a.al_id,\n"
                    + "p.pro_itemcode,\n"
                    + "rtrim(p.pro_genero) as pro_genero,\n"
                    + "p.pro_talla,\n"
                    + "p.pro_nomenclatura,\n"
                    + "a.al_cantidad,\n"
                    + "p.pro_vidautil,\n"
                    + "a.al_estado,\n"
                    + "a.al_costo,\n"
                    + "a.ubi_id,\n"
                    + "a.pro_id,\n"
                    + "p.pro_color,\n"
                    + "p.pro_logo,\n"
                    + "rt.u_cantidad,\n"
                    + "rt.tpe_id\n"
                    + "FROM\n"
                    + "tab_almacen a\n"
                    + "INNER JOIN tab_producto p ON a.pro_id = p.pro_id\n"
                    + "INNER JOIN tab_ropatrabajo rt ON p.pro_itemcode = rt.u_item\n"
                    + "WHERE\n"
                    + "a.al_estado = 1 AND\n"
                    + "a.al_id = " + id + " \n"
                    + "AND rt.tpe_id = ? ");
            pst.setInt(1, sol.getTpe_id());
            st = pst.executeQuery();
            while (st.next()) {
                cantidad = st.getInt("al_cantidad");
                al.setAl_cantidad(cantidad);
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_talla(st.getString("pro_talla"));
                if (st.getString("pro_genero").equals("M")) {
                    al.setPro_genero("MASCULINO");
                }
                if (st.getString("pro_genero").equals("F")) {
                    al.setPro_genero("FEMENINO");
                }
                if (st.getString("pro_genero").equals("")) {
                    al.setPro_genero("UNISEX");
                }
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setAl_costo(st.getFloat("al_costo"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_material(st.getString("pro_material"));
//                al.setDeta_id(st.getInt("deta_id"));
                al.setCantasignado(st.getInt("u_cantidad"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return al;
    }

    public Almacen SeleccionarCantidadItemTalento(int id) throws Exception {
        ResultSet st;
        int cantidad = 0;
        Almacen al = new Almacen();
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_producto.pro_itemname,\n"
                    + "dbo.tab_producto.pro_descripcion,\n"
                    + "dbo.tab_producto.pro_material,\n"
                    + "dbo.tab_almacen.al_id,\n"
                    + "dbo.tab_producto.pro_itemcode,\n"
                    + "dbo.tab_producto.pro_genero,\n"
                    + "dbo.tab_producto.pro_talla,\n"
                    + "dbo.tab_producto.pro_nomenclatura,\n"
                    + "dbo.tab_almacen.al_cantidad,\n"
                    + "dbo.tab_producto.pro_vidautil,\n"
                    + "dbo.tab_almacen.al_estado,\n"
                    + "dbo.tab_almacen.al_costo,\n"
                    + "dbo.tab_almacen.ubi_id,\n"
                    + "dbo.tab_almacen.pro_id,\n"
                    + "dbo.tab_producto.pro_color,\n"
                    + "dbo.tab_producto.pro_logo,\n"
                    + "(select deta_id from tab_detalleasignacion as det where det.al_id=tab_almacen.al_id) AS deta_id\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_almacen\n"
                    + "INNER JOIN dbo.tab_producto ON dbo.tab_almacen.pro_id = dbo.tab_producto.pro_id\n"
                    + "WHERE\n"
                    + "dbo.tab_almacen.al_estado = 1 AND\n"
                    + "dbo.tab_almacen.al_id = " + id);
//            pst.setInt(1, sol.getTpe_id());
            st = pst.executeQuery();
            while (st.next()) {
                cantidad = st.getInt("al_cantidad");
                al.setAl_cantidad(cantidad);
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_genero(st.getString("pro_genero"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setAl_costo(st.getFloat("al_costo"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_material(st.getString("pro_material"));
                al.setDeta_id(st.getInt("deta_id"));
                al.setCantasignado(st.getInt("u_cantidad"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return al;
    }

    public Almacen SeleccionarCantidadItem(int id, int sol_id) throws Exception {
        ResultSet st;
        int cantidad = 0;
        Almacen al = new Almacen();
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT \n"
                    + "dbo.tab_producto.pro_itemname,\n"
                    + "dbo.tab_producto.pro_descripcion,\n"
                    + "dbo.tab_producto.pro_material,\n"
                    + "dbo.tab_almacen.al_id,\n"
                    + "dbo.tab_producto.pro_itemcode,\n"
                    + "dbo.tab_producto.pro_genero,\n"
                    + "dbo.tab_producto.pro_talla,\n"
                    + "dbo.tab_producto.pro_nomenclatura,\n"
                    + "dbo.tab_almacen.al_cantidad,\n"
                    + "dbo.tab_producto.pro_vidautil,\n"
                    + "dbo.tab_almacen.al_estado,\n"
                    + "dbo.tab_almacen.al_costo,\n"
                    + "dbo.tab_almacen.ubi_id,\n"
                    + "dbo.tab_almacen.pro_id,\n"
                    + "dbo.tab_producto.pro_color,\n"
                    + "dbo.tab_producto.pro_logo,\n"
                    + "dbo.tab_detalleasignacion.deta_estadoprenda,\n"
                    + "dbo.tab_detalleasignacion.deta_id,dbo.tab_detalleasignacion.deta_cantidadsolicitada,rt.u_cantidad \n"
                    + "FROM \n"
                    + "dbo.tab_almacen \n"
                    + "INNER JOIN dbo.tab_producto ON dbo.tab_almacen.pro_id = dbo.tab_producto.pro_id \n"
                    + "INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id \n"
                    + "JOIN tab_solicitud s on s.sol_id = " + sol_id + " \n"
                    + "JOIN tab_usuariohistorial h on h.ush_id = s.usu_idempleado \n"
                    + "JOIN tab_areatrabajocargo ar on ar.car_id = h.car_id \n"
                    + "JOIN tab_ropatrabajo rt on rt.tpe_id = ar.tpe_id "
                    + "WHERE \n"
                    + "dbo.tab_almacen.al_estado = 1 AND \n"
                    + "dbo.tab_almacen.al_id = " + id + " AND \n"
                    + "dbo.tab_detalleasignacion.sol_id = " + sol_id + " AND \n"
                    + "dbo.tab_detalleasignacion.deta_estadoprenda = 1 "
                    + "AND rt.tpe_id = ar.tpe_id AND rt.u_item = dbo.tab_producto.pro_itemcode");
            st = pst.executeQuery();
            while (st.next()) {
                cantidad = st.getInt("al_cantidad");
                al.setAl_cantidad(cantidad);
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_genero(st.getString("pro_genero"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setAl_costo(st.getFloat("al_costo"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_material(st.getString("pro_material"));
                al.setDeta_id(st.getInt("deta_id"));
                al.setCantasignado(st.getInt("deta_cantidadsolicitada"));
                al.setMaxcantidad(st.getInt("u_cantidad"));

            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return al;
    }

    /**
     *
     * @param id id del almacen
     * @param al2
     * @return
     * @throws Exception
     */
    public Almacen SeleccionarCantidadItemAlmacen(int id, Almacen al2) throws Exception {
        ResultSet st;
        int cantidad = 0;
        int cantidadlimitada = al2.getMaxcantidad();
        Almacen al = new Almacen();
        try {
            this.Conectar();
            String varselect = "SELECT p.pro_itemname, p.pro_descripcion, p.pro_material, a.al_id, p.pro_itemcode, \n"
                    + "p.pro_genero, p.pro_talla, p.pro_nomenclatura, a.al_cantidad, p.pro_vidautil, a.al_estado, \n"
                    + "a.al_costo, a.ubi_id, a.pro_id, p.pro_color, p.pro_logo, r.u_cantidad\n"
                    + "FROM \n"
                    + "dbo.tab_almacen a\n"
                    + "JOIN tab_producto p ON a.pro_id = p.pro_id \n"
                    + "JOIN (select r.u_item,max(r.ropatrab_id) ropa_id from tab_ropatrabajo r group by r.u_item ) ropa\n"
                    + "on ropa.u_item = p.pro_itemcode\n"
                    + "JOIN tab_ropatrabajo r on r.ropatrab_id = ropa.ropa_id\n"
                    + "WHERE a.al_estado = 1 AND a.al_id = " + id;
//            String varselect = "SELECT p.pro_itemname, p.pro_descripcion, p.pro_material, a.al_id, p.pro_itemcode, p.pro_genero, p.pro_talla, \n" +
//"p.pro_nomenclatura, a.al_cantidad, p.pro_vidautil, a.al_estado, a.al_costo, a.ubi_id, a.pro_id, p.pro_color, p.pro_logo,d.deta_cantidadsolicitada,rt.u_cantidad \n" +
//"FROM tab_almacen a\n" +
//"JOIN tab_producto p ON a.pro_id = p.pro_id \n" +
//"JOIN tab_solicitud s on s.sol_id = "+sol_id+" \n" +
//"JOIN tab_detalleasignacion d on d.sol_id = s.sol_id AND d.al_id = a.al_id\n" +
//"JOIN tab_usuariohistorial h on h.ush_id = s.usu_idempleado \n" +
//"JOIN tab_areatrabajocargo ar on ar.car_id = h.car_id\n" +
//"JOIN tab_ropatrabajo rt on rt.tpe_id = ar.tpe_id\n" +
//"WHERE a.al_estado = 1 AND a.al_id = "+id+" \n" +
//"AND d.sol_id = "+sol_id+" \n" +
//"AND d.deta_estadoprenda = 1\n" +
//"AND rt.tpe_id = ar.tpe_id\n" +
//"AND rt.u_item = p.pro_itemcode";
            PreparedStatement pst = this.getCn().prepareCall(varselect);
            st = pst.executeQuery();
            while (st.next()) {
                cantidad = st.getInt("al_cantidad");
                al.setAl_cantidad(cantidad);
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_genero(st.getString("pro_genero"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setAl_costo(st.getFloat("al_costo"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_material(st.getString("pro_material"));
                al.setCantasignado(cantidadlimitada);
                al.setMaxcantidad(cantidadlimitada);
                al.setMaxcantidad(st.getInt("u_cantidad"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return al;
    }
}
