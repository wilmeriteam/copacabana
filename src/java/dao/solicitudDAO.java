package dao;

import ManagenBean.solicitudBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Almacen;
import model.Solicitud;
import model.Tipoprenda;
import model.Excepcional;
//import org.apache.log4j.Logger;
import org.primefaces.component.calendar.Calendar;

public class solicitudDAO extends DAO {

//    private static final Logger logger = Logger.getLogger(solicitudDAO.class);
    
    public int returnExcepcional(int usu_id) throws Exception{
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        int i = 0;
        try {
            this.Conectar();
            sql = "select * from tab_excepcional where usuario_id = "+usu_id;
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                i++;
            }
        } catch (Exception e) {
            throw e;
        }
        if(i > 0){
            return 1;
        } else {
            return 0;
        }
    }

    public List<Solicitud> listarSolicitud(int usu) throws Exception {

        List<Solicitud> arr = new ArrayList<>();
        ResultSet rs;
        String sql;
        int i = 0;
        try {
            this.Conectar();
            sql = "select c.car_nombre nomcargo,s.suc_direccion sucursal,(u.usu_nombre +' ' + u.usu_apellido) nomusuario_empleado,so.excepcional, \n"
                    + "case when not exists ("
                    + "	select * from tab_solicitud s5 join tab_detalleasignacion d5 on d5.sol_id = s5.sol_id \n" +
"	join tab_almacen a5 on a5.al_id = d5.al_id\n" +
"	join tab_producto p5 on p5.pro_id = a5.pro_id\n" +
"	where s5.usu_idempleado = so.usu_idempleado and d5.deta_estadoprenda = 1 and exists (\n" +
"		select * from tab_solicitud s1 join tab_detalleasignacion d1 on d1.sol_id = s1.sol_id \n" +
"		join tab_almacen a1 on a1.al_id = d1.al_id\n" +
"		join tab_producto p1 on p1.pro_id = a1.pro_id\n" +
"		where s1.usu_idempleado = so.usu_idempleado and d1.deta_estadoprenda = 0 and d1.sol_id != so.sol_id and d1.deta_id = d5.deta_idpadre and p1.igr_id = p5.igr_id \n" +
"	) "
                    + ") then 0 else 1 end actividad,\n" +
                "so.sol_id,convert(varchar(12),so.sol_fecha,105) sol_fecha,so.usu_idempleado,so.sol_estado,m.mot_nombremotivo +' ' + CAST( so.sol_descripcion AS varchar(100)) mot_nombremotivo \n" +
                "from tab_solicitud so\n" +
                "join tab_usuariohistorial h on h.ush_id = so.usu_idempleado\n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join tab_cargo c on c.car_id = h.car_id\n" +
                "join tab_sucursales s on s.suc_id = h.suc_id\n" +
                "join tab_motivodescuento m on m.mot_id = so.mtv_id\n" +
                "where so.sol_estado = 1 and so.usu_idsolicitante = " + usu;
//            logger.info("ArraySolicitud: " + sql);
            PreparedStatement st = this.getCn().prepareCall(sql);
            
            rs = st.executeQuery();
//            String[] explodefecha;
            while (rs.next()) {
                Solicitud sol = new Solicitud();
                i++;
                sol.setSol_id(rs.getInt("sol_id"));
//                explodefecha = (rs.getString("sol_fecha")).split("-");
                sol.setSol_fecha(rs.getString("sol_fecha"));
                sol.setNomcargo(rs.getString("nomcargo"));
                sol.setSucursal(rs.getString("sucursal"));
                sol.setNomusuario_empleado(rs.getString("nomusuario_empleado"));
//                sol.setSol_descripcion(rs.getString("sol_descripcion"));
                sol.setSol_estado(rs.getInt("sol_estado"));
                sol.setMotivo(rs.getString("mot_nombremotivo"));
                sol.setExcepcional(rs.getString("excepcional"));
                sol.setActividad(rs.getInt("actividad"));
                sol.setAlfanumeric(i);
                arr.add(sol);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public List<Solicitud> listarSolicitudExcepcional(int usu) throws Exception{
        List<Solicitud> arr = new ArrayList<>();
        ResultSet rs;
        String sql;
        int i = 0;
        try {
            this.Conectar();
            sql = "select c.car_nombre nomcargo,s.suc_direccion sucursal,(u.usu_nombre +' ' + u.usu_apellido) nomusuario_empleado, \n" +
                "so.sol_id,convert(varchar(12),so.sol_fecha,105) sol_fecha,so.usu_idempleado,so.sol_estado,m.mot_nombremotivo,so.sol_descripcion \n" +
                "from tab_solicitud so\n" +
                "join tab_usuariohistorial h on h.ush_id = so.usu_idempleado\n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join tab_cargo c on c.car_id = h.car_id\n" +
                "join tab_sucursales s on s.suc_id = h.suc_id\n" +
                "join tab_motivodescuento m on m.mot_id = so.mtv_id\n" +
                "where so.sol_estado = 1 and so.sol_id = 3 and so.usu_idsolicitante = " + usu;
//            logger.info("ArraySolicitud: " + sql);
            PreparedStatement st = this.getCn().prepareCall(sql);
            rs = st.executeQuery();
            String[] explodefecha;
            while (rs.next()) {
                Solicitud sol = new Solicitud();
                i++;
                sol.setSol_id(rs.getInt("sol_id"));
//                explodefecha = (rs.getString("sol_fecha")).split("-");
                sol.setSol_fecha(rs.getString("sol_fecha"));
                sol.setNomcargo(rs.getString("nomcargo"));
                sol.setSucursal(rs.getString("sucursal"));
                sol.setNomusuario_empleado(rs.getString("nomusuario_empleado"));
                sol.setSol_descripcion(rs.getString("sol_descripcion"));
                sol.setSol_estado(rs.getInt("sol_estado"));
                sol.setMotivo(rs.getString("mot_nombremotivo"));
                sol.setAlfanumeric(i);
                arr.add(sol);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }        
        return arr;
    }
    public void actualizarEstado(Solicitud sol) throws Exception {
//        ResultSet st;
//        int i = 0;
        try {
            this.Conectar();
//            if (acc == 1) {
//                PreparedStatement pstsel = this.getCn().prepareStatement("update tab_solicitud set sol_estado=1 where sol_id="+id);
//                pstsel.setInt(1, sol.getSol_id());
//                pstsel.executeUpdate();
//            } else {
            PreparedStatement pstsel = this.getCn().prepareStatement("update tab_solicitud set usu_idempleado=?,mtv_id=?,sol_descripcion=?,sol_estado=1 where sol_id=?");
            pstsel.setInt(1, sol.getUsu_idempleado());
            pstsel.setInt(2, sol.getMot_id());
            pstsel.setString(3, sol.getSol_descripcion());
            pstsel.setInt(4, sol.getSol_id());
            pstsel.executeUpdate();
//            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }

    }

    public int modificarsolicitudExcepcional(Solicitud men, int ush_id, List<Almacen> lista, int idsol) throws Exception{
        String sql;
        PreparedStatement pst;
        try {
            this.Conectar();
            for (Almacen row : lista) {
                sql = "insert into tab_detalleasignacion values(" + idsol + "," + row.getAl_id() + ",1,NULL,NULL,1,NULL,GETDATE(),(case when " + row.getDeta_id() + " = 0 then null else " + row.getDeta_id() + " end))";
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return idsol;
    }
    
    /**
     *
     * @param men atributos de la solicitud
     * @param ush_id tab_usuariohistorial solicitante
     * @param lista atributos de almacen
     * @param idsol id de la solicitud
     * @return retorna valores para emitir los mensajes de error
     * @throws Exception
     */
    public int modificarsolicitudDao(Solicitud men, int ush_id, List<Almacen> lista, int idsol) throws Exception {
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        int message = 0;

        try {
            this.Conectar();

            //Verificamos si algúna prenda ya fue asignado
            sql = "select case when exists \n"
                    + "(select d.deta_estadoprenda from tab_solicitud s join tab_detalleasignacion d on d.sol_id = s.sol_id\n"
                    + "where s.sol_id = " + idsol + " and d.deta_estadoprenda = 2) then 3 else 0 end mess";
//            logger.info("Verifica prenda asignada: " + sql);
            pst = getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                message = rs.getInt("mess");
            }
            //En caso de que fue asignado, enviamos un mensaje de que no puede modificar la solicitud
            if (message == 3) {
                return message;
            } else {
                //Actualizamos Solicitud
                sql = "UPDATE tab_solicitud set sol_fechaasignacion = GETDATE(), "
                        + "sol_descripcion = '" + men.getSol_descripcion() + "',mtv_id = " + men.getMot_id() + " where sol_id = " + idsol;
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return 0;
    }

        /**
     *
     * @param men atributos de la Solicitud
     * @param ush_id is de tab_usuariohistorial Solicitante
     * @param lista Almacen
     * @return
     * @throws Exception
     */
    public int registrarsolicitudExcepcional(Solicitud men, int ush_id, List<Almacen> lista) throws Exception {
        String sql;
        PreparedStatement pst;
        ResultSet rs;
//        int message = 0;
        int idsol = 0;

        try {
            this.Conectar();
            //Registramos la solicitud
//            logger.info("total solicitados2:"+lista.size());
            if (lista.size() > 0) {
                pst = getCn().prepareStatement("insert into dbo.tab_solicitud values(GETDATE()," + ush_id + ","
                        + "" + men.getUsu_idempleado() + ",NULL,NULL,' - Excepcional. " + men.getSol_descripcion() + "',1,7,(select max(sol_nroform)+ 1 from tab_solicitud),'E')", PreparedStatement.RETURN_GENERATED_KEYS);
                pst.executeUpdate();
                //obtenemos el id de la solicitud creada
                rs = pst.getGeneratedKeys();

                while (rs.next()) {
                    idsol = rs.getInt(1);
                }
            } else {
                return 2;
            }
            //Verificamos todos los registros marcados en la vista
            for (Almacen row : lista) {
                sql = "insert into tab_detalleasignacion values(" + idsol + "," + row.getAl_id() + ",1,NULL,NULL,1,NULL,GETDATE(),(case when " + row.getDeta_id() + " = 0 then null else " + row.getDeta_id() + " end))";
//                logger.info("insertmalo: " + sql);
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();
            }
            //Si la solicitud no tiene ningún registro en detalleasinacion procedemos a eliminar la solicitud
//            pst = getCn().prepareStatement("delete from tab_solicitud where \n" +
//                                            "(select case when not exists \n" +
//                                            "(select * from tab_detalleasignacion d where d.sol_id = "+idsol+") then 1 else 0 end) = 1 and sol_id = "+idsol);
//            pst.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return idsol;
    }

    
    /**
     *
     * @param men atributos de la Solicitud
     * @param ush_id is de tab_usuariohistorial Solicitante
     * @param lista Almacen
     * @return
     * @throws Exception
     */
    public int registrarsolicitudDao(Solicitud men, int ush_id, List<Almacen> lista) throws Exception {
        String sql;
//        int parametroLimite = 0;
//        int asignacionFaltante = 0;
//        int totalCantidadAsignado = 0;
        PreparedStatement pst;
        ResultSet rs;
//        int message = 0;
        int idsol = 0;

        //se valide (Parámetro límite -> es el máximo en cantidad a asignar, 
        //           Asignación faltante -> es la asignación realizada en otra solicitud anterior que sumados con la nueva solicitud de prendas no debe esceder el parámetro límite, 
        //           CAntidad solicitada -> es la cantidad de prendas exigidas por el solicitante )
        try {
            this.Conectar();
            //Registramos la solicitud
//            logger.info("total solicitados2:"+lista.size());
            if (lista.size() > 0) {
                pst = getCn().prepareStatement("insert into dbo.tab_solicitud values(GETDATE()," + ush_id + ","
                        + "" + men.getUsu_idempleado() + ",NULL,NULL,'" + men.getSol_descripcion() + "',1," + men.getMot_id() + ",(select max(sol_nroform)+ 1 from tab_solicitud),null)", PreparedStatement.RETURN_GENERATED_KEYS);
                pst.executeUpdate();
                //obtenemos el id de la solicitud creada
                rs = pst.getGeneratedKeys();

                while (rs.next()) {
                    idsol = rs.getInt(1);
                }
            } else {
                return 2;
            }
            
            
            //Verificamos todos los registros marcados en la vista
            for (Almacen row : lista) {

                //Obtenemos parametroLimite
//                sql = "select t.u_cantidad\n"
//                        + " FROM tab_solicitud s\n"
//                        + " join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n"
//                        + " join tab_areatrabajocargo ar on ar.car_id = h.car_id\n"
//                        + " join tab_almacen a on a.al_id = " + row.getAl_id() + "\n"
//                        + " join tab_producto p on p.pro_id = a.pro_id\n"
//                        + " join tab_ropatrabajo t on (t.tpe_id = ar.tpe_id AND u_item = p.pro_itemcode)\n"
//                        + " where s.sol_id = " + idsol;
////                logger.info("Cant Solicitada: "+sql);
//                pst = this.getCn().prepareStatement(sql);
//                rs = pst.executeQuery();
//                while (rs.next()) {
//                    parametroLimite = rs.getInt("u_cantidad");
//                }
                //Obtenemos asignacionFaltante
//                sql = "select count(p.igr_id) total from tab_detalleasignacion d \n"
//                        + "join tab_solicitud s on d.sol_id = s.sol_id \n"
//                        + "join tab_almacen a on a.al_id = d.al_id\n"
//                        + "join tab_producto p on p.pro_id = a.pro_id\n"
//                        + "where d.deta_estadoprenda = 1 and p.igr_id = (\n"
//                        + "select p.igr_id from tab_detalleasignacion d join tab_almacen a on a.al_id = d.al_id \n"
//                        + "join tab_producto p on p.pro_id = a.pro_id where a.al_id = " + row.getAl_id() + " group by p.igr_id)\n"
//                        + "and s.usu_idempleado = (select usu_idempleado from tab_solicitud where sol_id = " + idsol + ") group by igr_id";
//                logger.info("Faltante" + sql);
//                pst = getCn().prepareStatement(sql);
//                rs = pst.executeQuery();
//                while (rs.next()) {
//                    asignacionFaltante = rs.getInt("total");
//                }
                //Nuevo valor de parametroLimite
//                parametroLimite = parametroLimite - asignacionFaltante;

                //Obtenemos la cantidad a asignar //Verificando esta condición
//                if(row.getCantasignado() < parametroLimite){
//                    totalCantidadAsignado = row.getCantasignado();
//                } else {
//                    totalCantidadAsignado = parametroLimite;
//                }
//                System.out.println(men.getMot_id());
//                logger.info("ParametroLimite: " + parametroLimite);
//                if (parametroLimite > 0) {
//                    totalCantidadAsignado = row.getCantasignado();
                    if(men.getMot_id() == 7){
                        sql = "insert into tab_detalleasignacion values(" + idsol + "," + row.getAl_id() + ",1,NULL,NULL,1,NULL,GETDATE(),NULL)";
                    } else {
                        sql = "insert into tab_detalleasignacion values(" + idsol + "," + row.getAl_id() + ",1,NULL,NULL,1,NULL,GETDATE()," + row.getDeta_id() + ")";
                    }                    
//                    logger.info("insertmalo: " + sql);
                    pst = getCn().prepareStatement(sql);
                    pst.executeUpdate();
//                }

                //realizamos la inserción según la cantidad total a asignar
//                for (int i = 0; i < totalCantidadAsignado; i++ ) {
//                    sql = "insert into tab_detalleasignacion values(" + idsol + "," + row.getAl_id() + ",1,NULL,NULL,1,NULL,GETDATE())";
//                    logger.info("insertmalo: "+sql);
//                    pst = getCn().prepareStatement(sql);
//                    pst.executeUpdate();
//                }
            }
            //Si la solicitud no tiene ningún registro en detalleasinacion procedemos a eliminar la solicitud
            pst = getCn().prepareStatement("delete from tab_solicitud where \n" +
                                            "(select case when not exists \n" +
                                            "(select * from tab_detalleasignacion d where d.sol_id = "+idsol+") then 1 else 0 end) = 1 and sol_id = "+idsol);
            pst.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return 0;
    }

//    public void registrarSolicitudExcepcional(){
//        
//    }
    
    public void eliminarSolicitud(Solicitud men) throws Exception {
        PreparedStatement pst;
        try {
            this.Conectar();
            pst = this.getCn().prepareStatement("update dbo.tab_solicitud set sol_estado=0 where sol_id=? ");
            pst.setInt(1, men.getSol_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public Solicitud leerSolicitud(Solicitud men) throws Exception {
        ResultSet st;
        int usu_id = 0;
        int tip_id = 0;
        int solid = 0;
        Solicitud solicitudmodel = null;
        solicitudBean solben = new solicitudBean();
        String sql;
        try {
            this.Conectar();
            sql = "SELECT\n"
                    + "                    dbo.tab_cargo.car_nombre,\n"
                    + "                    dbo.tab_cargo.car_id,\n"
                    + "                    dbo.tab_sucursales.suc_direccion,\n"
                    + "                    dbo.tab_sucursales.ubi_id,\n"
                    + "                    dbo.tab_solicitud.mtv_id,\n"
                    + "                    dbo.tab_solicitud.sol_id,\n"
                    + "                    dbo.tab_solicitud.sol_fecha,\n"
                    + "                    dbo.tab_solicitud.usu_idsolicitante,\n"
                    + "                    dbo.tab_solicitud.usu_idempleado,\n"
                    + "                    dbo.tab_solicitud.sol_fechaasignacion,\n"
                    + "                    dbo.tab_solicitud.sol_fechadevolucion,\n"
                    + "                    dbo.tab_solicitud.sol_descripcion,\n"
                    + "                    dbo.tab_solicitud.sol_estado\n"
                    + "                    \n"
                    + "                    FROM\n"
                    + "                    dbo.tab_solicitud\n"
                    + "                    INNER JOIN dbo.tab_usuariohistorial ON dbo.tab_usuariohistorial.ush_id = dbo.tab_solicitud.usu_idempleado\n"
                    + "                    INNER JOIN dbo.tab_cargo ON dbo.tab_cargo.car_id = dbo.tab_usuariohistorial.car_id\n"
                    + "                    INNER JOIN dbo.tab_sucursales ON dbo.tab_sucursales.suc_id = dbo.tab_usuariohistorial.suc_id\n"
                    + "                    WHERE\n"
                    + "                    dbo.tab_solicitud.sol_id = "+men.getSol_id()+" AND\n"
                    + "                    dbo.tab_solicitud.sol_estado = 1";
//            logger.info("Leersolicitud: " + sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            st = pst.executeQuery();
            solicitudmodel = new Solicitud();

            while (st.next()) {

                solicitudmodel.setSol_id(st.getInt("sol_id"));
                solid = st.getInt("sol_id");
                solicitudmodel.setUsu_idsolicitante(st.getInt("usu_idsolicitante"));
                solicitudmodel.setUsu_idempleado(st.getInt("usu_idempleado"));
                solicitudmodel.setNomcargo(st.getString("car_nombre"));
                solicitudmodel.setSucursal(st.getString("suc_direccion"));
                solicitudmodel.setMot_id(st.getInt("mtv_id"));
                solicitudmodel.setSol_descripcion(st.getString("sol_descripcion"));
                solicitudmodel.setCar_id(st.getInt("car_id"));
                solicitudmodel.setUbi_id(st.getInt("ubi_id"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return solicitudmodel;
    }

    public Solicitud ObtenerSolicitud(int usu) throws Exception {
        ResultSet st;

        Solicitud sol = new Solicitud();
        int i = 0;
        if (usu == 0) {
            sol.setNomcargo("");
            sol.setSucursal("");
        }
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_cargo.car_nombre,\n"
                    + "dbo.tab_usuariohistorial.car_id,\n"
                    + "dbo.tab_usuariohistorial.ush_id,\n"
                    + "dbo.tab_sucursales.ubi_id,\n"
                    + "(rtrim(dbo.tab_sucursales.suc_direccion)+' '+rtrim(dbo.tab_ubicacion.ubi_nombre)) AS sucursal,\n"
                    + "dbo.tab_areatrabajocargo.tpe_id,\n"
                    + "rtrim(dbo.tab_usuario.usu_sexo) as sexo\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_usuariohistorial\n"
                    + "INNER JOIN dbo.tab_cargo ON dbo.tab_usuariohistorial.car_id = dbo.tab_cargo.car_id\n"
                    + "INNER JOIN dbo.tab_sucursales ON dbo.tab_usuariohistorial.suc_id = dbo.tab_sucursales.suc_id\n"
                    + "INNER JOIN dbo.tab_ubicacion ON dbo.tab_sucursales.ubi_id = dbo.tab_ubicacion.ubi_id\n"
                    + "INNER JOIN dbo.tab_areatrabajocargo ON dbo.tab_cargo.car_id = dbo.tab_areatrabajocargo.car_id\n"
                    + "INNER JOIN dbo.tab_usuario ON dbo.tab_usuario.usu_id = dbo.tab_usuariohistorial.usu_id\n"
                    + "WHERE\n"
                    + "dbo.tab_usuariohistorial.ush_id =" + usu);
            st = pst.executeQuery();
            while (st.next()) {

                sol.setNomcargo(st.getString("car_nombre"));
                sol.setSucursal(st.getString("sucursal"));
                sol.setUsu_idempleado(st.getInt("ush_id"));
                sol.setCar_id(st.getInt("car_id"));
                sol.setUbi_id(st.getInt("ubi_id"));
                sol.setTpe_id(st.getInt("tpe_id"));
                sol.setUsu_sexo(st.getString("sexo"));
                i++;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }

        return sol;
    }

    public void borrarSolicitudDao(Solicitud del) throws Exception {
        ResultSet st;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from tab_detalleasignacion where sol_id=? and deta_estadoprenda=1");
            pst.setInt(1, del.getSol_id());
            st = pst.executeQuery();
            while (st.next()) {
                PreparedStatement pstdel = this.getCn().prepareCall("delete from tab_detalleasignacion where deta_id=" + st.getInt("deta_id") + " and deta_estadoprenda=1");
                pstdel.executeUpdate();
            }
            PreparedStatement pstdelsol = this.getCn().prepareStatement("delete from tab_solicitud where sol_id=? and sol_estado=1");
            pstdelsol.setInt(1, del.getSol_id());
            pstdelsol.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
    
    public void borrarDetalleExcepcional(int detaid) throws Exception{
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("delete from tab_detalleasignacion where deta_id = " + detaid);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
}
