package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Motivodescuento;
//import org.apache.log4j.Logger;

public class motivosolicitudDAO extends DAO {
//    private static final Logger logger = Logger.getLogger(almacenDAO.class);
    public ArrayList<Motivodescuento> listarSelectMotivoSolicitud(int rol_id) throws Exception {
        ResultSet st;
        ArrayList<Motivodescuento> arr = new ArrayList<>();
        Motivodescuento mt = null;
        String sql;
        try {
            this.Conectar();
            sql = "select * from tab_motivodescuento where mot_estado=1 and mot_id in (1,2,5,7)";
//            logger.info("Motivo seleccion: " + sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);
            st = pst.executeQuery();
            while (st.next()) {
                mt = new Motivodescuento();
                mt.setMot_id(st.getInt("mot_id"));
                mt.setMot_nombremotivo(st.getString("mot_nombremotivo"));
                arr.add(mt);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

}
