/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Devolucion;
//import org.apache.log4j.Logger;

public class devolucionDAO extends DAO {
//    private static final Logger logger = Logger.getLogger(almacenDAO.class);
    public List<Devolucion> listarDevolucionDao(String palabra) throws Exception {

        List<Devolucion> arr = new ArrayList<>();
        ResultSet rs;
        int i = 0;
//        int u = 0;
        try {
            this.Conectar();
            String sql;
            sql = "with devoluciones (sol_id,usu_idempleado,empleado,al_id,prenda,deta_id,fechaasignacion,deta_estadoprenda,deta_fechatentativa,motivo,id_motivo,boton,descripcion) AS (\n" +
                "select s.sol_id,s.usu_idempleado,u.usu_nombre+' '+u.usu_apellido+' ('+c.car_nombre+')' empleado,d.al_id,p.pro_itemname+' '+p.pro_material prenda,d.deta_id,\n" +
                "CONVERT(VARCHAR(10), d.deta_fechaasignacion, 105) fechaasignacion,d.deta_estadoprenda,CONVERT(VARCHAR(10), d.deta_fechatentativa, 105) deta_fechatentativa,\n" +
                "	case when (select s1.mtv_id from tab_solicitud s1 join tab_detalleasignacion d1 on d1.sol_id = s1.sol_id where deta_idpadre = d.deta_id) > 0\n" +
                "	then (select s1.mtv_id from tab_solicitud s1 join tab_detalleasignacion d1 on d1.sol_id = s1.sol_id where deta_idpadre = d.deta_id)\n" +
                "	else s.mtv_id\n" +
                "	end mtv_id,1,\n" +
                "	case mtv_id when 6 then 1 else\n" +
"	(case when (select s1.mtv_id from tab_solicitud s1 join tab_detalleasignacion d1 on d1.sol_id = s1.sol_id where deta_idpadre = d.deta_id) > 0\n" +
"	then 1\n" +
"	else 0 end ) end\n" +
"	boton,s.sol_descripcion\n" +
                "from tab_solicitud s\n" +
                "join tab_detalleasignacion d on d.sol_id = s.sol_id\n" +
                "join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join tab_cargo c on c.car_id = h.car_id\n" +
                "join tab_almacen a on a.al_id = d.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id\n" +
                "AND d.deta_estadoprenda = 2\n" +
                ")\n" +
                "select sol_id,usu_idempleado,empleado,al_id,prenda,deta_id,fechaasignacion,deta_estadoprenda,deta_fechatentativa,m.mot_nombremotivo,m.mot_id,boton,descripcion from devoluciones \n" +
                "join tab_motivodescuento m on m.mot_id = devoluciones.motivo\n" +
                "where empleado like '%"+palabra+"%' and boton != 0";
//            logger.info("Devolución: "+sql);
            PreparedStatement st = this.getCn().prepareCall(sql);
            rs = st.executeQuery();
//            String[] fecha1;//asignado
//            String[] fecha2;//tentativa
            while (rs.next()) {
                Devolucion dev = new Devolucion();
                dev.setSol_id(rs.getInt("sol_id"));
                dev.setNomempleado(rs.getString("empleado"));
                dev.setNomitem(rs.getString("prenda"));
                dev.setUsu_idempleado(rs.getInt("usu_idempleado"));
                dev.setAl_id(rs.getInt("al_id"));
                dev.setAlfanumeric(++i);
                dev.setDeta_id(rs.getInt("deta_id"));
                dev.setDeta_estadoprenda(rs.getInt("deta_estadoprenda"));
                dev.setSol_fechaasignacion(rs.getString("fechaasignacion"));
//                dev.setCandidadsolicitada(rs.getInt("cantidad"));
                dev.setDeta_fechatentativa(rs.getString("deta_fechatentativa"));
                dev.setMotivo(rs.getString("mot_nombremotivo"));
                dev.setMot_id(rs.getInt("mot_id"));
                dev.setStatusBoton(rs.getInt("boton"));
                arr.add(dev);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    /**
     * 
     * @param de atributos de toda devolucines
     * @return Lista de los campos importantes de devoluciones
     * @throws Exception 
     */
    public Devolucion leerIdDao(Devolucion de) throws Exception {
        ResultSet rs;
        Devolucion dev = null;
//        logger.info("sisissii");
        try {
            this.Conectar();
//            String sql = "with devolucion (deta_id,sol_id,usu_idempleado,sol_fechaasignacion,pro_genero,pro_talla,car_nombre,usu_nombre,usu_apellido,pro_itemname,\n" +
//                        "pro_material,al_id,pro_itemcode,al_cantidad,al_costo,pro_color,pro_logo,deta_cantidadsolicitada,motivo,idmotivo) AS (\n" +
//                        "SELECT d.deta_id,s.sol_id,s.usu_idempleado,CONVERT(VARCHAR(10), hd.fecha, 105) sol_fechaasignacion,p.pro_genero,p.pro_talla,\n" +
//                        "c.car_nombre,u.usu_nombre,u.usu_apellido,p.pro_itemname,\n" +
//                        "p.pro_material,a.al_id,p.pro_itemcode,a.al_cantidad,\n" +
//                        "a.al_costo,p.pro_color,p.pro_logo,d.deta_cantidadsolicitada,\n" +
//                        "(select ss.mtv_id from tab_solicitud ss join tab_detalleasignacion dd on dd.sol_id = ss.sol_id\n" +
//                        "		where dd.deta_id = (select MAX(dd.deta_id) restriccion from tab_solicitud ss \n" +
//                        "	join tab_detalleasignacion dd on dd.sol_id = ss.sol_id where ss.usu_idempleado = s.usu_idempleado\n" +
//                        "	AND dd.deta_estadoprenda <> 0 AND dd.al_id = d.al_id group by dd.al_id)) motivo,1\n" +
//                        "FROM dbo.tab_almacen a\n" +
//                        "JOIN  dbo.tab_detalleasignacion d ON a.al_id = d.al_id\n" +
//                        "JOIN dbo.tab_solicitud s ON s.sol_id = d.sol_id\n" +
//                        "JOIN dbo.tab_usuariohistorial h ON s.usu_idempleado = h.ush_id\n" +
//                        "JOIN dbo.tab_cargo c ON c.car_id = h.car_id\n" +
//                        "JOIN dbo.tab_usuario u ON u.usu_id = h.usu_id\n" +
//                        "JOIN dbo.tab_producto p ON p.pro_id = a.pro_id\n" +
//                        "JOIN dbo.tab_histoasignadevuelve hd on hd.deta_id = d.deta_id\n" +
//                        "WHERE a.al_estado = 1 AND h.ush_estado = 1 AND a.al_id ="+ de.getAl_id()+" and d.deta_id= "+de.getDeta_id()+" AND hd.tipo = 'A'\n" +
//                        ") \n" +
//                        "select deta_id,sol_id,usu_idempleado,sol_fechaasignacion,pro_genero,pro_talla,car_nombre,usu_nombre,usu_apellido,\n" +
//                        "pro_itemname,pro_material,al_id,pro_itemcode,al_cantidad,al_costo,pro_color,pro_logo,deta_cantidadsolicitada,m.mot_nombremotivo,m.mot_id\n" +
//                        "from devolucion \n" +
//                        "join tab_motivodescuento m on m.mot_id = devolucion.motivo";

            String sql = "with devolucion (deta_id,sol_id,usu_idempleado,sol_fechaasignacion,pro_genero,pro_talla,car_nombre,usu_nombre,usu_apellido,pro_itemname,\n" +
                    "pro_material,al_id,pro_itemcode,al_cantidad,al_costo,pro_color,pro_logo,deta_cantidadsolicitada,motivo,idmotivo,descripcion) AS (\n" +
                    "SELECT d.deta_id,s.sol_id,s.usu_idempleado,CONVERT(VARCHAR(10),d.deta_fechaasignacion, 105) sol_fechaasignacion,p.pro_genero,p.pro_talla,\n" +
                    "c.car_nombre,u.usu_nombre,u.usu_apellido,p.pro_itemname,\n" +
                    "p.pro_material,a.al_id,p.pro_itemcode,a.al_cantidad,\n" +
                    "a.al_costo,p.pro_color,p.pro_logo,d.deta_cantidadsolicitada,\n" +
                    "(select ss.mtv_id from tab_solicitud ss join tab_detalleasignacion dd on dd.sol_id = ss.sol_id\n" +
                    "		where dd.deta_id = (select MAX(dd.deta_id) restriccion from tab_solicitud ss \n" +
                    "	join tab_detalleasignacion dd on dd.sol_id = ss.sol_id where ss.usu_idempleado = s.usu_idempleado\n" +
                    "	AND dd.deta_estadoprenda <> 0 AND dd.al_id = d.al_id group by dd.al_id)) motivo,1,s.sol_descripcion\n" +
                    "FROM dbo.tab_almacen a\n" +    
                    "JOIN  dbo.tab_detalleasignacion d ON a.al_id = d.al_id\n" +
                    "JOIN dbo.tab_solicitud s ON s.sol_id = d.sol_id\n" +
                    "JOIN dbo.tab_usuariohistorial h ON s.usu_idempleado = h.ush_id\n" +
                    "JOIN dbo.tab_cargo c ON c.car_id = h.car_id\n" +
                    "JOIN dbo.tab_usuario u ON u.usu_id = h.usu_id\n" +
                    "JOIN dbo.tab_producto p ON p.pro_id = a.pro_id\n" +
                    "WHERE a.al_estado = 1 AND a.al_id ="+ de.getAl_id()+" and d.deta_id= "+de.getDeta_id()+"\n" +
                    ") \n" +
                    "select deta_id,sol_id,usu_idempleado,sol_fechaasignacion,pro_genero,pro_talla,car_nombre,usu_nombre,usu_apellido,\n" +
                    "pro_itemname,pro_material,al_id,pro_itemcode,al_cantidad,al_costo,pro_color,pro_logo,deta_cantidadsolicitada,m.mot_nombremotivo,m.mot_id,descripcion\n" +
                    "from devolucion \n" +
                    "join tab_motivodescuento m on m.mot_id = devolucion.motivo";
            
//            logger.info("Devolucion recibir: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                dev = new Devolucion();
                dev.setSol_id(rs.getInt("sol_id"));
                dev.setNomempleado(rs.getString("usu_nombre") + " " + rs.getString("usu_apellido"));
                dev.setNomitem(rs.getString("pro_itemname") + " " + rs.getString("pro_color") + " " + rs.getString("pro_material") + " Talla(" + rs.getString("pro_talla") + ")");
                dev.setPro_itemname(rs.getString("pro_itemname"));
                dev.setUsu_idempleado(rs.getInt("usu_idempleado"));
                dev.setPro_genero(rs.getString("pro_genero"));
                dev.setPro_talla(rs.getString("pro_talla"));
                dev.setCar_nombre(rs.getString("car_nombre"));
//                dev.setCar_id(rs.getInt("car_id"));
                dev.setPro_descripcion(rs.getString("descripcion"));
                dev.setPro_material(rs.getString("pro_material"));
                dev.setPro_itemcode(rs.getString("pro_itemcode"));
//                dev.setPro_nomenclatura(rs.getString("pro_nomenclatura"));
                dev.setAl_cantidad(rs.getInt("al_cantidad"));
//                dev.setPro_vidautil(rs.getString("pro_vidautil"));
//                dev.setAl_costo(rs.getFloat("al_costo"));
                dev.setPro_color(rs.getString("pro_color"));
                dev.setPro_logo(rs.getString("pro_logo"));
                dev.setDeta_id(rs.getInt("deta_id"));
                dev.setAl_id(rs.getInt("al_id"));
//                dev.setDeta_estadoprenda(rs.getInt("deta_estadoprenda"));
                dev.setCandidadsolicitada(rs.getInt("deta_cantidadsolicitada"));
                dev.setSol_fechaasignacion(rs.getString("sol_fechaasignacion"));
                dev.setMotivo(rs.getString("mot_nombremotivo"));
                dev.setMot_id(rs.getInt("mot_id"));
                dev.setEpr_id(1);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return dev;

    }

    /**
     * 
     * @param dev Atributo de devolucion
     * @throws Exception 
     * @description Guarda en las tablas corespondientes todas las devoluciones y calcula las multas
     */
    public void guardarDevolucionDao(Devolucion dev,int usu_id,int solidant) throws Exception {
        String sqlinsert,sql;
        try {
            this.Conectar();
            //Genera el cálculo de descuento de items o prendas devueltas
            sqlinsert = "insert into tab_aplicadescuento "
                    + "select d.deta_id,"+dev.getMot_id()+",dt.devuelto, \n" +
                    "	(select sum(total)\n" +
                    "	from tab_aplicatipodescuento at\n" +
                    "	join \n" +
                    "	(select \n" +
                    "	td.tde_id,\n" +
                    "	case when td.tde_porcentaje is null \n" +
                    "		then \n" +
                    "			case \n" +
                    "				when (p.pro_vidautil > DATEDIFF(MONTH,d.deta_fechaasignacion,getdate()))\n" +
                    "				then ((a.al_costo + (case when i1.iva_porcentaje is null then 0 else i1.iva_porcentaje end * a2.al_costo))/p.pro_vidautil) * (p.pro_vidautil - (cast((select DateDiff(dd,d.deta_fechaasignacion,DateAdd(dd,0,GETDATE()))) as float)/30)) \n" +
                    "				else 0\n" +
                    "			end \n" +
                    "		else td.tde_porcentaje * (a.al_costo + (case when i1.iva_porcentaje is null then 0 else i1.iva_porcentaje end * a2.al_costo))\n" +
                    "	end total\n" +
                    "	from tab_tipodescuento td join tab_almacen a on a.al_id = a2.al_id\n" +
                    "	join tab_producto p on p.pro_id = a.pro_id) cd\n" +
                    "	on cd.tde_id = at.tde_id\n" +
                    "	where at.rde_id = (select rde_id from tab_reglasdescuento where mot_id = "+dev.getMot_id()+" and ede_id = dt.devuelto) GROUP BY at.rde_id)  total,"+solidant+"\n" +
                    "from tab_devoluciontemp dt \n" +
                    "left join tab_iva i1 on i1.iva_id = (select ad2.iva_id from tab_reglasdescuento r2 join tab_aplicatipodescuento ad2 on ad2.rde_id = r2.rde_id\n" +
                    "	where r2.mot_id = "+dev.getMot_id()+" and r2.ede_id = dt.devuelto group by ad2.iva_id)\n" +
                    "join tab_detalleasignacion d on d.deta_id = dt.deta_id\n" +
                    "join tab_almacen a2 on a2.al_id = d.al_id"; 
//            logger.info("Insert Descuento: "+sqlinsert);
            System.out.println();
            PreparedStatement pstinsert = this.getCn().prepareStatement(sqlinsert);
            pstinsert.executeUpdate();

            //Actualizamos el estado de tab_detalleasignacion "0" (Cero estado devuelto)
            sql = "UPDATE\n" +
                    "    tab_detalleasignacion \n" +
                    "SET\n" +
                    "    deta_estadoprenda = 0,\n" +
                    "    deta_fechadevolucion = GETDATE()\n" +
                    "FROM\n" +
                    "    tab_detalleasignacion d\n" +
                    "    JOIN tab_devoluciontemp dt\n" +
                    "        ON dt.deta_id = d.deta_id\n" +
                    "WHERE \n" +
                    "	dt.almacen_id = "+usu_id;
//            PreparedStatement pstupdeta = this.getCn().prepareStatement("update tab_detalleasignacion set deta_estadoprenda=0,deta_fechadevolucion=getdate() where deta_id="+dev.getDeta_id());
            PreparedStatement pstupdeta = this.getCn().prepareStatement(sql);
            pstupdeta.executeUpdate();
//            sql = "UPDATE d\n" +
//                    "  SET d.deta_estadoprenda = 0\n" +
//                    "  FROM tab_detalleasignacion d\n" +
//                    "  JOIN tab_solicitud s\n" +
//                    "  ON d.sol_id = s.sol_id\n" +
//                    "  WHERE d.deta_idpadre = "+dev.getDeta_id()+" and s.sol_estado = 0;";
            sql = "UPDATE\n" +
                    "    tab_detalleasignacion \n" +
                    "SET\n" +
                    "    deta_estadoprenda = 0 \n" +
                    "FROM\n" +
                    "    tab_detalleasignacion d\n" +
                    "    JOIN tab_devoluciontemp dt\n" +
                    "        ON dt.deta_id = d.deta_idpadre \n" +
                    "    JOIN tab_solicitud s on s.sol_id = d.sol_id\n" +
                    "WHERE \n" +
                    "	dt.almacen_id = "+usu_id+" and s.mtv_id in (3,4,6)";
            PreparedStatement pstupdeta2 = this.getCn().prepareCall(sql);
            pstupdeta2.executeUpdate();
            
            //Insertamos en el Log
            sql = "insert into tab_histoasignadevuelve select dt.deta_id,GETDATE(),'D',"+usu_id+" from tab_devoluciontemp dt";
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
    public void guardaDevolucionTemporal (Devolucion dev, int almacen_id) throws Exception{
        String sql;
        try {
            this.Conectar();
            sql = "insert into tab_devoluciontemp values ("+dev.getSol_id()+","+dev.getDeta_id()+","+almacen_id+","+dev.getEpr_id()+")";
//            logger.info("insert bbb: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }   

    public void deleteDevolucionTemporal (int almacen_id) throws Exception{
        String sql;
        try {
            this.Conectar();
            sql = "delete from tab_devoluciontemp where almacen_id = "+almacen_id;
//            logger.info("borrar temp: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
            
    public List<Devolucion> listdev(String pal) throws Exception{
        List<Devolucion> arr = new ArrayList<>();
        ResultSet rs;
        String sql;
        int i = 0;
        try {
            this.Conectar();
            sql = "select s.sol_id,(u.usu_nombre+' ' +u.usu_apellido+' ('+c.car_nombre+')') empleado,\n" +
                "(u2.usu_nombre+' ' +u2.usu_apellido+' ('+c2.car_nombre+')') solicitante,m.mot_nombremotivo,CONVERT(VARCHAR(10), s.sol_fecha, 105) sol_fecha\n" +
                "from tab_solicitud s \n" +
                "join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join tab_cargo c on c.car_id = h.car_id\n" +
                "join tab_usuariohistorial h2 on h2.ush_id = s.usu_idsolicitante\n" +
                "join tab_usuario u2 on u2.usu_id = h2.usu_id\n" +
                "join tab_cargo c2 on c2.car_id = h2.car_id\n" +
                "join tab_motivodescuento m on m.mot_id = s.mtv_id\n" +
                "where s.sol_estado = 1 and s.mtv_id != 7 and ("
                    + "select sum(d2.deta_estadoprenda)\n" +
                "	from tab_solicitud s2 \n" +
                "	join tab_detalleasignacion d on d.sol_id = s2.sol_id\n" +
                "	left join tab_detalleasignacion d2 on d.deta_idpadre = d2.deta_id \n" +
                "	where s2.sol_id = s.sol_id\n" +
                ") > 0 and ( u.usu_nombre like '%"+pal+"%' or u.usu_apellido like '%"+pal+"%' or u2.usu_nombre like '%"+pal+"%' or u2.usu_apellido like '%"+pal+"%' ) "
                    + " order by s.sol_fecha desc";
//            logger.info("lista devolucion: "+sql);
            PreparedStatement st = this.getCn().prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                Devolucion dev = new Devolucion();
                dev.setSol_id(rs.getInt("sol_id"));
                dev.setNomempleado(rs.getString("empleado"));
                dev.setNomsolicitante(rs.getString("solicitante"));
                dev.setMotivo(rs.getString("mot_nombremotivo"));
                dev.setSol_fecha(rs.getString("sol_fecha"));
                dev.setAlfanumeric(++i);
                arr.add(dev);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
    public Devolucion listardevoluciondetalle(int id) throws Exception {
        Devolucion dev = null;
//        List<Devolucion> arr = new ArrayList<>();
        String sql;
        ResultSet rs;
        try {
            this.Conectar();

            sql = "SELECT (RTRIM(u.usu_nombre) + ' ' +RTRIM(u.usu_apellido) ) nomusuario_empleado,c.car_nombre nomcargo,(RTRIM(su.suc_direccion) +' '+RTRIM(b.ubi_nombre)) sucursal,\n" +
                "(RTRIM(u2.usu_nombre) + ' ' +RTRIM(u2.usu_apellido) ) nomusuario_solicitante,md.mot_id,md.mot_nombremotivo,\n" +
                "s.sol_id,CONVERT(VARCHAR(10), s.sol_fecha, 105) sol_fecha,s.usu_idsolicitante,s.usu_idempleado,b.ubi_id,b.ubi_nombre,c.car_id,dv.division, \n"+
                "u.usu_nombre,u.usu_apellido,case when u.usu_sexo = 'F' then 'FEMENINO' else 'MASCULINO' end usu_sexo,u.usu_ci,s.sol_nroform \n" +
                "FROM tab_solicitud s \n" +
                "JOIN tab_usuariohistorial h ON h.ush_id = s.usu_idempleado \n" +
                "JOIN tab_cargo c on c.car_id = h.car_id \n" +
                "JOIN tab_sucursales su on su.suc_id = h.suc_id \n" +
                "JOIN tab_usuario u on u.usu_id = h.usu_id \n" +
                "JOIN tab_ubicacion b on b.ubi_id = su.ubi_id \n" +
                "JOIN tab_usuariohistorial h2 ON h2.ush_id = s.usu_idsolicitante\n" +
                "JOIN tab_usuario u2 on u2.usu_id = h2.usu_id \n" +
                "JOIN tab_motivodescuento md on md.mot_id = s.mtv_id \n" +
                "JOIN tab_division dv on h.division = dv.div_id \n" +
                "where s.sol_id = "+id;
//                logger.info("Lista Cabecera devolucion: "+sql);
            PreparedStatement st = this.getCn().prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                dev = new Devolucion();
                dev.setSol_id(rs.getInt("sol_id"));
                dev.setNomsolicitante(rs.getString("nomusuario_solicitante"));
                dev.setNomempleado(rs.getString("nomusuario_empleado"));
                dev.setCar_nombre(rs.getString("nomcargo"));
                dev.setUsu_nombre(rs.getString("usu_nombre"));
                dev.setUsu_apellido(rs.getString("usu_apellido"));
                dev.setSucursal(rs.getString("sucursal"));
                dev.setUsu_idempleado(rs.getInt("usu_idempleado"));
                dev.setCar_id(rs.getInt("car_id"));
                dev.setSol_fecha(rs.getString("sol_fecha"));
                dev.setMotivo(rs.getString("mot_nombremotivo"));
                dev.setMot_id(rs.getInt("mot_id"));
                dev.setPro_genero(rs.getString("usu_sexo"));
                dev.setUsu_ci(rs.getString("usu_ci"));
                dev.setUbi_nombre(rs.getString("ubi_nombre"));
                dev.setDivision(rs.getString("division"));
                dev.setNro_form(rs.getInt("sol_nroform"));
                
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return dev;
    }   

    public ArrayList<Devolucion> listarDevolucionOneRow(int idsol, int usu_id, Devolucion dev,int ush_id) throws Exception{
        ArrayList arr = new ArrayList();
        ResultSet rs;
        String sql;
        int i = 0;
        try {
            this.Conectar();
            sql = "select s.sol_id,p.igr_id,d.deta_id,CONVERT(VARCHAR(10), d.deta_fechaasignacion, 105) fechaasignacion,\n" +
                "CONVERT(VARCHAR(10), d.deta_fechatentativa, 105) deta_fechatentativa,d.deta_estadoprenda,\n" +
                "d.al_id,p.pro_itemname+' '+p.pro_material prenda,s.sol_descripcion,p.pro_itemcode, \n" +
                "case when dt.devuelto = 1 then 'SI' when dt.devuelto = 2 then 'NO' end devuelto, \n"+
                "case when exists (select * from tab_devoluciontemp where sol_id = s.sol_id and deta_id = d.deta_id and almacen_id = " + ush_id +") then 0 else 1 end boton\n" +
                "from tab_solicitud s join tab_detalleasignacion d on s.sol_id = d.sol_id \n" +
                "join tab_almacen a on a.al_id = d.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id \n"+
                "left join tab_devoluciontemp dt on dt.deta_id = d.deta_id and dt.almacen_id = " + ush_id + 
                " where s.usu_idempleado = "+usu_id+" and d.deta_estadoprenda = 2 and p.igr_id in (\n" +
                "	select p2.igr_id from tab_solicitud s2 join tab_detalleasignacion d2 on s2.sol_id = d2.sol_id \n" +
                "	join tab_almacen a2 on a2.al_id = d2.al_id\n" +
                "	join tab_producto p2 on p2.pro_id = a2.pro_id\n" +
                "	where s2.sol_id = "+idsol+" and d2.deta_estadoprenda = 1 and d2.deta_idpadre = d.deta_id \n" +
                ")";
//            logger.info("lista dev: " + sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Devolucion al = new Devolucion();
                al.setAlfanumeric(++i);
                al.setDeta_id(rs.getInt("deta_id"));
                al.setSol_fechaasignacion(rs.getString("fechaasignacion"));
                al.setDeta_fechatentativa(rs.getString("deta_fechatentativa"));
                al.setDeta_estadoprenda(rs.getInt("deta_estadoprenda"));
                al.setAl_id(rs.getInt("al_id"));
                al.setPro_itemcode(rs.getString("pro_itemcode"));
                al.setPro_itemname(rs.getString("prenda"));
                al.setStatusBoton(rs.getInt("boton"));
                al.setDevuelto(rs.getString("devuelto"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();            
        }
        return arr;
    }    

    public ArrayList<Devolucion> listarDevolucionTemp(int sol_id, int usu_id, Devolucion dev) throws Exception{
        ArrayList arr = new ArrayList();
        ResultSet rs;
        String sql;
        int i = 0;
        try {
            this.Conectar();
            sql = "select s.sol_id,p.igr_id,d.deta_id,CONVERT(VARCHAR(10), d.deta_fechaasignacion, 105) fechaasignacion,\n" +
                "CONVERT(VARCHAR(10), d.deta_fechatentativa, 105) deta_fechatentativa,d.deta_estadoprenda,\n" +
                "d.al_id,p.pro_itemname+' '+p.pro_material prenda,s.sol_descripcion,p.pro_itemcode \n" +
                "from tab_solicitud s join tab_detalleasignacion d on s.sol_id = d.sol_id \n" +
                "join tab_almacen a on a.al_id = d.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id\n" +
                "where s.usu_idempleado = 7 and d.deta_estadoprenda = 2 and p.igr_id in (\n" +
                "	select p.igr_id from tab_solicitud s join tab_detalleasignacion d on s.sol_id = d.sol_id \n" +
                "	join tab_almacen a on a.al_id = d.al_id\n" +
                "	join tab_producto p on p.pro_id = a.pro_id\n" +
                "	where s.sol_id = "+ sol_id +" and d.deta_estadoprenda = 1\n" +
                ") and d.deta_id in (select deta_id from tab_devoluciontemp where sol_id = "+ dev.getSol_id() +" and almacen_id = "+usu_id+")";
//            logger.info("seleccion dev 5: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Devolucion al = new Devolucion();
                al.setAlfanumeric(++i);
                al.setDeta_id(rs.getInt("deta_id"));
                al.setSol_fechaasignacion(rs.getString("fechaasignacion"));
                al.setDeta_fechatentativa(rs.getString("deta_fechatentativa"));
                al.setDeta_estadoprenda(rs.getInt("deta_estadoprenda"));
                al.setAl_id(rs.getInt("al_id"));
                al.setPro_itemcode(rs.getString("pro_itemcode"));
                al.setPro_itemname(rs.getString("prenda"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();            
        }
        return arr;
    }      

    public ArrayList<Devolucion> generaReporteReimpresion(int sol_id) throws Exception { 
        ArrayList arr = new ArrayList();
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        int i = 0;
        try {
            this.Conectar();
            sql = "select s.sol_id,d.deta_id,p.pro_itemcode,p.pro_itemname,convert(varchar,d.deta_fechaasignacion,105) fechaasignacion,\n" +
                    "case when dt.devuelto = 1 then 'SI' when dt.devuelto = 2 then 'NO' end devuelto,\n" +
                    "case when (p.pro_vidautil > DATEDIFF(MONTH,d.deta_fechaasignacion,getdate())) \n" +
                    "then p.pro_vidautil - (cast((select DateDiff(dd,d.deta_fechaasignacion,DateAdd(dd,0,GETDATE()))) as float)/30)\n" +
                    "else 0 end tiempovida,\n" +
                    "ad.apd_totaldescuento\n" +
                    "from tab_devoluciontemp dt \n" +
                    "join tab_detalleasignacion d on d.deta_id = dt.deta_id\n" +
                    "join tab_solicitud s on s.sol_id = d.sol_id\n" +
                    "join tab_almacen a on a.al_id = d.al_id\n" +
                    "join tab_producto p on p.pro_id = a.pro_id\n" +
                    "join tab_aplicadescuento ad on ad.deta_id = dt.deta_id\n" +
                    "where s.sol_id = "+sol_id;
         
//            logger.info("reporte devolucion: "+sql);
            System.err.println(sql);
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Devolucion dev = new Devolucion();
                dev.setAlfanumeric(++i);
                dev.setPro_itemcode(rs.getString("pro_itemcode"));
                dev.setPro_itemname(rs.getString("pro_itemname"));
                dev.setSol_fechaasignacion(rs.getString("fechaasignacion"));
                dev.setDevuelto(rs.getString("devuelto"));
                dev.setVidautil(rs.getFloat("tiempovida"));
                dev.setDescuento(rs.getFloat("apd_totaldescuento"));
                arr.add(dev);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();            
        }
        return arr;
    }
    
    public ArrayList<Devolucion> generaReporte(int ush_id) throws Exception { 
        ArrayList arr = new ArrayList();
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        int i = 0;
        try {
            this.Conectar();
            sql = "select s.sol_id,d.deta_id,p.pro_itemcode,p.pro_itemname,convert(varchar,d.deta_fechaasignacion,105) fechaasignacion,\n" +
                    "case when dt.devuelto = 1 then 'SI' when dt.devuelto = 2 then 'NO' end devuelto,\n" +
                    "case when (p.pro_vidautil > DATEDIFF(MONTH,d.deta_fechaasignacion,getdate())) \n" +
                    "then p.pro_vidautil - (cast((select DateDiff(dd,d.deta_fechaasignacion,DateAdd(dd,0,GETDATE()))) as float)/30)\n" +
                    "else 0 end tiempovida,\n" +
                    "ad.apd_totaldescuento\n" +
                    "from tab_devoluciontemp dt \n" +
                    "join tab_detalleasignacion d on d.deta_id = dt.deta_id\n" +
                    "join tab_solicitud s on s.sol_id = d.sol_id\n" +
                    "join tab_almacen a on a.al_id = d.al_id\n" +
                    "join tab_producto p on p.pro_id = a.pro_id\n" +
                    "join tab_aplicadescuento ad on ad.deta_id = dt.deta_id\n" +
                    "where dt.almacen_id = "+ush_id;
         
//            logger.info("reporte devolucion: "+sql);
            System.err.println(sql);
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Devolucion dev = new Devolucion();
                dev.setAlfanumeric(++i);
                dev.setPro_itemcode(rs.getString("pro_itemcode"));
                dev.setPro_itemname(rs.getString("pro_itemname"));
                dev.setSol_fechaasignacion(rs.getString("fechaasignacion"));
                dev.setDevuelto(rs.getString("devuelto"));
                dev.setVidautil(rs.getFloat("tiempovida"));
                dev.setDescuento(rs.getFloat("apd_totaldescuento"));
                arr.add(dev);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();            
        }
        return arr;
    }
    
    public void updateSolicitud(int usu_id,int sol_id) throws Exception{        
        String sql;
        try {
            this.Conectar();
            sql = "UPDATE tab_solicitud\n" +
                "SET sol_estado = 0, sol_fechadevolucion = GETDATE()\n" +
                "WHERE sol_id = "+sol_id+" and \n" +
                "(select case when \n" +
                "(select sum(d.deta_estadoprenda)\n" +
                "from tab_solicitud s join tab_detalleasignacion d on s.sol_id = d.sol_id \n" +
                "join tab_almacen a on a.al_id = d.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id \n" +
                "where s.usu_idempleado = "+usu_id+" and p.igr_id in (\n" +
                "	select p2.igr_id from tab_solicitud s2 join tab_detalleasignacion d2 on s2.sol_id = d2.sol_id \n" +
                "	join tab_almacen a2 on a2.al_id = d2.al_id\n" +
                "	join tab_producto p2 on p2.pro_id = a2.pro_id\n" +
                "	where s2.sol_id = "+sol_id+" and d2.deta_estadoprenda = 1 and d2.deta_idpadre = d.deta_id\n" +
                ") ) > 0 then 'si' else 'no' end ) = 'no' and mtv_id in (3,4,6)";
//            logger.info("update Sol: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
}
