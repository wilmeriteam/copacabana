package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Sucursal;

/**
 *
 * @author Wilmer
 */
public class sucursalDAO extends DAO {
    public List<Sucursal> listSucursal() throws Exception{
        PreparedStatement pst;
        ResultSet st;
        List<Sucursal> arr = new ArrayList<>();
        String sql;
        try {
            this.Conectar();
            sql = "select suc_id,suc_direccion,suc_estado,ubi_id from tab_sucursales";
            pst = getCn().prepareStatement(sql);
            st = pst.executeQuery();
            while(st.next()){
                Sucursal suc = new Sucursal();
                suc.setSuc_id(st.getInt("suc_id"));
                suc.setSuc_direccion(st.getString("suc_direccion"));
                suc.setSucestado(st.getInt("suc_estado"));
                suc.setUbi_id(st.getInt("ubi_id"));
                arr.add(suc);
            }
        } catch (Exception e) {
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
}
