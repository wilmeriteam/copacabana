package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Cargo;

/**
 *
 * @author Wilmer
 */
public class cargoDAO extends DAO{
    public List<Cargo> listCargo() throws Exception{
        PreparedStatement pst;
        ResultSet st;
        List<Cargo> arr = new ArrayList<>();
        String sql;
        try {
            this.Conectar();
            sql = "select car_id,car_nombre from tab_cargo";
            pst = this.getCn().prepareStatement(sql);
            st = pst.executeQuery();
            while(st.next()){
                Cargo car = new Cargo();
                car.setCar_id(st.getInt("car_id"));
                car.setCar_nombre(st.getString("car_nombre"));
                arr.add(car);
            }
        } catch (Exception e) {
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
    public List<Cargo> SelectListCargos() throws Exception {
        ResultSet rs;
        List<Cargo> arr = new ArrayList<>();
        String sql;
        try {
            this.Conectar();
            sql = "select car_id,car_nombre from tab_cargo where car_estado = 1";
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            
            while( rs.next()){
                Cargo car = new Cargo();
                car.setCar_nombre(rs.getString("car_nombre"));
                car.setCar_id(rs.getInt("car_id"));
                arr.add(car);
            }
        } catch (Exception e) {
            throw e;
        }finally {
            this.Cerrar();
        }
        return arr;
    }
}
