package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Puestotrabajo;

public class puestotrabajoDAO extends DAO {

    public List<Puestotrabajo> listarPuestotrabajo() throws Exception {

        List<Puestotrabajo> arr = new ArrayList<>();
        ResultSet rs;

        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("select* from dbo.tab_puestotrabajo where ptb_estado=1");
            rs = st.executeQuery();
            while (rs.next()) {
                Puestotrabajo men = new Puestotrabajo();
                men.setPtb_id(rs.getInt("ptb_id"));
                men.setPtb_nombre(rs.getString("ptb_nombre"));
                men.setPtb_estado(rs.getInt("ptb_estado"));
                arr.add(men);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public void registrarDao(Puestotrabajo men) throws Exception {

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("insert into dbo.tab_puestotrabajo values(?,1)");
            pst.setString(1, men.getPtb_nombre());
            pst.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void modificarDao(Puestotrabajo men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_puestotrabajo set ptb_nombre=? where ptb_id=? and ptb_estado=1");
            pst.setString(1, men.getPtb_nombre());
            pst.setInt(2, men.getPtb_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void eliminarPuestotrabajo(Puestotrabajo men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_puestotrabajo set ptb_estado=2 where ptb_id=? ");
            pst.setInt(1, men.getPtb_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public Puestotrabajo leerPuestotrabajo(Puestotrabajo men) throws Exception {
        ResultSet st;
        Puestotrabajo puestotrabajomodel = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from dbo.tab_puestotrabajo where ptb_id=? and ptb_estado=1");
            pst.setInt(1, men.getPtb_id());
            st = pst.executeQuery();
            while (st.next()) {
                puestotrabajomodel = new Puestotrabajo();
                puestotrabajomodel.setPtb_id(st.getInt("ptb_id"));
                puestotrabajomodel.setPtb_nombre(st.getString("ptb_nombre"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return puestotrabajomodel;
    }

}
