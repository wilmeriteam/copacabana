package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Datosempresa;

public class datosempresaDAO extends DAO {

    public void registrarDao(Datosempresa men) throws Exception {
        ResultSet st;
        int u = 0;
        int id=0;
        try {
            this.Conectar();
            PreparedStatement pstsearh = this.getCn().prepareCall("select* from tab_datosempresa");
            st = pstsearh.executeQuery();
            while (st.next()) {
                u++;
                id=st.getInt("datemp_id");
            }
            if (u == 0) {
                PreparedStatement pst = this.getCn().prepareStatement("insert into dbo.tab_datosempresa values(100,?,?,?,?,?)");
                pst.setString(1, men.getDatemp_nombreempresa());
                pst.setString(2, men.getDatemp_direccion());
                pst.setString(3, men.getDatemp_telefono());
                pst.setString(4, men.getDatemp_nit());
                pst.setString(5, men.getDatemp_logo());
                pst.executeUpdate();
            } else {
                PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_datosempresa set datemp_nombreempresa=?,datemp_direccion=?,datemp_telefono=?,datemp_nit=?,datemp_logo=? where datemp_id="+id);
                pst.setString(1, men.getDatemp_nombreempresa());
                pst.setString(2, men.getDatemp_direccion());
                pst.setString(3, men.getDatemp_telefono());
                pst.setString(4, men.getDatemp_nit());
                pst.setString(5, men.getDatemp_logo());
                pst.executeUpdate();
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }




    public Datosempresa leerDatosempresa() throws Exception {
        ResultSet st;
        Datosempresa datosempresamodel = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("select * from dbo.tab_datosempresa");
            st = pst.executeQuery();
            while (st.next()) {
                datosempresamodel = new Datosempresa();
                datosempresamodel.setDatemp_id(st.getInt("datemp_id"));
                datosempresamodel.setDatemp_nombreempresa(st.getString("datemp_nombreempresa"));
                datosempresamodel.setDatemp_direccion(st.getString("datemp_direccion"));
                datosempresamodel.setDatemp_telefono(st.getString("datemp_telefono"));
                datosempresamodel.setDatemp_nit(st.getString("datemp_nit"));
                datosempresamodel.setDatemp_logo(st.getString("datemp_logo"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return datosempresamodel;
    }

}
