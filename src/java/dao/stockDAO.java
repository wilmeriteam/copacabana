/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import model.Stock;
//import org.apache.log4j.Logger;

/**
 *
 * @author wilmer
 */
public class stockDAO extends DAO{
//    private static final Logger logger = Logger.getLogger(almacenDAO.class);
    public ArrayList<Stock> generaCsvHead(String date1, String date2) throws Exception{
        ArrayList arr = new ArrayList();
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        try {
            this.Conectar();
            sql = "delete from parameterHead";
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
            
            sql = "DBCC CHECKIDENT (parameterHead, RESEED,0)";
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
            
            sql = "insert into parameterHead select s.sol_id,max(s.usu_idempleado) usu_idempleado,cast(convert(varchar, d.deta_fechaasignacion, 105) as DATE) deta_fechaasignacion\n" +
                " from tab_solicitud s join tab_detalleasignacion d on s.sol_id = d.sol_id\n" +
                " where s.mtv_id not in (3,4,6) and d.deta_fechaasignacion is not null and cast(convert(varchar, d.deta_fechaasignacion, 105) as DATE) between '"+date1+"' and '"+date2+"'\n" +
                "group by s.sol_id,cast(convert(varchar, d.deta_fechaasignacion, 105) as DATE)";
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
            
            sql = "select a.RecordKey,pe.UTS_GRUPO,UTS_SUBGRUPO,a.deta_fechaasignacion Docdate, a.deta_fechaasignacion DocDueDate,\n" +
                "u.usu_nombre+' - '+ar.area comment, u.usu_nombre+' - '+ar.area journal,pe.PaymentGroupCode,pe.Series from parameterHead a\n" +
                "join tab_usuariohistorial h on h.ush_id = a.usu_idempleado\n" +
                "join tab_usuario u on u.usu_id = h.usu_id\n" +
                "join parameterCode pc on pc.division = h.division and pc.area = h.Area\n" +
                "join tab_area ar on ar.area_id = h.Area\n" +
                "join parameterExport pe on pe.EmpAntiguo = h.TipoEmp \n" +
                "where a.deta_fechaasignacion between '"+date1+"' and '"+date2+"' order by a.RecordKey";
//            logger.info("head: " + sql);
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Stock sto = new Stock();
                sto.setIdSol(rs.getInt("RecordKey"));
                sto.setUstGrupo(rs.getString("UTS_GRUPO"));
                sto.setUstSubcargo(rs.getString("UTS_SUBGRUPO"));
                sto.setDocDate(rs.getString("Docdate"));
                sto.setDocDueDate(rs.getString("DocDueDate"));
                sto.setJournal(rs.getString("journal"));
                sto.setPaymentGroupCode(rs.getInt("PaymentGroupCode"));
                sto.setSeries(rs.getInt("Series"));
                arr.add(sto);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
    public ArrayList<Stock> generaCsvLine() throws Exception{
        ArrayList arr = new ArrayList();
        String sql;
        PreparedStatement pst;
        ResultSet rs;
        try {
            this.Conectar();
            sql = "select j.RecordKey,p.pro_itemcode,pc.acountcode,u.sigla,pc.costingcode,j.quantity,j.emp from (\n" +
                "	select MAX(s.RecordKey) RecordKey,d.al_id,max(s.usu_idempleado) emp ,COUNT(d.al_id) quantity from parameterHead s \n" +
                "	join tab_detalleasignacion d on s.sol_id = d.sol_id where d.deta_fechaasignacion is not null group by s.sol_id,d.al_id\n" +
                ") j join tab_almacen a on a.al_id = j.al_id\n" +
                "join tab_usuariohistorial h on h.ush_id = j.emp\n" +
                "join tab_ubicacion u on u.ubi_id = a.ubi_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id\n" +
                "join parameterCode pc on pc.area = h.Area and pc.division = h.division\n" +
                "order by j.RecordKey";
//            logger.info("line: " + sql);
            pst = this.getCn().prepareStatement(sql);
            rs = pst.executeQuery();
            while(rs.next()){
                Stock sto = new Stock();
                sto.setIdSol(rs.getInt("RecordKey"));
                sto.setItemCode(rs.getString("pro_itemcode"));
                sto.setAcountcode(rs.getString("acountcode"));
                sto.setSigla(rs.getString("sigla"));
                sto.setCostingCode(rs.getString("costingcode"));
                sto.setQuatity(rs.getInt("quantity"));
                arr.add(sto);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
}
