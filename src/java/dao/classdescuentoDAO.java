package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Classdescuento;

public class classdescuentoDAO extends DAO {
    
    public List<Classdescuento> listarClassdescuento() throws Exception {
        
        List<Classdescuento> arr = new ArrayList<>();
        ResultSet rs;
        
        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("select* from dbo.tab_classdescuento where clas_estado=1");
            rs = st.executeQuery();
            while (rs.next()) {
                Classdescuento men = new Classdescuento();
                men.setClas_id(rs.getInt("clas_id"));
                men.setClas_nombre(rs.getString("clas_nombre"));
                men.setClas_tipo(rs.getString("clas_tipo"));
                men.setClas_estado(rs.getInt("clas_estado"));
                arr.add(men);
            }
            
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
    public void registrarDao(Classdescuento men) throws Exception {
        
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("insert into dbo.tab_classdescuento values(?,?,1)");
            pst.setString(1, men.getClas_nombre());
             pst.setString(2, men.getClas_tipo());
            pst.executeUpdate();
            
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void modificarDao(Classdescuento men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_classdescuento set clas_nombre=?,clas_tipo=? where clas_id=? and clas_estado=1");
            pst.setString(1, men.getClas_nombre());
            pst.setString(2, men.getClas_tipo());
            pst.setInt(3, men.getClas_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
  public void eliminarClassdescuento(Classdescuento men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_classdescuento set clas_estado=2 where clas_id=? ");
            pst.setInt(1, men.getClas_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
    public Classdescuento leerClassdescuento(Classdescuento men) throws Exception {
        ResultSet st;
        Classdescuento clas=null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from dbo.tab_classdescuento where clas_id=? and clas_estado=1");
            pst.setInt(1, men.getClas_id());
            st = pst.executeQuery();
            while (st.next()) {
                clas=new Classdescuento();
                clas.setClas_id(st.getInt("clas_id"));
                clas.setClas_nombre(st.getString("clas_nombre"));
                clas.setClas_tipo(st.getString("clas_tipo"));
            }
        } catch (Exception e) {
            throw e;
        }finally{
        this.Cerrar();
        }
        return clas;
    }
    
}
