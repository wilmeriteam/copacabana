package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Menu;

public class menuDAO extends DAO {

    public List<Menu> listarMenu() throws Exception {

        List<Menu> arr = new ArrayList<>();
        ResultSet rs;

        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("select * from dbo.tab_menu where men_estado=1");
            rs = st.executeQuery();
            while (rs.next()) {
                Menu men = new Menu();
                men.setMen_id(rs.getInt("men_id"));
                men.setMen_nombre(rs.getString("men_nombre"));
                men.setMen_alias(rs.getString("men_alias"));
                men.setMen_estado(rs.getInt("men_estado"));
                arr.add(men);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public List<Menu> listarMenuRol(String usu_id) throws Exception {

        List<Menu> arr = new ArrayList<>();
        ResultSet rs;
        int usu = Integer.parseInt(usu_id);
        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("SELECT\n" +
"                    dbo.tab_menu.men_id,\n" +
"                    rtrim(dbo.tab_menu.men_nombre) as men_nombre,\n" +
"                    rtrim(dbo.tab_menu.men_alias) as men_alias\n" +
"\n" +
"FROM\n" +
"                    dbo.tab_rol\n" +
"                    INNER JOIN dbo.tab_usuariohistorial ON dbo.tab_rol.rol_id = dbo.tab_usuariohistorial.rol_id\n" +
"                    INNER JOIN dbo.tab_permisos ON dbo.tab_permisos.rol_id = dbo.tab_rol.rol_id\n" +
"                    INNER JOIN dbo.tab_menu ON dbo.tab_permisos.men_id = dbo.tab_menu.men_id\n" +
"WHERE\n" +
"dbo.tab_menu.men_estado = 1 AND\n" +
"dbo.tab_usuariohistorial.ush_estado = 1 AND\n" +
"dbo.tab_usuariohistorial.ush_id = "+usu);
            rs = st.executeQuery();
            while (rs.next()) {
                Menu men = new Menu();
                men.setMen_id(rs.getInt("men_id"));
                men.setMen_nombre(rs.getString("men_nombre"));
                men.setMen_alias(rs.getString("men_alias"));
                arr.add(men);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public void registrarDao(Menu men) throws Exception {

        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("insert into dbo.tab_menu values(?,?,1)");
            pst.setString(1, men.getMen_nombre());
            pst.setString(2, men.getMen_alias());
            pst.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void modificarDao(Menu men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_menu set men_nombre=?,men_alias=? where men_id=? and men_estado=1");
            pst.setString(1, men.getMen_nombre());
            pst.setString(2, men.getMen_alias());
            pst.setInt(3, men.getMen_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void eliminarMenu(Menu men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_menu set men_estado=2 where men_id=? ");
            pst.setInt(1, men.getMen_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public Menu leerMenu(Menu men) throws Exception {
        ResultSet st;
        Menu menumodel = null;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("select * from dbo.tab_menu where men_id=? and men_estado=1");
            pst.setInt(1, men.getMen_id());
            st = pst.executeQuery();
            while (st.next()) {
                menumodel = new Menu();
                menumodel.setMen_id(st.getInt("men_id"));
                menumodel.setMen_nombre(st.getString("men_nombre"));
                menumodel.setMen_alias(st.getString("men_alias"));
                menumodel.setMen_estado(st.getInt("men_estado"));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return menumodel;
    }

    public ArrayList<Menu> SelectListMenu() throws Exception {
        ResultSet st;
        Menu menumodel = null;
        ArrayList arr = new ArrayList();
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("select * from dbo.tab_menu where men_estado=1");
            st = pst.executeQuery();
            while (st.next()) {
                menumodel = new Menu();
                menumodel.setMen_id(st.getInt("men_id"));
                menumodel.setMen_nombre(st.getString("men_nombre"));
                arr.add(menumodel);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
}
