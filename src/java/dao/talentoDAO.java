package dao;

import ManagenBean.talentoBean;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Almacen;
import model.Talento;
import model.Tipoprenda;
//import org.apache.log4j.Logger;
import org.primefaces.component.calendar.Calendar;

public class talentoDAO extends DAO {
//    private static final Logger logger = Logger.getLogger(talentoDAO.class);
    
    /**
     * 
     * @param ususol id del solicitante
     * @return arr Lista de todos los solicitantes de items y prendas nuevas en talento (talento.xhtml)
     * @throws Exception 
     */
    public List<Talento> listarTalento(int ususol) throws Exception {

        List<Talento> arr = new ArrayList<>();
        ResultSet rs;
        //contador incremental
        int i = 0;
        try {
            this.Conectar();
            String sql = "select u.usu_nombre+' '+u.usu_apellido nomusuario_empleado,c.car_nombre nomcargo,su.suc_direccion sucursal,s.sol_id,s.sol_fecha,m.mot_nombremotivo+' '+cast(s.sol_descripcion as varchar) mot_nombremotivo\n" +
            "from tab_solicitud s \n" +
            "join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
            "join tab_usuario u on u.usu_id = h.usu_id\n" +
            "join tab_cargo c on c.car_id = h.car_id\n" +
            "join tab_sucursales su on su.suc_id = h.suc_id\n" +
            "join tab_motivodescuento m on m.mot_id = s.mtv_id\n" +
            "where s.usu_idsolicitante = "+ususol+" AND EXISTS (select * from tab_solicitud JOIN tab_detalleasignacion d on d.sol_id = dbo.tab_solicitud.sol_id\n" +
            "where dbo.tab_solicitud.usu_idsolicitante = "+ususol+" AND d.sol_id = dbo.tab_solicitud.sol_id AND d.deta_estadoprenda = 1) AND s.sol_estado = 1 AND s.mtv_id in (1,2,5,7) order by sol_fecha desc";
//            logger.info("ListarTalento: "+sql);
            PreparedStatement st = this.getCn().prepareCall(sql);
            rs = st.executeQuery();
            String[] explodefecha;
            while (rs.next()) {
                Talento sol = new Talento();
                sol.setSol_id(rs.getInt("sol_id"));
                explodefecha = (rs.getString("sol_fecha")).split("-");
                sol.setSol_fecha(explodefecha[2] + "-" + explodefecha[1] + "-" + explodefecha[0]);
                sol.setNomcargo(rs.getString("nomcargo"));
                sol.setSucursal(rs.getString("sucursal"));
                sol.setNomusuario_empleado(rs.getString("nomusuario_empleado"));
                sol.setNombremotivo(rs.getString("mot_nombremotivo"));
                sol.setAlfanumeric(++i);
                arr.add(sol);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public void actualizarEstado(int sol, int usu_idemp) throws Exception {
        ResultSet st;
        int i = 0;
        try {
            this.Conectar();
            PreparedStatement pstsel = this.getCn().prepareStatement("select* from tab_solicitud where sol_estado=2 and sol_id=" + sol);
            st = pstsel.executeQuery();
            while (st.next()) {
                i++;
            }
            if (i == 0) {
                PreparedStatement pst = this.getCn().prepareCall("update tab_solicitud set sol_estado=1,usu_idempleado=" + usu_idemp + " where sol_id=" + sol);
                pst.executeUpdate();
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public void registrarDao(Talento ta, int ush_id) throws Exception {
        String sql;
        ResultSet st;
        ResultSet stmax;
        ResultSet fechastl;
        ResultSet fechast2;
        ResultSet CantidadLimite;
        java.util.Date fecha = new Date();
        long lnMilisegundos = fecha.getTime();
        java.sql.Date sqlDate = new java.sql.Date(lnMilisegundos);
        int sol_id = 0;
        int cantLimite = 0;
        Date itemfechaAl = null;
        Date itemfechaUsu = null;
        Date fechaselect = null;
        try {
            this.Conectar();
            //actualizar el usuario a antigo

            PreparedStatement pstu = this.getCn().prepareCall("UPDATE tab_usuariohistorial set ush_usuario=2 where ush_id=? and ush_estado=1");
            pstu.setInt(1, ta.getUsu_idempleado());
            pstu.executeUpdate();

            PreparedStatement pstsol = this.getCn().prepareStatement("insert into dbo.tab_solicitud values('" + sqlDate + "'," + ush_id + ",?,NULL,NULL,?,1,?,(select max(sol_nroform)+ 1 from tab_solicitud))");
            pstsol.setInt(1, ta.getUsu_idempleado());
            pstsol.setString(2, ta.getSol_descripcion());
            pstsol.setInt(3, ta.getMot_id());
            pstsol.executeUpdate();
            
            PreparedStatement pst2 = this.getCn().prepareCall("select MAX(sol_id) as sol_id from tab_solicitud");
            stmax = pst2.executeQuery();

            while (stmax.next()) {
                sol_id = stmax.getInt("sol_id");
            }
            sql = "select ca.car_id,ca.atcar_id,p.igr_id, ca.tpe_id,r.u_item,a.al_id,a.ubi_id,p.pro_itemcode,p.pro_itemname,p.pro_unidad,pro_color, \n" +
                "p.pro_descripcion, p.pro_genero, p.pro_talla, p.pro_logo,p.pro_material,p.pro_vidautil,a.al_cantidad,a.al_costo,r.u_cantidad\n" +
                "from tab_areatrabajocargo ca \n" +
                "join tab_ropatrabajo r on r.tpe_id = ca.tpe_id\n" +
                " join (select MAX(p.pro_itemcode) itemcode\n" +
                " from tab_producto p join tab_itemgrupo i on i.igr_id = p.igr_id\n" +
                " where (RTRIM(p.pro_genero) = '' OR RTRIM(p.pro_genero) = '"+ta.getUsh_sexo()+"') group by p.igr_id) temp on temp.itemcode = r.u_item\n" +
                "join tab_producto p on r.u_item = p.pro_itemcode\n" +
                "join tab_almacen a on a.pro_id = p.pro_id\n" +
                "where ca.car_id = "+ ta.getCar_id()+" AND a.ubi_id = "+ta.getUbi_id()+" AND p.pro_estado = 1 \n" +
                "and (RTRIM(p.pro_genero) = '' OR RTRIM(p.pro_genero) = '"+ta.getUsh_sexo()+"')";
            PreparedStatement pst = this.getCn().prepareCall(sql);
//            pst.setInt(1, ta.getCar_id());
//            pst.setInt(2, ta.getUbi_id());
//            pst.setString(3, ta.getUsh_sexo());
            st = pst.executeQuery();
            while (st.next()) {
//fecha del producto
                PreparedStatement pstal = this.getCn().prepareStatement("SELECT\n"
                        + "DATEADD ( MONTH ,(dbo.tab_producto.pro_vidautil), GETDATE()) AS fechaf\n"
                        + "FROM\n"
                        + "dbo.tab_almacen\n"
                        + "INNER JOIN dbo.tab_producto ON dbo.tab_producto.pro_id = dbo.tab_almacen.pro_id\n"
                        + "WHERE\n"
                        + "dbo.tab_almacen.al_id = " + st.getInt("al_id"));
                fechastl = pstal.executeQuery();
                while (fechastl.next()) {
                    itemfechaAl = fechastl.getDate("fechaf");
                }
//fecha del usuario
                PreparedStatement pstusu = this.getCn().prepareCall("SELECT\n"
                        + "dbo.tab_usuariohistorial.ush_ffinal\n"
                        + "FROM\n"
                        + "dbo.tab_usuariohistorial\n"
                        + "INNER JOIN dbo.tab_solicitud ON dbo.tab_solicitud.usu_idempleado = dbo.tab_usuariohistorial.ush_id\n"
                        + "WHERE\n"
                        + "dbo.tab_solicitud.sol_id = " + sol_id + " AND\n"
                        + "dbo.tab_solicitud.sol_estado = 1");
                fechast2 = pstusu.executeQuery();
                while (fechast2.next()) {
                    itemfechaUsu = fechast2.getDate("ush_ffinal");
                }
                if (itemfechaUsu == null) {
                    fechaselect = itemfechaAl;
                } else {
                    if (itemfechaUsu.getDate() < itemfechaAl.getDate()) {
                        fechaselect = itemfechaUsu;
                    } else {
                        fechaselect = itemfechaAl;
                    }
                }
                //Obtenemos la cantidad límite parametrizada para el registro consecuente
                sql = "select t.u_cantidad\n" +
                                "FROM tab_solicitud s\n" +
                                "join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
                                "join tab_areatrabajocargo ar on ar.car_id = h.car_id\n" +
                                "join tab_almacen a on a.al_id = " + st.getInt("al_id") + "\n" +
                                "join tab_producto p on p.pro_id = a.pro_id\n" +
                                "join tab_ropatrabajo t on (t.tpe_id = ar.tpe_id AND u_item = p.pro_itemcode)\n" +
                                "where s.sol_id = " + sol_id;
                PreparedStatement getCantLimite = this.getCn().prepareCall(sql);
                CantidadLimite = getCantLimite.executeQuery();
                while(CantidadLimite.next()){
                    cantLimite = CantidadLimite.getInt("u_cantidad");
                }
                for(int i = 0; i < cantLimite; i++){                    
                    sql = "insert into tab_detalleasignacion "
                            + "values(" + sol_id + "," + st.getInt("al_id") + ",1,'" + fechaselect + "',NULL,1,NULL,NULL,NULL)";
//                    logger.info("Inserta Talento: "+sql);
                    PreparedStatement insertdeta = this.getCn().prepareCall(sql);
                    insertdeta.executeUpdate();
                }
                
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }

    }
    
    public int registrartalentoDao(Talento men, int ush_id, List<Almacen> lista) throws Exception {
        String sql;
//        int parametroLimite = 0;
//        int asignacionFaltante = 0;
//        int totalCantidadAsignado = 0;
        PreparedStatement pst;
        ResultSet rs;
        int idsol = 0;
       
        try {
            this.Conectar();
            //Registramos la solicitud
//            logger.info("total solicitados2:"+lista.size());
            this.getCn().setAutoCommit(false);
//            if (lista.size() > 0) {
                //Crear Solicitud de Devolución en RRHH
                sql = "insert into dbo.tab_solicitud values(GETDATE()," + ush_id + ","+ men.getUsu_idempleado() + 
                        ",NULL,NULL,'" + men.getClasempleado()+ "',1," + men.getMotivo()+ ",(select max(sol_nroform)+ 1 from tab_solicitud),NULL)";
                pst = getCn().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
                pst.executeUpdate();
                //obtenemos el id de la solicitud creada
                rs = pst.getGeneratedKeys();

                while (rs.next()) {
                    idsol = rs.getInt(1);
                }                
                sql = "insert into tab_detalleasignacion \n" +
                    "select "+idsol+",un.al_id,1,null,null,1,null,GETDATE(),un.deta_id from (\n" +
//                    "	select igr_id,al_id,deta_id, (cantorig - cantsol) cantfinal  from (\n" +
                    "		select a1.igr_id,a1.cantorig,a1.al_id,deta_id,case when b1.cant is null then 0 else b1.cant end cantsol from (\n" +
                    "			select a.igr_id,a.cant cantorig,a.al_id,a.deta_id from (\n" +
                    "				select p.igr_id,max(a.al_id) al_id,max(d.deta_id) deta_id, COUNT(p.igr_id) cant from tab_solicitud s join tab_detalleasignacion d on d.sol_id = s.sol_id \n" +
                    "				join tab_almacen a on a.al_id = d.al_id\n" +
                    "				join tab_producto p on p.pro_id = a.pro_id\n" +
                    "				where s.usu_idempleado = "+ men.getUsu_idempleado() +" and d.deta_estadoprenda = 2 group by p.igr_id\n" +
                    "			) a left join (\n" +
                    "				select p.igr_id,max(rt.u_cantidad) cant from tab_areatrabajocargo atc\n" +
                    "				join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                    "				join tab_producto p on p.pro_itemcode = rt.u_item\n" +
                    "				where atc.car_id = "+ men.getCar_id()+" and (p.pro_genero = '"+men.getUsh_sexo()+"' OR p.pro_genero = '') group by p.igr_id \n" +
                    "			) b on b.igr_id = a.igr_id and b.cant = a.cant where b.igr_id is null\n" +
                    "		) a1 left join (\n" +
                    "			select p.igr_id,max(rt.u_cantidad) cant from tab_areatrabajocargo atc\n" +
                    "			join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                    "			join tab_producto p on p.pro_itemcode = rt.u_item\n" +
                    "			where atc.car_id = "+ men.getCar_id()+" and (p.pro_genero = '"+men.getUsh_sexo()+"' OR p.pro_genero = '') group by p.igr_id \n" +
                    "		) b1 on b1.igr_id = a1.igr_id\n" +
//                    "	) f cross join Numeros where n<=(cantorig - cantsol) \n" +
                    ") un";
//                logger.info("inserta devolucion: "+sql);
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();
                                
                sql = "insert into dbo.tab_solicitud values(GETDATE()," + ush_id + ","+ men.getUsu_idempleado() + 
                        ",NULL,NULL,'DOTACIÓN',1,7,(select max(sol_nroform)+ 1 from tab_solicitud), NULL)";
                pst = getCn().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
                pst.executeUpdate();
                //obtenemos el id de la solicitud creada
                rs = pst.getGeneratedKeys();
                while (rs.next()) {
                    idsol = rs.getInt(1);
                }                
                
                for (Almacen row : lista) {
                    sql = "insert into tab_detalleasignacion values(" + idsol + "," + row.getAl_id() + ",1,NULL,NULL,1,NULL,GETDATE()," + row.getDeta_id() + ")";
                    pst = getCn().prepareStatement(sql);
                    pst.executeUpdate();
                }
                sql = "delete tab_solicitud from tab_solicitud s  where not exists (select sol_id from tab_detalleasignacion where sol_id = s.sol_id)";
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();
                
                sql = "UPDATE tab_usuariohistorial set clasempleado = null where ush_id = "+men.getUsu_idempleado();
                pst = getCn().prepareStatement(sql);
                pst.executeUpdate();                
                this.getCn().commit();                
//            } else {
//                return 2;
//            }
        } catch (Exception e) {
            try {
                this.getCn().rollback();
            } catch (SQLException ex1) {
                System.err.println("No se pudo deshacer" + ex1.getMessage());
            }            
            throw e;
        } finally {
            this.Cerrar();
        }
        return 0;
    }
    
//    public int registrarDao(Talento men, int ush_id, Almacen al, int solid) throws Exception {
//
//        java.util.Date fecha = new Date();
//        long lnMilisegundos = fecha.getTime();
//        java.sql.Date sqlDate = new java.sql.Date(lnMilisegundos);
//
//        ResultSet st;
//        ResultSet st1;
//        ResultSet stveri;
//        ResultSet stusu;
//        ResultSet stal;
//        int sol_id = 0;
//        int cantidad_almacen = 0;
//        Date fechafinal = null;
//        Date itemfecha = null;
//        Date fechaselect = null;
//        ResultSet stver;
//        try {
//            this.Conectar();
//            PreparedStatement pst4 = this.getCn().prepareCall("SELECT\n"
//                    + "dbo.tab_solicitud.sol_id,\n"
//                    + "dbo.tab_solicitud.sol_fecha,\n"
//                    + "dbo.tab_solicitud.usu_idsolicitante,\n"
//                    + "dbo.tab_solicitud.usu_idempleado,\n"
//                    + "dbo.tab_solicitud.sol_fechaasignacion,\n"
//                    + "dbo.tab_solicitud.sol_fechadevolucion,\n"
//                    + "dbo.tab_solicitud.sol_descripcion,\n"
//                    + "dbo.tab_solicitud.sol_estado,\n"
//                    + "dbo.tab_solicitud.mtv_id,\n"
//                    + "(select ush_ffinal from tab_usuariohistorial as usu where usu.ush_id=tab_solicitud.usu_idempleado) AS usu_fechaf,\n"
//                    + "dbo.tab_detalleasignacion.deta_id,\n"
//                    + "(SELECT \n"
//                    + "DATEADD ( day ,(CONVERT(INT,dbo.tab_almacen.al_tiempovida)*30), GETDATE())\n"
//                    + "from tab_almacen where al_id=tab_detalleasignacion.al_id) AS fechaitem,\n"
//                    + "dbo.tab_detalleasignacion.al_id\n"
//                    + "\n"
//                    + "FROM\n"
//                    + "dbo.tab_solicitud\n"
//                    + "INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_solicitud.sol_id = dbo.tab_detalleasignacion.sol_id\n"
//                    + "WHERE\n"
//                    + "dbo.tab_solicitud.usu_idempleado = ?");
//            pst4.setInt(1, men.getUsu_idempleado());
//            st1 = pst4.executeQuery();
//            int i = 0;
//
//            while (st1.next()) {
//                i++;
//                sol_id = st1.getInt("sol_id");
//                fechafinal = st1.getDate("usu_fechaf");
//                itemfecha = st1.getDate("fechaitem");
//            }
//
//            if (i == 0) {
//                PreparedStatement verificar = this.getCn().prepareStatement("SELECT\n"
//                        + "dbo.tab_usuariohistorial.ush_id,\n"
//                        + "dbo.tab_usuariohistorial.rol_id,\n"
//                        + "dbo.tab_usuariohistorial.ush_ffinal\n"
//                        + "\n"
//                        + "FROM\n"
//                        + "dbo.tab_usuariohistorial\n"
//                        + "INNER JOIN dbo.tab_usuario ON dbo.tab_usuario.usu_id = dbo.tab_usuariohistorial.usu_id\n"
//                        + "INNER JOIN dbo.tab_solicitud ON dbo.tab_solicitud.usu_idempleado = dbo.tab_usuariohistorial.ush_id\n"
//                        + "WHERE\n"
//                        + "dbo.tab_solicitud.usu_idempleado = ?");
//                verificar.setInt(1, men.getUsu_idempleado());
//                stver = verificar.executeQuery();
//                while (stver.next()) {
//                    i++;
//                }
//                if (i == 0) {
//                    PreparedStatement pst = this.getCn().prepareStatement("insert into dbo.tab_solicitud values('" + sqlDate + "'," + ush_id + ",?,NULL,NULL,NULL,1,?)");
//                    pst.setInt(1, men.getUsu_idempleado());
//                    pst.setInt(2, men.getMtv_id());
//                    pst.executeUpdate();
//                }
//                PreparedStatement pst2 = this.getCn().prepareCall("select MAX(sol_id) as sol_id from tab_solicitud");
//                st = pst2.executeQuery();
//
//                while (st.next()) {
//                    sol_id = st.getInt("sol_id");
//                }
//                PreparedStatement pstusu = this.getCn().prepareStatement("select ush_ffinal from tab_usuariohistorial where ush_id=?");
//                pstusu.setInt(1, men.getUsu_idempleado());
//                stusu = pstusu.executeQuery();
//                while (stusu.next()) {
//                    fechafinal = stusu.getDate("ush_ffinal");
//                }
//                PreparedStatement pstal = this.getCn().prepareStatement("SELECT\n"
//                        + "DATEADD ( day ,(CONVERT(INT,dbo.tab_almacen.al_tiempovida)*30), GETDATE()) as fechaf\n"
//                        + "FROM\n"
//                        + "dbo.tab_almacen\n"
//                        + "WHERE\n"
//                        + "dbo.tab_almacen.al_id = ?");
//                pstal.setInt(1, al.getAl_id());
//                stal = pstal.executeQuery();
//                while (stal.next()) {
//                    itemfecha = stal.getDate("fechaf");
//                }
//                if (fechafinal == null) {
//                    fechaselect = itemfecha;
//
//                } else {
//                    if (fechafinal.getDate() < itemfecha.getDate()) {
//                        fechaselect = fechafinal;
//                    } else {
//                        fechaselect = itemfecha;
//                    }
//                }
//                PreparedStatement pst1 = this.getCn().prepareStatement("insert into dbo.tab_detalleasignacion values(" + sol_id + ",?,1,'" + fechaselect + "',NULL)");
//                pst1.setInt(1, al.getAl_id());
//                pst1.executeUpdate();
//            } else {
//                if (fechafinal == null) {
//                    fechaselect = itemfecha;
//                } else {
//
//                    if (fechafinal.getDate() < itemfecha.getDate()) {
//                        fechaselect = fechafinal;
//                    } else {
//                        fechaselect = itemfecha;
//                    }
//                }
//                PreparedStatement pstveri = this.getCn().prepareStatement("select * from dbo.tab_detalleasignacion where sol_id=" + sol_id + " and al_id=?");
//                pstveri.setInt(1, al.getAl_id());
//                stveri = pstveri.executeQuery();
//                int numRows = 0;
//                while (stveri.next()) {
//                    numRows++;
//                }
//                if (numRows == 0) {
//                    PreparedStatement pst13 = this.getCn().prepareStatement("insert into dbo.tab_detalleasignacion values(" + sol_id + ",?,1,'" + fechafinal + "',NULL)");
//                    pst13.setInt(1, al.getAl_id());
//                    pst13.executeUpdate();
//                }
//            }
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            this.Cerrar();
//        }
//        return sol_id;
//    }

    public int cantidadRopa(Talento ta) throws Exception {
        ResultSet st;
        int u = 0;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement(" select ca.car_id,ca.atcar_id,ca.tpe_id,r.u_item,a.al_id,a.ubi_id,p.pro_itemcode,p.pro_itemname,p.pro_unidad,pro_color, \n"
                    + "p.pro_descripcion, p.pro_genero, p.pro_talla, p.pro_logo,p.pro_material,p.pro_vidautil,a.al_cantidad,a.al_costo\n"
                    + "from tab_areatrabajocargo ca \n"
                    + "join tab_ropatrabajo r on r.tpe_id = ca.tpe_id\n"
                    + " join (select MAX(p.pro_itemcode) itemcode\n"
                    + " from tab_producto p join tab_itemgrupo i on i.igr_id = p.igr_id\n"
                    + " --where (p.pro_genero is null OR p.pro_genero = 'F')\n"
                    + " group by p.igr_id) temp on temp.itemcode = r.u_item\n"
                    + "join tab_producto p on r.u_item = p.pro_itemcode\n"
                    + "join tab_almacen a on a.pro_id = p.pro_id\n"
                    + "where ca.car_id = ? AND a.ubi_id = ? AND p.pro_estado = 1\n"
                    + "and (RTRIM(p.pro_genero)='' OR RTRIM(p.pro_genero) = ?)");
            pst.setInt(1, ta.getCar_id());
            pst.setInt(2, ta.getUbi_id());
            pst.setString(3, ta.getUsh_sexo());
            st = pst.executeQuery();
            while (st.next()) {
                u++;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return u;
    }

    public ArrayList<Almacen> listaralmacenEvento(Talento ta) throws Exception {

        ResultSet st;
        ResultSet stselect;
        ArrayList<Almacen> arr = new ArrayList<>();
        Almacen al;
        int cont = 0;
        try {
            this.Conectar();

            String sql = "select S.igr_id,S.al_id,p.pro_itemcode,p.pro_itemname,p.pro_logo,p.pro_material,p.pro_color,p.pro_genero,a.al_cantidad from (\n" +
                "	SELECT T.igr_id,T.al_id FROM (\n" +
                "		select p.igr_id,min(a.al_id) al_id,min(rt.u_cantidad) cant from tab_areatrabajocargo atc \n" +
                "		join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                "		join tab_producto p on p.pro_itemcode = rt.u_item \n" +
                "		join tab_almacen a on a.pro_id = p.pro_id \n" +
                "		where atc.car_id = "+ta.getCar_id()+" and (p.pro_genero = '"+ta.getUsh_sexo()+"' or p.pro_genero = '') and a.ubi_id = "+ta.getUbi_id()+" group by p.igr_id\n" +
                "	) T\n" +
                "	CROSS JOIN Numeros WHERE n<=T.cant\n" +
                ") S join tab_almacen a on a.al_id = S.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id\n" +
                "where S.igr_id not in (\n" +
                "	select p.igr_id from tab_solicitud s join tab_detalleasignacion d on d.sol_id = s.sol_id \n" +
                "	join tab_almacen a on a.al_id = d.al_id\n" +
                "	join tab_producto p on p.pro_id = a.pro_id\n" +
                "	where s.usu_idempleado = "+ta.getUsu_idempleado()+" and d.deta_estadoprenda = 2)"
                    + "union all\n" +
                "select a1.igr_id,a1.al_id,pro_itemcode,pro_itemname,pro_logo,pro_material,pro_color,pro_genero,al_cantidad from (\n" +
                "select p.igr_id, count(p.igr_id) cant, max(a.al_id) al_id,max(p.pro_itemcode) pro_itemcode,MAX(p.pro_itemname) pro_itemname,\n" +
                "max(p.pro_logo) pro_logo,max(p.pro_material) pro_material,MAX(p.pro_color) pro_color,MAX(p.pro_genero) pro_genero,\n" +
                "MAX(a.al_cantidad) al_cantidad from tab_solicitud s join tab_detalleasignacion d on d.sol_id = s.sol_id \n" +
                "join tab_almacen a on a.al_id = d.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id\n" +
                "where s.usu_idempleado = "+ta.getUsu_idempleado()+" and d.deta_estadoprenda = 2 group by p.igr_id ) a1 left join \n" +
                "(select p.igr_id,min(rt.u_cantidad) cant from tab_areatrabajocargo atc \n" +
                "join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                "join tab_producto p on p.pro_itemcode = rt.u_item \n" +
                "join tab_almacen a on a.pro_id = p.pro_id \n" +
                "where atc.car_id = "+ta.getCar_id()+" and (p.pro_genero = '"+ta.getUsh_sexo()+"' or p.pro_genero = '') and a.ubi_id = "+ta.getUbi_id()+" group by p.igr_id) a2 on a1.igr_id = a2.igr_id\n" +
                "where a2.cant is not null and a2.cant > a1.cant";
//            logger.info("Listar Almacen Evento: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            
            stselect = pst.executeQuery();
            while (stselect.next()) {
                al = new Almacen();
                al.setAl_id(stselect.getInt("al_id"));
                al.setPro_itemcode(stselect.getString("pro_itemcode"));
                al.setPro_itemname(stselect.getString("pro_itemname"));
                al.setPro_material(stselect.getString("pro_material"));
                al.setAl_cantidad(stselect.getInt("al_cantidad"));
                al.setPro_color(stselect.getString("pro_color"));
                al.setPro_genero(stselect.getString("pro_genero"));
                al.setPro_logo(stselect.getString("pro_logo"));
                al.setCantidadcalculado(stselect.getInt("al_cantidad"));
                al.setAlfanumeric(++cont);
                arr.add(al);
            }
//            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public void eliminarTalento(Talento men) throws Exception {
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("update dbo.tab_solicitud set sol_estado=0 where sol_id=? ");
            pst.setInt(1, men.getSol_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    public Talento leerTalento(Talento men) throws Exception {
        ResultSet st;
        int usu_id = 0;
        Talento talentomodel = null;
        talentoBean solben = new talentoBean();
        try {
            this.Conectar();
            String sql = "SELECT\n"
                    + "(SELECT\n"
                    + "                    dbo.tab_cargo.car_nombre\n"
                    + "                    FROM\n"
                    + "                    dbo.tab_usuariohistorial\n"
                    + "                    INNER JOIN dbo.tab_cargo ON dbo.tab_usuariohistorial.car_id = dbo.tab_cargo.car_id\n"
                    + "                    WHERE\n"
                    + "                    dbo.tab_usuariohistorial.ush_id = tab_solicitud.usu_idempleado) AS nomcargo,\n"
                    + "(SELECT\n"
                    + "                    	(rtrim(tab_sucursales.suc_direccion)+' '+rtrim(dbo.tab_ubicacion.ubi_nombre)) as sucursal\n"
                    + "                    	FROM\n"
                    + "                    	dbo.tab_usuariohistorial\n"
                    + "                    	INNER JOIN dbo.tab_sucursales ON dbo.tab_usuariohistorial.suc_id = dbo.tab_sucursales.suc_id\n"
                    + "                    	INNER JOIN dbo.tab_ubicacion ON dbo.tab_sucursales.ubi_id = dbo.tab_ubicacion.ubi_id\n"
                    + "                    	WHERE\n"
                    + "                    	dbo.tab_usuariohistorial.ush_id = tab_solicitud.usu_idempleado) AS sucursal,\n"
                    + "dbo.tab_solicitud.sol_id,\n"
                    + "dbo.tab_solicitud.sol_fecha,\n"
                    + "dbo.tab_solicitud.usu_idsolicitante,\n"
                    + "dbo.tab_solicitud.usu_idempleado,\n"
                    + "dbo.tab_solicitud.sol_fechaasignacion,\n"
                    + "dbo.tab_solicitud.sol_fechadevolucion,\n"
                    + "dbo.tab_solicitud.sol_descripcion,\n"
                    + "dbo.tab_solicitud.sol_estado,\n"
                    + "dbo.tab_grupoclasetrabajo.gclas_id,\n"
                    + "dbo.tab_grupoclasetrabajo.tip_id,\n"
                    + "dbo.tab_grupoclasetrabajo.clast_id\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_solicitud\n"
                    + "INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_solicitud.sol_id = dbo.tab_detalleasignacion.sol_id\n"
                    + "INNER JOIN dbo.tab_almacen ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "INNER JOIN dbo.tab_grupoclasetrabajo ON dbo.tab_grupoclasetrabajo.gclas_id = dbo.tab_almacen.gclas_id\n"
                    + "WHERE\n"
                    + "(dbo.tab_solicitud.sol_estado = 1) AND\n"
                    + "dbo.tab_solicitud.sol_id = "+men.getSol_id();
//            logger.info("LeerTalento: "+sql);            
            PreparedStatement pst = this.getCn().prepareStatement(sql);            
//            pst.setInt(1, men.getSol_id());

            st = pst.executeQuery();
            talentomodel = new Talento();
            Tipoprenda prenda = new Tipoprenda();
            int tip_id = 0;
            int solid = 0;
            while (st.next()) {
                talentomodel.setSol_id(st.getInt("sol_id"));
                solid = st.getInt("sol_id");
                talentomodel.setUsu_idsolicitante(st.getInt("usu_idsolicitante"));
                talentomodel.setUsu_idempleado(st.getInt("usu_idempleado"));
                talentomodel.setNomcargo(st.getString("nomcargo"));
                talentomodel.setSucursal(st.getString("sucursal"));
                talentomodel.setTip_id(st.getInt("tip_id"));
                talentomodel.setClast_id(st.getInt("clast_id"));
//                prenda.setTip_id(st.getInt("tip_id"));
            }
//            solben.tipoprenda.setTip_id(tip_id);
//            solben.setTipoprenda(prenda);
//            solben.tipoprenda.setTip_id(solid);
//            =prenda.getTip_id();
//solben.setSolid(solid);

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return talentomodel;
    }

    public Talento ObtenerTalento(int usu) throws Exception {
        ResultSet st;
        Talento sol = new Talento();
//        int i = 0;
        if (usu == 0) {
            sol.setNomcargo("");
            sol.setSucursal("");
        }
        try {
            this.Conectar();

            String sql = "SELECT c.car_nombre,h.car_id,h.ush_id,ub.ubi_id,u.usu_ci ci,(s.suc_direccion+' '+ub.ubi_nombre) sucursal, u.usu_sexo,h.car_anterior,"
                    + "case when (h.clasempleado = 'F') then 'REASIGNADO DE CARGO' WHEN (h.clasempleado = 'R') then 'RATIFICADO' when (h.clasempleado = 'N') then 'NUEVO' ELSE 'ANTIGUO' END clasempleado,"
                    + "case when (h.clasempleado = 'F') then 4 WHEN (h.clasempleado = 'R') then 3 when (h.clasempleado = 'N') then 7 ELSE 2 END mot_id,h.turno\n" +
                        "FROM tab_usuariohistorial h\n" +
                        "JOIN tab_cargo c ON h.car_id = c.car_id\n" +
                        "INNER JOIN tab_sucursales s ON h.suc_id = s.suc_id\n" +
                        "INNER JOIN tab_ubicacion ub ON s.ubi_id = ub.ubi_id\n" +
                        "INNER JOIN tab_usuario u ON u.usu_id = h.usu_id\n" +
                        "WHERE h.ush_id = "+usu;
//            logger.info("talento: "+sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);
            st = pst.executeQuery();
            while (st.next()) {

                sol.setNomcargo(st.getString("car_nombre"));
                sol.setSucursal(st.getString("sucursal"));
                sol.setUsu_idempleado(st.getInt("ush_id"));
                sol.setCar_id(st.getInt("car_id"));
                sol.setCi(st.getInt("ci"));
                sol.setUbi_id(st.getInt("ubi_id"));
                sol.setUsh_sexo(st.getString("usu_sexo"));
                sol.setCaranterior(st.getInt("car_anterior"));
                sol.setClasempleado(st.getString("clasempleado"));
                sol.setMot_id(st.getInt("mot_id"));
                sol.setMotivo(st.getInt("mot_id"));
                sol.setTurno(st.getString("turno"));
//                i++;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }

        return sol;
    }
}
