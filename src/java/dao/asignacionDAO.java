/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.Almacen;
import model.Asignacion;
import model.Asignacionsolicitud;
//import org.apache.log4j.Logger;

/**
 *
 * @author Wilmer
 */
public class asignacionDAO extends DAO {
//    private static final Logger logger = Logger.getLogger(almacenDAO.class);    
    public Asignacion leerasignacion(int as) throws Exception {

        ResultSet st1;
        ResultSet st2;
        Asignacion row = new Asignacion();;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_solicitud.sol_id,\n"
                    + "rtrim(usuariosolicitante.usu_nombre) +' ' + rtrim(usuariosolicitante.usu_apellido) AS nomsolicitante,\n"
                    + "rtrim(usuarioempleado.usu_nombre) + ' '+ rtrim(usuarioempleado.usu_apellido) AS nomempleado,\n"
                    + "dbo.tab_cargo.car_nombre\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_solicitud\n"
                    + "INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_solicitud.sol_id = dbo.tab_detalleasignacion.sol_id\n"
                    + "INNER JOIN dbo.tab_usuariohistorial AS solicitante ON solicitante.ush_id = dbo.tab_solicitud.usu_idsolicitante\n"
                    + "INNER JOIN dbo.tab_usuariohistorial AS empleado ON empleado.ush_id = dbo.tab_solicitud.usu_idempleado\n"
                    + "INNER JOIN dbo.tab_usuario AS usuariosolicitante ON usuariosolicitante.usu_id = solicitante.usu_id\n"
                    + "INNER JOIN dbo.tab_usuario AS usuarioempleado ON usuarioempleado.usu_id = empleado.usu_id\n"
                    + "INNER JOIN dbo.tab_cargo ON empleado.car_id = dbo.tab_cargo.car_id\n"
                    + "WHERE\n"
                    + "dbo.tab_solicitud.sol_id = 14");
            st1 = pst.executeQuery();
            while (st1.next()) {

                row.setSol_id(st1.getInt("sol_id"));
                row.setNomempleado(st1.getString("nomempleado"));
                row.setNomsolicitante(st1.getString("nomsolicitante"));
                row.setCar_nombre(st1.getString("car_nombre"));
            }
            PreparedStatement pst1 = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_solicitud.sol_id,\n"
                    + "dbo.tab_solicitud.sol_fecha,\n"
                    + "dbo.tab_almacen.al_genero,\n"
                    + "dbo.tab_almacen.al_talla,\n"
                    + "dbo.tab_almacen.al_color,\n"
                    + "dbo.tab_tipoprenda.tip_nombre\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_almacen\n"
                    + "INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "INNER JOIN dbo.tab_solicitud ON dbo.tab_solicitud.sol_id = dbo.tab_detalleasignacion.sol_id\n"
                    + "INNER JOIN dbo.tab_grupoclasetrabajo ON dbo.tab_almacen.gclas_id = dbo.tab_grupoclasetrabajo.gclas_id\n"
                    + "INNER JOIN dbo.tab_tipoprenda ON dbo.tab_tipoprenda.tip_id = dbo.tab_grupoclasetrabajo.tip_id\n"
                    + "WHERE\n"
                    + "dbo.tab_solicitud.sol_id = 14");
            st2 = pst.executeQuery();
            String[] fech;
            while (st2.next()) {
                fech = (st2.getString("sol_fecha")).split("-");
                row.setNomempleado(st1.getString("nomempleado"));
                row.setNomsolicitante(st1.getString("nomsolicitante"));
                row.setCar_nombre(st1.getString("car_nombre"));
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return row;
    }

    public List<Asignacion> listarasignaciones(String pal) throws Exception {

        List<Asignacion> arr = new ArrayList<>();
        ResultSet rs;
        int i = 0;
        try {
            this.Conectar();
            System.out.println("asignacion");
            String getSql = "select s.sol_id,u.usu_nombre+' '+u.usu_apellido+' ('+c.car_nombre+')' nomusuario_solicitante,u2.usu_nombre+' '+u2.usu_apellido+' ('+c2.car_nombre+')' nomusuario_empleado,\n" +
"m.mot_nombremotivo nomsolicitud,c.car_nombre nomcargo ,s.sol_fecha,s.sol_estado\n" +
"from tab_solicitud s \n" +
"join tab_usuariohistorial h on s.usu_idsolicitante = h.ush_id\n" +
"join tab_usuario u on u.usu_id = h.usu_id\n" +
"join tab_usuariohistorial h2 on s.usu_idempleado = h2.ush_id\n" +
"join tab_usuario u2 on u2.usu_id = h2.usu_id\n" +
"join tab_motivodescuento m on m.mot_id = s.mtv_id\n" +
"join tab_cargo c on c.car_id = h.car_id\n" +
"join tab_cargo c2 on c2.car_id = h2.car_id\n" +
"where s.sol_estado = 1 and s.mtv_id not in (3,4,6) "
                    + "and ( u.usu_nombre like '%"+pal+"%' or u.usu_apellido like '%"+pal+"%' or u2.usu_nombre like '%"+pal+"%' or u2.usu_apellido like '%"+pal+"%' ) "
                    + "order by s.sol_id desc";
            System.out.println(getSql);
//            logger.info("Lista primera: "+getSql);
            PreparedStatement st = this.getCn().prepareCall(getSql);
            rs = st.executeQuery();
            String[] fecha;
            while (rs.next()) {
                //Inyectar aqui el web service automático
                Asignacion asig = new Asignacion();
                i++;
                asig.setSol_id(rs.getInt("sol_id"));
                asig.setNomsolicitante(rs.getString("nomusuario_solicitante"));
                asig.setNomempleado(rs.getString("nomusuario_empleado"));
                asig.setCar_nombre(rs.getString("nomcargo"));
                fecha = rs.getString("sol_fecha").split("-");
                asig.setFechasolicitud(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);
                asig.setSol_estado(rs.getInt("sol_estado"));
                asig.setAlfanumeric(i);
                asig.setMotivo_solicitud(rs.getString("nomsolicitud"));
                arr.add(asig);

            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    /**
     * 
     * @param id solicitud
     * @return
     * @throws Exception 
     * @description Lista la cabecera: Empleado, Cargo, sucursal, Fecha de Solicitud, Solicitante
     */
    public Asignacionsolicitud listarasignacionesdetalle(int id) throws Exception {
        Asignacionsolicitud asig = null;

        ResultSet rs;
        try {
            this.Conectar();
                String getsql = "SELECT (RTRIM(u.usu_nombre) + ' ' +RTRIM(u.usu_apellido) ) nomusuario_empleado,c.car_nombre nomcargo,(RTRIM(su.suc_direccion) +' '+RTRIM(b.ubi_nombre)) sucursal,\n" +
                "h.ush_fingreso fechai,h.ush_ffinal fechaf,(RTRIM(u2.usu_nombre) + ' ' +RTRIM(u2.usu_apellido) ) nomusuario_solicitante,\n" +
                "s.sol_id,s.sol_fecha,s.usu_idsolicitante,s.usu_idempleado,s.sol_fechaasignacion,s.sol_fechadevolucion,s.sol_descripcion,\n" +
                "s.sol_estado,suc_direccion,b.ubi_id,b.ubi_nombre,c.car_id,s.mtv_id,u.usu_nombre,u.usu_apellido,s.sol_nroform,md.mot_nombremotivo,s.sol_nroform,"
                + "u.usu_ci,CASE WHEN u.usu_sexo = 'M' THEN 'Masculino' WHEN u.usu_sexo = 'F' THEN 'Femenino' END usu_sexo,dv.division\n" +
                "FROM tab_solicitud s \n" +
                "JOIN tab_usuariohistorial h ON h.ush_id = s.usu_idempleado\n" +
                "JOIN tab_cargo c on c.car_id = h.car_id\n" +
                "JOIN tab_sucursales su on su.suc_id = h.suc_id\n" +
                "JOIN tab_usuario u on u.usu_id = h.usu_id\n" +
                "JOIN tab_ubicacion b on b.ubi_id = su.ubi_id\n" +
                "JOIN tab_usuariohistorial h2 ON h2.ush_id = s.usu_idsolicitante\n" +
                "JOIN tab_usuario u2 on u2.usu_id = h2.usu_id\n" +
                "JOIN tab_motivodescuento md on md.mot_id = s.mtv_id \n" +
                "JOIN tab_division dv on h.division = dv.div_id \n" +
                "where s.sol_id = "+id+" \n" +
                "AND EXISTS(SELECT * FROM tab_detalleasignacion where sol_id = "+id+" AND deta_estadoprenda = 1)";
//                logger.info("Lista Cabecera: "+getsql);
            PreparedStatement st = this.getCn().prepareCall(getsql);
            rs = st.executeQuery();
            String[] fecha;
            while (rs.next()) {
                asig = new Asignacionsolicitud();
                asig.setSol_id(rs.getInt("sol_id"));
                asig.setNomsolicitante(rs.getString("nomusuario_solicitante"));
                asig.setNomempleado(rs.getString("nomusuario_empleado"));
                asig.setCar_nombre(rs.getString("nomcargo"));
                asig.setSucursal(rs.getString("sucursal"));
                asig.setCi(rs.getString("usu_ci"));
                asig.setGenero(rs.getString("usu_sexo"));
                asig.setDpto(rs.getString("division"));
                asig.setSol_fechaasignacion(rs.getString("sol_fechaasignacion"));
                asig.setSol_fechadevolucion(rs.getString("sol_fechadevolucion"));
                asig.setFechai(rs.getString("fechai"));
                asig.setFechaf(rs.getString("fechaf"));
                asig.setSol_estado(rs.getInt("sol_estado"));
                asig.setApellidos(rs.getString("usu_apellido"));
                asig.setNombres(rs.getString("usu_nombre"));
                asig.setArea(rs.getString("suc_direccion"));
                asig.setDepartamento(rs.getString("ubi_nombre"));
                asig.setUsu_idempleado(rs.getInt("usu_idempleado"));
                asig.setCar_id(rs.getInt("car_id"));
                fecha = rs.getString("sol_fecha").split("-");
                asig.setFechasolicitud(fecha[2] + "-" + fecha[1] + "-" + fecha[0]);
                asig.setUbi_id(rs.getInt("ubi_id"));
                asig.setNroform(rs.getInt("sol_nroform"));
                asig.setMotivosolicitud(rs.getString("mot_nombremotivo"));
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return asig;
    }

    public int verificardobleasignado(int sol_id, int idemp) throws Exception {
        int j = 0;
        int al_id = 0;
        ResultSet st;
        ResultSet st1;
        try {
            this.Conectar();

            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_solicitud.usu_idempleado,\n"
                    + "dbo.tab_solicitud.usu_idsolicitante,\n"
                    + "dbo.tab_usuariohistorial.rol_id\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_almacen\n"
                    + "INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "INNER JOIN dbo.tab_solicitud ON dbo.tab_solicitud.sol_id = dbo.tab_detalleasignacion.sol_id\n"
                    + "INNER JOIN dbo.tab_usuariohistorial ON dbo.tab_usuariohistorial.ush_id = dbo.tab_solicitud.usu_idsolicitante\n"
                    + "WHERE\n"
                    + "dbo.tab_almacen.al_id = " + al_id + " AND\n"
                    + "dbo.tab_solicitud.usu_idempleado = " + idemp + " AND\n"
                    + "dbo.tab_detalleasignacion.deta_estadoprenda = 2");
            st = pst.executeQuery();
            while (st.next()) {
                j = 1;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return j;
    }

    public int verificarCantidadItem(int sol_id) throws Exception {
        ResultSet st;
        int i = 0;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_almacen.al_cantidad\n"
                    + "FROM\n"
                    + "dbo.tab_almacen\n"
                    + "INNER JOIN dbo.tab_detalleasignacion ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "WHERE\n"
                    + "dbo.tab_detalleasignacion.sol_id = " + sol_id + " and tab_almacen.al_cantidad=0");
            st = pst.executeQuery();
            while (st.next()) {
                i = 1;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return i;
    }

    /**
     * 
     * @param idsol Solicitud
     * @param usu_id empleado
     * @return lista de items o prendas por asignar (que su estado sea 1)
     * @throws Exception 
     */
    public ArrayList<Almacen> listarAlmacenOneRow(int idsol, int usu_id) throws Exception {
        ArrayList arr = new ArrayList();
        ResultSet st;
        String sql;
//        int al_id;
        int i = 0;
        try {
            this.Conectar();
            
            sql = "select p.igr_id,a.al_id,a.ubi_id,p.pro_itemname,pro_itemcode,p.pro_unidad,pro_color,d.deta_id,d.deta_cantidadsolicitada, \n" +
"p.pro_descripcion, p.pro_genero, p.pro_talla, p.pro_logo,p.pro_material,p.pro_vidautil,a.al_cantidad,a.al_costo,tr.trp_nombre,\n" +
"1 restriccion,case when at.deta_id IS NULL then 0 else 1 end asignado  \n" +
"from tab_detalleasignacion d \n" +
"JOIN tab_almacen a on a.al_id = d.al_id \n" +
"JOIN tab_producto p on p.pro_id = a.pro_id\n" +
"JOIN tab_tiporopaprenda tr ON p.trp_id = tr.trp_id\n" +
"JOIN tab_solicitud s on s.sol_id = d.sol_id \n" +
"LEFT JOIN tab_asignaciontemp at on at.deta_id = d.deta_id\n" +
"where d.deta_estadoprenda = 1 AND a.al_estado = 1 and s.mtv_id not in (3,4,6) and d.sol_id= "+idsol;

//            logger.info("despues de asignar: "+sql);
            System.out.println(sql);
            PreparedStatement pst = this.getCn().prepareCall(sql);
            st = pst.executeQuery();
            while (st.next()) {
//                logger.info(st.getInt("al_id")+", "+st.getString("pro_itemname")+", "+st.getInt("restriccion"));
                Almacen al = new Almacen();
                al.setAl_id(st.getInt("al_id"));
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_material(st.getString("pro_material"));
                al.setAl_cantidad(st.getInt("al_cantidad"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_genero(st.getString("pro_genero"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setDeta_id(st.getInt("deta_id"));
                al.setIgr_id(st.getInt("igr_id"));
                al.setAl_cantidad(st.getInt("al_cantidad"));
                al.setAlfanumeric(++i);
                al.setRestriccion(st.getInt("restriccion"));
                al.setAsignado(st.getInt("asignado"));
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }

    public ArrayList<Almacen> listarAlmacenReporte(int ush_id) throws Exception {
        ArrayList arr = new ArrayList();
//        String where = null;
        ResultSet st;
        int al_id = 0;
        int cant = 0;
        int i = 0;
        int u = 0;
//        ResultSet st1;
        String sql;
        try {
            this.Conectar();
            sql = "select s.sol_id,u.usu_nombre +' '+u.usu_apellido almacenero,\n" +
"a.al_id,a.ubi_id,p.pro_itemname,pro_itemcode,p.pro_unidad,pro_color,d.deta_id,d.deta_cantidadsolicitada, \n" +
"p.pro_descripcion, p.pro_genero, p.pro_talla, p.pro_logo,p.pro_material,p.pro_vidautil,a.al_cantidad,round(a.al_costo/0.87,0) al_costo,tr.trp_nombre,1 cantidad \n" +
"from tab_asignaciontemp at \n" +
"join tab_detalleasignacion d on d.deta_id = at.deta_id\n" +
"join tab_solicitud s on s.sol_id = d.sol_id\n" +
"join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
"join tab_usuario u on u.usu_id = h.usu_id\n" +
"join tab_almacen a on a.al_id = d.al_id\n" +
"join tab_producto p on p.pro_id = a.pro_id\n" +
"join tab_tiporopaprenda tr on p.trp_id = tr.trp_id\n" +
"where a.al_cantidad > 0 and a.al_estado = 1 and at.almacen_id = "+ush_id;
//            logger.info("Reporte: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            st = pst.executeQuery();
            while (st.next()) {
                i++;
                Almacen al = new Almacen();
                al.setAl_id(st.getInt("al_id"));
                al_id = st.getInt("al_id");
                al.setPro_itemname(st.getString("pro_itemname"));
                al.setPro_descripcion(st.getString("pro_descripcion"));
                al.setPro_itemcode(st.getString("pro_itemcode"));
                al.setPro_material(st.getString("pro_material"));
                al.setAl_cantidad(st.getInt("al_cantidad"));
                al.setPro_color(st.getString("pro_color"));
                al.setPro_genero(st.getString("pro_genero"));
                al.setPro_talla(st.getString("pro_talla"));
                al.setPro_vidautil(st.getInt("pro_vidautil"));
                al.setPro_logo(st.getString("pro_logo"));
                al.setDeta_id(st.getInt("deta_id"));
                al.setPro_unidad(st.getString("pro_unidad"));
                al.setCantidad(st.getInt("cantidad"));
                al.setAl_costo(st.getFloat("al_costo"));
                cant = st.getInt("al_cantidad");
                al.setSol_id(st.getInt("sol_id"));
                al.setAlfanumeric(i);
                if (cant == 0) {
                    u = 1;
                }
                al.setEstadoasignar(u);
                al.setTrp_nombre(st.getString("trp_nombre"));
                al.setCantasignado(st.getInt("deta_cantidadsolicitada"));
                al.setUsuarioalmacen("almacenero");
                arr.add(al);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    /**
     * *
     * @param id Solicitud
     * @param al id del almacen
     * @throws Exception 
     * @Description Realiza las asignaciones de una prenda o item calculando: el stock inicial y final, cambia de estado en las asignaciones
     */
    public void updateSolicitudItem(int id, Almacen al, int usu_id) throws Exception {
        String sql;

        try {
            this.Conectar();
                sql = "update tab_detalleasignacion set deta_estadoprenda=2, deta_fechaasignacion = GETDATE(),"
                        + "deta_fechatentativa = (select DATEADD(MONTH,p.pro_vidautil,GETDATE()) fecha from tab_almacen a "
                        + "join tab_producto p on a.pro_id = p.pro_id where a.al_id = " + al.getAl_id() + ") where deta_id="+al.getDeta_id();
//                logger.info("actualiza estado asignacion detalle: "+sql);
                PreparedStatement pstupdateitem = this.getCn().prepareCall(sql);
                pstupdateitem.executeUpdate();
                //Actualizamos el nuevo stock en el almacen
                sql = "update tab_almacen set al_cantidad=al_cantidad-1 where al_id = "+al.getAl_id();
//                logger.info("actualiza stock: "+sql);
                PreparedStatement pstalmacen = this.getCn().prepareCall(sql);
                pstalmacen.executeUpdate();
                //Si no existe ningun item o prenda pendiente de detalleasignacion entonces ela solicitud habrá finalizado
                sql = "update tab_solicitud set sol_fechaasignacion=getdate(),sol_estado=2 where sol_id=" + id +" AND NOT EXISTS (\n" +
                    "select * from tab_detalleasignacion where sol_id = "+id+" and deta_estadoprenda = 1)";
//                logger.info("pase por aqui: "+sql);
                PreparedStatement pst = this.getCn().prepareCall(sql);
                pst.executeUpdate();
                //Insertamos en el Log
                sql = "insert into tab_histoasignadevuelve values ("+al.getDeta_id()+",GETDATE(),'A',"+usu_id+")";
                pst = this.getCn().prepareStatement(sql);
                pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    /**
     * 
     * @param deta_id
     * @param almacen_id 
     * @description Registra datos temporalmente en la tabla
     */
    public void upateAsignaciontemp(int deta_id, int almacen_id) throws Exception{
        String sql;
        PreparedStatement pst;
        try {
            this.Conectar();
            sql = "insert into tab_asignaciontemp values ("+deta_id+","+almacen_id+")";
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally{
            this.Cerrar();
        }
    }
    /**
     * 
     * @param almacen_id
     * @throws Exception 
     * @description Elimina todas las solicitudes de un usuario de almacen
     */
    public void deleteDevolucionTemporal (int almacen_id) throws Exception{
        String sql;
        try {
            this.Conectar();
            sql = "delete from tab_asignaciontemp where almacen_id = "+almacen_id;
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
    
    public void updateSolicitud(int id, String fechaf) throws Exception {
        java.util.Date fecha = new Date();
        long lnMilisegundos = fecha.getTime();
        java.sql.Date sqlDate = new java.sql.Date(lnMilisegundos);
        ResultSet st;
        int deta_id = 0;
        int cantidad = 0;
        int al_id = 0;
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareCall("update tab_solicitud set sol_fechaasignacion='" + sqlDate + "',sol_estado=2 where sol_id=" + id);
            pst.executeUpdate();

            PreparedStatement pstsearh = this.getCn().prepareCall("SELECT\n"
                    + "dbo.tab_detalleasignacion.deta_id,\n"
                    + "dbo.tab_detalleasignacion.sol_id,\n"
                    + "dbo.tab_detalleasignacion.al_id,\n"
                    + "dbo.tab_detalleasignacion.deta_estadoprenda,\n"
                    + "dbo.tab_detalleasignacion.deta_fechatentativa,\n"
                    + "dbo.tab_detalleasignacion.deta_fechadevolucion,\n"
                    + "dbo.tab_almacen.al_cantidad\n"
                    + "\n"
                    + "FROM\n"
                    + "dbo.tab_detalleasignacion\n"
                    + "INNER JOIN dbo.tab_almacen ON dbo.tab_almacen.al_id = dbo.tab_detalleasignacion.al_id\n"
                    + "WHERE\n"
                    + "dbo.tab_detalleasignacion.sol_id =" + id);
            st = pstsearh.executeQuery();
            while (st.next()) {
                deta_id = st.getInt("deta_id");
                al_id = st.getInt("al_id");
                cantidad = (st.getInt("al_cantidad")) - 1;

                PreparedStatement pstalmacen = this.getCn().prepareCall("update tab_almacen set al_cantidad=" + cantidad + " where al_id=" + al_id);
                pstalmacen.executeUpdate();

                PreparedStatement pstupdateitem = this.getCn().prepareCall("update tab_detalleasignacion set deta_estadoprenda=2 where deta_id=" + deta_id);
                pstupdateitem.executeUpdate();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

    /**
     * @param al Atributos completos de la tabla tab_detalleasignacion y tab_almacen
     * @param detaid id de la tabla tab_detalleasignacino (deta_id)
     * @throws Exception 
     */
    public void editarAlmacenAsignacion(Almacen al, int detaid) throws Exception {
        try {
            this.Conectar();
            String sql = "update tab_detalleasignacion set al_id=? where deta_id=" + detaid + " and deta_estadoprenda=1";
//            logger.info("otra prenda: "+sql);
            PreparedStatement pst = this.getCn().prepareStatement(sql);
            pst.setInt(1, al.getAl_id());
            pst.executeUpdate();
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }
    /**
     * @Description Cierra todas solicitudes que fueron autogenerados por los webservice
     * Cierra las solicitudes de Ratificación y Reasignación de cargo
     * @throws Exception 
     */
    public void cerrarSolicitudes() throws Exception {
        String sql;
        PreparedStatement pst;
        try {
            this.Conectar();
            //Cerrando solicitudes autogenerados por web service
//            sql = "WITH UpdateList_view AS (\n" +
//                "select * from tab_solicitud s where not exists (select * from tab_detalleasignacion where sol_id = s.sol_id and deta_estadoprenda = 2)\n" +
//                ")\n" +
//                "update UpdateList_view \n" +
//                "set sol_estado = 0;";
//            pst = this.getCn().prepareStatement(sql);
//            pst.executeUpdate();
            //cerrando solicitudes de RAtificaicón y Reasignació de CArgo
            sql = "update tab_solicitud set sol_estado = 0 where mtv_id in (3,4)";
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();
            sql = "WITH UpdateList_view AS (\n"
                    + "	select * from tab_solicitud s where s.sol_id not in (select sol_id from tab_detalleasignacion where sol_id = s.sol_id)\n"
                    + ")\n"
                    + "update UpdateList_view \n"
                    + "set sol_estado = 0";
            pst = this.getCn().prepareStatement(sql);
            pst.executeUpdate();            
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

}
