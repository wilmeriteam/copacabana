package dao;

import java.sql.PreparedStatement;
import model.Almacen;

public class detalleasignacionDAO extends DAO {
//TEMPORAL MENTE MODIFICADO
    public void eliminarDetalleAsignacion(Almacen deta) throws Exception {
     
        try {
            this.Conectar();
            PreparedStatement pst = this.getCn().prepareStatement("delete from dbo.tab_detalleasignacion where deta_id=? ");
            pst.setInt(1, deta.getDeta_id());
            pst.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
    }

}
