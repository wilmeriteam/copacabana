package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Area;

/**
 *
 * @author Wilmer
 */
public class areaDAO extends DAO {
    public List<Area> listarAreas() throws Exception {

        List<Area> arr = new ArrayList<>();
        ResultSet rs;

        try {
            this.Conectar();
            PreparedStatement st = this.getCn().prepareCall("select * from tab_area");
            rs = st.executeQuery();
            while (rs.next()) {
                Area are = new Area();
                are.setId(rs.getInt("area_id"));
                are.setArea(rs.getString("area"));
                are.setActivo(rs.getInt("activo"));
                arr.add(are);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
}
