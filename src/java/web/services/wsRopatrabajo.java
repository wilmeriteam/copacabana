/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Wilmer
 */
public class wsRopatrabajo extends DAO {

    public void updateInsertRopatrabajo() throws JSONException, Exception {
        String json = hello("Wil");

        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
//        String[][] cargo = new String[jsonArray.length()][2];
        int estado = 0;
        //Obtenemos los datos Json en la variable cargo
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            try {
                Conectar();
                this.getCn().setAutoCommit(false);
                PreparedStatement st = this.getCn().prepareCall("select ropatrab_id,tpe_id,u_item,u_cantidad from tab_ropatrabajo");
                rs = st.executeQuery();

                estado = 0;
                while (rs.next()) {
                    if (Integer.parseInt(rec.getString("ropatrab_id")) == rs.getInt("ropatrab_id")) {
                        PreparedStatement pst = getCn().prepareCall("update tab_ropatrabajo set tpe_id='" + Integer.parseInt(rec.getString("tipopersonal_id"))
                                + "', u_item = '" + rec.getString("u_item")
                                + "', u_cantidad = '" + rec.getInt("cantidad")
                                + "' where ropatrab_id = " + Integer.parseInt(rec.getString("ropatrab_id")));
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar un insert
                if (estado == 0) {
                    String inserta = "INSERT INTO dbo.tab_ropatrabajo values(" + Integer.parseInt(rec.getString("ropatrab_id"))
                            + "," + Integer.parseInt(rec.getString("tipopersonal_id")) + ",'" + rec.getString("u_item") + "',"
                            + rec.getString("cantidad") + ")";
                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
        }

        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) {
        web.service.NewWebServiceRopatrabajo_Service service = new web.service.NewWebServiceRopatrabajo_Service();
        web.service.NewWebServiceRopatrabajo port = service.getNewWebServiceRopatrabajoPort();
        return port.hello(name);
    }

}
