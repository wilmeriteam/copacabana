/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class wsProductoPersonal extends DAO{
    public void updateInsertProducto(String cargo, String itemcode) throws JSONException, Exception{
        String json = hello(cargo);
        String json2 = hello_1(itemcode);
        
        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
        int estado = 0;

        //Obtenemos los datos Json en la variable cargo
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            try{
            Conectar();
            PreparedStatement st = this.getCn().prepareCall("SELECT pro_itemcode FROM tab_producto");
            rs = st.executeQuery();
//            String nombreCargo = null;
            estado = 0;

            String color = (rec.getString("color").equals("NULO")) ? "" : rec.getString("color");
            String descripcion = (rec.getString("descripcion").equals("NULO")) ? "" : rec.getString("descripcion");
            String genero = (rec.getString("genero").equals("UNISEX")) ? "" : rec.getString("genero");
            String talla = (rec.getString("talla").equals("NULO")) ? "" : rec.getString("talla");
            String logo = (rec.getString("logo").equals("NULO")) ? "" : rec.getString("logo");
            String material = (rec.getString("material").equals("NULO")) ? "" : rec.getString("material");

            while(rs.next()){
                if(rec.getString("itemcode").equals(rs.getString("pro_itemcode")) ){
                    String actualiza = "UPDATE tab_producto set "
                                + "pro_itemcode='" + rec.getString("itemcode") + "', "
                                + "pro_itemname='" + rec.getString("itemname") + "', "
                                + "pro_unidad='" + rec.getString("unidad") + "', "
                                + "pre_id= dbo.ufnGetTipoPrenda(RTRIM('" + rec.getString("prenda_id") + "')), "
                                + "pro_color='" + color + "', "
                                + "pro_descripcion='" + descripcion + "', "
                                + "pro_genero='" + genero + "', "
                                + "pro_talla='" + talla + "', "
                                + "pro_logo='" + logo + "', "
                                + "pro_material='" + material + "', "
                                + "trp_id=dbo.ufnGetTipoRopaPrenda(RTRIM('" + rec.getString("tiporopaprenda_id") + "')), "
                                + "pro_vidautil=" + Integer.parseInt(rec.getString("vidautil")) + ", "
                                + "igr_id=dbo.ufnGetItemGrupo(RTRIM('" + rec.getString("itemgrupo") + "')), "
                                + "pro_estado=" + rec.getString("estado") 
                                + " where pro_itemcode = '" + rec.getString("itemcode") + "'";
                    System.out.println(actualiza);
                    PreparedStatement pst = getCn().prepareCall(actualiza);
                    pst.executeUpdate();
                    estado = 1;
                }
            }
            //El estado 0 Significa que el registro no existe en la BD y debemos realizar un insert
            if(estado == 0){
                //Si no existiera el registro procedemos a la inserción del mismo
                String inserta = "INSERT INTO dbo.tab_producto "
                        + "values('" 
                        + rec.getString("itemcode") + "',"
                        + "'" + rec.getString("itemname")+"',"
                        + "'" + rec.getString("itemcode") + "',"
                        + "'" + rec.getString("itemname")+"',"
                        + "'" + rec.getString("unidad") + "',"
                        + "dbo.ufnGetTipoPrenda(RTRIM('" + rec.getString("prenda_id")+"')),"
                        + "'" + color + "',"
                        + "'" + descripcion+"',"
                        + "'" + genero + "',"
                        + "'" + talla+"',"
                        + "'" + logo + "',"
                        + "'" + material+"',"
                        + "dbo.ufnGetTipoRopaPrenda(RTRIM('" + rec.getString("tiporopaprenda_id") + "')), "
                        + Integer.parseInt(rec.getString("vidautil")) + ","
                        + "dbo.ufnGetItemGrupo(RTRIM('" + rec.getString("itemgrupo") + "')), "
                        + "'" + rec.getString("estado")+"')";
                System.out.println(inserta);
                PreparedStatement pst = getCn().prepareStatement(inserta);
                pst.executeUpdate();
                
                //Ahora registramos el producto en almacen
                JSONArray jsonAlmacen = new JSONArray(json2);
                
                for (int j = 0; j < jsonAlmacen.length(); j++) {
                    JSONObject datoAlmacen = jsonAlmacen.getJSONObject(j);
//                    System.out.println("esto es cantidad"+datoAlmacen.getInt("cantidad"));

                    String insertaalm = "INSERT INTO dbo.tab_almacen VALUES ("
                            + datoAlmacen.getInt("cantidad") + ","
                            + datoAlmacen.getDouble("costo") + ","
                            + "(select ubi_id from tab_ubicacion where sigla = '"+datoAlmacen.getString("ubicacio_id")+"'),"
                            + "(select pro_id FROM tab_producto where pro_itemcode = '"+datoAlmacen.getString("producto_id")+"')" + ","
                            + "(select trp_id from tab_tiporopaprenda where trp_nombre = RTRIM('"+datoAlmacen.getString("tiporopaprenda_id")+"'),"
                            + datoAlmacen.getInt("estado")+","
                            + ")";
                    System.out.println(datoAlmacen);
                    PreparedStatement pst2 = getCn().prepareStatement(insertaalm);
                    pst2.executeUpdate();
                }
            }
                
            } catch (Exception e) {
                throw e;
            } finally {
                Cerrar();
            }
        }   
        
        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) {
        web.service.WebServiceProdPersonal_Service service = new web.service.WebServiceProdPersonal_Service();
        web.service.WebServiceProdPersonal port = service.getWebServiceProdPersonalPort();
        return port.hello(name);
    }

    private static String hello_1(java.lang.String name) {
        web.service.WebServiceAlmacenPersonal_Service service = new web.service.WebServiceAlmacenPersonal_Service();
        web.service.WebServiceAlmacenPersonal port = service.getWebServiceAlmacenPersonalPort();
        return port.hello(name);
    }
   
}
