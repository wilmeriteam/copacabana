/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Administrador
 */
public class wsOficinas extends DAO {

    public void updateInsertOficina() throws JSONException, Exception {
        String json = hello("Wil");
        String sql;
        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
//        String[][] cargo = new String[jsonArray.length()][2];
        int estado = 0;

        //Obtenemos los datos Json en la variable cargo
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
//            cargo[i][0] = rec.getString("cargo");
//            cargo[i][1] = rec.getString("estado");
            try {
                Conectar();
                this.getCn().setAutoCommit(false);
                PreparedStatement st = this.getCn().prepareCall("select suc_id,suc_direccion,suc_estado,ubi_id from tab_sucursales");
                rs = st.executeQuery();
//            String nombreCargo = null;
                estado = 0;
                while (rs.next()) {
//                int contid = Integer.parseInt(rec.getString("id"));
                    if (Integer.parseInt(rec.getString("id")) == rs.getInt("suc_id")) {
                        sql = "update tab_sucursales set suc_direccion='" + rec.getString("nombre")
                                + "', suc_estado = " + Integer.parseInt(rec.getString("estado"))
                                + ", ubi_id = " + Integer.parseInt(rec.getString("ubi_id"))
                                + " where suc_id = " + Integer.parseInt(rec.getString("id"));
                        System.out.println(sql);
                        PreparedStatement pst = getCn().prepareCall(sql);
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar un insert
                if (estado == 0) {
                     sql = "INSERT INTO dbo.tab_sucursales values(" + rec.getInt("id") + ",'" + rec.getString("nombre") + "'," + rec.getInt("estado") + "," + rec.getInt("ubi_id") + ")";
                    System.out.println(sql);
                    PreparedStatement pst = getCn().prepareStatement(sql);
                    pst.executeUpdate();
                }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
        }

        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) {
        web.service.WebServiceOficinas_Service service = new web.service.WebServiceOficinas_Service();
        web.service.WebServiceOficinas port = service.getWebServiceOficinasPort();
        return port.hello(name);
    }
}
