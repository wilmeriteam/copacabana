/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Wilmer
 */
public class wsAlmacen extends DAO {

    public void updateInsertAlmacen() throws JSONException, Exception {
        String json = hello("Wil");

        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
        int estado = 0;

        //Obtenemos los datos Json en la variable cargo
        try {
        Conectar();
        this.getCn().setAutoCommit(false);
            
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
                
                PreparedStatement st = this.getCn().prepareCall(
                        "select a.al_id,a.al_cantidad,a.al_costo,u.sigla,p.pro_itemcode,a.al_estado \n"
                        + "from tab_almacen a join tab_ubicacion u on u.ubi_id = a.ubi_id\n"
                        + "join tab_producto p on p.pro_id = a.pro_id;");
                rs = st.executeQuery();

                estado = 0;
                while (rs.next()) {
                    if (rec.getString("producto_id").equals(rs.getString("pro_itemcode"))
                            && rec.getString("ubicacio_id").equals(rs.getString("sigla"))) {
                        String sql = "update tab_almacen set al_cantidad = " + rec.getInt("cantidad") + ", al_costo = " + rec.getDouble("costo") + "\n"
                                + "from tab_almacen a join tab_producto p on p.pro_id = a.pro_id\n"
                                + "join tab_ubicacion u on u.ubi_id = a.ubi_id\n"
                                + "where u.sigla = '" + rec.getString("ubicacio_id") + "' and p.pro_itemcode = '" + rec.getString("producto_id") + "'";
                        PreparedStatement pst = getCn().prepareCall(sql);
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar un insert
                if (estado == 0) {
//                String inserta = "INSERT INTO dbo.tab_almacen values("+rec.getInt("id")+",'" + rec.getString("nombre") + "',1)";
                    String inserta = "insert into tab_almacen \n"
                            + "select " + rec.getInt("cantidad") + "," + rec.getDouble("costo") + ",(select ubi_id from tab_ubicacion where sigla = '" + rec.getString("ubicacio_id") + "'),p.pro_id,1 \n"
                            + "from tab_producto p where p.pro_itemcode = '" + rec.getString("producto_id") + "'";
                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
            }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
//            System.out.println(cargo[i][0]+" - "+cargo[i][1]);
//        }
        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) {
        web.service.WebServiceAlmacen_Service service = new web.service.WebServiceAlmacen_Service();
        web.service.WebServiceAlmacen port = service.getWebServiceAlmacenPort();
        return port.hello(name);
    }
}
