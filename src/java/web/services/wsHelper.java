/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import dao.almacenDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
//import org.apache.log4j.Logger;
//import org.apache.log4j.Priority;
import static web.services.wsEmpleado.DATE_FORMAT;

/**
 *
 * @author Wilmer
 */
public class wsHelper extends DAO{
//    private static final Logger logger = Logger.getLogger(almacenDAO.class);
    /**
     * 
     * @param g3ffinal
     * @param ush_id
     * @throws SQLException
     * @throws Exception 
     * @Description Ejecuta un proeceso de desviculación Laboral
     */
    public void desvinculacionlaboral (String g3ffinal, int ush_id) throws SQLException, Exception {
        PreparedStatement st;
        ResultSet rs;        
        String sql;
        int idsol = 0;
        Conectar();
//        logger.info("Ingresamos a wsHelper");
        if(this.isDateValid(g3ffinal)){
            //Cambiamos de estado cualquier solicitud pendiente del usuario
            sql = "UPDATE tab_solicitud set sol_estado = 2 where usu_idempleado = "+ush_id;
//            logger.info("Disvinculación Laboral: "+sql);
            System.out.println(sql);
            st = this.getCn().prepareStatement(sql);
            st.executeUpdate();

            //Creamos una nueva solicitud "Desvinculación laboral"
            sql = "insert into dbo.tab_solicitud values(GETDATE(),(select MAX(ush_id) from tab_usuariohistorial where rol_id = 27),"
                    +ush_id+",NULL,NULL,'Desvinculación Laboral - Autogenerado',1,6,(select max(sol_nroform)+ 1 from tab_solicitud),NULL)";
            System.out.println(sql);
            st = this.getCn().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
            st.executeUpdate();

            //obtenemos el id de la solicitud creada
            rs = st.getGeneratedKeys();
            while (rs.next()) {
                idsol = rs.getInt(1);
            }

            //Insertamos los detalles de la solicitud como Desvinculación laboral
            sql = "insert into tab_detalleasignacion \n" +
                        "select "+idsol+",d.al_id,1,NULL,NULL,1,GETDATE(),GETDATE(),d.deta_id from tab_solicitud s \n" +
                        "join tab_detalleasignacion d on d.sol_id = s.sol_id \n" +
                        "where usu_idempleado = "+ush_id+" AND d.deta_estadoprenda = 2";

            System.out.println(sql);

            st = this.getCn().prepareStatement(sql);
            st.executeUpdate();
            //Actualizamos el estado de cada prenda
//            sql = "update tab_detalleasignacion set deta_estadoprenda = 0\n" +
//                        "where deta_id in (select d.deta_id from tab_solicitud s \n" +
//                        "join tab_detalleasignacion d on d.sol_id = s.sol_id \n" +
//                        "where usu_idempleado = "+ush_id+" AND d.deta_estadoprenda = 2\n" +
//                        ")";
//            System.out.println(sql);
//            st = this.getCn().prepareStatement(sql);
//            st.executeUpdate();
            //cambiamos el estado del usuario
            sql = "UPDATE tab_usuariohistorial set ush_estado = 0,ush_ffinal='"+g3ffinal+"' where ush_id = "+ush_id;
            System.out.println(sql);
            st = this.getCn().prepareStatement(sql);
            st.executeUpdate();
        }   
    }
    
    /**
     * 
     * @param cargo_id
     * @param ush_id
     * @param ubi_id
     * @param sexo
     * @throws SQLException 
     * @Description Ejecuta un proceso de Reasignación de Cargo - Area
     */
    public void reasignacioncargo(int cargo_id, int ush_id, int ubi_id, String sexo, String clasempleado) throws SQLException, Exception{
        PreparedStatement st;
        ResultSet rs;
        String det;
        int cEmpleado = 0;
        if(clasempleado.equals("R")){
            cEmpleado = 3;
            det = "Ratificado - Autogenerado";
        } else {
            cEmpleado = 4;
            det = "Reasignación de Cargo - Area - Autogenerado";
        }
        String sql;
        int idsol = 0;
        int cant = 0;
//        int igr = 0;
        Conectar();
        //Si el empleado es 3-> Ratificado o 4->Reasignado de Cargo - Area
        if( cEmpleado == 0 ){
            //Creamos una solicitud de "Reasignacion de Cargo - Area"
            sql = "insert into dbo.tab_solicitud values(GETDATE(),1,"
                    +ush_id+",NULL,NULL,'"+ det +"',1,"+cEmpleado+",(select max(sol_nroform)+ 1 from tab_solicitud),NULL)";
            System.out.println(sql);
            st = this.getCn().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
            st.executeUpdate();
            //obtenemos el id de la solicitud creada
            rs = st.getGeneratedKeys();
            while (rs.next()) {
                idsol = rs.getInt(1);
            }
            //Generamos la devolución de todas las prendas
            sql = "insert into tab_detalleasignacion\n" +
                "select "+idsol+",d.al_id,1,null,null,1,null,GETDATE(),d.deta_id from tab_detalleasignacion d \n" +
                "join tab_solicitud s on s.sol_id = d.sol_id \n" +
                "join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
                "join tab_almacen a on a.al_id = d.al_id\n" +
                "join tab_producto p on p.pro_id = a.pro_id\n" +
                "where s.usu_idempleado = "+ush_id+" and d.deta_estadoprenda = 2";
            System.out.println(sql);
            st = getCn().prepareStatement(sql);
            st.executeUpdate();
            sql = "select aa.al_id,aa.igr_id,aa.cantidad,o.cant_nuevo,aa.cantidad-o.cant_nuevo total from (\n" +
                "	select max(d.al_id) al_id,count(d.al_id) cantidad,p.igr_id\n" +
                "	from tab_detalleasignacion d join tab_solicitud s on s.sol_id = d.sol_id \n" +
                "	join tab_almacen a on a.al_id = d.al_id\n" +
                "	join tab_producto p on p.pro_id = a.pro_id\n" +
                "	where s.sol_id = "+idsol+" group by p.igr_id\n" +
                ") aa left join (	\n" +
                "	select max(a.al_id) al_id,max(rt.u_cantidad) cant_nuevo,max(p.pro_itemcode) pro_itemcode,p.igr_id \n" +
                "	from tab_areatrabajocargo atc join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                "	join tab_producto p on p.pro_itemcode = rt.u_item\n" +
                "	join tab_almacen a on a.pro_id = p.pro_id and a.ubi_id = "+ubi_id+"\n" +
                "	where atc.car_id = "+cargo_id+" and (p.pro_genero = '"+sexo+"' or p.pro_genero = '') group by p.igr_id\n" +
                ") o on aa.igr_id = o.igr_id where cant_nuevo is not null";
            System.out.println(sql);
            st = getCn().prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getInt("total") <= 0){
                    sql = "delete from tab_detalleasignacion where sol_id = "+idsol+" and al_id = "+rs.getInt("al_id");
                    System.out.println("elimina 1: "+sql);
                    st = getCn().prepareStatement(sql);
                    st.executeUpdate();                
                } else if(rs.getInt("total") > 0){
                    sql = "delete top("+rs.getInt("cant_nuevo")+") from tab_detalleasignacion where sol_id = "+idsol+" and al_id = "+rs.getInt("al_id");
                    if(ush_id == 257){
                        System.out.println("total: "+ rs.getInt("total") +" consulta \n"+sql);
                    }
    //                System.out.println(sql);
                    st = getCn().prepareStatement(sql);
                    st.executeUpdate();                
                }
            }

            //Creamos una solicitud de "Dotación por Reasignación de Cargo - Area"
            sql = "insert into dbo.tab_solicitud values(GETDATE(),1,"
                    +ush_id+",NULL,NULL,'Dotación por Reasignación de Cargo - Area',1,7,(select MAX(sol_nroform)+1 from tab_solicitud),NULL)";
            System.out.println(sql);
            st = this.getCn().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
            st.executeUpdate();
            //obtenemos el id de la solicitud creada
            rs = st.getGeneratedKeys();
            while (rs.next()) {
                idsol = rs.getInt(1);
            }        
            sql = "select antiguo1.al_id al_id_antiguo,antiguo1.pro_itemcode,antiguo1.cantidad cant_antiguo,antiguo1.igr_id,nuevo1.al_id al_id_nuevo,nuevo1.cant_nuevo,nuevo1.pro_itemcode,nuevo1.igr_id igr_nuevo from (\n" +
            "	select max(a.al_id) al_id,max(rt.u_cantidad) cant_nuevo,max(p.pro_itemcode) pro_itemcode,p.igr_id from tab_areatrabajocargo atc join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
            "	join tab_producto p on p.pro_itemcode = rt.u_item\n" +
            "	join tab_almacen a on a.pro_id = p.pro_id and a.ubi_id = "+ubi_id+"\n" +
            "	where atc.car_id = "+cargo_id+" and (p.pro_genero = '"+sexo+"' or p.pro_genero = '') group by p.igr_id\n" +
            ") nuevo1 left join (\n" +
            "	select nuevo.al_id,nuevo.pro_itemcode,nuevo.igr_id,antiguo.cantidad from (\n" +
            "		select a.al_id,rt.u_cantidad,p.pro_itemcode,p.igr_id from tab_areatrabajocargo atc join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
            "		join tab_producto p on p.pro_itemcode = rt.u_item\n" +
            "		join tab_almacen a on a.pro_id = p.pro_id and a.ubi_id = "+ubi_id+"\n" +
            "		where atc.car_id = "+cargo_id+" and (p.pro_genero = '"+sexo+"' or p.pro_genero = '')\n" +
            "	) nuevo left join (\n" +
            "		select d.al_id,COUNT(d.al_id) cantidad,MAX(p.pro_itemcode) pro_itemcode,max(igr_id) igr_id from tab_detalleasignacion d \n" +
            "		join tab_solicitud s on s.sol_id = d.sol_id \n" +
            "		join tab_usuariohistorial h on h.ush_id = s.usu_idempleado\n" +
            "		join tab_almacen a on a.al_id = d.al_id\n" +
            "		join tab_producto p on p.pro_id = a.pro_id\n" +
            "		where s.usu_idempleado = "+ush_id+"\n" +
            "		group by d.al_id,p.igr_id\n" +
            "	) antiguo on antiguo.al_id = nuevo.al_id where cantidad is not null\n" +
            ") antiguo1 on nuevo1.igr_id = antiguo1.igr_id where (cant_nuevo > antiguo1.cantidad OR antiguo1.cantidad is null)";
            System.out.println(sql);
            st = getCn().prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                cant = (rs.getInt("cant_antiguo") > 0) ? rs.getInt("cant_antiguo") : 1000;
                if(rs.getInt("cant_nuevo") > cant){
                    cant = rs.getInt("cant_nuevo") - rs.getInt("cant_antiguo");
                    for(int i = 0 ; i < cant; i++){
                        sql = "insert into tab_detalleasignacion values(\n" +
                                    idsol+","+rs.getInt("al_id_antiguo")+",1,NULL,NULL,1,NULL,GETDATE(),NULL)";
                        if(ush_id == 257){
                            System.out.println("Dotar uno:"+sql);
                        }

                        st = this.getCn().prepareStatement(sql);
                        st.executeUpdate();
                    }  
                } else if (!(rs.getInt("cant_antiguo") > 0)){
                    for(int i = 0 ; i < rs.getInt("cant_nuevo"); i++){
                        sql = "insert into tab_detalleasignacion values(\n" +
                                    idsol+","+rs.getInt("al_id_nuevo")+",1,NULL,NULL,1,NULL,GETDATE(),NULL)";
                        if(ush_id == 257){
                            System.out.println("Dotar dos:"+sql);
                        }                    
    //                    System.out.println(sql);
                        st = this.getCn().prepareStatement(sql);
                        st.executeUpdate();
                    }
                }
    //            igr = rs.getInt("igr_id");
            }
        }
        //Actualizamos el cargo del empleado en el SRT
        sql = "update tab_usuariohistorial set car_anterior = car_id, car_id = "+cargo_id+", clasempleado = '"+clasempleado+"' where ush_id = "+ush_id;
        System.out.println(sql);
        st = getCn().prepareStatement(sql);
        st.executeUpdate();
    }

    /**
     * 
     * @param cargo_id
     * @param ush_id
     * @param ubi_id
     * @param sexo
     * @throws SQLException 
     * @Description Ejecuta un proceso de Ratificación
     */
    public void ratificado(int cargo_id, int ush_id, int ubi_id, String sexo) throws SQLException, Exception{
        PreparedStatement st5;
        ResultSet rs;        
        String sql;
        int idsol = 0;
        Conectar();
        //Creamos una solicitud de "Reasignacion de Cargo - Area"
        sql = "insert into dbo.tab_solicitud values(GETDATE(),(select MAX(ush_id) from tab_usuariohistorial where rol_id = 27),"
                +ush_id+",NULL,NULL,'Ratificado - Autogenerado',1,3,(select max(sol_nroform)+ 1 from tab_solicitud))";
//        logger.info("Ratificado: "+sql);
        System.out.println(sql);
        st5 = getCn().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
        System.out.println("Paso por aqui");
        st5.executeUpdate();
        
        //obtenemos el id de la solicitud creada
        rs = st5.getGeneratedKeys();
        
        while (rs.next()) {
            idsol = rs.getInt(1);
        }
        sql = "select d.deta_id,d.al_id from tab_detalleasignacion d join tab_solicitud s on s.sol_id =  d.sol_id\n" +
                "where s.usu_idempleado = "+ush_id+" and d.deta_estadoprenda = 2 and d.al_id not in \n" +
                "(select a.al_id from tab_areatrabajocargo atc join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                "join tab_producto p on rt.u_item = p.pro_itemcode join tab_almacen a on a.pro_id = p.pro_id\n" +
                "where atc.car_id = "+cargo_id+" and a.ubi_id = "+ubi_id+")";
        System.out.println(sql);
        st5 = getCn().prepareStatement(sql);
        rs = st5.executeQuery();
        while(rs.next()){
            sql = "insert into tab_detalleasignacion values(\n" +
                        idsol+","+rs.getInt("al_id")+",1,NULL,NULL,1,NULL,GETDATE(),"+rs.getInt("deta_id")+")";
            st5 = this.getCn().prepareStatement(sql);
            st5.executeUpdate();
        }

        //Creamos una solicitud de "Reasignación de Cargo - Area - Dotación"
        sql = "insert into dbo.tab_solicitud values(GETDATE(),(select MAX(ush_id) from tab_usuariohistorial where rol_id = 27),"
                +ush_id+",NULL,NULL,'Ratificado - Dotación',1,7,NULL)";
        System.out.println(sql);
        st5 = this.getCn().prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
        st5.executeUpdate();
        //obtenemos el id de la solicitud creada
        rs = st5.getGeneratedKeys();
        while (rs.next()) {
            idsol = rs.getInt(1);
        }
        sql = "select max(a.al_id) al_id,p.igr_id,max(rt.u_cantidad) u_cantidad from tab_areatrabajocargo atc join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                "join tab_producto p on rt.u_item = p.pro_itemcode join tab_almacen a on a.pro_id = p.pro_id\n" +
                "	where atc.car_id = "+cargo_id+" and a.ubi_id = "+ubi_id+" and a.al_id not in(\n" +
                "	select d.al_id from tab_detalleasignacion d join tab_solicitud s on s.sol_id =  d.sol_id\n" +
                "	where s.usu_idempleado = "+ush_id+" and d.deta_estadoprenda = 2 and d.al_id in \n" +
                "		(select a.al_id from tab_areatrabajocargo atc join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                "		join tab_producto p on rt.u_item = p.pro_itemcode join tab_almacen a on a.pro_id = p.pro_id\n" +
                "		where atc.car_id = "+cargo_id+" and a.ubi_id = "+ubi_id+")) " +
                "               and (p.pro_genero = '"+sexo+"' OR p.pro_genero = '') group by p.igr_id";
        System.out.println(sql);
        st5 = getCn().prepareStatement(sql);
        rs = st5.executeQuery();
        while(rs.next()){
            for(int i = 0 ; i < rs.getInt("u_cantidad"); i++){
                sql = "insert into tab_detalleasignacion values(\n" +
                            idsol+","+rs.getInt("al_id")+",1,NULL,NULL,1,NULL,GETDATE(),NULL)";
                System.out.println(sql);
                st5 = this.getCn().prepareStatement(sql);
                st5.executeUpdate();
            }
        }
        //Actualizamos el cargo del empleado en el SRT
        sql = "update tab_usuariohistorial set car_id = "+cargo_id+" where ush_id = "+ush_id;
        System.out.println(sql);
        st5 = getCn().prepareStatement(sql);
        st5.executeUpdate();
    }    
    /**
     * 
     * @param date
     * @return true or false
     */
    public static boolean isDateValid(String date) {
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }    
}
