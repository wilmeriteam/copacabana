/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import static jdk.nashorn.internal.objects.NativeString.trim;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import web.service.Exception_Exception;
/**
 *
 * @author Administrador
 */
public class wsProducto extends DAO {

    public void updateInsertProducto() throws JSONException, Exception {
        String json = hello("Wil");
        String sql;
        int estado = 0;
        ResultSet rs;
        JSONArray jsonArray = new JSONArray(json);
//        ResultSet rs;

        //Obtenemos los datos Json en la variable cargo
        try {
            Conectar();
            this.getCn().setAutoCommit(false);
            
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject rec = jsonArray.getJSONObject(i);
                sql = "SELECT pro_id,pro_itemcode,pro_itemname,pro_unidad,pro_prenda,pro_color,pro_descripcion,"
                        + "pro_genero,pro_talla,pro_logo,pro_material,trp_id,pro_vidautil,igr_id,"
                        + "pro_nomenclatura,pro_estado FROM tab_producto";
                PreparedStatement st = this.getCn().prepareCall(sql);
                rs = st.executeQuery();

                estado = 0;

                String color = (rec.getString("color").equals("NULO")) ? "" : rec.getString("color");
                String descripcion = (rec.getString("descripcion").equals("NULO")) ? "" : rec.getString("descripcion");
                String genero = (rec.getString("genero").equals("UNISEX")) ? "" : rec.getString("genero");
                String talla = (rec.getString("talla").equals("NULO")) ? "" : rec.getString("talla");
                String logo = (rec.getString("logo").equals("NULO")) ? "" : rec.getString("logo");
                String material = (rec.getString("material").equals("NULO")) ? "" : rec.getString("material");

                while (rs.next()) {
//                    System.out.println(trim(rec.getString("itemcode")) + " == " + trim(rs.getString("pro_itemcode")));
//                    if (trim(rec.getString("itemcode")) == trim(rs.getString("pro_itemcode"))) {
                    if (rec.getString("itemcode").equals(rs.getString("pro_itemcode"))) {
                        sql = "UPDATE tab_producto set \n" +
                            "pro_itemcode = '"+rec.getString("itemcode")+"',\n" +
                            "pro_itemname = '" + rec.getString("itemname") + "',\n" +
                            "pro_unidad = '" + rec.getString("unidad") + "',\n" +
                            "pro_prenda = '" + rec.getString("prenda_id") + "',\n" +
                            "pro_color = '" + color + "',\n" +
                            "pro_descripcion = '" + descripcion + "',\n" +
                            "pro_genero = '" +genero + "',\n" +
                            "pro_talla = '"+talla+"',\n" +
                            "pro_logo = '"+logo+"',\n" +
                            "pro_material = '"+material+"',\n" +
                            //"trp_id = dbo.ufnGetTipoRopaPrenda(RTRIM('" + rec.getString("tiporopaprenda_id") + "')),\n" +
                            "trp_id = "+ Integer.parseInt(rec.getString("tiporopaprenda_id")) +",\n" +
                            "pro_vidautil = '" + rec.getString("vidautil") + "',\n" +
                            "igr_id = "+ Integer.parseInt(rec.getString("itemgrupo")) +",\n" +
                            "pro_nomenclatura = null,\n" +
                            "pro_estado = '" + rec.getString("estado") + "'\n" +
                            "where pro_itemcode = '"+rec.getString("itemcode")+"'";
                        System.out.println(sql);
                        PreparedStatement pst = getCn().prepareStatement(sql);
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                if (estado == 0) {
                    String inserta = "insert into tab_producto values \n"
                            + "('" + rec.getString("itemcode") + "','" + rec.getString("itemname") + "','" + rec.getString("unidad")
                            + "',RTRIM('" + rec.getString("prenda_id") + "'),'" + color + "','" + descripcion + "','" + genero + "','" + talla + "','" + logo + "','" + material + "',\n"
                            + Integer.parseInt(rec.getString("tiporopaprenda_id")) +",'" + rec.getString("vidautil") + "',"
                            + Integer.parseInt(rec.getString("itemgrupo")) + ",null,'" + rec.getString("estado") + "')";
                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
            }
            this.getCn().commit();
        } catch (Exception e) {
            try {
                this.getCn().rollback();
            } catch (SQLException ex1) {
                System.err.println("No se pudo deshacer" + ex1.getMessage());
            }
            throw e;
        } finally {
            Cerrar();
        }
//            System.out.println(cargo[i][0]+" - "+cargo[i][1]);
//        }

        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) {
        web.service.WebServiceProducto_Service service = new web.service.WebServiceProducto_Service();
        web.service.WebServiceProducto port = service.getWebServiceProductoPort();
        return port.hello(name);
    }

}
