/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Wilmer
 */
public class wsAreatrabajocargo extends DAO {

    public void updateInsertAreatrabajocargo() throws JSONException, Exception {
        String json = hello("Wil");

        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
        int estado = 0;

        //Obtenemos los datos Json en la variable cargo
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            try {
                Conectar();
                this.getCn().setAutoCommit(false);
                PreparedStatement st = this.getCn().prepareCall("select atcar_id,car_id,tpe_id from tab_areatrabajocargo;");
                rs = st.executeQuery();

                estado = 0;
                while (rs.next()) {
                    if (Integer.parseInt(rec.getString("Code")) == rs.getInt("atcar_id")) {
                        PreparedStatement pst = getCn().prepareCall("update tab_areatrabajocargo set "
                                + "car_id=" + Integer.parseInt(rec.getString("u_cargo_rrhh")) + ","
                                + "tpe_id=" + Integer.parseInt(rec.getString("u_cargo"))
                                + " where atcar_id = " + Integer.parseInt(rec.getString("Code")));
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar un insert
                if (estado == 0) {
//                ix++;
                    String inserta = "INSERT INTO dbo.tab_areatrabajocargo values(" + Integer.parseInt(rec.getString("Code")) + ","
                            + Integer.parseInt(rec.getString("u_cargo_rrhh")) + ","
                            + Integer.parseInt(rec.getString("u_cargo")) + ")";
                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
//            System.out.println(cargo[i][0]+" - "+cargo[i][1]);
        }

        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) {
        web.service.NewWebServiceAreatrabajocargo_Service service = new web.service.NewWebServiceAreatrabajocargo_Service();
        web.service.NewWebServiceAreatrabajocargo port = service.getNewWebServiceAreatrabajocargoPort();
        return port.hello(name);
    }

}
