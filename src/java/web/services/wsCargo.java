/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import web.service.Exception_Exception;

/**
 *
 * @author wilmer
 */
public class wsCargo extends DAO {

    public void updateInsertCargo() throws JSONException, Exception {
        String json = hello("Wil");

        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
//        String[][] cargo = new String[jsonArray.length()][2];
        int estado = 0;

        //Obtenemos los datos Json en la variable cargo
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            try {
                Conectar();
                this.getCn().setAutoCommit(false);                
                PreparedStatement st = this.getCn().prepareCall("SELECT car_id,car_nombre,car_estado FROM tab_cargo");
                rs = st.executeQuery();

                estado = 0;
                while (rs.next()) {
                    if (Integer.parseInt(rec.getString("id")) == rs.getInt("car_id")) {
                        PreparedStatement pst = getCn().prepareCall("update tab_cargo set car_nombre='" + rec.getString("cargo") + "', car_estado = " + Integer.parseInt(rec.getString("estado"))
                                + " where car_id = " + Integer.parseInt(rec.getString("id")));
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar un insert
                if (estado == 0) {
                    String inserta = "INSERT INTO dbo.tab_cargo values(" + rec.getInt("id") + ",'" + rec.getString("cargo") + "'," + rec.getInt("estado") + ")";
                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
        }

        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) throws Exception_Exception {
        web.service.WebServiceCargo_Service service = new web.service.WebServiceCargo_Service();
        web.service.WebServiceCargo port = service.getWebServiceCargoPort();
        return port.hello(name);
    }

}
