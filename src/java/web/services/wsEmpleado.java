/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
//import org.apache.log4j.Logger;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author wilmer
 */
public class wsEmpleado extends DAO {
//    final static String DATE_FORMAT = "dd-MM-yyyy";

    final static String DATE_FORMAT = "yyyyMMdd";
//    private static final Logger logger = Logger.getLogger(wsEmpleado.class);

    public void updateInsertEmpleado() throws JSONException, Exception {
        String json = hello("Wil");
        String sql;
        JSONArray jsonArray = new JSONArray(json);
        wsHelper emProcces = new wsHelper();
        ResultSet rs;
        ResultSet rs2;
        int estado = 0;
        int idsol = 0;
        //Obtenemos los datos Json 
//        System.out.println(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            try {
                Conectar();
                this.getCn().setAutoCommit(false);
                sql = "select u.usu_id,h.ush_id,h.ush_fingreso,\n"
                        + "case when CONVERT(VARCHAR(10),h.ush_ffinal,121) IS NULL then 'NULO' else CONVERT(VARCHAR(10),h.ush_ffinal,112) end \n"
                        + "ush_ffinal,h.car_id,h.suc_id,su.ubi_id from tab_usuario u join tab_usuariohistorial h on h.usu_id = u.usu_id \n"
                        + "join tab_sucursales su on su.suc_id = h.suc_id";

                System.out.println(sql);
                PreparedStatement st = this.getCn().prepareStatement(sql);
                rs = st.executeQuery();
                estado = 0;
                int rescambio = 0;
                while (rs.next()) {
                    //Verificamos que el usuario en la BD SRT sea igual al del WebService
                    if (Integer.parseInt(rec.getString("idUsuarioG3")) == rs.getInt("usu_id")) {

                        System.out.println(rs.getInt("usu_id") + "->" + rs.getString("ush_ffinal") + " - " + rec.getString("idUsuarioG3") + "->" + rec.getString("ffinal"));
//                        logger.info("Verificamos car_id: " + rs.getInt("car_id") + " != " + rec.getInt("cargo_id") + " && " + rec.getInt("tipoempleado"));
                        //DESVINCULACION LABORAL
                        if (!rs.getString("ush_ffinal").equals(rec.getString("ffinal"))) {
                            emProcces.desvinculacionlaboral(rec.getString("ffinal"), rs.getInt("ush_id"));
                            estado = 1;
                        } //REASIGNACION DE CARGO - AREA Y RATIFICACION
                        else if (rs.getInt("car_id") != rec.getInt("cargo_id")) {
                            
                            sql = "select case when tpe_id = (select tpe_id from tab_areatrabajocargo where car_id = "+rs.getInt("car_id")+") "
                                    + "then 1 else 0 end valor\n" +
                                    "from tab_areatrabajocargo where car_id = "+rec.getInt("cargo_id");
                            PreparedStatement st2 = this.getCn().prepareStatement(sql);
                            rs2 = st2.executeQuery();
                            while(rs2.next()){
                                rescambio = rs2.getInt("valor");
                            }
                            //Creamos una solicitud de "Reasignacion de Cargo - Area"
                            if(rescambio == 1){
                                sql = "update tab_usuariohistorial set car_anterior = car_id, car_id = "+rec.getInt("cargo_id")+" where ush_id = "+rs.getInt("ush_id");
                                PreparedStatement pst4 = this.getCn().prepareStatement(sql);
                                pst4.executeUpdate();
                            } else {
                                emProcces.reasignacioncargo(rec.getInt("cargo_id"), rs.getInt("ush_id"),
                                        rs.getInt("ubi_id"), rec.getString("sexo"), rec.getString("clasempleado"));
                            }
                            estado = 1;
                        } else {

                            String updateEmpleado = "update tab_usuario set "
                                    + "usu_nombre = '" + rec.getString("nombres") + "',"
                                    + "usu_apellido = '" + rec.getString("apellidos") + "',"
                                    + "usu_ci = '" + rec.getString("ci") + "',"
                                    + "usu_sexo='" + rec.getString("sexo") + "',"
                                    + "usu_fnacimiento = '" + rec.getString("fnacimiento") + "' "
                                    + "where usu_id = " + rec.getInt("idUsuarioG3");
                            System.out.println(updateEmpleado);
                            PreparedStatement pst3 = getCn().prepareStatement(updateEmpleado);
                            pst3.executeUpdate();

                            String updateUsuarioHistorial = "UPDATE tab_usuariohistorial SET "
                                    + "ush_fingreso = '" + rec.getString("fingreso") + "',"
                                    + "ush_ffinal = CASE WHEN 'NULO' = '" + rec.getString("ffinal") + "' THEN NULL ELSE '" + rec.getString("ffinal") + "' END,"
                                    + //                            "ush_estado = '"+rec.getInt("estado")+"'," +
                                    //                            "car_id = "+rec.getInt("cargo_id")+"," +
                                    "suc_id = " + rec.getInt("sucursal_id") + ","
                                    + "ush_usuario = " + rec.getInt("usuario") + ", "
                                    + "Area=" + rec.getInt("area2") + ", "
                                    + "division="+ rec.getInt("division") + ", "
                                    + "turno='"+ rec.getString("turno") + "' "
                                    + "WHERE usu_id = " + rec.getInt("idUsuarioG3");
                            System.out.println(updateUsuarioHistorial);
                            PreparedStatement pst4 = getCn().prepareCall(updateUsuarioHistorial);
                            pst4.executeUpdate();

                            //                        pst = getCn().prepareStatement("UPDATE tab_usuario set ush_ffinal = NULL WHERE ush_ffinal = '19000101'");
                            //                        pst.executeUpdate();
                            estado = 1;
                        }
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar el insert
                if (estado == 0) {

                    String inserta = "INSERT INTO dbo.tab_usuario values(" + rec.getInt("idUsuarioG3")
                            + ",'" + rec.getString("nombres")
                            + "','" + rec.getString("apellidos")
                            + "','" + rec.getString("ci") + "','" + rec.getString("sexo") + "','"
                            + rec.getString("fnacimiento") + "',1)";

                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                    //Habilitar esta sentencia para producción
                    //                Date fbaja = (rec.getString("ffinal").equals("NULO")) ? "" : Date.valueOf(rec.getString("ffinal"));
                    //                System.out.println(inserta);
                    //                inserta = "INSERT INTO dbo.tab_usuariohistorial values("+rec.getInt("idUsuarioG3")+
                    //                        ",'" + rec.getString("fingreso") + 
                    //                        "',CASE WHEN 'NULO' = '"+rec.getString("ffinal")+"' THEN NULL ELSE '"+rec.getString("ffinal")+"' END,"
                    //                        + "'" + rec.getString("nick") + "',NULL,"+
                    //                        rec.getInt("estado")+","+
                    //                        rec.getInt("cargo_id")+",29,"+
                    //                        rec.getInt("sucursal_id")+","+
                    //                        rec.getInt("usuario")+"," +
                    //                        rec.getInt("area2")+"," +
                    //                        rec.getInt("tipoempleado")+")";

                    inserta = "INSERT INTO dbo.tab_usuariohistorial values(" + rec.getInt("idUsuarioG3")
                            + ",'" + rec.getString("fingreso")
                            + "',CASE WHEN 'NULO' = '" + rec.getString("ffinal") + "' THEN NULL ELSE '" + rec.getString("ffinal") + "' END,"
                            + "'" + rec.getString("nick") + "',NULL,"
                            + rec.getInt("estado") + ","
                            + rec.getInt("cargo_id") + ",29,"
                            + rec.getInt("sucursal_id") + ",1,"
                            + rec.getInt("area2") + ","
                            + rec.getInt("tipoempleado") + ",'N',NULL,"
                            + rec.getInt("division")+",'"
                            + rec.getString("turno")+"')";

                    System.out.println(inserta);
                    pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
//            System.out.println(cargo[i][0]+" - "+cargo[i][1]);
        }

        System.out.println("Fin");
    }

    public static boolean isDateValid(String date) {
        try {
            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    private static String hello(java.lang.String name) {
        web.service.WebServiceEmpleado_Service service = new web.service.WebServiceEmpleado_Service();
        web.service.WebServiceEmpleado port = service.getWebServiceEmpleadoPort();
        return port.hello(name);
    }
}
