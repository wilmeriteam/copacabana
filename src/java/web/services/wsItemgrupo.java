/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Wilmer
 */
public class wsItemgrupo extends DAO {

    public void updateInsertItemgrupo() throws JSONException, Exception {
        String json = hello("Wil");

        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
        int estado = 0;

        //Obtenemos los datos Json en la variable cargo
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            try {
                Conectar();
                this.getCn().setAutoCommit(false);
                PreparedStatement st = this.getCn().prepareCall("select igr_id,grupo from tab_itemgrupo;");
                rs = st.executeQuery();

                estado = 0;
                while (rs.next()) {
                    if (Integer.parseInt(rec.getString("code")) == rs.getInt("igr_id")) {
                        PreparedStatement pst = getCn().prepareCall("update tab_itemgrupo set grupo='" + rec.getString("name") + "'"
                                + " where igr_id = " + Integer.parseInt(rec.getString("code")));
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar un insert
                if (estado == 0) {
//                ix++;
                    String inserta = "INSERT INTO dbo.tab_itemgrupo values(" + Integer.parseInt(rec.getString("code")) + ",'" + rec.getString("name") + "')";
                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
//            System.out.println(cargo[i][0]+" - "+cargo[i][1]);
        }

        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) {
        web.service.NewWebServiceItemgrupo_Service service = new web.service.NewWebServiceItemgrupo_Service();
        web.service.NewWebServiceItemgrupo port = service.getNewWebServiceItemgrupoPort();
        return port.hello(name);
    }

}
