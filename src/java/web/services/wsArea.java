package web.services;

import dao.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import web.service.Exception_Exception;

/**
 *
 * @author Wilmer
 */
public class wsArea extends DAO {
    public void updateInsertArea() throws JSONException, Exception {
        String json = hello("Wil");

        JSONArray jsonArray = new JSONArray(json);
        ResultSet rs;
//        String[][] cargo = new String[jsonArray.length()][2];
        int estado = 0;

        //Obtenemos los datos Json en la variable cargo
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject rec = jsonArray.getJSONObject(i);
            try {
                Conectar();
                this.getCn().setAutoCommit(false);                
                PreparedStatement st = this.getCn().prepareCall("select area_id,area,activo from tab_area");
                rs = st.executeQuery();

                estado = 0;
                while (rs.next()) {
                    if (Integer.parseInt(rec.getString("areaid")) == rs.getInt("area_id")) {
                        PreparedStatement pst = getCn().prepareCall("update tab_area set area='" + rec.getString("area") + "', activo = " + rec.getInt("activo")
                                + " where area_id = " + rec.getInt("areaid"));
                        pst.executeUpdate();
                        estado = 1;
                    }
                }
                //El estado 1 Significa que el registro no existe en la BD y debemos realizar un insert
                if (estado == 0) {
                    String inserta = "INSERT INTO dbo.tab_area values(" + rec.getInt("areaid") + ",'" + rec.getString("area") + "'," + rec.getInt("activo") + ")";
                    System.out.println(inserta);
                    PreparedStatement pst = getCn().prepareStatement(inserta);
                    pst.executeUpdate();
                }
                this.getCn().commit();
            } catch (Exception e) {
                try {
                    this.getCn().rollback();
                } catch (SQLException ex1) {
                    System.err.println("No se pudo deshacer" + ex1.getMessage());
                }
                throw e;
            } finally {
                Cerrar();
            }
        }

        System.out.println("Fin");
    }

    private static String hello(java.lang.String name) throws Exception_Exception {
        web.service.WebServiceArea_Service service = new web.service.WebServiceArea_Service();
        web.service.WebServiceArea port = service.getWebServiceAreaPort();
        return port.hello(name);
    }
}

