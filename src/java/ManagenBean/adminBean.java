/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.datosempresaDAO;
import dao.menuDAO;
import dao.usuariohistorialDAO;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import model.Datosempresa;
import model.Menu;
import model.Rol;
import model.Usuariohistorial;


@ManagedBean
@RequestScoped
//@SessionScoped
//@ApplicationScoped
public class adminBean {
//private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(adminBean.class);

    private final HttpServletRequest httpServletR;
    private final FacesContext faceCont;
    private FacesMessage facesMg;
    public String ush_id;
    public int rol_id;
    private Usuariohistorial usuariohistorial = new Usuariohistorial();
    private String urlweb;
    private Datosempresa datosempresa=new Datosempresa();

    public Datosempresa getDatosempresa() {
        return datosempresa;
    }

    public void setDatosempresa(Datosempresa datosempresa) {
        this.datosempresa = datosempresa;
    }

    public String getUrlweb() {
        return urlweb;
    }

    public void setUrlweb(String urlweb) {
        this.urlweb = urlweb;
    }

    public Usuariohistorial getUsuariohistorial() {
        return usuariohistorial;
    }

    public void setUsuariohistorial(Usuariohistorial usuariohistorial) {
        this.usuariohistorial = usuariohistorial;
    }

    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }
    private String titulopagina = "Sistema de Ropa de Trabajo";
    private List<Menu> arraymenu;

    public List<Menu> getArraymenu() {
        return arraymenu;
    }

    public void setArraymenu(List<Menu> arraymenu) {
        this.arraymenu = arraymenu;
    }
    private List<String> arraym = new ArrayList<String>();

    public List<String> getArraym() {
        return arraym;
    }

    public void setArraym(List<String> arraym) {
        this.arraym = arraym;
    }

    public String getTitulopagina() {
        return titulopagina;
    }

    public void setTitulopagina(String titulopagina) {
        this.titulopagina = titulopagina;
    }

    public String getUsu_id() {
        return ush_id;
    }

    public void setUsu_id(String ush_id) {
        this.ush_id = ush_id;
    }

    public void mostrarLogo() throws Exception {
        try {
            datosempresaDAO dao = new datosempresaDAO();
            datosempresa = dao.leerDatosempresa();
        } catch (Exception e) {
            throw e;
        }
    }

    public adminBean() throws Exception {
          this.mostrarLogo();
     
        faceCont = FacesContext.getCurrentInstance();
        httpServletR = (HttpServletRequest) faceCont.getExternalContext().getRequest();
        if (httpServletR.getSession().getAttribute("ush_id") != null) {
            ush_id = httpServletR.getSession().getAttribute("ush_id").toString();
            rol_id = Integer.parseInt(httpServletR.getSession().getAttribute("rol_id").toString());
//            httpServletR.setAttribute(rol, rol_id);
//            logger.info("Rol: "+rol_id);
            Rol rl=new Rol();
            rl.setRol_id(rol_id);
            menuDAO dao = new menuDAO();
            arraymenu = dao.listarMenuRol(ush_id);
        }
        if (httpServletR.getSession().getAttribute("ush_id") == null) {
            ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
            String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
            try {
                ctx.redirect(ctxPath + "/faces/login.xhtml");         
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
    }

    public String VerificarRol() {
        return ush_id;
    }

    public void CerrarSession() {
        httpServletR.getSession().removeAttribute("ush_id");
        httpServletR.getSession().removeAttribute("rol_id");
        httpServletR.getSession().removeAttribute("excep_id");
        facesMg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Cerrado la session", null);
        faceCont.addMessage(null, facesMg);
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        try {
            ctx.redirect(ctxPath + "/faces/login.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
//        return "login";
    }

    public FacesMessage getFacesMg() {
        return facesMg;
    }

    public void setFacesMg(FacesMessage facesMg) {
        this.facesMg = facesMg;
    }

    public String MostrarNombre() throws Exception {
        String nom = "";
        usuariohistorialDAO usudao;
        if (httpServletR.getSession().getAttribute("ush_id") != null) {
            ush_id = httpServletR.getSession().getAttribute("ush_id").toString();
            usudao = new usuariohistorialDAO();
            nom = usudao.nomUsuariorol(ush_id);
        }
        return nom;
    }

    public String getNombre() throws Exception {
        String nom = "";
        usuariohistorialDAO usudao;
        if (httpServletR.getSession().getAttribute("ush_id") != null) {
            ush_id = httpServletR.getSession().getAttribute("ush_id").toString();
            usudao = new usuariohistorialDAO();
            nom = usudao.nomUsuario(ush_id);
        }
        return nom;
    }

    public void leerUsuarioBean() throws Exception {
        int idusu = Integer.parseInt(ush_id);
        try {
            usuariohistorialDAO dao = new usuariohistorialDAO();
            usuariohistorial = dao.leerUsuariodao(idusu);
        } catch (Exception e) {
            throw e;
        }
    }

    public void guardarCambioclave() throws Exception {
        int idusu = Integer.parseInt(ush_id);
        String clave = usuariohistorial.getClavenuevo();
        try {
            usuariohistorialDAO dao = new usuariohistorialDAO();
            dao.saveclavedao(idusu, clave);
        } catch (Exception e) {
            throw e;
        }
    }

}
