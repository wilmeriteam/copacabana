/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.ubicacionDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Ubicacion;
import model.Ubicacionsucursal;
import web.services.wsOficinas;

@ManagedBean
@SessionScoped
public class ubicacionBean {

    private Ubicacionsucursal ubicacionsucursal = new Ubicacionsucursal();
    private List<Ubicacionsucursal> arrayubicacion;
    private List<Ubicacion> arrayubiselect;

    public List<Ubicacion> getArrayubiselect() {
        return arrayubiselect;
    }

    public void setArrayubiselect(List<Ubicacion> arrayubiselect) {
        this.arrayubiselect = arrayubiselect;
    }

    public Ubicacionsucursal getUbicacionsucursal() {
        return ubicacionsucursal;
    }

    public void setUbicacionsucursal(Ubicacionsucursal ubicacionsucursal) {
        this.ubicacionsucursal = ubicacionsucursal;
    }

    public List<Ubicacionsucursal> getArrayubicacion() {
        return arrayubicacion;
    }

    public void setArrayubicacion(List<Ubicacionsucursal> arrayubicacion) {
        this.arrayubicacion = arrayubicacion;
    }
    private String accion;
    private String nompagina = "Gestor de Áreas";

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
        this.limpiar();
    }

    public ubicacionBean() throws Exception {
        this.listarUbicacion();
    }

    public void listarUbicacion() throws Exception {
        ubicacionDAO ubicaciondao = new ubicacionDAO();
        try {
            arrayubicacion = ubicaciondao.listarUbicacion();
            arrayubiselect =ubicaciondao.SelectListUbicacion();
        } catch (Exception e) {
            throw e;
        }
    }

    public void execwsOficinas() throws Exception {
        try {
            wsOficinas ini = new wsOficinas();
            ini.updateInsertOficina();
        } catch (Exception e) {
            throw e;
        }
    }

    public void operar() throws Exception {

        if (accion.equals("Registrar")) {
            this.registrar();
            this.limpiar();
        } else {
            this.modificar();
            this.limpiar();
        }

    }

    public void registrar() throws Exception {
        try {
            ubicacionDAO dao = new ubicacionDAO();
            dao.registrarDao(ubicacionsucursal);
            this.listarUbicacion();
        } catch (Exception e) {
            throw e;
        }
    }

    public void modificar() throws Exception {
        try {
            ubicacionDAO dao = new ubicacionDAO();
            dao.modificarDao(ubicacionsucursal);
            this.listarUbicacion();
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerModificar(Ubicacionsucursal suc) throws Exception {
        ubicacionDAO dao;
        Ubicacionsucursal ubicaciondb;
        try {
            dao = new ubicacionDAO();
            ubicaciondb = dao.leerUbicacion(suc);
            if (ubicaciondb != null) {
                this.accion = "Modificar";
                this.ubicacionsucursal = ubicaciondb;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerEliminar(Ubicacionsucursal suc) throws Exception {
        ubicacionDAO dao;
        try {
            dao = new ubicacionDAO();
            dao.eliminarUbicacion(suc);
            this.listarUbicacion();
        } catch (Exception e) {
            throw e;
        }
    }

    public void limpiar() {
        this.ubicacionsucursal.setSuc_id(0);
        this.ubicacionsucursal.setSuc_direccion("");
        this.ubicacionsucursal.setSuc_ubicacion("");
    }

}
