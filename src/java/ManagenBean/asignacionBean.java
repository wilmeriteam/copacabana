package ManagenBean;

import dao.almacenDAO;
import dao.asignacionDAO;
import dao.usuariohistorialDAO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
//import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Almacen;
import model.Asignacion;
import model.Asignacionsolicitud;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import org.apache.log4j.Logger;

@ManagedBean
@SessionScoped
public class asignacionBean {

//    private static final Logger logger = Logger.getLogger(almacenDAO.class); 
    private List<Asignacion> arrayasignacion;
    private List<Asignacionsolicitud> arrayasignacionsolicitud;

    private String accion;
    private int sol_id;
    private String nompagina = "Detalle de Solicitud";
    private Asignacionsolicitud asigdetalle = new Asignacionsolicitud();
    private List<Almacen> listalmacen;
    private Asignacion asignacion = new Asignacion();
    private Almacen almacen = new Almacen();
    private List<SelectItem> itemsSelect;
    private int deta_id;
    private List<Persona> personas = new ArrayList<>();
    private int cantidadestado;
    private List<Almacen> listalmacenReporte;
    private int prueba;
    private String ruta;
    private String palabra = "";
    private final FacesContext faceCont;
    private final HttpServletRequest httpServletR;
    private int ush_id;

    public String getRuta() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        ruta = (String) servletContext.getRealPath("/file/logo.png");
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public int getPrueba() {
        return prueba;
    }

    public void setPrueba(int prueba) {
        this.prueba = prueba;
    }

    public List<Almacen> getListalmacenReporte() {
        return listalmacenReporte;
    }

    public void setListalmacenReporte(List<Almacen> listalmacenReporte) {
        this.listalmacenReporte = listalmacenReporte;
    }

    public int getCantidadestado() {
        return cantidadestado;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public void setCantidadestado(int cantidadestado) {
        this.cantidadestado = cantidadestado;
    }

//    public List<Persona> getPersonas() {
//        Persona per = new Persona();
//        per.setNombres("freddy");
//        per.setApellidos("flores");
//        per.setTelefono("232323");
//        personas.add(per);
//        per = new Persona();
//        per.setNombres("freddy2");
//        per.setApellidos("flores2");
//        per.setTelefono("2323233333");
//        personas.add(per);
//        return personas;
//    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public int getDeta_id() {
        return deta_id;
    }

    public void setDeta_id(int deta_id) {
        this.deta_id = deta_id;
    }

    public List<SelectItem> getItemsSelect() throws Exception {
        return itemsSelect;
    }

    public void setItemsSelect(List<SelectItem> itemsSelect) {
        this.itemsSelect = itemsSelect;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public List<Almacen> getListalmacen() {
        return listalmacen;
    }

    public void setListalmacen(List<Almacen> listalmacen) {
        this.listalmacen = listalmacen;
    }

    public Asignacionsolicitud getAsigdetalle() {
        return asigdetalle;
    }

    public void setAsigdetalle(Asignacionsolicitud asigdetalle) {
        this.asigdetalle = asigdetalle;
    }

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public int getSol_id() {
        return sol_id;
    }   

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public List<Asignacionsolicitud> getArrayasignacionsolicitud() {
        return arrayasignacionsolicitud;
    }

    public void setArrayasignacionsolicitud(List<Asignacionsolicitud> arrayasignacionsolicitud) {
        this.arrayasignacionsolicitud = arrayasignacionsolicitud;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public List<Asignacion> getArrayasignacion() {
        return arrayasignacion;
    }

    public void setArrayasignacion(List<Asignacion> arrayasignacion) {
        this.arrayasignacion = arrayasignacion;
    }

    public Asignacion getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(Asignacion asignacion) {
        this.asignacion = asignacion;
    }

    public asignacionBean() throws Exception {
        
        faceCont = FacesContext.getCurrentInstance();
        httpServletR = (HttpServletRequest) faceCont.getExternalContext().getRequest();
        ush_id = Integer.parseInt(httpServletR.getSession().getAttribute("ush_id").toString());
        this.listarasignaciones(palabra);
        
    }
    
    public void buscarUsuario() throws Exception {
        try {
            this.listarasignaciones(palabra);
        } catch (Exception e) {
            throw e;
        }
    }
 

    public void listarasignaciones(String pal) throws Exception {
        asignacionDAO dao = new asignacionDAO();
        try {
//            dao.cerrarSolicitudes();
            arrayasignacion = dao.listarasignaciones(pal);
        } catch (Exception e) {
            throw e;
        }
    }
//TEMPORAL MENTE MODIFICADO

    /**
     * 
     * @param id Solicitud
     * @throws Exception En proceso de análisis el throw arroja un error que perjudica la vista
     */
    public void obtenerDato(int id) throws Exception {
        sol_id = id;
       
        try { 
//            asigdetalle.getSol_id();
            asignacionDAO dao = new asignacionDAO();
            asigdetalle = dao.listarasignacionesdetalle(id);
            dao.deleteDevolucionTemporal(ush_id);
            if(asigdetalle != null){
                listalmacen = dao.listarAlmacenOneRow(sol_id, asigdetalle.getUsu_idempleado());
            } 
            else {
                ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
                String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
                try {
                    ctx.redirect(ctxPath + "/faces/asignacion.xhtml");
                } catch (Exception e) {
                    e.printStackTrace();
                }                
            }
                

        } catch (Exception e) {
//            throw e;
        }

    }

    /**
     * 
     * @param id tab_detalleasignacion (deta_id)
     * @throws Exception 
     */
    public void modificarItem(int id) throws Exception {
        try {
            asignacionDAO dao = new asignacionDAO();
            dao.editarAlmacenAsignacion(almacen, id);
            listalmacen = dao.listarAlmacenOneRow(sol_id, asigdetalle.getUsu_idempleado());
//            listalmacenReporte = dao.listarAlmacenReporte(sol_id);
        } catch (Exception e) {
            throw e;
        }
    }
    /**
     * @param id Solicitud
     * @param al Atributos completos de las tablas Almacen y asignaciondetalle
     */
    public void updateAsignarItem(Almacen al) throws Exception {
//        adminBean dm = new adminBean();
//        int usu_id = Integer.parseInt(dm.getUsu_id());
        
        try {
            asignacionDAO dao = new asignacionDAO();
            //si la cantidad es cero entonces no debe actualizar nada
            if (al.getAl_cantidad() > 0 ){
//                dao.updateSolicitudItem(id, al, usu_id);
                dao.upateAsignaciontemp(al.getDeta_id(), ush_id);
                listalmacen = dao.listarAlmacenOneRow(sol_id, asigdetalle.getUsu_idempleado());
//                listalmacenReporte = dao.listarAlmacenReporte(id);
            }
        } catch (Exception e) {
            throw e;
        }
    }    
    
//TEMPORAL MENTE MODIFICADO

    public void leerModificar(Almacen as, int idempleado) throws Exception {
        int ubi = 0;
        int id = as.getAl_id();
        ubi = asigdetalle.getUbi_id();
        System.out.println(as.getDeta_id());
        itemsSelect = new ArrayList<SelectItem>();
        try {
            almacenDAO dao = new almacenDAO();
            List<Almacen> al = dao.listarAlmacenSelectItem(ubi, id, idempleado);
            for (Almacen row : al) {
                SelectItem selectclaseropa = new SelectItem(row.getAl_id(), row.getRopa());
                itemsSelect.add(selectclaseropa);
            }
            
            almacen = dao.SeleccionarCantidadItem(id, asigdetalle.getSol_id());
//            deta_id = almacen.getDeta_id();
            deta_id = as.getDeta_id();
            System.out.println(itemsSelect);
        } catch (Exception e) {
            throw e;
        }
    }

    public void updateSolicitud(int id) {
        String fechaf = asigdetalle.getFechaf();
        try {
            asignacionDAO dao = new asignacionDAO();
            dao.updateSolicitud(id, fechaf);
            this.listarasignaciones(palabra);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectAjaxCantidad(AjaxBehaviorEvent event) throws Exception {
        int valor = almacen.getAl_id();
        
//        int solid = almacen.get
        try {
            almacenDAO dao = new almacenDAO();
            almacen = dao.SeleccionarCantidadItemAlmacen(valor,almacen);
        } catch (Exception e) {
            throw e;
        }
    }

    public void exportarPdf() throws IOException, JRException, Exception {
        String nom;
        String nomreporte;
        String nomFormulario;
        String datosempresa;
        adminBean adm = new adminBean();
        nom = adm.getNombre();
        datosempresa = adm.getDatosempresa().getDatemp_nombreempresa();
        nomreporte = asigdetalle.getNomempleado();
        nomFormulario = nomreporte;
        nomreporte = nomreporte.replace(" ","");
        asignacionDAO dao = new asignacionDAO();
        listalmacenReporte = dao.listarAlmacenReporte(ush_id);
        String datosemp = "Yo: "+nomFormulario+" con C.I.: "+asigdetalle.getCi()+", desempeño mi trabajo en la Sección de: "+asigdetalle.getDpto()+" , he recibido de la empresa "+datosempresa+" Ropa de Trabajo y Equipos de Protección Personal completamente nuevos de acuerdo al siguiente detalle:";

//        System.out.println(nom);
        if(!listalmacenReporte.isEmpty()){
            for (Almacen row : listalmacenReporte) {
                dao.updateSolicitudItem(row.getSol_id(), row, ush_id);
            }
            dao.deleteDevolucionTemporal(ush_id);
            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("empleado", asigdetalle.getNomempleado());
            parametros.put("solicitante", asigdetalle.getNomsolicitante());
            parametros.put("almacen", nom);
            parametros.put("cargo", asigdetalle.getCar_nombre());
            parametros.put("area", asigdetalle.getArea());
            parametros.put("departamento", asigdetalle.getDepartamento());
            parametros.put("fechasolicitud", asigdetalle.getFechasolicitud());
            parametros.put("nombres", nomFormulario);
            parametros.put("apellidos", asigdetalle.getApellidos());
            parametros.put("nroform", asigdetalle.getNroform());
            parametros.put("ci", asigdetalle.getCi());
            parametros.put("genero",asigdetalle.getGenero());
            parametros.put("depto",asigdetalle.getDpto());
            parametros.put("motivosolicitud", asigdetalle.getMotivosolicitud());
            parametros.put("datosempresa", datosempresa);
            parametros.put("datosemp", datosemp);
            parametros.put("ruta", this.getRuta());
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/comprobante.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, new JRBeanCollectionDataSource(this.getListalmacenReporte()));
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.addHeader("Content-disposition", "attachment; filename="+nomreporte+".pdf");
            ServletOutputStream stream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
            stream.flush();
            stream.close();
            FacesContext.getCurrentInstance().responseComplete();
        }
    }

    /**
     * 
     * @param solid Id de Solicitud
     * @throws Exception 
     * @description Asigna todos los items al empleado usando las validaciones correspondientes, 
     */
    public void asignarTodos() throws Exception{
//            asignacionDAO dao = new asignacionDAO();
//            asigdetalle = dao.listarasignacionesdetalle(solid);
//            if(asigdetalle != null){
//                logger.info("Aqui asigdetalle: "+listalmacen.size());
//                List<Almacen> asignarTodos = dao.listarAlmacenOneRow(listalmacenReporte., asigdetalle.getUsu_idempleado());
                for (Almacen row : listalmacenReporte) {
//                    SelectItem selectprenda = new SelectItem(row.getTip_id(), row.getTip_nombre());
//                    tipoprendaselect.add(selectprenda);
//                    row.getRestriccion();
                    
                    if(row.getRestriccion() != 0){
//                        logger.info("wilwil: "+row.getRestriccion());
//                        this.updateAllasign(row);
//                    } else {
//                        logger.info("Error: No se puede adicionar");
                    }
                }
//            } 
//            else {
//                ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
//                String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
//                try {
//                    ctx.redirect(ctxPath + "/faces/asignacion.xhtml");
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }                
//            }
    }
}
