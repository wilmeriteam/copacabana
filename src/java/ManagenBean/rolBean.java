/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.menuDAO;
import dao.permisosDAO;
import dao.rolDAO;
import java.util.ArrayList;
import java.util.List;
//import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import model.Menu;
import model.Permisos;
import model.Selectmenu;
import model.Rol;

@ManagedBean
@SessionScoped
public class rolBean {

    private Rol rol = new Rol();
    private List<Rol> arrayrol;
    private List<Selectmenu> arraymenu = new ArrayList<Selectmenu>();
    private List<String> selectMenu = new ArrayList<String>();
    private String[] permisos;

    public List<String> getSelectMenu() throws Exception {
        selectMenu.removeAll(arrayrol);
        return selectMenu;

    }

    public void setSelectMenu(List<String> selectMenu) {
        this.selectMenu = selectMenu;
    }

    public List<Selectmenu> getArraymenu() {
        return arraymenu;
    }

    public void setArraymenu(List<Selectmenu> arraymenu) {
        this.arraymenu = arraymenu;
    }

    private String accion;

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
        this.limpiar();
    }

    public rolBean() throws Exception {
        this.listarRol();
             this.arraymenu.clear();
        ArrayList<Menu> men = new ArrayList<>();
        menuDAO md = new menuDAO();
        men = md.SelectListMenu();
        for (Menu row : men) {
            arraymenu.add(new Selectmenu(row.getMen_id(), row.getMen_nombre()));
        }

    }

    public List<Rol> getArrayrol() {
        return arrayrol;
    }

    public void setArrayrol(List<Rol> arrayrol) {
        this.arrayrol = arrayrol;
    }

    public void listarRol() throws Exception {
        rolDAO roldao = new rolDAO();
        try {
            arrayrol = roldao.listarRoles();
        } catch (Exception e) {
            throw e;
        }
    }

    public void guardarSelect() {

        try {
            permisosDAO dao = new permisosDAO();
            dao.GuardarListaPermisos(rol, selectMenu);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void operar() throws Exception {

        if (accion.equals("Registrar")) {
            this.registrar();
            this.limpiar();
        } else {
            this.modificar();
            this.limpiar();
        }

    }

    public void registrar() throws Exception {
        try {
            rolDAO dao = new rolDAO();
            dao.registrarDao(rol);
            this.listarRol();
        } catch (Exception e) {
            throw e;
        }
    }

    public void modificar() throws Exception {
        try {
            rolDAO dao = new rolDAO();
            dao.modificarDao(rol);
            this.listarRol();
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerModificar(Rol rl) throws Exception {
        rolDAO dao;
        Rol roldb;
        try {
            dao = new rolDAO();
            roldb = dao.leerRol(rl);
            if (roldb != null) {
                this.accion = "Modificar";
                this.rol = roldb;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerID(Rol rl) throws Exception {
        permisosDAO dao;
        Rol roldb;
        permisos=null;
        this.selectMenu.clear();
        try {
            dao = new permisosDAO();
            roldb = dao.Obtenerid(rl);
            permisos = dao.ObtenerlistPermisos(rl);
            for (String row : permisos) {
                this.selectMenu.add(row);
            }
            this.rol = roldb;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void leerEliminar(Rol rl) throws Exception {
        rolDAO dao;
        try {
            dao = new rolDAO();
            dao.eliminarRol(rl);
            this.listarRol();
        } catch (Exception e) {
            throw e;
        }
    }

    public void limpiar() {
        this.rol.setRol_id(0);
        this.rol.setRol_nombre("");
        this.rol.setRol_estado(0);

    }

//    public void listarMenu() throws Exception {
//
//        try {
//            menuDAO men = new menuDAO();
//            arrmenu = men.listarMenu();
//        } catch (Exception e) {
//            throw e;
//        }
//    }
}
