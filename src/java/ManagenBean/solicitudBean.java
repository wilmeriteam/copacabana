/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.almacenDAO;
import dao.tipoprendaDAO;
import dao.claseropatrabajoDAO;
import dao.detalleasignacionDAO;
import dao.motivosolicitudDAO;
import dao.solicitudDAO;
import dao.usuariohistorialDAO;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Almacen;
import model.Cargo;
import model.Claseropatrabajo;
import model.Detalleasignacion;
import model.Motivodescuento;
import model.Motivosolicitud;
import model.Solicitud;
import model.Sucursal;
import model.Tipoprenda;
import model.Ubicacion;
import model.Usuariohistorial;
//import org.apache.log4j.Logger;

@ManagedBean
@SessionScoped
public class solicitudBean {

//    private static final Logger logger = Logger.getLogger(almacenDAO.class);
    
    private List<Solicitud> arraysolicitud;
    private List<Usuariohistorial> arrayusuario;
    private List<Motivodescuento> selectmotivosolicitud;
    private List<SelectItem> selectUsuarios;
    private List<SelectItem> itemsSelect;
    private List<SelectItem> claseropaSelect;
    private List<Almacen> listalmacen;
    private List<Almacen> listaexcepcional;
    private FacesMessage facesMg;

    private List<SelectItem> tipoprendaselect;

    private Usuariohistorial usuario = new Usuariohistorial();
    private Ubicacion ubicacion = new Ubicacion();
    private Sucursal sucursal = new Sucursal();
    private Cargo cargo = new Cargo();
    private String nomcargo;
    private String accion;
    private Solicitud solicitud = new Solicitud();
    private Almacen almacen = new Almacen();
    public Tipoprenda tipoprenda = new Tipoprenda();
    private Claseropatrabajo claseropatrabajo = new Claseropatrabajo();
    public String cadenaItems = null;
    private int cantidaditem;
    private final HttpServletRequest httpServletR;
    private final FacesContext faceCont;
    private int ush_id = 0;
    private int rol_id;
    private int excep;
    private String errormsg = "asd";
    private String usid;
    private List<String> arrayvector = new ArrayList<String>();
    private String mensaje;
    private int modificarver;
    private boolean show = false;

    public int getExcep(){
        return excep;
    }

    public void setExcep(int excep) {
        this.excep = excep;
    }


    public List<Almacen> getListaexcepcional() {
        return listaexcepcional;
    }

    public void setListaexcepcional(List<Almacen> listaexcepcional) {
        this.listaexcepcional = listaexcepcional;
    }

    public int getModificarver() {
        return modificarver;
    }

    public void setModificarver(int modificarver) throws Exception {
        this.setMensaje("");
        this.limpiar(); 
        listaexcepcional = null;
        
//        if (modificarver == 3){
//            this.listarSolicitudExcepcional();
//        }
        this.modificarver = modificarver;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<String> getArrayvector() {
        return arrayvector;
    }

    public void setArrayvector(List<String> arrayvector) {
        this.arrayvector = arrayvector;
    }
    private List<Almacen> selectItemsol = new ArrayList<Almacen>();

    public List<Almacen> getSelectItemsol() {
        return selectItemsol;
    }

    public void setSelectItemsol(List<Almacen> selectItemsol) {
        this.selectItemsol = selectItemsol;
    }

//    public String[] getArrayselect() {
//        return arrayselect;
//    }
//
//    public void setArrayselect(String[] arrayselect) {
//        this.arrayselect = arrayselect;
//    }
    public String getUsid() {
        return usid;
    }

    public void setUsid(String usid) {
        this.usid = usid;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public int getUsh_id() {
        return ush_id;
    }

    public void setUsh_id(int ush_id) {
        this.ush_id = ush_id;
    }
    private Detalleasignacion detalleasig = new Detalleasignacion();

    private int solid;

    public List<Motivodescuento> getSelectmotivosolicitud() {
        return selectmotivosolicitud;
    }

    public void setSelectmotivosolicitud(List<Motivodescuento> selectmotivosolicitud) {
        this.selectmotivosolicitud = selectmotivosolicitud;
    }

    public FacesMessage getFacesMg() {
        return facesMg;
    }

    public void setFacesMg(FacesMessage facesMg) {
        this.facesMg = facesMg;
    }

    public Detalleasignacion getDetalleasig() {
        return detalleasig;
    }

    public void setDetalleasig(Detalleasignacion detalleasig) {
        this.detalleasig = detalleasig;
    }

    public int getSolid() {
        return solid;
    }

    public void setSolid(int solid) {
        this.solid = solid;
    }

    public int getCantidaditem() {
        return cantidaditem;
    }

    public void setCantidaditem(int cantidaditem) {
        this.cantidaditem = cantidaditem;
    }

    public String getCadenaItems() {
        return cadenaItems;
    }

    public void setCadenaItems(String cadenaItems) {
        this.cadenaItems = cadenaItems;
    }

    public List<Almacen> getListalmacen() {
        return listalmacen;
    }

    public void setListalmacen(List<Almacen> listalmacen) {
        this.listalmacen = listalmacen;
    }

    public List<SelectItem> getItemsSelect() {
        return itemsSelect;
    }

    public void setItemsSelect(List<SelectItem> itemsSelect) {
        this.itemsSelect = itemsSelect;
    }

    public List<SelectItem> getClaseropaSelect() {

        return claseropaSelect;
    }

    public void setClaseropaSelect(List<SelectItem> claseropaSelect) {
        this.claseropaSelect = claseropaSelect;
    }

    public Claseropatrabajo getClaseropatrabajo() {
        return claseropatrabajo;
    }

    public void setClaseropatrabajo(Claseropatrabajo claseropatrabajo) {
        this.claseropatrabajo = claseropatrabajo;
    }

    public Tipoprenda getTipoprenda() {
        return tipoprenda;
    }

    public void setTipoprenda(Tipoprenda tipoprenda) {
        this.tipoprenda = tipoprenda;
    }

    public String getNomcargo() {
        return nomcargo;
    }

    public void setNomcargo(String nomcargo) {
        this.nomcargo = nomcargo;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Usuariohistorial getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuariohistorial usuario) {
        this.usuario = usuario;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    private String nompagina = "Gestor de Solicitudes";

    public List<Usuariohistorial> getArrayusuario() {
        return arrayusuario;
    }

    public void setArrayusuario(List<Usuariohistorial> arrayusuario) {
        this.arrayusuario = arrayusuario;
    }

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public List<Solicitud> getArraysolicitud() {
        return arraysolicitud;
    }

    public void setArraysolicitud(List<Solicitud> arraysolicitud) {
        this.arraysolicitud = arraysolicitud;
    }

    public List<SelectItem> getSelectUsuarios() {
        return selectUsuarios;
    }

    public void setSelectUsuarios(List<SelectItem> selectUsuarios) {
        this.selectUsuarios = selectUsuarios;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) throws Exception {
        this.setMensaje("");
        this.limpiar();
        this.accion = accion;

    }

    //Constructor
    public solicitudBean() throws Exception {
        faceCont = FacesContext.getCurrentInstance();
        httpServletR = (HttpServletRequest) faceCont.getExternalContext().getRequest();
//        arraysolicitud = null;
        this.limpiar();
//        this.selectmotivosolicitud=null;
        this.listarSolicitud();
//        this.getExcepcional();
        usuariohistorialDAO usudao = new usuariohistorialDAO();
        arrayusuario = usudao.listarNombres();
//        this.getExcepcional();
        this.setExcep( Integer.parseInt(httpServletR.getSession().getAttribute("excep_id").toString()));
        this.selectmtvlista();

    }
//TEMPORAL MENTE MODIFICADO

    public void EliminarRegistro(Almacen deta) throws Exception {
        try {
            detalleasignacionDAO daodeta = new detalleasignacionDAO();
            daodeta.eliminarDetalleAsignacion(deta);
            this.cargarListTableItem();
        } catch (Exception e) {
            throw e;
        }
    }

    public void selectmtvlista() throws Exception {
        motivosolicitudDAO dao = new motivosolicitudDAO();
//        adminBean adm = new adminBean();
        rol_id = Integer.parseInt(httpServletR.getSession().getAttribute("rol_id").toString());
//        int rolid = adm.getRol_id();
        try {
            this.selectmotivosolicitud = dao.listarSelectMotivoSolicitud(rol_id);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<SelectItem> getTipoprendaselect() {
        tipoprendaselect = new ArrayList<>();
        tipoprendaDAO dp = new tipoprendaDAO();
        try {
            List<Tipoprenda> tp = dp.selectTipoprenda();
            for (Tipoprenda row : tp) {
                SelectItem selectprenda = new SelectItem(row.getTip_id(), row.getTip_nombre());
                tipoprendaselect.add(selectprenda);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tipoprendaselect;
    }

    public List<SelectItem> getSelectItems() throws Exception {
        selectUsuarios = new ArrayList<SelectItem>();

        adminBean ad = new adminBean();
//        ush_id = Integer.parseInt(httpServletR.getSession().getAttribute("ush_id").toString());
//        ush_id = Integer.parseInt(httpServletR.getSession().getAttribute("ush_id").toString());
        ush_id = Integer.parseInt(ad.VerificarRol());
        usuariohistorialDAO usudao = new usuariohistorialDAO();
        try {
            List<Usuariohistorial> usuarios = usudao.listarNombresSolicicitud(ush_id);
            for (Usuariohistorial usu : usuarios) {
                SelectItem selectItem = new SelectItem(usu.getUsh_id(), usu.getUsu_nombre() + " " + usu.getUsu_apellido());
                selectUsuarios.add(selectItem);
            }                
        } catch (Exception e) {
        }

        return selectUsuarios;
    }
    
    public List<SelectItem> getSelectItemsExcepcional() throws Exception {
        selectUsuarios = new ArrayList<SelectItem>();

        adminBean ad = new adminBean();
        ush_id = Integer.parseInt(ad.VerificarRol());
        usuariohistorialDAO usudao = new usuariohistorialDAO();
        try {
            List<Usuariohistorial> usuarios = usudao.listarNombresSolicicitudExcepcional(ush_id);
            for (Usuariohistorial usu : usuarios) {
                SelectItem selectItem = new SelectItem(usu.getUsh_id(), usu.getUsu_nombre() + " " + usu.getUsu_apellido());
                selectUsuarios.add(selectItem);
            }
        } catch (Exception e) {
        }

        return selectUsuarios;
    }

    public void listarSolicitud() throws Exception {
        adminBean adm = new adminBean();
        int usu = Integer.parseInt(adm.getUsu_id());
        solicitudDAO solicituddao = new solicitudDAO();
        arraysolicitud = null;
        try {
            arraysolicitud = solicituddao.listarSolicitud(usu);
            System.out.println();
        } catch (Exception e) {
            throw e;
        }
    }

//    public int getExcepcional() throws Exception{
//        int ret;
//        try {
//            solicitudDAO sdao = new solicitudDAO();
//            ret = sdao.returnExcepcional(ush_id);
//        } catch (Exception e) {
//            throw e;
//        }
//        return (ret == 1) ? 1:0;
//        int excepcional = Integer.parseInt(httpServletR.getSession().getAttribute("excep_id").toString());
//        System.out.println(excepcional);
//        return excepcional;
//    }
//    public void listarSolicitudExcepcional() throws Exception{
//        adminBean adm = new adminBean();
//        int usu = Integer.parseInt(adm.getUsu_id());
//        solicitudDAO solicituddao = new solicitudDAO();
//        arraysolicitud = null;
//        try {
//            arraysolicitud = solicituddao.listarSolicitudExcepcional(usu);
//        } catch (Exception e) {
//            throw e;
//        }
//    }
    
    public void operar() throws Exception {
        this.updateEstado();
    }

    public void eliminarSolicitud(Solicitud sol) throws Exception {
        try {
            solicitudDAO dadel = new solicitudDAO();
            dadel.borrarSolicitudDao(sol);
            this.listarSolicitud();
        } catch (Exception e) {
            throw e;
        }
    }
    
    /**
     * Elimina la solicitud excepcional
     */
    public void eliminarExcepcional(int detaid, int idsol) throws Exception{
        try {
            solicitudDAO del = new solicitudDAO();
            almacenDAO daoal = new almacenDAO();
            del.borrarDetalleExcepcional(detaid);
            listaexcepcional = daoal.listarExcepcionalaux(idsol);
            
        } catch (Exception e) {
            throw e;
        }
    }

    public void updateEstado() throws Exception {
        try {
            solicitudDAO ds = new solicitudDAO();
            ds.actualizarEstado(solicitud);
            this.listarSolicitud();
            this.limpiar();
        } catch (Exception e) {
            throw e;
        }
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
        try {
            ctx.redirect(ctxPath + "/faces/solicitud.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void registrar() throws Exception {
        adminBean dm = new adminBean();
        int usu_id = Integer.parseInt(dm.getUsu_id());
        int idmensaje = 0;
        try {
            if (modificarver == 1) {
                solicitudDAO dao = new solicitudDAO();
                idmensaje = dao.registrarsolicitudDao(solicitud, usu_id, selectItemsol);
            }
            if (modificarver == 2) {
                solicitudDAO dao = new solicitudDAO();
                idmensaje = dao.modificarsolicitudDao(solicitud, usu_id, selectItemsol, solid);
            }

        } catch (Exception e) {
            throw e;
        }
        if (idmensaje == 1) {
            mensaje = "No puede crear solicitud de dotación y Reasignación";
        }
        if (idmensaje == 2) {
            mensaje = "Seleccione registros";
        }
        if (idmensaje == 3) {
            mensaje = "No puede modificar por que una o más prendas fueron ya asignados.";
        }

        if (idmensaje == 0) {
            ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
            String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
            try {
                ctx.redirect(ctxPath + "/faces/solicitud.xhtml");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

        public void registrarExcepcional() throws Exception {
        adminBean dm = new adminBean();
        int usu_id = Integer.parseInt(dm.getUsu_id());
        int idmensaje = 0;
        int idsol;
        solicitudDAO dao = new solicitudDAO();
        almacenDAO daoal = new almacenDAO();
        try {
            if (modificarver == 1) {
                
                idsol = dao.registrarsolicitudExcepcional(solicitud, usu_id, selectItemsol);
                
                listaexcepcional = daoal.listarExcepcionalaux(idsol);
                idmensaje = 0;
//                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
//                ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
            }
            if (modificarver == 2) {
                idsol = dao.modificarsolicitudExcepcional(solicitud, usu_id, selectItemsol, solid);
                listaexcepcional = daoal.listarExcepcionalaux(idsol);
                idmensaje = 4;
            }

        } catch (Exception e) {
            throw e;
        }
        if (idmensaje == 1) {
            mensaje = "No puede crear solicitud de dotación y Reasignación";
        }
        if (idmensaje == 2) {
            mensaje = "Seleccione registros";
        }
        if (idmensaje == 3) {
            mensaje = "No puede modificar por que una o más prendas fueron ya asignados.";
        }

        if (idmensaje == 0) {
            ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
            String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
            try {
                ctx.redirect(ctxPath + "/faces/solicitud.xhtml");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
        
    public void leerModificar(Solicitud solidfila) throws Exception {
        solicitudDAO dao;
        Solicitud solicituddb;
        almacenDAO aldao;
        int car_id = 0;
        int ubi_id = 0;
        itemsSelect = new ArrayList<SelectItem>();
        solid = solidfila.getSol_id();
        try {
            dao = new solicitudDAO();
            solicituddb = dao.leerSolicitud(solidfila);
            if (solicituddb != null) {
                this.accion = "Modificar";
                this.modificarver = 2;
                this.solicitud = solicituddb;
                aldao = new almacenDAO();
                listalmacen = aldao.listarAlmacenOneRow(solicitud.getSol_id());
                car_id = solicitud.getCar_id();
                ubi_id = solicitud.getUbi_id();
                List<Almacen> al = aldao.listarAlmacenSelectItemSolicitudAsignacion(car_id, ubi_id);
                for (Almacen row : al) {
                    SelectItem selectclaseropa = new SelectItem(row.getAl_id(), row.getRopa());
                    itemsSelect.add(selectclaseropa);
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerModificarExcepcional(Solicitud solidfila) throws Exception {
        
        Solicitud solicituddb;
        almacenDAO aldao = new almacenDAO();
        int car_id = 0;
        int ubi_id = 0;
        itemsSelect = new ArrayList<SelectItem>();
        solid = solidfila.getSol_id();
        System.out.println(modificarver);
        try {
            solicitudDAO dao = new solicitudDAO();
            solicituddb = dao.leerSolicitud(solidfila);
            if (solicituddb != null) {
                //Grid Inferior
                try {
                    solicitud = dao.ObtenerSolicitud(solicituddb.getUsu_idempleado());
                    listalmacen = aldao.listarExcepcional(solicitud);
        //            System.out.println("asdf");
                } catch (Exception e) {
                    throw e;
                }
//                if(modificarver == 1){
//                    listaexcepcional = null;
//                }
                
//                this.accion = "Modificar";
                this.modificarver = 2;
                this.solicitud = solicituddb;
//                listalmacen = aldao.listarAlmacenOneRow(solicitud.getSol_id());
//                car_id = solicitud.getCar_id();
//                ubi_id = solicitud.getUbi_id();
//                List<Almacen> al = aldao.listarAlmacenSelectItemSolicitudAsignacion(car_id, ubi_id);
//                for (Almacen row : al) {
//                    SelectItem selectclaseropa = new SelectItem(row.getAl_id(), row.getRopa());
//                    itemsSelect.add(selectclaseropa);
//                }
                almacenDAO daoal = new almacenDAO();
                listaexcepcional = daoal.listarExcepcionalaux(solicitud.getSol_id());
                
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void leerEliminar(Solicitud men) throws Exception {
        solicitudDAO dao;
        try {
            dao = new solicitudDAO();
            dao.eliminarSolicitud(men);
            this.listarSolicitud();
        } catch (Exception e) {
            throw e;
        }
    }

    public void limpiar() {
        this.solicitud.setSol_id(0);
        this.solicitud.setSol_fecha("");
        this.solicitud.setUsu_idsolicitante(0);
        this.solicitud.setUsu_idempleado(0);
        this.solicitud.setSol_fechaasignacion("");
        this.solicitud.setSol_fechadevolucion("");
        this.solicitud.setSol_estado(0);
        this.solicitud.setNomcargo("");
        this.solicitud.setNomusuario_empleado("");
        this.solicitud.setSol_descripcion("");
        this.solicitud.setSucursal("");
        this.tipoprenda.setTip_id(0);
        this.claseropatrabajo.setClast_id(0);
        this.almacen.setAl_id(0);
//        this.solid = 0;
        this.listalmacen = null;
//        this.almacen.setAl_id(0);
        this.almacen.setAl_cantidad(0);
        this.almacen.setAl_costo(0);

        this.solicitud.setMot_id(0); 
    }

    public void cargarDatos() throws Exception {
        int valor = solicitud.getUsu_idempleado();
        itemsSelect = new ArrayList<SelectItem>();
        ush_id = valor;
        int motivo;
        motivo = solicitud.getMot_id();
//        logger.info("Motivo: " + motivo);
//        int motivo;
        try {
            solicitudDAO dao = new solicitudDAO();
            almacenDAO daoal = new almacenDAO();
            solicitud = dao.ObtenerSolicitud(valor);
            System.out.println();
            if(motivo == 7){
                listalmacen = daoal.listarAlmacenSolicitudNuevo(solicitud); 
            } else {
                listalmacen = daoal.listarAlmacenSolicitud(solicitud,motivo); 
            }
            System.out.println();
        } catch (Exception e) {
            throw e;
        }
//        setnom
    }

    public void cargarExcepcional(AjaxBehaviorEvent event) throws Exception{
        int valor = solicitud.getUsu_idempleado();
        almacenDAO daoal = new almacenDAO();
        solicitudDAO dao = new solicitudDAO();
//        listalmacen = daoal.
        try {
            solicitud = dao.ObtenerSolicitud(valor);
            listalmacen = daoal.listarExcepcional(solicitud);
//            System.out.println("asdf");
        } catch (Exception e) {
            throw e;
        }
    }

    //Grid superior
    public void cargarExcepcional2(AjaxBehaviorEvent event, int sol) throws Exception{
//        int valor = solicitud.getUsu_idempleado();
        almacenDAO daoal = new almacenDAO();
//        solicitudDAO dao = new solicitudDAO();
//        listalmacen = daoal.
        try {
            listaexcepcional = daoal.listarExcepcionalaux(sol);
        } catch (Exception e) {
            throw e;
        }
    }

    public void cargarSelectClase(AjaxBehaviorEvent event) throws Exception {

        claseropaSelect = new ArrayList<SelectItem>();
        try {
            int valor = tipoprenda.getTip_id();
            claseropatrabajoDAO dao = new claseropatrabajoDAO();
            List<Claseropatrabajo> clasr = dao.obtenerSelectClaseRopa(valor);
            for (Claseropatrabajo row : clasr) {
                SelectItem selectclaseropa = new SelectItem(row.getClast_id(), row.getClast_nombre());
                claseropaSelect.add(selectclaseropa);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.itemsSelect = null;
        }
    }

    public void cargarListTableItem() throws Exception {

        try {
            almacenDAO dao = new almacenDAO();
            listalmacen = dao.listarAlmacen(solicitud, solid);
        } catch (Exception e) {
            throw e;
        }

    }

    public void selectAjaxCantidad(AjaxBehaviorEvent event) {
        int valor = almacen.getAl_id();
        try {
            almacenDAO dao = new almacenDAO();
            almacen = dao.SeleccionarCantidadItemSolicitud(valor, solicitud);
//            almacen.setAl_cantidad(cantidaditem);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
