/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.stockDAO;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Stock;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRCsvExporter;

@ManagedBean
@RequestScoped
public class reportesBean {
    private List<Stock> listaHeadCsv;
    private List<Stock> listaLineCsv;
    private Date date1;
    private Date date2;

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public List<Stock> getListaLineCsv() {
        return listaLineCsv;
    }

    public void setListaLineCsv(List<Stock> listaLineCsv) {
        this.listaLineCsv = listaLineCsv;
    }

    public List<Stock> getListaHeadCsv() {
        return listaHeadCsv;
    }

    public void setListaHeadCsv(List<Stock> listaHeadCsv) {
        this.listaHeadCsv = listaHeadCsv;
    }
        
    @SuppressWarnings("null")
    public void exportCsv() throws IOException, JRException, Exception {
        stockDAO dao = new stockDAO();
        DateFormat fecha = new SimpleDateFormat("yyyy-MM-dd");
        String fecha1 = fecha.format(date1);
        String fecha2 = fecha.format(date2);
        listaHeadCsv = dao.generaCsvHead(fecha1,fecha2);
        if(!listaHeadCsv.isEmpty()) {
            JRCsvExporter exporter = new JRCsvExporter(); 
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/txthead.jasper"));        
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), null, new JRBeanCollectionDataSource(this.getListaHeadCsv())); 
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
            StringBuffer buffer;
            buffer = new StringBuffer();
            exporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, buffer);
            exporter.exportReport(); 
            try {
                HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment;filename=cabecera.csv");
                response.getOutputStream().write(buffer.toString().getBytes());
                response.flushBuffer();                
            } catch (Exception e) {
                System.out.print(e);
            }
        } 
    }      
    @SuppressWarnings("null")
    public void exportCsvLine() throws IOException, JRException, Exception {
        stockDAO dao = new stockDAO();
        listaLineCsv = dao.generaCsvLine();
        if(!listaLineCsv.isEmpty()) {
            JRCsvExporter exporter = new JRCsvExporter(); 
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/txtline.jasper"));        
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), null, new JRBeanCollectionDataSource(this.getListaLineCsv())); 
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint); 
            StringBuffer buffer;
            buffer = new StringBuffer();
            exporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, buffer);
            exporter.exportReport(); 
            try {
                HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment;filename=linea.csv");
                response.getOutputStream().write(buffer.toString().getBytes());
                response.flushBuffer();                
            } catch (Exception e) {
                System.out.print(e);
            }            
        } 
    }      

}
