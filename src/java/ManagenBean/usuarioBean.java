package ManagenBean;

import dao.areaDAO;
import dao.cargoDAO;
import dao.divisionDAO;
import dao.motivosolicitudDAO;
import dao.rolDAO;
import dao.solicitudDAO;
import dao.sucursalDAO;
import dao.ubicacionDAO;
import dao.almacenDAO;
import dao.usuariohistorialDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Area;
import model.Rol;
import model.Cargo;
import model.Division;
import model.Sucursal;
import model.Ubicacion;
import model.Usuariohistorial;
import model.Solicitud;
import model.Almacen;
//import org.apache.log4j.Logger;
//import model.Excepcional;
//import 

@ManagedBean
@SessionScoped
public class usuarioBean {
//    private static final Logger logger = Logger.getLogger(usuarioBean.class);
    private Usuariohistorial usuariohis = new Usuariohistorial();
    private List<Usuariohistorial> arrayusu;
    private List<Usuariohistorial> arraycodigo;
    private List<Ubicacion> arrayubicacion;
    private List<Rol> arrayrol;
    private List<Cargo> arraycargo;
    private List<Sucursal> arraysucursal;
    private List<Area> arrayarea;
    private List<Division> arraydivision;
    private String palabra = "";
    private Almacen almacen = new Almacen();
    private boolean value1;
    private String Sexo;
    private int modificarver;
    private Ubicacion ubicacion = new Ubicacion();
    private int pendientetotal = 0;

    public int getPendientetotal() {
        return pendientetotal;
    }

    public void setPendientetotal(int pendientetotal) {
        this.pendientetotal = pendientetotal;
    }
    
    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    
    public List<Ubicacion> getArrayubicacion() {
        return arrayubicacion;
    }

    public void setArrayubicacion(List<Ubicacion> arrayubicacion) {
        this.arrayubicacion = arrayubicacion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    
    public int getModificarver() {
        return modificarver;
    }

    public void setModificarver(int modificarver) {
        this.limpiar();
        this.modificarver = modificarver;
    }

    public List<Area> getArrayarea() throws Exception {
        areaDAO daoarea = new areaDAO();
        arrayarea = daoarea.listarAreas();
        return arrayarea;
    }

    public void setArrayarea(List<Area> arrayarea) {
        this.arrayarea = arrayarea;
    }

    public List<Division> getArraydivision() throws Exception {
        divisionDAO daodiv = new divisionDAO();
        arraydivision = daodiv.listarDivision();
        return arraydivision;
    }

    public void setArraydivision(List<Division> arraydivision) {
        this.arraydivision = arraydivision;
    }

    public List<Sucursal> getArraysucursal() throws Exception {
        sucursalDAO daosuc = new sucursalDAO();
        arraysucursal = daosuc.listSucursal();
        return arraysucursal;
    }

    public void setArraysucursal(List<Sucursal> arraysucursal) {
        this.arraysucursal = arraysucursal;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }
    
    public List<Cargo> getArraycargo() throws Exception {
        cargoDAO daocargo = new cargoDAO();
        arraycargo = daocargo.listCargo();
        return arraycargo;
    }

    public void setArraycargo(List<Cargo> arraycargo) {
        this.arraycargo = arraycargo;
    }

    public boolean isValue1() throws Exception {
        rolDAO exc = new rolDAO();
        try {
            value1 = exc.getExcepcional(usuariohis.getUsh_id());
        } catch (Exception e) {
            throw e;
        }
        return value1;
    }

    public void setValue1(boolean value1)  {
        this.value1 = value1;
    }
    
    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    private String nompagina = "Lista de Usuarios";

    public List<Rol> getArrayrol() throws Exception {
        rolDAO daorol = new rolDAO();
        arrayrol = daorol.listarRoles();
        return arrayrol;
    }

    public void setArrayrol(List<Rol> arrayrol) {
        this.arrayrol = arrayrol;
    }

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public List<Usuariohistorial> getArrayusu() {
        return arrayusu;
    }

    public void setArrayusu(List<Usuariohistorial> arrayusu) {
        this.arrayusu = arrayusu;
    }

    public List<Usuariohistorial> getArraycodigo() {
        return arraycodigo;
    }

    public void setArraycodigo(List<Usuariohistorial> arraycodigo) {
        this.arraycodigo = arraycodigo;
    }

    public Usuariohistorial getUsuariohis() {
        return usuariohis;
    }

    public void setUsuariohis(Usuariohistorial usuariohis) {
        this.usuariohis = usuariohis;
    }

    public usuarioBean() throws Exception {
        this.listarUsuarios(palabra);
        this.listarUbicacion();
    }

    public void listarUsuarios(String pal) throws Exception {
        usuariohistorialDAO dao = new usuariohistorialDAO();
        try {
            arrayusu = dao.listarUsuarios(pal);
        } catch (Exception e) {
            throw e;
        }
    }

    public void listarUbicacion() throws Exception {
        ubicacionDAO ubiDAO = new ubicacionDAO();
        arrayubicacion = null;
        try {
            arrayubicacion = ubiDAO.SelectListUbicacion();
        } catch (Exception e) {
            throw e;
        }
    }
    public void listarCodigo(int cod) throws Exception {
        usuariohistorialDAO dao = new usuariohistorialDAO();
        try {
            arraycodigo = dao.listaCodigo(cod);
        } catch (Exception e) {
            throw e;
        }
    }

    public void buscarItem(String cod,int ubicacion) throws Exception {
         almacenDAO alm = new almacenDAO();
        try {
            almacen = alm.obtenerItem(cod,ubicacion);
//            System.out.println(codigo.toArray());
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void leerUsuario(Usuariohistorial usuh) throws Exception {
        Usuariohistorial ushist = new Usuariohistorial();
        try {
            usuariohistorialDAO dao = new usuariohistorialDAO();
            ushist = dao.leerUsuario(usuh);
            this.modificarver = 2;
            if (ushist != null) {
                this.usuariohis = ushist;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void saveUsuario() throws Exception {
        usuariohistorialDAO dao = new usuariohistorialDAO();
        try {
            dao.insertCredencial(usuariohis,value1);            
            this.listarUsuarios(palabra);
        } catch (Exception e) {
            throw e;
        }
    }

//    public void inUsuario() throws Exception{
    public void inUsuario() throws Exception{    
        usuariohistorialDAO dao = new usuariohistorialDAO();
        if(this.modificarver == 1){
            try {
                dao.insertarUsuario(usuariohis);            
                this.listarUsuarios(palabra);
            } catch (Exception e) {
                throw e;
            }            
        } else {
            try {
                dao.updateUsuario(usuariohis);
                this.listarUsuarios(palabra);
            } catch (Exception e) {
                throw e;
            }            
        }
    }
    
    public void bucarUsuario() throws Exception {
        try {
            this.listarUsuarios(palabra);
        } catch (Exception e) {
            throw e;
        }
    }

    public void bucarCodigo() throws Exception {
        try {
            pendientetotal = 0;
            this.buscarItem(palabra,ubicacion.getUbi_id());
            this.listarCodigo(almacen.getAl_id());
            this.totalCant();
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void totalCant(){
        for (Usuariohistorial u : arraycodigo){
            pendientetotal += u.getCantpendiente();
        }
    }
    
    public void limpiar(){
        this.usuariohis.setArea(0);
        this.usuariohis.setCar_id(0);
        this.usuariohis.setDivision(0);
        this.usuariohis.setSuc_id(0);
        this.usuariohis.setUsh_id(0);
        this.usuariohis.setUsu_nombre("");
        this.usuariohis.setUsu_apellido("");
        this.usuariohis.setUsu_sexo("");
    }

}
