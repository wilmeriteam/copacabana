/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.DAO;
import dao.solicitudDAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Excepcional;
//import org.apache.log4j.Logger;

/**
 *
 * @author Wilmer
 */
public class excepcionalDAO extends DAO{
//    private static final Logger logger = Logger.getLogger(solicitudDAO.class);
    
    public List<Excepcional> listarSolicitud(int ubi, String genero) throws Exception {
        String sql;
        List<Excepcional> arr = new ArrayList<>();
        ResultSet rs;
//        int i = 0;
        try {
            this.Conectar();
            sql = "select pt.igr_id,pt.al_id,pt.cant, pt.pro_id,p.pro_itemname,p.pro_material,p.pro_color,p.pro_vidautil,pt.stock from (\n" +
                "	select p.igr_id,min(a.al_id) al_id,min(p.pro_id) pro_id,min(rt.u_cantidad) cant,max(a.al_cantidad) stock from tab_areatrabajocargo atc\n" +
                "	join tab_ropatrabajo rt on rt.tpe_id = atc.tpe_id\n" +
                "	join tab_producto p on p.pro_itemcode = rt.u_item\n" +
                "	join tab_almacen a on a.pro_id = p.pro_id\n" +
                "	where (p.pro_genero = 'M' or p.pro_genero = '') and a.ubi_id = 1\n" +
                "	group by p.igr_id ) \n" +
                "pt join tab_producto p on p.pro_id = pt.pro_id";
            PreparedStatement st = this.getCn().prepareCall(sql);
            rs = st.executeQuery();
            String[] explodefecha;
            while (rs.next()) {
                Excepcional sol = new Excepcional();
                sol.setSol_id(rs.getInt("sol_id"));
                explodefecha = (rs.getString("sol_fecha")).split("-");
                sol.setSol_fecha(explodefecha[2] + "-" + explodefecha[1] + "-" + explodefecha[0]);
                sol.setNomcargo(rs.getString("nomcargo"));
                sol.setSucursal(rs.getString("sucursal"));
                sol.setNomusuario_empleado(rs.getString("nomusuario_empleado"));
                sol.setSol_descripcion(rs.getString("sol_descripcion"));
                sol.setSol_estado(rs.getInt("sol_estado"));
//                sol.setAlfanumeric(i);
                arr.add(sol);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            this.Cerrar();
        }
        return arr;
    }
    
}
