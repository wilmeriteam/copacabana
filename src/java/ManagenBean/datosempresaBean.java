
package ManagenBean;

import dao.datosempresaDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Datosempresa;

@ManagedBean
@SessionScoped
public class datosempresaBean {

    private Datosempresa datosempresa = new Datosempresa();
    private String nompagina="Datos de la Empresa";

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public Datosempresa getDatosempresa() {
        return datosempresa;
    }

    public void setDatosempresa(Datosempresa datosempresa) {
        this.datosempresa = datosempresa;
    }


    public datosempresaBean() throws Exception {
        this.leerModificar();
    }

   public void registrar() throws Exception {
        try {
            datosempresaDAO dao = new datosempresaDAO();
            dao.registrarDao(datosempresa);
            this.leerModificar();
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerModificar() throws Exception{
        datosempresaDAO dao;
        Datosempresa datosempresadb;
        try {
            dao=new datosempresaDAO();
            datosempresadb=dao.leerDatosempresa();
            if(datosempresadb!=null){
                this.datosempresa=datosempresadb;
            }
        } catch (Exception e) {
            throw e;
        }
    }


}
