/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.clapersonalDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Clapersonal;

@ManagedBean
@SessionScoped
public class clapersonalBean {

    private Clapersonal clapersonal = new Clapersonal();
    private List<Clapersonal> arrayclapersonal;
    private String accion;
    private String nompagina="Gestor de Personal";

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public Clapersonal getClapersonal() {
        return clapersonal;
    }

    public void setClapersonal(Clapersonal clapersonal) {
        this.clapersonal = clapersonal;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
        this.limpiar();
    }

    public clapersonalBean() throws Exception {
        this.listarClapersonal();
    }

    public List<Clapersonal> getArrayclapersonal() {
        return arrayclapersonal;
    }

    public void setArrayclapersonal(List<Clapersonal> arrayclapersonal) {
        this.arrayclapersonal = arrayclapersonal;
    }

    public void listarClapersonal() throws Exception {
        clapersonalDAO clapersonaldao = new clapersonalDAO();
        try {
            arrayclapersonal = clapersonaldao.listarClapersonal();
        } catch (Exception e) {
            throw e;
        }
    }

    public void operar() throws Exception {

        if (accion.equals("Registrar")) {
            this.registrar();
            this.limpiar();
        } else {
            this.modificar();
            this.limpiar();
        }

    }

    public void registrar() throws Exception {
        try {
            clapersonalDAO dao = new clapersonalDAO();
            dao.registrarDao(clapersonal);
            this.listarClapersonal();
        } catch (Exception e) {
            throw e;
        }
    }

    public void modificar() throws Exception {
        try {
            clapersonalDAO dao = new clapersonalDAO();
            dao.modificarDao(clapersonal);
            this.listarClapersonal();
        } catch (Exception e) {
            throw e;
        }
    }
    public void leerModificar(Clapersonal men) throws Exception{
        clapersonalDAO dao;
        Clapersonal clapersonaldb;
        try {
            dao=new clapersonalDAO();
            clapersonaldb=dao.leerClapersonal(men);
            if(clapersonaldb!=null){
                this.accion="Modificar";
                this.clapersonal=clapersonaldb;
            }
        } catch (Exception e) {
            throw e;
        }
    }
 public void leerEliminar(Clapersonal men) throws Exception{
        clapersonalDAO dao;
        try {
            dao=new clapersonalDAO();
            dao.eliminarClapersonal(men);
            this.listarClapersonal();
          } catch (Exception e) {
            throw e;
        }
    }
    public void limpiar() {
        this.clapersonal.setClap_id(0);
        this.clapersonal.setClap_nombre("");
        this.clapersonal.setClap_estado(0);
    }

}
