/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.asignacionDAO;
import dao.devolucionDAO;
import dao.reimpresionDAO;
import java.io.File;

import java.io.IOException;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Almacen;
import model.Asignacion;
import model.Asignacionsolicitud;
import model.Devolucion;
import model.Solicitud;
import model.Usuariohistorial;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 *
 * @author Wilmer
 */
@ManagedBean
@SessionScoped
public class reimpresionBean {
    private List<Usuariohistorial> arrayasignacion;

    private String accion;
    private int sol_id;
    private String nompagina = "Detalle de Reimpresión";

    private String ruta;
    private String palabra = "";
    private final FacesContext faceCont;
    private final HttpServletRequest httpServletR;
    private int ush_id;  
    private Usuariohistorial asigdetalle = new Usuariohistorial();
    private List<Solicitud> solicitudreimpresion;
    private List<Almacen> listalmacenReporte;
    private Devolucion asignadetalle = new Devolucion();
    private List<Devolucion> listadevolucionReporte;
    
   private Asignacionsolicitud asignreporte = new Asignacionsolicitud();

    public reimpresionBean() throws Exception{
        faceCont = FacesContext.getCurrentInstance();
        httpServletR = (HttpServletRequest) faceCont.getExternalContext().getRequest();
        ush_id = Integer.parseInt(httpServletR.getSession().getAttribute("ush_id").toString());
    }
    
    public void buscarUsuario() throws Exception {
        try {
            this.listarreimpresion(palabra);
        } catch (Exception e) {
            throw e;
        }
    }    

    public void listarreimpresion(String pal) throws Exception {
        reimpresionDAO dao = new reimpresionDAO();
        try {
            arrayasignacion = dao.listarreimpresion(pal);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void obtenerDato(int id) throws Exception {
        ush_id = id;
       
        try { 
//            asigdetalle.getSol_id();
            reimpresionDAO dao = new reimpresionDAO();
            asigdetalle = dao.listarasignacionesdetalle(id);
            

            if(asigdetalle != null){
                System.out.println("introducir");
                solicitudreimpresion = dao.listarAlmacenOneRow(ush_id);
            } 
            else {
                ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
                String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
                try {
                    ctx.redirect(ctxPath + "/faces/reimpresion.xhtml");
                } catch (Exception e) {
                    e.printStackTrace();
                }                
            }
                

        } catch (Exception e) {
//            throw e;
        }

    }


    public void aexportarPdf(int sol_id) throws IOException, JRException, Exception {
        String nom;
        String nomreporte;
        String nomFormulario;
        String datosempresa;
        adminBean adm = new adminBean();
        nom = adm.getNombre();
        datosempresa = adm.getDatosempresa().getDatemp_nombreempresa();

        reimpresionDAO dao = new reimpresionDAO();
        asignreporte = dao.getCabeceraReporte(sol_id);
        nomreporte = asignreporte.getNomempleado();
        nomFormulario = nomreporte;
        nomreporte = nomreporte.replace(" ","");
        
        listalmacenReporte = dao.dataReporte(sol_id);
        String datosemp = "Yo: "+nomFormulario+" con C.I.: "+asignreporte.getCi()+", desempeño mi trabajo en la Sección de: "+asignreporte.getDpto()+" , he recibido de la empresa "+datosempresa+" Ropa de Trabajo y Equipos de Protección Personal completamente nuevos de acuerdo al siguiente detalle:";

//        System.out.println(nom);
        if(!listalmacenReporte.isEmpty()){
            FacesContext context = FacesContext.getCurrentInstance();
            ServletContext servleContext = (ServletContext) context.getExternalContext().getContext();

            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("empleado", asignreporte.getNomempleado());
            parametros.put("solid", sol_id);
            parametros.put("solicitante", asignreporte.getNomsolicitante());
            parametros.put("almacen", nom);
            parametros.put("cargo", asignreporte.getCar_nombre());
            parametros.put("area", asignreporte.getArea());
            parametros.put("departamento", asignreporte.getDepartamento());
            parametros.put("fechasolicitud", asignreporte.getFechasolicitud());
            parametros.put("nombres", nomFormulario);
            parametros.put("apellidos", asignreporte.getApellidos());
            parametros.put("nroform", asignreporte.getNroform());
            parametros.put("ci", asignreporte.getCi());
            parametros.put("genero",asignreporte.getGenero());
            parametros.put("depto",asignreporte.getDpto());
            parametros.put("motivosolicitud", asignreporte.getMotivosolicitud());
            parametros.put("datosempresa", datosempresa);
            parametros.put("compromiso", datosemp);
            parametros.put("logo", this.getRuta());
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reimpresion.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, new JRBeanCollectionDataSource(this.getListalmacenReporte()));
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.addHeader("Content-disposition", "attachment; filename="+nomreporte+".pdf");
            ServletOutputStream stream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
            stream.flush();
            stream.close();
            FacesContext.getCurrentInstance().responseComplete();
        }
    } 
    
    public void exportarPdf(int sol_id) throws IOException, JRException, Exception {
        devolucionDAO dao = new devolucionDAO();
        asignadetalle = dao.listardevoluciondetalle(sol_id);

        listadevolucionReporte = dao.generaReporteReimpresion(sol_id);

        if(!listadevolucionReporte.isEmpty()) {
            String nom;
            String nomreporte;
            adminBean adm = new adminBean();
            nom = adm.getNombre();
            nomreporte = asignadetalle.getNomempleado();
            nomreporte = nomreporte.replace(" ","");
    //        System.out.println(nom);
            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("empleado", asignadetalle.getNomempleado());
            parametros.put("solicitante", asignadetalle.getNomsolicitante());
            parametros.put("almacen", nom);
            parametros.put("cargo", asignadetalle.getCar_nombre());
            parametros.put("area", asignadetalle.getSucursal());
            parametros.put("departamento", asignadetalle.getUbi_nombre());
            parametros.put("fechasolicitud", asignadetalle.getSol_fecha());
            parametros.put("nombres", asignadetalle.getUsu_nombre());
            parametros.put("apellidos", asignadetalle.getUsu_apellido());
            parametros.put("nroform", asignadetalle.getNro_form());
            parametros.put("ci", asignadetalle.getUsu_ci());
            parametros.put("genero",asignadetalle.getPro_genero());
            parametros.put("depto",asignadetalle.getDivision());
            parametros.put("motivosolicitud", asignadetalle.getMotivo());
            parametros.put("ruta", this.getRuta());
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reimpdevolucion.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, new JRBeanCollectionDataSource(this.getListadevolucionReporte()));
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.addHeader("Content-disposition", "attachment; filename="+nomreporte+".pdf");
            ServletOutputStream stream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
            stream.flush();
            stream.close();
            FacesContext.getCurrentInstance().responseComplete();  
        }
    }  
    
    public List<Usuariohistorial> getArrayasignacion() {
        return arrayasignacion;
    }

    public void setArrayasignacion(List<Usuariohistorial> arrayasignacion) {
        this.arrayasignacion = arrayasignacion;
    }
    
    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public String getRuta() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        ruta = (String) servletContext.getRealPath("/file/logo.png");
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public int getUsh_id() {
        return ush_id;
    }

    public void setUsh_id(int ush_id) {
        this.ush_id = ush_id;
    }

    public Usuariohistorial getAsigdetalle() {
        return asigdetalle;
    }

    public void setAsigdetalle(Usuariohistorial asigdetalle) {
        this.asigdetalle = asigdetalle;
    }

    public List<Solicitud> getSolicitudreimpresion() {
        return solicitudreimpresion;
    }

    public void setSolicitudreimpresion(List<Solicitud> solicitudreimpresion) {
        this.solicitudreimpresion = solicitudreimpresion;
    }

    public Asignacionsolicitud getAsignreporte() {
        return asignreporte;
    }

    public void setAsignreporte(Asignacionsolicitud asignreporte) {
        this.asignreporte = asignreporte;
    }

    public List<Almacen> getListalmacenReporte() {
        return listalmacenReporte;
    }

    public void setListalmacenReporte(List<Almacen> listalmacenReporte) {
        this.listalmacenReporte = listalmacenReporte;
    }

    public Devolucion getAsignadetalle() {
        return asignadetalle;
    }

    public void setAsignadetalle(Devolucion asignadetalle) {
        this.asignadetalle = asignadetalle;
    }

    public List<Devolucion> getListadevolucionReporte() {
        return listadevolucionReporte;
    }

    public void setListadevolucionReporte(List<Devolucion> listadevolucionReporte) {
        this.listadevolucionReporte = listadevolucionReporte;
    }

}
