/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.classdescuentoDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Classdescuento;

@ManagedBean
@SessionScoped
public class classdescuentoBean {

    private Classdescuento classdescuento = new Classdescuento();
    private List<Classdescuento> arrayclassdescuento;
    private String accion;
    private String nompagina="Gestor de Clases de Descuento";

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public Classdescuento getClassdescuento() {
        return classdescuento;
    }

    public void setClassdescuento(Classdescuento classdescuento) {
        this.classdescuento = classdescuento;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
        this.limpiar();
    }

    public classdescuentoBean() throws Exception {
        this.listarClassdescuento();
    }

    public List<Classdescuento> getArrayclassdescuento() {
        return arrayclassdescuento;
    }

    public void setArrayclassdescuento(List<Classdescuento> arrayclassdescuento) {
        this.arrayclassdescuento = arrayclassdescuento;
    }

    public void listarClassdescuento() throws Exception {
        classdescuentoDAO classdescuentodao = new classdescuentoDAO();
        try {
            arrayclassdescuento = classdescuentodao.listarClassdescuento();
        } catch (Exception e) {
            throw e;
        }
    }

    public void operar() throws Exception {

        if (accion.equals("Registrar")) {
            this.registrar();
            this.limpiar();
        } else {
            this.modificar();
            this.limpiar();
        }

    }

    public void registrar() throws Exception {
        try {
            classdescuentoDAO dao = new classdescuentoDAO();
            dao.registrarDao(classdescuento);
            this.listarClassdescuento();
        } catch (Exception e) {
            throw e;
        }
    }

    public void modificar() throws Exception {
        try {
            classdescuentoDAO dao = new classdescuentoDAO();
            dao.modificarDao(classdescuento);
            this.listarClassdescuento();
        } catch (Exception e) {
            throw e;
        }
    }
    public void leerModificar(Classdescuento men) throws Exception{
        classdescuentoDAO dao;
        Classdescuento classdescuentodb;
        try {
            dao=new classdescuentoDAO();
            classdescuentodb=dao.leerClassdescuento(men);
            if(classdescuentodb!=null){
                this.accion="Modificar";
                this.classdescuento=classdescuentodb;
            }
        } catch (Exception e) {
            throw e;
        }
    }
 public void leerEliminar(Classdescuento men) throws Exception{
        classdescuentoDAO dao;
        try {
            dao=new classdescuentoDAO();
            dao.eliminarClassdescuento(men);
            this.listarClassdescuento();
          } catch (Exception e) {
            throw e;
        }
    }
    public void limpiar() {
        this.classdescuento.setClas_id(0);
        this.classdescuento.setClas_nombre("");
        this.classdescuento.setClas_tipo("");
        this.classdescuento.setClas_estado(0);
    }

}
