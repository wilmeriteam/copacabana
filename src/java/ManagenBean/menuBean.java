
package ManagenBean;

import dao.menuDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Menu;

@ManagedBean
@SessionScoped
public class menuBean {

    private Menu menu = new Menu();
    private List<Menu> arraymenu;
    private String accion;
    private String nompagina="Gestor de menús";

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
        this.limpiar();
    }

    public menuBean() throws Exception {
        this.listarMenu();
    }

    public List<Menu> getArraymenu() {
        return arraymenu;
    }

    public void setArraymenu(List<Menu> arraymenu) {
        this.arraymenu = arraymenu;
    }

    public void listarMenu() throws Exception {
        menuDAO menudao = new menuDAO();
        try {
            arraymenu = menudao.listarMenu();
        } catch (Exception e) {
            throw e;
        }
    }

    public void operar() throws Exception {

        if (accion.equals("Registrar")) {
            this.registrar();
            this.limpiar();
        } else {
            this.modificar();
            this.limpiar();
        }

    }

    public void registrar() throws Exception {
        try {
            menuDAO dao = new menuDAO();
            dao.registrarDao(menu);
            this.listarMenu();
        } catch (Exception e) {
            throw e;
        }
    }

    public void modificar() throws Exception {
        try {
            menuDAO dao = new menuDAO();
            dao.modificarDao(menu);
            this.listarMenu();
        } catch (Exception e) {
            throw e;
        }
    }
    public void leerModificar(Menu men) throws Exception{
        menuDAO dao;
        Menu menudb;
        try {
            dao=new menuDAO();
            menudb=dao.leerMenu(men);
            if(menudb!=null){
                this.accion="Modificar";
                this.menu=menudb;
            }
        } catch (Exception e) {
            throw e;
        }
    }
 public void leerEliminar(Menu men) throws Exception{
        menuDAO dao;
        try {
            dao=new menuDAO();
            dao.eliminarMenu(men);
            this.listarMenu();
          } catch (Exception e) {
            throw e;
        }
    }
    public void limpiar() {
        this.menu.setMen_id(0);
        this.menu.setMen_nombre("");
        this.menu.setMen_alias("");
    }

}
