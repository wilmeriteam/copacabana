/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.puestotrabajoDAO;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Puestotrabajo;

@ManagedBean
@SessionScoped
public class puestotrabajoBean {

    private Puestotrabajo puestotrabajo = new Puestotrabajo();
    private List<Puestotrabajo> arraypuestotrabajo;
    private String accion;
    private String nompagina="Gestor Ropa de Puesto de trabajo";

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public Puestotrabajo getPuestotrabajo() {
        return puestotrabajo;
    }

    public void setPuestotrabajo(Puestotrabajo puestotrabajo) {
        this.puestotrabajo = puestotrabajo;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
        this.limpiar();
    }

    public puestotrabajoBean() throws Exception {
        this.listarPuestotrabajo();
    }

    public List<Puestotrabajo> getArraypuestotrabajo() {
        return arraypuestotrabajo;
    }

    public void setArraypuestotrabajo(List<Puestotrabajo> arraypuestotrabajo) {
        this.arraypuestotrabajo = arraypuestotrabajo;
    }

    public void listarPuestotrabajo() throws Exception {
        puestotrabajoDAO puestotrabajodao = new puestotrabajoDAO();
        try {
            arraypuestotrabajo = puestotrabajodao.listarPuestotrabajo();
        } catch (Exception e) {
            throw e;
        }
    }

    public void operar() throws Exception {

        if (accion.equals("Registrar")) {
            this.registrar();
            this.limpiar();
        } else {
            this.modificar();
            this.limpiar();
        }

    }

    public void registrar() throws Exception {
        try {
            puestotrabajoDAO dao = new puestotrabajoDAO();
            dao.registrarDao(puestotrabajo);
            this.listarPuestotrabajo();
        } catch (Exception e) {
            throw e;
        }
    }

    public void modificar() throws Exception {
        try {
            puestotrabajoDAO dao = new puestotrabajoDAO();
            dao.modificarDao(puestotrabajo);
            this.listarPuestotrabajo();
        } catch (Exception e) {
            throw e;
        }
    }
    public void leerModificar(Puestotrabajo men) throws Exception{
        puestotrabajoDAO dao;
        Puestotrabajo puestotrabajodb;
        try {
            dao=new puestotrabajoDAO();
            puestotrabajodb=dao.leerPuestotrabajo(men);
            if(puestotrabajodb!=null){
                this.accion="Modificar";
                this.puestotrabajo=puestotrabajodb;
            }
        } catch (Exception e) {
            throw e;
        }
    }
 public void leerEliminar(Puestotrabajo men) throws Exception{
        puestotrabajoDAO dao;
        try {
            dao=new puestotrabajoDAO();
            dao.eliminarPuestotrabajo(men);
            this.listarPuestotrabajo();
          } catch (Exception e) {
            throw e;
        }
    }
    public void limpiar() {
       this.puestotrabajo.setPtb_id(0);
       this.puestotrabajo.setPtb_nombre("");
       this.puestotrabajo.setPtb_estado(0);
    }

}
