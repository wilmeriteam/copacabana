package ManagenBean;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import web.services.wsAlmacen;
import web.services.wsArea;
import web.services.wsAreatrabajocargo;
import web.services.wsCargo;
import web.services.wsDivision;
import web.services.wsEmpleado;
import web.services.wsItemgrupo;
import web.services.wsOficinas;
import web.services.wsProducto;
import web.services.wsProductoPersonal;
import web.services.wsRopatrabajo;
import web.services.wsTipopersonal;

/**
 *
 * @author Wilmer
 */
@ManagedBean
@SessionScoped
public class webserviceBean {
    
//    FacesContext context = FacesContext.getCurrentInstance();    

    public void eWsOficinas() throws Exception {
        try {
            wsOficinas ini = new wsOficinas();
            ini.updateInsertOficina();
        } catch (Exception e) {
            throw e;
        }
    }
    
    /**
     * @Description Consume web service de un determinado producto y cargo
     * @throws Exception 
     * @param cargo codigo de prenda
     */
    public void eWsProductoPersonal() throws Exception{
        try {
            wsProductoPersonal pp = new wsProductoPersonal();
            pp.updateInsertProducto("-PERSONAL DE CAFETERIA", "8-00-06-0002");
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void eWsCargo() throws Exception{
        try {
            wsCargo cargo = new wsCargo();
            cargo.updateInsertCargo();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsAlmacen() throws Exception{
        try {
            wsAlmacen al = new wsAlmacen();
            al.updateInsertAlmacen();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsArea() throws Exception{
        try {
            wsArea area = new wsArea();
            area.updateInsertArea();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsAreaTrabajo() throws Exception{
        try {
            wsAreatrabajocargo atrabajo = new wsAreatrabajocargo();
            atrabajo.updateInsertAreatrabajocargo();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsDivision() throws Exception{
        try {
            wsDivision division = new wsDivision();
            division.updateInsertDivision();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsEmpleado() throws Exception{
        try {
            wsEmpleado empleado = new wsEmpleado();
            empleado.updateInsertEmpleado();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsItemgrupo() throws Exception{
        try {
            wsItemgrupo igrupo = new wsItemgrupo();
            igrupo.updateInsertItemgrupo();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsProducto() throws Exception{
        try {
            wsProducto producto = new wsProducto();
            producto.updateInsertProducto();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsRopatrabajo() throws Exception{
        try {
            wsRopatrabajo rtrabajo = new wsRopatrabajo();
            rtrabajo.updateInsertRopatrabajo();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    
    public void eWsTipopersonal() throws Exception{
        try {
            wsTipopersonal tpersonal = new wsTipopersonal();
            tpersonal.updateInsertTipopersonal();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
}
