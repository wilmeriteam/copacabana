/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.datosempresaDAO;
import dao.usuariohistorialDAO;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import model.Datosempresa;
//import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author flex
 */
@ManagedBean
@RequestScoped
public class loginBean {

    private String usuario;
    private String clave;
    private final FacesContext faceCont;
    private FacesMessage facesMg;
    private Datosempresa datosempresa = new Datosempresa();

    public Datosempresa getDatosempresa() {
        return datosempresa;
    }

    public void setDatosempresa(Datosempresa datosempresa) {
        this.datosempresa = datosempresa;
    }

    public loginBean() throws Exception {
        faceCont = FacesContext.getCurrentInstance();
        this.mostrarLogo();
        
    }

    public void mostrarLogo() throws Exception {
        try {
            datosempresaDAO dao = new datosempresaDAO();
            datosempresa = dao.leerDatosempresa();
        } catch (Exception e) {
            throw e;
        }
    }

    public String login() throws Exception {
        usuariohistorialDAO daousu;
        daousu = new usuariohistorialDAO();
        if (daousu.VerificarUsuario(usuario, clave)) {
            facesMg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Acceso correcto", null);
            faceCont.addMessage(null, facesMg);
            return "principal";
        } else {
            facesMg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Clave incorrecta", null);
            faceCont.addMessage(null, facesMg);
            return "login";
        }
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

}
