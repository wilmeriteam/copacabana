/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagenBean;

import dao.almacenDAO;
import dao.tipoprendaDAO;
import dao.claseropatrabajoDAO;
import dao.detalleasignacionDAO;
import dao.motivosolicitudDAO;
import dao.talentoDAO;
import dao.usuariohistorialDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Almacen;
import model.Cargo;
import model.Claseropatrabajo;
import model.Detalleasignacion;
import model.Motivodescuento;
import model.Motivosolicitud;
import model.Talento;
import model.Sucursal;
import model.Tipoprenda;
import model.Ubicacion;
import model.Usuariohistorial;
import web.services.wsEmpleado;
import web.services.wsOficinas;

@ManagedBean
@SessionScoped
public class talentoBean {
//private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(almacenDAO.class);
    
    private List<Talento> arraytalento;
    private List<Usuariohistorial> arrayusuario;
    private List<Motivodescuento> selectmotivotalento;
    private List<SelectItem> selectUsuarios;
    private List<SelectItem> itemsSelect;
    private List<SelectItem> claseropaSelect;
    private List<Almacen> listalmacen;
    private FacesMessage facesMg;

    private List<SelectItem> tipoprendaselect;

    private Usuariohistorial usuario = new Usuariohistorial();
    private Ubicacion ubicacion = new Ubicacion();
    private Sucursal sucursal = new Sucursal();
    private Cargo cargo = new Cargo();
    private String nomcargo;
    private String accion;
    private Talento talento = new Talento();
    private Almacen almacen = new Almacen();
    public Tipoprenda tipoprenda = new Tipoprenda();
    private Claseropatrabajo claseropatrabajo = new Claseropatrabajo();
    public String cadenaItems = null;
    private int cantidaditem;
    private final HttpServletRequest httpServletR;
    private final FacesContext faceCont;
    private int ush_id = 0;
    private int rol_id;
   private int cantidadropa;

    public int getCantidadropa() {
        return cantidadropa;
    }

    public void setCantidadropa(int cantidadropa) {
        this.cantidadropa = cantidadropa;
    }

    public int getRol_id() {
        return rol_id;
    }

    public void setRol_id(int rol_id) {
        this.rol_id = rol_id;
    }

    public int getUsh_id() {
        return ush_id;
    }

    public void setUsh_id(int ush_id) {
        this.ush_id = ush_id;
    }
    private Detalleasignacion detalleasig = new Detalleasignacion();

    private int solid;

    public List<Motivodescuento> getSelectmotivotalento() {
        return selectmotivotalento;
    }

    public void setSelectmotivosolicitud(List<Motivodescuento> selectmotivotalento) {
        this.selectmotivotalento = selectmotivotalento;
    }

    public FacesMessage getFacesMg() {
        return facesMg;
    }

    public void setFacesMg(FacesMessage facesMg) {
        this.facesMg = facesMg;
    }

    public Detalleasignacion getDetalleasig() {
        return detalleasig;
    }

    public void setDetalleasig(Detalleasignacion detalleasig) {
        this.detalleasig = detalleasig;
    }

    public int getSolid() {
        return solid;
    }

    public void setSolid(int solid) {
        this.solid = solid;
    }

    public int getCantidaditem() {
        return cantidaditem;
    }

    public void setCantidaditem(int cantidaditem) {
        this.cantidaditem = cantidaditem;
    }

    public String getCadenaItems() {
        return cadenaItems;
    }

    public void setCadenaItems(String cadenaItems) {
        this.cadenaItems = cadenaItems;
    }

    public List<Almacen> getListalmacen() {
        return listalmacen;
    }

    public void setListalmacen(List<Almacen> listalmacen) {
        this.listalmacen = listalmacen;
    }

    public List<SelectItem> getItemsSelect() {
        return itemsSelect;
    }

    public void setItemsSelect(List<SelectItem> itemsSelect) {
        this.itemsSelect = itemsSelect;
    }

    public List<SelectItem> getClaseropaSelect() {

        return claseropaSelect;
    }

    public void setClaseropaSelect(List<SelectItem> claseropaSelect) {
        this.claseropaSelect = claseropaSelect;
    }

    public Claseropatrabajo getClaseropatrabajo() {
        return claseropatrabajo;
    }

    public void setClaseropatrabajo(Claseropatrabajo claseropatrabajo) {
        this.claseropatrabajo = claseropatrabajo;
    }

    public Tipoprenda getTipoprenda() {
        return tipoprenda;
    }

    public void setTipoprenda(Tipoprenda tipoprenda) {
        this.tipoprenda = tipoprenda;
    }

    public String getNomcargo() {
        return nomcargo;
    }

    public void setNomcargo(String nomcargo) {
        this.nomcargo = nomcargo;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Usuariohistorial getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuariohistorial usuario) {
        this.usuario = usuario;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    private String nompagina = "Gestor de Talento";

    public List<Usuariohistorial> getArrayusuario() {
        return arrayusuario;
    }

    public void setArrayusuario(List<Usuariohistorial> arrayusuario) {
        this.arrayusuario = arrayusuario;
    }

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }

    public List<Talento> getArraytalento() {
        return arraytalento;
    }

    public void setArraytalento(List<Talento> arraytalento) {
        this.arraytalento = arraytalento;
    }

    public List<SelectItem> getSelectUsuarios() {
        return selectUsuarios;
    }

    public void setSelectUsuarios(List<SelectItem> selectUsuarios) {
        this.selectUsuarios = selectUsuarios;
    }

    public Talento getTalento() {
        return talento;
    }

    public void setTalento(Talento talento) {
        this.talento = talento;
    }

    public String getAccion() {
        return accion;
    }

//    private List<Almacen> selectItemsol = new ArrayList<Almacen>();
//
//    public List<Almacen> getSelectItemsol() {
//        return selectItemsol;
//    }
//
//    public void setSelectItemsol(List<Almacen> selectItemsol) {
//        this.selectItemsol = selectItemsol;
//    }
    
    private List<Almacen> selectItemTal = new ArrayList<Almacen>();

    public List<Almacen> getSelectItemTal() {
        return selectItemTal;
    }

    public void setSelectItemTal(List<Almacen> selectItemTal) {
        this.selectItemTal = selectItemTal;
    }
    
    public void setAccion(String accion) throws Exception {

        this.limpiar();
        this.accion = accion;

    }

    //Constructor
    public talentoBean() throws Exception {
        faceCont = FacesContext.getCurrentInstance();
        httpServletR = (HttpServletRequest) faceCont.getExternalContext().getRequest();
//        arraytalento = null;
        try {
            this.limpiar();
            this.listarTalento();
            usuariohistorialDAO usudao = new usuariohistorialDAO();
            arrayusuario = usudao.listarNombres();
            this.selectmtvlista();
            
        } catch (Exception e) {
            throw e;
        }


    }
//TEMPORAL MENTE MODIFICADO
//    public void EliminarRegistro(Almacen deta) throws Exception {
//        try {
//            detalleasignacionDAO daodeta = new detalleasignacionDAO();
//            daodeta.eliminarDetalleAsignacion(deta);
//            this.cargarListTableItem();
//        } catch (Exception e) {
//            throw e;
//        }
//    }

    public void selectmtvlista() throws Exception {
        motivosolicitudDAO dao = new motivosolicitudDAO();
        adminBean adm = new adminBean();
        int rolid=adm.getRol_id();
        try {
            this.selectmotivotalento = dao.listarSelectMotivoSolicitud(rolid);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<SelectItem> getTipoprendaselect() {
        tipoprendaselect = new ArrayList<>();
        tipoprendaDAO dp = new tipoprendaDAO();
        try {
            List<Tipoprenda> tp = dp.selectTipoprenda();
            for (Tipoprenda row : tp) {
                SelectItem selectprenda = new SelectItem(row.getTip_id(), row.getTip_nombre());
                tipoprendaselect.add(selectprenda);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tipoprendaselect;
    }

    public List<SelectItem> getSelectItems() {
        selectUsuarios = new ArrayList<SelectItem>();
        usuariohistorialDAO usudao = new usuariohistorialDAO();
        int cont = 0;
//        logger.info("pase por aqui");
        try {
            List<Usuariohistorial> usuarios = usudao.listarNombres();
            for (Usuariohistorial usu : usuarios) {
                SelectItem selectItem = new SelectItem(usu.getUsh_id(), usu.getUsu_nombre() + " " + usu.getUsu_apellido());
                
                selectUsuarios.add(selectItem);
            }
        } catch (Exception e) {
        }

        return selectUsuarios;
    }

    public void listarTalento() throws Exception {
        adminBean adm=new adminBean();
        ush_id=Integer.parseInt(adm.getUsu_id());
        talentoDAO talentodao = new talentoDAO();
        arraytalento = null;
        try {
            arraytalento = talentodao.listarTalento(ush_id);
            
        } catch (Exception e) {
            throw e;
//            logger.info(e);
        }
    }
    
    public void execwsEmpleado() throws Exception {
        try {
            wsEmpleado ini = new wsEmpleado();
            ini.updateInsertEmpleado();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Proceso concluido satisfactoriamente"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Ocurrió un Error"));
            throw e;
        }
    }
    

    public void operar() throws Exception {
        this.registrar();
    }

    public void updateEstado() throws Exception {

        try {
            
            
            talentoDAO ds = new talentoDAO();
            ds.actualizarEstado(solid, talento.getUsu_idempleado());
            this.listarTalento();
            this.limpiar();
        } catch (Exception e) {
            throw e;
        }
    }

    public void registrar() throws Exception {
        adminBean dm = new adminBean();
        int usu_id = Integer.parseInt(dm.getUsu_id());
//        ush_id=talento.getMot_id();
        try {
            talentoDAO dao=new talentoDAO();
            dao.registrartalentoDao(talento,usu_id,selectItemTal);
//            dao.registrarDao(talento,usu_id);
             
        } catch (Exception e) {
            throw e;
        }      
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
        try {
            ctx.redirect(ctxPath + "/faces/talento.xhtml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.listarTalento();
    }

//    public void registrar() throws Exception {
//        adminBean dm = new adminBean();
//        int usu_id = Integer.parseInt(dm.getUsu_id());
//        int idmensaje = 0;
//        try {
//            if (modificarver == 1) {
//                solicitudDAO dao = new solicitudDAO();
//                idmensaje = dao.registrarsolicitudDao(solicitud, usu_id, selectItemsol);
//            }
//            if (modificarver == 2) {
//                solicitudDAO dao = new solicitudDAO();
//                idmensaje = dao.modificarsolicitudDao(solicitud, usu_id, selectItemsol, solid);
//            }
//
//        } catch (Exception e) {
//            throw e;
//        }
//        if (idmensaje == 1) {
//            mensaje = "No puede crear solicitud de dotación y Reasignación";
//        }
//        if (idmensaje == 2) {
//            mensaje = "Seleccione registros";
//        }
//        if (idmensaje == 3) {
//            mensaje = "No puede modificar por que una o más prendas fueron ya asignados.";
//        }
//
//        if (idmensaje == 0) {
//            ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
//            String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
//            try {
//                ctx.redirect(ctxPath + "/faces/solicitud.xhtml");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
    public void leerModificar(Talento solidfila) throws Exception {
        talentoDAO dao;
        Talento talentodb;
        almacenDAO aldao;
        solid = solidfila.getSol_id();
        claseropaSelect = new ArrayList<SelectItem>();
        claseropatrabajoDAO daort;
        try {
            dao = new talentoDAO();
            talentodb = dao.leerTalento(solidfila);
            if (talentodb != null) {
                this.accion = "Modificar";
                this.talento = talentodb;
//                 this.cargarListTableItem();
                tipoprenda.setTip_id(talento.getTip_id());
                claseropatrabajo.setClast_id(talento.getClast_id());

                daort = new claseropatrabajoDAO();

                List<Claseropatrabajo> clasr = daort.obtenerSelectClaseRopaAll();
                for (Claseropatrabajo row : clasr) {
                    SelectItem selectclaseropa = new SelectItem(row.getClast_id(), row.getClast_nombre());
                    claseropaSelect.add(selectclaseropa);
                }
                aldao = new almacenDAO();
                listalmacen = aldao.listarAlmacenOneRow(talento.getSol_id());

            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerEliminar(Talento men) throws Exception {
        talentoDAO dao;
        try {
            dao = new talentoDAO();
            dao.eliminarTalento(men);
            this.listarTalento();
        } catch (Exception e) {
            throw e;
        }
    }

    public void limpiar() {
        
        this.talento.setSol_id(0);
        this.talento.setSol_fecha("");
        this.talento.setUsu_idsolicitante(0);
        this.talento.setUsu_idempleado(0);
        this.talento.setSol_fechaasignacion("");
        this.talento.setSol_fechadevolucion("");
        this.talento.setSol_estado(0);
        this.talento.setNomcargo("");
        this.talento.setNomusuario_empleado("");
        this.talento.setSol_descripcion("");
        this.talento.setSucursal("");
        this.tipoprenda.setTip_id(0);
        this.claseropatrabajo.setClast_id(0);
        this.almacen.setAl_id(0);
        this.solid = 0;
        this.listalmacen = null;
        this.almacen.setAl_id(0);
        this.almacen.setAl_cantidad(0);
        this.almacen.setAl_costo(0);
        this.talento.setMot_id(0);
        this.talento.setCi(0);

    }

    public void cargarDatos(AjaxBehaviorEvent event) throws Exception {
        int valor = talento.getUsu_idempleado();
        cantidadropa = 0;
        ush_id = valor;
        try {
            talentoDAO dao = new talentoDAO();
            talento = dao.ObtenerTalento(valor);
            cantidadropa=dao.cantidadRopa(talento);
            listalmacen=dao.listaralmacenEvento(talento); 
        } catch (Exception e) {
            throw e;
        }
//        setnom
    }
    
    public void cargarSelectClase(AjaxBehaviorEvent event) throws Exception {

        claseropaSelect = new ArrayList<SelectItem>();
        try {
            int valor = tipoprenda.getTip_id();
            claseropatrabajoDAO dao = new claseropatrabajoDAO();
            List<Claseropatrabajo> clasr = dao.obtenerSelectClaseRopa(valor);
            for (Claseropatrabajo row : clasr) {
                SelectItem selectclaseropa = new SelectItem(row.getClast_id(), row.getClast_nombre());
                claseropaSelect.add(selectclaseropa);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            this.itemsSelect = null;
        }
    }

//    public void cargarSelectItems(AjaxBehaviorEvent event) throws Exception {
//
//        itemsSelect = new ArrayList<SelectItem>();
//        try {
//            int valor = claseropatrabajo.getClast_id();
//            almacenDAO dao = new almacenDAO();
//            List<Almacen> al = dao.listarAlmacenSelectItem(valor);
//            for (Almacen row : al) {
//                SelectItem selectclaseropa = new SelectItem(row.getAl_id(), row.getRopa());
//                itemsSelect.add(selectclaseropa);
//            }
//        } catch (Exception e) {
//            throw e;
//        }
//    }

    public void cargarListTableItem() throws Exception {

        try {
            almacenDAO dao = new almacenDAO();
            listalmacen = dao.listarAlmacentalento(talento, solid);
            
        } catch (Exception e) {
            throw e;
        }

    }

    public void selectAjaxCantidad(AjaxBehaviorEvent event) {
        int valor = almacen.getAl_id();
        try {
            almacenDAO dao = new almacenDAO();
            almacen = dao.SeleccionarCantidadItemTalento(valor);
//            almacen.setAl_cantidad(cantidaditem);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
