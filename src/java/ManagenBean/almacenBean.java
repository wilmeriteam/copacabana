package ManagenBean;


import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import dao.cargoDAO;
import dao.almacenDAO;
import java.util.Date;
import model.Almacen;
import model.Cargo;
import model.Solicitud;

@ManagedBean
@SessionScoped
public class almacenBean {

    private List<Almacen> arrayalmacen;
    private String nompagina="Lista de Items";
    private List<Cargo> arraycargos;
    private List<Almacen> listalmacen;
    private Cargo cargo = new Cargo();
    private Almacen almacen = new Almacen();
    private Solicitud solicitud = new Solicitud();
    private String palabra = "";
    private Date finicial;
    private Date ffinal;

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Date getFinicial() {
        return finicial;
    }

    public void setFinicial(Date finicial) {
        this.finicial = finicial;
    }

    public Date getFfinal() {
        return ffinal;
    }

    
    public void setFfinal(Date ffinal) throws Exception {
        this.cargarProductos();
        this.ffinal = ffinal;
    }

    
    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) throws Exception {
        almacenDAO usuDAO = new almacenDAO();
        solicitud = usuDAO.kardexEmpleado(palabra);
        this.cargarProductos();
        this.palabra = palabra;
    }

    public List<Almacen> getListalmacen() {
        return listalmacen;
    }

    public void setListalmacen(List<Almacen> listalmacen) {
        this.listalmacen = listalmacen;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    
    public List<Cargo> getArraycargos() {
        return arraycargos;
    }

    public void setArraycargos(List<Cargo> arraycargos) {
        this.arraycargos = arraycargos;
    }

    public String getNompagina() {
        return nompagina;
    }

    public void setNompagina(String nompagina) {
        this.nompagina = nompagina;
    }


    public almacenBean() throws Exception {
//        this.listarAlmacen();
        this.listarCargos();
    }

    public List<Almacen> getArrayalmacen() {
        return arrayalmacen;
    }

    public void setArrayalmacen(List<Almacen> arrayalmacen) {
        this.arrayalmacen = arrayalmacen;
    }

    public void listarCargos() throws Exception {
        cargoDAO carDAO = new cargoDAO();
        try {
            arraycargos = carDAO.SelectListCargos();
        } catch (Exception e) {
            throw e;
        }
    }

    public void cargarProductos() throws Exception {
        try {
            almacenDAO dao = new almacenDAO();
            listalmacen = dao.getProductos(solicitud.getUsu_idempleado()); 
        } catch (Exception e) {
            throw e;
        }
    }    
    
    public void cargarDatos() throws Exception {
        int valor = cargo.getCar_id();
        try {
            almacenDAO dao = new almacenDAO();
            listalmacen = dao.listaAlmacenCantidad(valor); 
        } catch (Exception e) {
            throw e;
        }
    }
    
}
