package ManagenBean;

import javax.faces.bean.ManagedBean;
//import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import model.Devolucion;
import dao.devolucionDAO;
import dao.motivodescuentoDAO;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Motivodescuento;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@ManagedBean
@SessionScoped
public class devolucionBean {

    private final HttpServletRequest httpServletR;
    private final FacesContext faceCont;
    private Devolucion devolucion = new Devolucion();
    private List<Devolucion> arraydevolucion;
    private List<Devolucion> listdev;
    private List<Devolucion> listalmacendevolucion;    
    private List<Devolucion> listaselecciondevolucion;    
    private String nompagina = "Devolución de Ropa de Trabajo";
    private List<Motivodescuento> arrayselectmotivo;
    private Devolucion devoldetalle = new Devolucion();
    private Devolucion asigdetalle = new Devolucion();
    private List<Devolucion> listadevolucionReporte;
    private String palabra="";
    private String buscar="";
    private int sol_id;
    private int ush_id;

    public String getRuta() {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String ruta = (String) servletContext.getRealPath("/file/logo.png");
        return ruta;
    }

    public Devolucion getAsigdetalle() {
        return asigdetalle;
    }

    public void setAsigdetalle(Devolucion asigdetalle) {
        this.asigdetalle = asigdetalle;
    }

    public List<Devolucion> getListadevolucionReporte() {
        return listadevolucionReporte;
    }

    public void setListadevolucionReporte(List<Devolucion> listadevolucionReporte) {
        this.listadevolucionReporte = listadevolucionReporte;
    }

    public String getBuscar() {
        return buscar;
    }

    public void setBuscar(String buscar) {
        this.buscar = buscar;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public int getUsh_id() {
        return ush_id;
    }

    public void setUsh_id(int ush_id) {
        this.ush_id = ush_id;
    }

    public List<Devolucion> getListaselecciondevolucion() {
        return listaselecciondevolucion;
    }

    public void setListaselecciondevolucion(List<Devolucion> listaselecciondevolucion) {
        this.listaselecciondevolucion = listaselecciondevolucion;
    }

    public List<Devolucion> getListalmacendevolucion() {
        return listalmacendevolucion;
    }

    public void setListalmacendevolucion(List<Devolucion> listalmacendevolucion) {
        this.listalmacendevolucion = listalmacendevolucion;
    }

    public Devolucion getDevoldetalle() {
        return devoldetalle;
    }

    public void setDevoldetalle(Devolucion devoldetalle) {
        this.devoldetalle = devoldetalle;
    }

    public List<Devolucion> getListdev() {
        return listdev;
    }

    public void setListdev(List<Devolucion> listdev) {
        this.listdev = listdev;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }
    

    public List<Motivodescuento> getArrayselectmotivo() throws Exception {
        motivodescuentoDAO daomtd=new motivodescuentoDAO();
        arrayselectmotivo=daomtd.listarSelectDescuento();
        return arrayselectmotivo;
    }

    public void setArrayselectmotivo(List<Motivodescuento> arrayselectmotivo) {
        this.arrayselectmotivo = arrayselectmotivo;
    }

    public String getNompagina() {
        return nompagina;
    }

    public Devolucion getDevolucion() {
        return devolucion;
    }

    public void setDevolucion(Devolucion devolucion) {
        this.devolucion = devolucion;
    }

    public List<Devolucion> getArraydevolucion() {
        return arraydevolucion;
    }

    public void setArraydevolucion(List<Devolucion> arraydevolucion) {
        this.arraydevolucion = arraydevolucion;
    }

    public devolucionBean() throws Exception {
        faceCont = FacesContext.getCurrentInstance();
        httpServletR = (HttpServletRequest) faceCont.getExternalContext().getRequest();
        ush_id = Integer.parseInt(httpServletR.getSession().getAttribute("ush_id").toString());
//        this.listarDevolucionBean();
        this.listardevolucion(buscar);
//        devolucionDAO dao = new devolucionDAO();
//        this.listaselecciondevolucion = dao.listarDevolucionTemp(sol_id, ush_id);        
    }

    public void listarDevolucionBean() throws Exception {
        try {
            devolucionDAO dao = new devolucionDAO();
            this.arraydevolucion = dao.listarDevolucionDao(palabra);
        } catch (Exception e) {
            throw e;
        }
    }

    public void leerIdBean(Devolucion dev) throws Exception {
        try {
            devolucionDAO dao=new devolucionDAO();
           this.devolucion=dao.leerIdDao(dev);
           
        } catch (Exception e) {
            throw e;
        }
    }
    public void guardarDevolucionBean() throws Exception{
        adminBean dm = new adminBean();
        int usu_id = Integer.parseInt(dm.getUsu_id());
        try {
            devolucionDAO dao=new devolucionDAO();
//            dao.guardarDevolucionDao(devolucion,usu_id);
            dao.guardaDevolucionTemporal(devolucion,ush_id);
//            this.listarDevolucionBean();
            this.listardevolucion(palabra);
            this.listaselecciondevolucion = dao.listarDevolucionTemp(sol_id, ush_id,devolucion);
        } catch (Exception e) {
            throw e;
        }
   
    }
     public void bucarDevolucion() throws Exception {
        try {
            this.listarDevolucionBean();
        } catch (Exception e) {
            throw e;
        }
    }
     
    public void buscarUsuario() throws Exception {
        try {
            this.listardevolucion(buscar);
        } catch (Exception e) {
            throw e;
        }
    }     

    public void listardevolucion(String pal) throws Exception{ 
        try {
            devolucionDAO dao = new devolucionDAO();
            this.listdev = dao.listdev( pal);
            listalmacendevolucion = dao.listarDevolucionOneRow(sol_id, devoldetalle.getUsu_idempleado(),devolucion,ush_id);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void obtenerDato(int id) throws Exception {
        sol_id = id;
        try { 
            devolucionDAO dao = new devolucionDAO();
            dao.deleteDevolucionTemporal(ush_id);
            
//            devolucionDAO dao = new devolucionDAO();
            devoldetalle = dao.listardevoluciondetalle(id);
            if(devoldetalle != null){
                listalmacendevolucion = dao.listarDevolucionOneRow(sol_id, devoldetalle.getUsu_idempleado(),devolucion,ush_id);
                
//                listalmacenReporte = dao.listarAlmacenReporte(id);
            } 
            else {
                ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
                String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
                try {
                    ctx.redirect(ctxPath + "/faces/devolucion.xhtml");
                } catch (Exception e) {
                    e.printStackTrace();
                }                
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void exportarPdf() throws IOException, JRException, Exception {
        devolucionDAO dao = new devolucionDAO();
        asigdetalle = dao.listardevoluciondetalle(sol_id);
        dao.guardarDevolucionDao(devolucion,ush_id,devoldetalle.getSol_id());
        listadevolucionReporte = dao.generaReporte(ush_id);
        dao.deleteDevolucionTemporal(ush_id);
        listalmacendevolucion = dao.listarDevolucionOneRow(sol_id, devoldetalle.getUsu_idempleado(),devolucion,ush_id);
        dao.updateSolicitud(devoldetalle.getUsu_idempleado(), sol_id);
        if(!listadevolucionReporte.isEmpty()) {
            String nom;
            String nomreporte;
            adminBean adm = new adminBean();
            nom = adm.getNombre();
            nomreporte = asigdetalle.getNomempleado();
            nomreporte = nomreporte.replace(" ","");
    //        System.out.println(nom);
            Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("empleado", asigdetalle.getNomempleado());
            parametros.put("solicitante", asigdetalle.getNomsolicitante());
            parametros.put("almacen", nom);
            parametros.put("cargo", asigdetalle.getCar_nombre());
            parametros.put("area", asigdetalle.getSucursal());
            parametros.put("departamento", asigdetalle.getUbi_nombre());
            parametros.put("fechasolicitud", asigdetalle.getSol_fecha());
            parametros.put("nombres", asigdetalle.getUsu_nombre());
            parametros.put("apellidos", asigdetalle.getUsu_apellido());
            parametros.put("nroform", asigdetalle.getNro_form());
            parametros.put("ci", asigdetalle.getUsu_ci());
            parametros.put("genero",asigdetalle.getPro_genero());
            parametros.put("depto",asigdetalle.getDivision());
            parametros.put("motivosolicitud", asigdetalle.getMotivo());
            parametros.put("ruta", this.getRuta());
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/devoluciones.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, new JRBeanCollectionDataSource(this.getListadevolucionReporte()));
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.addHeader("Content-disposition", "attachment; filename="+nomreporte+".pdf");
            ServletOutputStream stream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
            stream.flush();
            stream.close();
            FacesContext.getCurrentInstance().responseComplete();  
        }
    }    
}
