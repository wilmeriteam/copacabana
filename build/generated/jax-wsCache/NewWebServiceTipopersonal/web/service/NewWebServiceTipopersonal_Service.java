
package web.service;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10-b140803.1500
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "NewWebServiceTipopersonal", targetNamespace = "http://service.web/", wsdlLocation = "http://localhost:8080/NewWebServiceTipopersonal/NewWebServiceTipopersonal?wsdl")
public class NewWebServiceTipopersonal_Service
    extends Service
{

    private final static URL NEWWEBSERVICETIPOPERSONAL_WSDL_LOCATION;
    private final static WebServiceException NEWWEBSERVICETIPOPERSONAL_EXCEPTION;
    private final static QName NEWWEBSERVICETIPOPERSONAL_QNAME = new QName("http://service.web/", "NewWebServiceTipopersonal");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/NewWebServiceTipopersonal/NewWebServiceTipopersonal?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        NEWWEBSERVICETIPOPERSONAL_WSDL_LOCATION = url;
        NEWWEBSERVICETIPOPERSONAL_EXCEPTION = e;
    }

    public NewWebServiceTipopersonal_Service() {
        super(__getWsdlLocation(), NEWWEBSERVICETIPOPERSONAL_QNAME);
    }

    public NewWebServiceTipopersonal_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), NEWWEBSERVICETIPOPERSONAL_QNAME, features);
    }

    public NewWebServiceTipopersonal_Service(URL wsdlLocation) {
        super(wsdlLocation, NEWWEBSERVICETIPOPERSONAL_QNAME);
    }

    public NewWebServiceTipopersonal_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, NEWWEBSERVICETIPOPERSONAL_QNAME, features);
    }

    public NewWebServiceTipopersonal_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public NewWebServiceTipopersonal_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns NewWebServiceTipopersonal
     */
    @WebEndpoint(name = "NewWebServiceTipopersonalPort")
    public NewWebServiceTipopersonal getNewWebServiceTipopersonalPort() {
        return super.getPort(new QName("http://service.web/", "NewWebServiceTipopersonalPort"), NewWebServiceTipopersonal.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns NewWebServiceTipopersonal
     */
    @WebEndpoint(name = "NewWebServiceTipopersonalPort")
    public NewWebServiceTipopersonal getNewWebServiceTipopersonalPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://service.web/", "NewWebServiceTipopersonalPort"), NewWebServiceTipopersonal.class, features);
    }

    private static URL __getWsdlLocation() {
        if (NEWWEBSERVICETIPOPERSONAL_EXCEPTION!= null) {
            throw NEWWEBSERVICETIPOPERSONAL_EXCEPTION;
        }
        return NEWWEBSERVICETIPOPERSONAL_WSDL_LOCATION;
    }

}
