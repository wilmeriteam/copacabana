
package web.service;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.10-b140803.1500
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "WebServiceOficinas", targetNamespace = "http://service.web/", wsdlLocation = "http://localhost:8080/WebWsdl/WebServiceOficinas?WSDL")
public class WebServiceOficinas_Service
    extends Service
{

    private final static URL WEBSERVICEOFICINAS_WSDL_LOCATION;
    private final static WebServiceException WEBSERVICEOFICINAS_EXCEPTION;
    private final static QName WEBSERVICEOFICINAS_QNAME = new QName("http://service.web/", "WebServiceOficinas");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/WebWsdl/WebServiceOficinas?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        WEBSERVICEOFICINAS_WSDL_LOCATION = url;
        WEBSERVICEOFICINAS_EXCEPTION = e;
    }

    public WebServiceOficinas_Service() {
        super(__getWsdlLocation(), WEBSERVICEOFICINAS_QNAME);
    }

    public WebServiceOficinas_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), WEBSERVICEOFICINAS_QNAME, features);
    }

    public WebServiceOficinas_Service(URL wsdlLocation) {
        super(wsdlLocation, WEBSERVICEOFICINAS_QNAME);
    }

    public WebServiceOficinas_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, WEBSERVICEOFICINAS_QNAME, features);
    }

    public WebServiceOficinas_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public WebServiceOficinas_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns WebServiceOficinas
     */
    @WebEndpoint(name = "WebServiceOficinasPort")
    public WebServiceOficinas getWebServiceOficinasPort() {
        return super.getPort(new QName("http://service.web/", "WebServiceOficinasPort"), WebServiceOficinas.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WebServiceOficinas
     */
    @WebEndpoint(name = "WebServiceOficinasPort")
    public WebServiceOficinas getWebServiceOficinasPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://service.web/", "WebServiceOficinasPort"), WebServiceOficinas.class, features);
    }

    private static URL __getWsdlLocation() {
        if (WEBSERVICEOFICINAS_EXCEPTION!= null) {
            throw WEBSERVICEOFICINAS_EXCEPTION;
        }
        return WEBSERVICEOFICINAS_WSDL_LOCATION;
    }

}
